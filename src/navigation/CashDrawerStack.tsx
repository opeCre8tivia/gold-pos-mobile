import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import CashDrawerOpsListScreen from '../screens/CashDrawerOpsListScreen';
import {
  CASHDRAWER_OPS_DETAILS_SCREEN,
  CASHDRAWER_OPS_LIST_SCREEN,
  CLOSE_CASHDRAWER_SCREEN,
  SHIFTREVIEW_SCREEN,
} from '../constants/screenNames';
import CashDrawerOpsDetailsScreen from '../screens/CashDrawerOpsDetailsScreen';
import ShiftReviewScreen from '../screens/ShiftReviewScreen';
import CloseCashdrawerScreen from '../screens/CloseCashdrawerScreen';

const CashDrawerStack = () => {
  const DrawerStack = createNativeStackNavigator();
  return (
    <DrawerStack.Navigator
      screenOptions={{headerShown: false}}
      initialRouteName={CASHDRAWER_OPS_LIST_SCREEN}>
      <DrawerStack.Screen
        name={CASHDRAWER_OPS_LIST_SCREEN}
        component={CashDrawerOpsListScreen}
      />
      <DrawerStack.Screen
        name={CASHDRAWER_OPS_DETAILS_SCREEN}
        component={CashDrawerOpsDetailsScreen}
      />
      <DrawerStack.Screen
        name={SHIFTREVIEW_SCREEN}
        component={ShiftReviewScreen}
      />
      <DrawerStack.Screen
        name={CLOSE_CASHDRAWER_SCREEN}
        component={CloseCashdrawerScreen}
      />
    </DrawerStack.Navigator>
  );
};

export default CashDrawerStack;

const styles = StyleSheet.create({});
