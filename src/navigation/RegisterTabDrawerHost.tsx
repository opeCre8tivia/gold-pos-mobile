import { StyleSheet, View,SafeAreaView } from 'react-native'
import React from 'react'
import { createDrawerNavigator } from '@react-navigation/drawer';
import { REGISTER_SCREEN } from '../constants/screenNames';
import Register from '../screens/Register';
import GPDrawerCustomContent from '../components/GPDrawerCustomContent';
import { dimentions } from '../constants/theme';

const RegisterTabDrawerHost = () => {

    const Drawer = createDrawerNavigator();

  return (
    <SafeAreaView style={{flex:1}}>
         <Drawer.Navigator
            initialRouteName={REGISTER_SCREEN}
            screenOptions={{
              drawerStyle:{
                width:dimentions.vw/6
              }
            }}
            drawerContent={()=>(
              <GPDrawerCustomContent/>
            )}
            >
            <Drawer.Screen name={REGISTER_SCREEN} component={Register} options={{headerShown:false}} />
         </Drawer.Navigator>  
    </SafeAreaView>
  )
}

export default RegisterTabDrawerHost

const styles = StyleSheet.create({})