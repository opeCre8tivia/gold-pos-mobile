import AsyncStorage from '@react-native-async-storage/async-storage';
import React, {useState, useEffect} from 'react';
import {StyleSheet, Text, View, SafeAreaView} from 'react-native';
import LinkDeviceScreen from '../screens/LinkDeviceScreen';
import AuthenticatedStack from './AuthenticatedStack';
import ClockingStack from './ClockingStack';
import {useSelector} from 'react-redux';
import {dimentions} from '../constants/theme';
import NetworkComponent from '../components/NetworkComponent';
import {useNetInfo} from '@react-native-community/netinfo';
import PinModal from '../screens/PinModal';

const NavHost = () => {
  const netInfo = useNetInfo();
  const {token, isSuccess} = useSelector((state: any) => state.linkingSlice);
  const {isShiftOn} = useSelector((state: any) => state.clockingSlice);
  const {showConfirmPinModal} = useSelector(
    (state: any) => state.confirmpinSlice,
  );
  const [isDeviceLinked, setIsDeviceLinked] = useState(false);
  const [isOffline, setIsOffline] = useState(false);
  // const [isShiftOn, setIsShiftOn] = useState(false)

  //net info
  useEffect(() => {
    netInfo.isConnected && netInfo.isInternetReachable && setIsOffline(false);
    !netInfo.isConnected && !netInfo.isInternetReachable && setIsOffline(true);
  }, [netInfo.isConnected, netInfo.isInternetReachable]);

  //confirm whether token is set the first time after qrcode scan
  useEffect(() => {
    isSuccess &&
      setTimeout(() => {
        confirmTokenIsSet();
      }, 6000);
  }, [isSuccess]);

  //confirm if token is set on every time the app loads
  useEffect(() => {
    confirmTokenIsSet();
  }, []);

  const confirmTokenIsSet = async () => {
    try {
      let _outletToken = await AsyncStorage.getItem('outlet_token');

      if (_outletToken) {
        setIsDeviceLinked(true);
      }
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <SafeAreaView style={{flex: 1}}>
      {!isDeviceLinked ? (
        <LinkDeviceScreen />
      ) : !isShiftOn ? (
        <ClockingStack />
      ) : (
        <AuthenticatedStack />
      )}
      {isOffline && <NetworkComponent />}

      {/* a modal that pops for users to confirm their PINs */}
      <PinModal visible={showConfirmPinModal} />
    </SafeAreaView>
  );
};

export default NavHost;

const styles = StyleSheet.create({});
