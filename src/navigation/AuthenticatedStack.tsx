import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import {HOME_SCREEN } from '../constants/screenNames';
import HomeScreen from '../screens/HomeScreen';
import RegisterTabDrawerHost from './RegisterTabDrawerHost';


const AuthenticatedStack = () => {

  const AuthStack = createNativeStackNavigator()

  return (
      <AuthStack.Navigator screenOptions={{
         headerShown:false,
         
      }} >
          <AuthStack.Screen name={HOME_SCREEN} component={HomeScreen} />
          
      </AuthStack.Navigator>
  )

}

export default AuthenticatedStack

const styles = StyleSheet.create({})