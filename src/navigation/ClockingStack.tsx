import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {
  CLOCKED_IN_HOME_SCREEN,
  CLOCKING_HOME_SCREEN,
  CLOCK_IN_SCREEN,
  ENTER_PIN_SCREEN,
  LANDING_SCREEN,
} from '../constants/screenNames';
import ClockingHomeScreen from '../screens/ClockingHomeScreen';
import EnterPinScreen from '../screens/EnterPinScreen';
import LandingScreen from '../screens/LandingScreen';
import ClockInScreen from '../screens/ClockInScreen';
import ClockInClockOutScreen from '../screens/ClockInClockOutScreen';

const ClockingStack = () => {
  const ClockingStack = createNativeStackNavigator();

  return (
    <ClockingStack.Navigator
      initialRouteName={LANDING_SCREEN}
      screenOptions={{
        headerShown: false,
      }}>
      <ClockingStack.Screen name={LANDING_SCREEN} component={LandingScreen} />
      <ClockingStack.Screen name={CLOCK_IN_SCREEN} component={ClockInScreen} />
      <ClockingStack.Screen
        name={CLOCKED_IN_HOME_SCREEN}
        component={ClockInClockOutScreen}
      />
      <ClockingStack.Screen
        name={CLOCKING_HOME_SCREEN}
        component={ClockingHomeScreen}
      />
      <ClockingStack.Screen
        name={ENTER_PIN_SCREEN}
        component={EnterPinScreen}
      />
    </ClockingStack.Navigator>
  );
};

export default ClockingStack;

const styles = StyleSheet.create({});
