import {StyleSheet} from 'react-native';
import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {CREATE_PRINTER} from '../constants/screenNames';
import CreatePrinterScreen from '../screens/CreatePrinterScreen';

const PrinterStack = () => {
  const PrinterStack = createNativeStackNavigator();
  return (
    <PrinterStack.Navigator initialRouteName={CREATE_PRINTER}>
      <PrinterStack.Screen
        name={CREATE_PRINTER}
        component={CreatePrinterScreen}
      />
    </PrinterStack.Navigator>
  );
};

export default PrinterStack;

const styles = StyleSheet.create({});
