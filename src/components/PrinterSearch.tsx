import {ActivityIndicator, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import GPButton from './GPButton';
import {dimentions} from '../constants/theme';

const PrinterSearch = () => {
  return (
    <View style={styles.printer_search_cont}>
      <View style={{height: 130, flexDirection: 'row'}}>
        <View style={{width: 80, height: 80}}>
          <ActivityIndicator size={60} color="#fff" />
        </View>
        <View style={{paddingHorizontal: 2}}>
          <Text style={{color: '#fff', fontSize: 16}}> Bluetooth Device</Text>
          <Text style={{color: '#fff', fontSize: 12}}>searching....</Text>
        </View>
      </View>

      <View
        style={{
          height: 50,
          alignItems: 'flex-end',
          justifyContent: 'center',
        }}>
        <GPButton title="Cancel" styles={{height: 40}} />
      </View>
    </View>
  );
};

export default PrinterSearch;

const styles = StyleSheet.create({
  printer_search_cont: {
    width: dimentions.col * 10,
    minHeight: dimentions.row * 6,
    backgroundColor: '#1F2937',
    padding: 10,
  },
});
