import React from 'react'
import { Image, StyleSheet, Text, View } from 'react-native'

interface Props {
    title:string,
    styles?:any,
    onPress?:()=>void
}

const PaymentOptionButton = ({title,styles,onPress}:Props) => {
  return (
    <View style={[styles_inner.main,styles]}>
       <Image source={require('../assets/icons/money.png')} />
      <Text style={{fontWeight:"400",fontSize:16,color:"#ffffff",marginHorizontal:12}}>
         {title}
      </Text>
    </View>
  )
}

export default PaymentOptionButton

const styles_inner = StyleSheet.create({
    main:{
        width:180,
        height:55,
        margin:18,
        backgroundColor:"#0069A5",
        flexDirection:"row",
        alignItems:"center",
        justifyContent:"flex-start",
        paddingHorizontal:14
    }
})