import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import GPButton from './GPButton';
import {dimentions} from '../constants/theme';
import {useDispatch, useSelector} from 'react-redux';
import {scanForBluetoothPrinters} from '../redux/actions/printer.actions';
import {RootState} from '../redux/store';

const SelectPrinter = () => {
  const dispatch = useDispatch<any>();
  const {isError, scan_error_message} = useSelector(
    (state: RootState) => state.printerSlice,
  );

  const scanForPrinters = () => {
    dispatch(scanForBluetoothPrinters());
  };

  return (
    <View style={styles.printer_list_cont}>
      {isError && (
        <View style={{height: 40}}>
          <Text style={{color: '#yellow', alignItems: 'center'}}>
            {scan_error_message}
          </Text>
        </View>
      )}

      <View style={styles.row}>
        <View style={{flex: 1}}>
          <Text style={{color: '#fff', fontSize: 8}}> Name</Text>
          <Text style={{color: '#fff', fontSize: 12}}></Text>
        </View>
      </View>

      <View style={styles.row}>
        <View style={{flex: 1}}>
          <Text style={{color: '#fff', fontSize: 8}}> Printer Model</Text>
          <Text style={{color: '#fff', fontSize: 12}}></Text>
        </View>
      </View>

      <View style={styles.row}>
        <View style={{flex: 1}}>
          <Text style={{color: '#fff', fontSize: 8}}> Bluetooth printer</Text>
          <Text style={{color: '#fff', fontSize: 12}}>Choose the device</Text>
        </View>

        <View
          style={{
            flex: 1,
            justifyContent: 'flex-end',
            alignItems: 'flex-end',
          }}>
          <GPButton
            title="Search"
            styles={{height: 40}}
            onPress={() => scanForPrinters()}
          />
        </View>
      </View>

      <View style={{height: 50}}></View>
    </View>
  );
};

export default SelectPrinter;

const styles = StyleSheet.create({
  printer_list_cont: {
    width: dimentions.col * 10,
    minHeight: dimentions.row * 6,
    backgroundColor: '#1F2937',
    padding: 10,
  },
  row: {
    flexDirection: 'row',
    height: 56,
    width: '100%',
    borderBottomWidth: 1,
    borderBottomColor: '#425062',
    paddingHorizontal: 10,
    paddingVertical: 6,
  },
});
