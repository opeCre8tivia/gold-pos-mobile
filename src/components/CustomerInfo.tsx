import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {dimentions} from '../constants/theme';
import {CustomerType} from '../types/main';

const CustomerInfo = ({customer}: {customer: CustomerType}) => {
  return (
    <View style={{flex: 1, marginTop: 10}}>
      {/* r1 */}
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          height: 40,
        }}>
        <Text style={{color: '#7a7a7a', width: dimentions.col * 4}}>
          First Name
        </Text>
        <Text
          style={{
            color: '#ffffff',
            textAlign: 'left',
            width: dimentions.col * 4,
          }}>
          {customer.first_name}
        </Text>
        <Text style={{color: '#7a7a7a', width: dimentions.col * 4}}>
          Company Name
        </Text>
        <Text style={{color: '#ffffff', width: dimentions.col * 4}}>
          {customer.company_name}
        </Text>
      </View>

      {/* r2 */}
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          height: 40,
        }}>
        <Text style={{color: '#7a7a7a', width: dimentions.col * 4}}>
          Last Name
        </Text>
        <Text
          style={{
            color: '#ffffff',
            textAlign: 'left',
            width: dimentions.col * 4,
          }}>
          {customer.last_name}
        </Text>
        <Text style={{color: '#7a7a7a', width: dimentions.col * 4}}>
          Phone number 1
        </Text>
        <Text style={{color: '#ffffff', width: dimentions.col * 4}}>
          {customer.phone_number_primary}
        </Text>
      </View>
      {/* r3 */}
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          height: 40,
        }}>
        <Text style={{color: '#7a7a7a', width: dimentions.col * 4}}>
          Email Address
        </Text>
        <Text
          style={{
            color: '#ffffff',
            textAlign: 'left',
            width: dimentions.col * 4,
          }}>
          {customer.email}
        </Text>
        <Text style={{color: '#7a7a7a', width: dimentions.col * 4}}>
          Phone number 2
        </Text>
        <Text style={{color: '#ffffff', width: dimentions.col * 4}}>
          {customer.phone_number_secondary}
        </Text>
      </View>
      {/* r4 */}
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          height: 40,
        }}>
        <Text style={{color: '#7a7a7a', width: dimentions.col * 4}}>TIN</Text>
        <Text
          style={{
            color: '#ffffff',
            textAlign: 'left',
            width: dimentions.col * 4,
          }}>
          {customer.tin}
        </Text>
        <Text style={{color: '#7a7a7a', width: dimentions.col * 4}}>
          Adress Line
        </Text>
        <Text style={{color: '#ffffff', width: dimentions.col * 4}}>
          {customer.address}
        </Text>
      </View>
      {/* r5 */}
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          height: 40,
        }}>
        <Text style={{color: '#7a7a7a', width: dimentions.col * 4}}>
          Zip Code
        </Text>
        <Text
          style={{
            color: '#ffffff',
            textAlign: 'left',
            width: dimentions.col * 4,
          }}>
          {customer.zip_code}
        </Text>
        <Text style={{color: '#7a7a7a', width: dimentions.col * 4}}>City</Text>
        <Text style={{color: '#ffffff', width: dimentions.col * 4}}>
          {customer.city}
        </Text>
      </View>
    </View>
  );
};

export default CustomerInfo;

const styles = StyleSheet.create({});
