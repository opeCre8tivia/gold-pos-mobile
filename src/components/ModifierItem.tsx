import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React,{useEffect} from 'react'
import { dimentions, size } from '../constants/theme'


interface Props {
    onPress?:()=>void,
    item:any,
    decor:{hex:string}
}

const ModifierItem = ({onPress,item,decor}:Props) => {

  return (
    <TouchableOpacity 
       style={styles.main}
       onPress={onPress}
       >
       <View style={styles.name}>
          <Text style={styles.name_text}>
            {/* {item?.name.length > 15 ? item?.name?.slice(0,16)+"...": item?.name} */}
            {item?.recipe_name && item.recipe_name} {item?.productName && item?.productName}
        </Text>
       </View>

       <View style={styles.price}>
          {/* <Text style={styles.price_text}> {item.price} </Text> */}
       </View>

       <View style={[styles.decor,{backgroundColor:decor.hex}]}></View>
    </TouchableOpacity>
  )
}

export default ModifierItem

const styles = StyleSheet.create({
    main:{
        width:dimentions.col*7.5/5,
        height:dimentions.row*13/5,
        margin:2,
        backgroundColor:"#2A333E"
    },
    name:{
      justifyContent:"flex-start",
      alignItems:"center",
      padding:5,
      flex:2,
     
    },
    name_text:{
        width:"100%",
       fontWeight:"700",
       fontSize:size.md,
       textAlign:"left",
       color:"#fff",
    },
    price:{
        flex:1,
        justifyContent:"flex-end",
        paddingVertical:5

    },
    price_text:{
        color:"#fff",
        fontWeight:"500",
        fontSize:size.sm,
        paddingHorizontal:3,
        textAlign:"left"
    },
    decor:{
        width:"100%",
        height:5,
        backgroundColor:"#B28E6A"
    }

})