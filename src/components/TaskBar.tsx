import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {colors} from '../constants/theme';

interface Props {
  title: string;
  canMoveBack?: boolean;
  navigation?: any;
  taskBarStyles?: object;
}

const TaskBar = ({
  navigation,
  title,
  canMoveBack = true,
  taskBarStyles,
}: Props) => {
  return (
    <View style={[styles.main, taskBarStyles]}>
      <Text style={styles.title}>{title}</Text>

      {canMoveBack === true ? (
        <TouchableOpacity
          onPress={() => navigation?.goBack()}
          style={styles.backContainer}>
          <Image source={require('../assets/icons/backArrow.png')} />
          <Text style={styles.backTxt}> Back </Text>
        </TouchableOpacity>
      ) : null}
    </View>
  );
};

export default TaskBar;

const styles = StyleSheet.create({
  main: {
    width: '100%',
    height: 70,
    backgroundColor: colors.task_bar_blue,
    justifyContent: 'center',
    position: 'relative',
  },
  title: {
    color: colors.white,
    fontSize: 18,
    fontWeight: '500',
    textAlign: 'center',
  },
  backContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    width: 60,
    height: 40,
    position: 'absolute',
    left: 20,
  },
  backTxt: {
    marginLeft: 4,
    color: colors.orange,
  },
});
