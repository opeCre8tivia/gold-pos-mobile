import { StyleSheet, Text, View,TouchableOpacity } from 'react-native'
import React from 'react'
import GPIcon from './GPIcon'
import { useNavigation } from '@react-navigation/native'

const UserProfile = () => {
  
 const navigation = useNavigation<any>()
 
  const openDrawer=()=>{
    navigation.openDrawer()
  }

  return (
    <TouchableOpacity 
       style={styles.main}>
         <GPIcon 
                 iconPath={require('../assets/icons/user.png')} 
                 text="PROFILE"
                 onPress={()=> openDrawer()}
                 iconStyles={{width:60,height:60}}  
              />
    </TouchableOpacity>
  )
}

export default UserProfile

const styles = StyleSheet.create({
    main:{
        width:80,
        height:80,
        justifyContent:"center",
        alignItems:"center",
        paddingTop:4
    }
})