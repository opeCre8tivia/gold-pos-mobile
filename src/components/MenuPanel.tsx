import React, {useState, useEffect, useRef} from 'react';
import {
  StyleSheet,
  Text,
  View,
  ToastAndroid,
  ScrollView,
  FlatList,
  TouchableOpacity,
  RefreshControl,
} from 'react-native';
import MenuSelectionPanel from './MenuSelectionPanel';
import MenuCategory from './MenuCategory';
import MenuItem from './MenuItem';
import {colors, dimentions, size} from '../constants/theme';
import {category_colors, item_colors} from '../constants/colorCodes';
import {useDispatch, useSelector} from 'react-redux';
import {
  addMenuItem,
  clearDirectSaleState,
  updateAddedMenuItem,
} from '../redux/features/directSale.slice';
import GPButton from './GPButton';
import {useNavigation} from '@react-navigation/native';
import {TAKING_PAYMENT_SCREEN} from '../constants/screenNames';
import {_getMenus} from '../redux/actions';
import ModifierItem from './ModifierItem';
import BackArrowIcon from '../assets/svgIcons/backArrow.icon';
import ClockIcon from '../assets/svgIcons/clock.icon';
import {ParkedOrder} from '../types/main';
import {RootState} from '../redux/store';
import {_parkOrder} from '../redux/actions/parking.actions';
import {Toast} from 'react-native-toast-message/lib/src/Toast';
import {clearOrderState} from '../redux/features/order.slice';
import {clearPaymentState} from '../redux/features/payment.slice';
import {addCommasToPrice} from '../utils/addCommasToPrice';
import {removeCustomerToAddToSale} from '../redux/features/customer.slice';

interface Menu {
  id: number;
  name: string;
  menuItemCategory: Category[];
}

interface Category {
  id: number;
  name: string;
  menuItems: Item[];
  taxRate: any;
}

interface Item {
  id: number;
  name: string;
  price: number;
  total: number;
  modifierSet: ModifierSet;
}

interface ModifierSet {
  name: string;
  recipeOptions: any[];
  ingredientOptions: any[];
}

const MenuPanel = () => {
  const dispatch = useDispatch<any>();
  const navigation = useNavigation<any>();
  const {outlet} = useSelector((state: any) => state.outletSlice);
  const {addedMenuItemsTotal, addedMenuItems, servedBy} = useSelector(
    (state: RootState) => state.directSaleSlice,
  );
  const {rowMenuData, selectedMenu} = useSelector(
    (state: RootState) => state.menuDataSlice,
  );
  const {parkError, parkResponseMessage, parkSuccess, loading} = useSelector(
    (state: RootState) => state.orderSlice,
  );
  const {customerToAddToSale} = useSelector(
    (state: RootState) => state.customerSlice,
  );
  const {currentUsersCashDrawerOperation} = useSelector(
    (state: RootState) => state.cashdrawersSlice,
  );

  const [items, setItems] = useState<Item[] | null>(null);
  const [menu, setMenu] = useState<Menu | null>(null);
  const [selectedCategoryName, setSelectedCategoryName] = useState<string>('');
  const [modifierSet, setModifierSet] = useState<ModifierSet | null>(null);
  const [options, setOptions] = useState<any>([]);
  const [currentTaxRate, setCurrentTaxRate] = useState<null | any>(null);
  const [recentlyAddedItem, setRecentlyAddedItem] = useState<null | any>(null);
  const [refreshing, setRefreshing] = React.useState(false);

  //index that we use to determine the next color
  let progressive_index = 0;

  useEffect(() => {
    dispatch(_getMenus());
  }, []);

  useEffect(() => {
    if (!selectedMenu) {
      rowMenuData.length > 0 && setMenu(rowMenuData[0]); //TODO: set selected menu
    }

    if (selectedMenu) {
      let _selected: any = rowMenuData.filter(
        (menu: any) => menu.id === selectedMenu,
      );

      _selected && _selected.length > 0 && setMenu(_selected[0]);
      _selected &&
        _selected.length > 0 &&
        setItems(_selected[0]?.menuItemCategory[0]?.menuItems);
      _selected &&
        _selected.length > 0 &&
        setSelectedCategoryName(
          _selected[0].menuItemCategory[0]?.menuItems[0]?.name,
        );
      _selected &&
        _selected.length > 0 &&
        setCurrentTaxRate(_selected[0].menuItemCategory[0]?.taxRate);
    }
  }, [rowMenuData, selectedMenu]);

  useEffect(() => {
    if (modifierSet) {
      let _recipeOptions = modifierSet.recipeOptions;
      let _ingredientOptions = modifierSet.ingredientOptions;
      setOptions([..._recipeOptions, ..._ingredientOptions]);
    }
  }, [modifierSet]);

  // parking orders

  useEffect(() => {
    if (parkError) {
      Toast.show({
        type: 'goldposred',
        text1: parkResponseMessage
          ? parkResponseMessage
          : 'Something has gone wrong',
        position: 'top',
        topOffset: 5,
      });

      setTimeout(() => {
        dispatch(clearOrderState());
      }, 4000);
    }

    if (parkSuccess) {
      Toast.show({
        type: 'goldposgreen',
        text1: parkResponseMessage
          ? parkResponseMessage
          : 'Operation is successfull',
        position: 'top',
        topOffset: 5,
      });

      setTimeout(() => {
        dispatch(clearOrderState());
        dispatch(removeCustomerToAddToSale());

        //reset sales
        dispatch(clearPaymentState());
        dispatch(clearDirectSaleState());
      }, 4000);
    }
  }, [parkError, parkSuccess]);

  /**
   *
   * Manage Refreshing
   * @returns
   */

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    dispatch(_getMenus());

    if (rowMenuData) {
      setRefreshing(false);
    }
  }, [rowMenuData]);

  const handleAddingMenuItem = (menuItem: {
    name: string;
    price: number;
    id: number;
  }) => {
    //check if item is already added
    let result = addedMenuItems.filter((e: Item) => e.id === menuItem.id);

    if (result.length > 0) {
      //display toast
      setModifierSet(null);
      setOptions([]);
      ToastAndroid.showWithGravityAndOffset(
        'Item Already Added',
        ToastAndroid.SHORT,
        ToastAndroid.BOTTOM,
        0,
        100,
      );
      return;
    }

    let _item = {
      id: menuItem?.id,
      name: menuItem?.name,
      qty: 1,
      price: menuItem?.price,
      total: menuItem?.price * 1,
      taxRate: currentTaxRate,
      modifiers: [],
    };

    dispatch(addMenuItem(_item));
    setRecentlyAddedItem(_item);
  };

  const handleAddingModifier = (item: any) => {
    //pick the last item in the state array
    let _itemToUpdate = recentlyAddedItem;
    //replace or modify state
    _itemToUpdate.modifiers = [..._itemToUpdate.modifiers, item];
    //dispatch
    dispatch(updateAddedMenuItem(_itemToUpdate));
  };

  const handleParkingOrder = () => {
    //add parked order to backend
    const order: ParkedOrder = {
      outletId: outlet.id,
      servedBy: servedBy.id,
      items: addedMenuItems,
      total: addedMenuItemsTotal,
      customerId: customerToAddToSale ? customerToAddToSale.id : undefined,
    };
    dispatch(_parkOrder(order));
  };

  return (
    <View style={styles.main}>
      <MenuSelectionPanel selectedCategoryName="" />
      {/* menu cats and more */}
      <View style={styles.main_wrapper}>
        <View style={styles.category_container}>
          <FlatList
            key={'_'}
            data={menu?.menuItemCategory}
            keyExtractor={(item: any, index: any) => '_' + item.id}
            showsVerticalScrollIndicator={false}
            horizontal={false}
            numColumns={2}
            style={{width: '100%', height: '100%'}}
            renderItem={({item, index}) => {
              let category_color = category_colors[index];
              return (
                <MenuCategory
                  color={category_color}
                  category_name={item.name}
                  onPress={() => {
                    setItems(item?.menuItems);
                    setSelectedCategoryName(item.name);
                    setCurrentTaxRate(item.taxRate);
                  }} //set items on cat click
                />
              );
            }}
            refreshControl={
              <RefreshControl
                refreshing={refreshing}
                onRefresh={() => onRefresh()}
              />
            }
          />
        </View>

        {!modifierSet ? (
          <View style={styles.item_container}>
            <View
              style={{
                height: dimentions.row * 1,
                width: '100%',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{color: '#fff', fontSize: size.md, textAlign: 'center'}}>
                {selectedCategoryName &&
                  selectedCategoryName[0].toUpperCase() +
                    selectedCategoryName.substring(1)}
              </Text>
            </View>

            {/* menu items */}
            <View style={styles.menu_item_container}>
              <FlatList
                key="#"
                data={items}
                keyExtractor={(item: any, index: any) => '#' + item.id}
                horizontal={false}
                numColumns={3}
                showsVerticalScrollIndicator={false}
                style={{width: '100%', height: '100%'}}
                renderItem={({item, index}) => {
                  let item_decor_color;

                  if (index > item_colors.length - 1) {
                    //the color does not exist
                    //start from index 0
                    progressive_index > item_colors.length - 1
                      ? 0
                      : progressive_index++;
                    item_decor_color = item_colors[progressive_index];
                  } else {
                    item_decor_color = item_colors[index];
                  }

                  return (
                    <>
                      {item_decor_color && (
                        <MenuItem
                          item={item}
                          decor={item_decor_color}
                          onPress={() => {
                            /**
                             * employee should only be able to perform this act if cash drawer is open
                             */
                            if (currentUsersCashDrawerOperation?.isActive) {
                              handleAddingMenuItem(item);
                              if (item.modifierSet) {
                                setModifierSet(item.modifierSet);
                              }
                            } else {
                              Toast.show({
                                type: 'goldposred',
                                text1:
                                  'You have no open cash drawer, Clock out please',
                                position: 'top',
                                topOffset: 10,
                              });
                            }
                          }}
                          key={index}
                        />
                      )}
                    </>
                  );
                }}
              />
            </View>

            <View style={styles.pay_button_container}>
              <GPButton
                Icon={ClockIcon}
                title={`Park`}
                loading={loading}
                styles={{
                  width: dimentions.col * 2,
                  height: dimentions.row * 1,
                  backgroundColor: colors.gp_green,
                  marginRight: 8,
                }}
                text_styles={{fontSize: size.lg}}
                onPress={() => addedMenuItemsTotal && handleParkingOrder()}
              />
              <GPButton
                title={`Pay USh: ${addCommasToPrice(addedMenuItemsTotal)}`}
                styles={{
                  width: dimentions.col * 5,
                  height: dimentions.row * 1,
                  backgroundColor: colors.gp_green,
                }}
                text_styles={{fontSize: size.lg}}
                onPress={() =>
                  addedMenuItemsTotal &&
                  navigation.navigate(TAKING_PAYMENT_SCREEN)
                }
              />
            </View>
          </View>
        ) : (
          // modifier options
          <View style={styles.item_container}>
            <TouchableOpacity
              onPress={() => {
                setModifierSet(null);
                setOptions([]);
              }}
              style={{
                height: dimentions.row * 1,
                width: '100%',
                flexDirection: 'row',
                justifyContent: 'flex-start',
                alignItems: 'center',
                paddingHorizontal: 4,
              }}>
              <View
                style={{
                  width: 10,
                  height: 10,
                  marginRight: 20,
                }}>
                <BackArrowIcon fill={colors.orange} width={5} height={5} />
              </View>
              <Text
                style={{
                  color: colors.orange,
                  fontSize: size.md,
                  textAlign: 'center',
                }}>
                {modifierSet &&
                  modifierSet.name[0].toUpperCase() +
                    modifierSet.name.substring(1)}{' '}
                Modifier set
              </Text>
            </TouchableOpacity>

            <View style={styles.menu_item_container}>
              <FlatList
                data={options}
                keyExtractor={(item: any, index: any) => item.id}
                numColumns={5}
                showsVerticalScrollIndicator={false}
                style={{width: '100%', height: '100%'}}
                renderItem={({item, index}) => {
                  let item_decor_color;

                  if (index > item_colors.length - 1) {
                    //the color does not exist
                    //start from index 0
                    progressive_index > item_colors.length - 1
                      ? 0
                      : progressive_index++;
                    item_decor_color = item_colors[progressive_index];
                  } else {
                    item_decor_color = item_colors[index];
                  }

                  return (
                    <>
                      {item_decor_color && (
                        <ModifierItem
                          item={item}
                          decor={item_decor_color}
                          onPress={() => {
                            handleAddingModifier(item);
                          }}
                          key={index}
                        />
                      )}
                    </>
                  );
                }}
              />
            </View>

            <View style={styles.pay_button_container}>
              <GPButton
                title={`Pay USh: ${addedMenuItemsTotal}`}
                styles={{
                  width: '90%',
                  height: dimentions.row * 1,
                  backgroundColor: colors.gp_green,
                }}
                text_styles={{fontSize: size.lg}}
                onPress={() =>
                  addedMenuItemsTotal > 0 &&
                  navigation.navigate(TAKING_PAYMENT_SCREEN)
                }
              />
            </View>
          </View>
        )}
      </View>
    </View>
  );
};

export default MenuPanel;

const styles = StyleSheet.create({
  main: {
    width: dimentions.col * 12,
    height: dimentions.row * 17,
    alignItems: 'center',
  },
  main_wrapper: {
    width: '100%',
    height: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  category_container: {
    width: dimentions.col * 4,
    height: dimentions.row * 15.2,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'flex-start',
    backgroundColor: colors.bg_light,
    marginHorizontal: 2,
  },
  menu_item_container: {
    width: dimentions.col * 8,
    height: dimentions.row * 12,
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },

  item_container: {
    position: 'relative',
    flex: 3,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'flex-start',
    padding: 2,
  },
  pay_button_container: {
    width: '100%',
    height: dimentions.row * 3,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
