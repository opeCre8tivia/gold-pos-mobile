import {StyleSheet, Text, View, Pressable, Image} from 'react-native';
import React from 'react';

interface PrintBtn {
  onPress: () => void;
}

const PrintTestButton = ({onPress}: PrintBtn) => {
  return (
    <Pressable style={styles.main} onPress={onPress}>
      <View>
        <Image source={require('../assets/icons/button_printer.png')} />
      </View>
      <Text style={{fontSize: 16, color: '#7a7a7a', marginHorizontal: 10}}>
        PRINT TEST
      </Text>
    </Pressable>
  );
};

export default PrintTestButton;

const styles = StyleSheet.create({
  main: {
    width: 150,
    height: 50,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
