import { Image, StyleSheet, Text, View } from 'react-native'
import React,{useState} from 'react'
import { colors, dimentions, size } from '../constants/theme'
import GPIcon from './GPIcon'
import { useDispatch, useSelector } from 'react-redux'
import GPButton from './GPButton'
import GPModal from './GPModal'
import { setShiftOf } from '../redux/features/clocking.slice'

const GPDrawerCustomContent = () => {
    const {servedBy } = useSelector((state: any) => state.directSaleSlice)
    const dispatch = useDispatch()
    const [showClockoutAlert,setShowClockoutAlert] = useState(false)

    const handleClockOut=()=>{
        //close drawer

        //dispatch clockout

        //if backend clockout dispatch internal clockout redux action
        dispatch(setShiftOf())
    }


  return (
    <View style={styles.main}>

        <View style={styles.top}>
        <View style={styles.icon_container}>
            <GPIcon
              iconPath={require('../assets/icons/user.png')}
              text={`${servedBy.first_name} ${servedBy.last_name}`}
              textStyles={{fontSize:size.sm,fontWeight:"300",textAlign:"center",marginTop:10}}
              
            />


        </View>
     

        </View>
        <View style={styles.bottom}>
           <GPButton
              title='CLOCK OUT'
              styles={{width:"99%",height:dimentions.row*1,borderRadius:0,marginTop:4,backgroundColor:colors.orange}}
              onPress={()=>setShowClockoutAlert(true) }
           />

           <GPModal
             title="ALERT"
             visible={showClockoutAlert}
             left_button_title="Yes"
             right_button_title='Cancel'
             onPressLeftButton={()=> handleClockOut()}
             onPressRightButton={()=> setShowClockoutAlert(!showClockoutAlert)}
             
           >
             <View style={{flex:1,justifyContent:"center",alignItems:"center"}}>
             <Text style={{textAlign:"center",fontWeight:"400",color:colors.white,fontSize:size.md}}> 
                Are you sure you want to Clock Out
            </Text>
             </View>
           </GPModal>

        </View>
      
    </View>
  )
}

export default GPDrawerCustomContent

const styles = StyleSheet.create({
    main:{
        width:dimentions.vw/6,
        height:"100%",
        justifyContent:"center",
        alignItems:"center"
    },
    top:{
      width:"100%",
      height:dimentions.row*4,
      backgroundColor:colors.orange,
      justifyContent:"center",
      alignItems:"center",
    },
    bottom:{
        width:"100%",
        height:dimentions.row*16,
        backgroundColor:colors.bg_light
    },
    icon_container:{
        height:"80%",
        justifyContent:"center",
        alignItems:"center",
        paddingTop:40
    },
      name_container:{
          width:"100%",
          height:"20%",
      },
      name_container_text:{
          color:colors.white,
          fontWeight:'300',
          fontSize:size.md,
          textAlign:"center"
      }
})