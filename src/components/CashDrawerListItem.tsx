import {
  Modal,
  Pressable,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useState} from 'react';
import {colors, dimentions} from '../constants/theme';
import {CashDrawer} from '../types/main';
import PadlockIcon from '../assets/svgIcons/padlock.icon';
import {useNavigation} from '@react-navigation/native';
import {FLOAT_COUNTING_SCREEN, REGISTER_SCREEN} from '../constants/screenNames';
import GPModal from './GPModal';
import GPButton from './GPButton';

type Props = {
  drawer: CashDrawer;
};

const CashDrawerListItem = ({drawer}: Props) => {
  const navigation = useNavigation<any>();
  const [showModal, setShowModal] = useState<boolean>(false);

  const handleNoCounting = () => {
    /**
     * when employee chooses not count we
     * -create a cashdrawer operation
     * -with currentbalance that is equivalent to the starting balance
     * -then navigate to sales
     */
  };

  return (
    <>
      <TouchableOpacity style={styles.main} onPress={() => setShowModal(true)}>
        <View style={styles.icon}>
          <PadlockIcon />
        </View>
        <Text style={styles.name}>{drawer.name}</Text>
        <View style={styles.details}>
          <Text style={styles.balance_text}>{drawer.startingBalance} </Text>
          <Text style={styles.sm_text}>starting balance</Text>
        </View>
      </TouchableOpacity>

      <Modal
        visible={showModal}
        transparent={true}
        animationType={'fade'}
        style={{flex: 1}}>
        <View
          style={{
            flex: 1,
            backgroundColor: 'rgba(0,0,0,0.9)',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View
            style={{
              width: dimentions.col * 8,
              height: dimentions.row * 7,
              backgroundColor: '#000',
            }}>
            <View
              style={{
                backgroundColor: '#303137',
                height: dimentions.row * 1.5,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Text
                style={{
                  textAlign: 'center',
                  fontSize: 18,
                  fontWeight: '400',
                  color: colors.white,
                }}>
                Require Float Count?
              </Text>
            </View>

            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                justifyContent: 'center',
                marginTop: dimentions.row * 1.5,
              }}>
              <GPButton
                title="Count"
                onPress={() =>
                  navigation.navigate(FLOAT_COUNTING_SCREEN, {
                    cashDrawerId: drawer.id,
                  })
                }
                styles={{
                  height: dimentions.row * 1.5,
                  backgroundColor: '#0069A5',
                }}
              />
              <GPButton
                title="No Count"
                onPress={() => handleNoCounting()}
                styles={{
                  height: dimentions.row * 1.5,
                  backgroundColor: '#303137',
                }}
              />
            </View>
          </View>
        </View>
      </Modal>
    </>
  );
};

export default CashDrawerListItem;

const styles = StyleSheet.create({
  main: {
    width: dimentions.col * 10,
    height: 80,
    backgroundColor: '#2A333E',
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 4,
    borderRadius: 6,
  },
  icon: {
    width: 30,
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 20,
  },
  name: {
    fontSize: 20,
    fontWeight: '500',
    color: colors.white,
    flex: 1,
    alignItems: 'flex-start',
  },
  details: {
    width: 100,
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 8,
  },
  balance_text: {
    color: colors.white,
    fontSize: 16,
    fontWeight: '600',
  },
  sm_text: {
    color: '#A5A5A5',
    fontSize: 10,
    fontWeight: '400',
  },
});
