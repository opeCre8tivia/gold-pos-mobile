import { Image, StyleSheet, Text, View } from 'react-native'
import React from 'react'

interface Props {
    isLight?:boolean,
    styles?:any,
    
}

const Logo = ({isLight=true,styles}:Props) => {
  return (
    <View style={[stylesx.main,styles]}>
        {
            isLight ? <Image source={require('../assets/img/lightlogo.png')} style={{width:"90%",height:"80%"}} resizeMode="contain" /> : <Image source={require('../assets/img/darklogo.png')} style={{width:"90%",height:"80%"}} resizeMode="contain" />
        }
    </View>
  )
}

export default Logo

const stylesx = StyleSheet.create({
    main:{
        minWidth:100,
        maxWidth:200,
        height:60,
        justifyContent:"center",
        alignItems:"center",
    }
})