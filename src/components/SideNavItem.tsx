import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import {colors, dimentions} from '../constants/theme';

interface Props {
  onPress: () => void;
  listStyles?: object;
  textStyles?: object;
  Icon: (a: Icon) => JSX.Element;
  title: string;
  fill?: string;
  width?: number;
  height?: number;
}

interface Icon {
  width?: number;
  height?: number;
  fill?: string;
}

const SideNavItem = ({
  onPress,
  listStyles,
  textStyles,
  Icon,
  title,
  fill,
  width,
  height,
}: Props) => {
  return (
    <TouchableOpacity onPress={onPress} style={styles.main}>
      <View style={styles.icon}>
        {Icon && <Icon fill={fill} height={height} width={width} />}
      </View>
      <Text style={[styles.title, textStyles]}>{title}</Text>
    </TouchableOpacity>
  );
};

export default SideNavItem;

const styles = StyleSheet.create({
  main: {
    width: '100%',
    height: dimentions.row * 1,
    marginVertical: 2,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  icon: {
    width: dimentions.col * 0.6,
    height: dimentions.row * 1,
    marginHorizontal: 10,
    padding: 6,
  },
  title: {
    color: '#fff',
    fontWeight: '400',
  },
});
