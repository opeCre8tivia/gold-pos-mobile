import React, {useState, useEffect} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {Picker} from '@react-native-picker/picker';
import {useDispatch, useSelector} from 'react-redux';
import {selectMenu} from '../redux/features/menuData.slice';
import {colors, dimentions, size} from '../constants/theme';

interface Props {
  selectedCategoryName: string;
}

const MenuSelectionPanel = ({selectedCategoryName}: Props) => {
  const dispatch = useDispatch<any>();
  const {rowMenuData} = useSelector((state: any) => state.menuDataSlice);
  const [selected, setSelected] = useState<any>(null);

  useEffect(() => {
    console.log(selected, 'selected menu valueeeeee');
    selected && dispatch(selectMenu(selected));
  }, [selected]);

  return (
    <View style={styles.main}>
      <Picker
        mode="dropdown"
        style={{color: '#ffffff', width: '40%'}}
        dropdownIconColor="#ffffff"
        selectedValue={selected}
        onValueChange={(itemValue, itemIndex) => setSelected(itemValue)}>
        {rowMenuData.length > 0 &&
          rowMenuData.map((menu: {id: number; name: string}, index: number) => (
            <Picker.Item label={menu.name} value={menu.id} key={index} />
          ))}
      </Picker>

      <View style={styles.categoryNameCont}>
        <Text style={styles.categoryNameText}>{selectedCategoryName}</Text>
      </View>
    </View>
  );
};

export default MenuSelectionPanel;

const styles = StyleSheet.create({
  main: {
    width: '100%',
    height: dimentions.row * 1,
    backgroundColor: '#303137',
    justifyContent: 'flex-start',
    paddingHorizontal: size.lg,
    flexDirection: 'row',
    alignItems: 'center',
  },
  categoryNameCont: {
    width: 'auto',
    height: '100%',
    marginHorizontal: size.lg,
    justifyContent: 'center',
    alignItems: 'center',
  },
  categoryNameText: {
    fontSize: size.md,
    color: colors.orange,
  },
});
