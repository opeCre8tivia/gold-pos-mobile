import { Pressable, StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { colors, dimentions } from '../constants/theme'


interface Message {
    isError:boolean,
    isSuccess:boolean,
    title:string,
    msg:string
}

interface Props {
    message:Message,
    onPress?:any
}

const GPSuccessOrErrorModal = ({message,onPress}:Props) => {
  return (
    <View style={styles.main}>
        
         <Pressable 
              onPress={onPress}
              style={styles.innerContainer}>


             <Text style={{color:message.isError ? "red" : "lime",fontSize:14,fontWeight:"500",textAlign:"center",marginBottom:10}}>
                {message.title}
             </Text>

             <Text style={{color:"#fff",fontSize:12,textAlign:"center"}}>
                {
                    message.msg
                }
             </Text>
         </Pressable>
    </View>
  )
}

export default GPSuccessOrErrorModal

const styles = StyleSheet.create({
    main:{
        flex:1,
        width:dimentions.vw,
        height:"100%",
        minHeight:dimentions.vh,
        justifyContent:"center",
        alignItems:"center",
        backgroundColor:"rgba(52,52,52,0.8)",
        position:"absolute",
        top:0,
        left:0,
        zIndex:20,

    
    },
    innerContainer:{
        position:"absolute",
        top:"20%",
        left:"25%",

        width:"50%",
        height:dimentions.vh/2,
        borderRadius:8,
        backgroundColor:colors.bg_light,
        justifyContent:"center",
        alignItems:"center",

    }
})