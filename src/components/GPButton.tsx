import {
  ActivityIndicator,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import React from 'react';
import {colors, size} from '../constants/theme';

interface Button {
  title: string;
  styles?: any;
  text_styles?: any;
  onPress?: () => void;
  loading?: boolean;
  Icon?: any;
  disabled?: boolean;
}
const GPButton = ({
  title,
  styles,
  text_styles,
  onPress,
  loading,
  Icon,
  disabled,
}: Button) => {
  return (
    <TouchableOpacity
      onPress={() => {
        if (!disabled && onPress) {
          onPress();
        }
      }}
      style={[styles_inner.main, styles, {opacity: disabled ? 0.5 : 1}]}>
      {Icon && (
        <View style={{width: 25, height: 25}}>
          <Icon />
        </View>
      )}
      {loading ? (
        <ActivityIndicator color="#fff" size={30} />
      ) : (
        <Text style={[styles_inner.text, text_styles]}>{title}</Text>
      )}
    </TouchableOpacity>
  );
};

export default GPButton;

const styles_inner = StyleSheet.create({
  main: {
    width: 118,
    height: 80,
    borderRadius: 3,
    padding: 2,
    backgroundColor: colors.orange,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    margin: 2,
  },
  text: {
    fontWeight: '500',
    fontSize: size.md,
    color: colors.white,
  },
});
