import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { colors, dimentions, size } from '../constants/theme'

interface Props {
    onPress?:()=>void
    category_name:string,
    color?:{hex:string}
}

const MenuCategory = ({onPress,category_name,color}:Props) => {
  return (
    <TouchableOpacity 
       onPress={onPress}
       style={[styles.main,{backgroundColor:color?.hex}]}>
      <Text style={styles.text}>{category_name}</Text>
    </TouchableOpacity>
  )
}

export default MenuCategory

const styles = StyleSheet.create({
    main:{
      width:dimentions.col*4/2,
      height:dimentions.row*17/7.5,
        backgroundColor:"#2A333E",
        margin:2,
        padding:1,
        justifyContent:"center",
        alignItems:"center"
    },
    text:{
        color:colors.white,
        fontSize:size.md,
        fontWeight:"700",
        textAlign:"left",


    }
})