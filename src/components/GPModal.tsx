import { Modal, StyleSheet, Text, View } from 'react-native'
import React, { Children } from 'react'
import { colors, dimentions } from '../constants/theme'
import GPButton from './GPButton'

interface GPModal {
  visible: boolean,
  title: string,
  text?: string,
  container_styles?: any,
  left_button_title: string,
  right_button_title: string,
  onPressLeftButton: () => void,
  onPressRightButton: () => void,
  children?: any

}

const GPModal = ({ visible, container_styles, title, text, left_button_title, right_button_title, onPressLeftButton, onPressRightButton, children }: GPModal) => {
  return (
    <Modal
      visible={visible}
      animationType={'fade'}
      transparent={true}
      style={{ flex: 1 }}
    >
      <View style={styles.main}>

        <View style={[styles.container, container_styles]}>
          <View style={styles.header}>
            <Text style={{ color: "#fff", fontSize: 14, fontWeight: "500" }}>
              {title}
            </Text>
          </View>

          {text && <Text style={{ color: "#fff", fontSize: 12, fontWeight: "500", paddingVertical: 20, textAlign: "center" }}>
            {
              text
            }
          </Text>}

          <View style={styles.children_container}>
            {children}
          </View>

          <View style={styles.button_container}>
            <GPButton
              title={left_button_title}
              onPress={onPressLeftButton}
              styles={{ width: "50%", height: dimentions.row*1}}
            />

            <GPButton
              title={right_button_title}
              onPress={onPressRightButton}
              styles={{ width: "50%", height: dimentions.row*1, backgroundColor: "#303137" }}
            />

          </View>

        </View>


      </View>
    </Modal>
  )
}

export default GPModal

const styles = StyleSheet.create({
  main: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: 'rgba(0,0,0,0.9)',

  },
  container: {
    width: dimentions.col * 6,
    height: dimentions.row * 7,
    alignItems: "center",
    justifyContent: "flex-start",
    backgroundColor: "#17181C"
  },
  header: {
    width: "100%",
    height: "10%",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#303137"
  },

  children_container: {
    width: "100%",
    height:"80%",
    justifyContent: "flex-start",
    alignItems: "center",
  },
  button_container: {
    width: "100%",
    height:"10%",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "flex-end",
    paddingHorizontal: 6,
    paddingBottom:2,
  }

})