import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import React, {useState, useEffect} from 'react';
import GPTextInput from './GPTextInput';
import {colors, dimentions} from '../constants/theme';
import GPButton from './GPButton';
import {RootState, useAppDispatch} from '../redux/store';
import {useSelector} from 'react-redux';
import {Toast} from 'react-native-toast-message/lib/src/Toast';
import {
  clearCustomerState,
  hideCreateCustomerModal,
} from '../redux/features/customer.slice';
import {_addCustomer, _listCustomers} from '../redux/actions/customers.actions';

const AddCustomer = () => {
  const {loading, isError, isSuccess, responseMessage} = useSelector(
    (state: RootState) => state.customerSlice,
  );
  const dispatch = useAppDispatch();
  const [firstName, setFirstName] = useState<string>('');
  const [lastName, setLastName] = useState<string>('');
  const [companyName, setCompanyName] = useState<string>('');
  const [emailAddress, setEmailAddress] = useState<string>('');
  const [phoneNumberOne, setPhoneNumberOne] = useState<string>('');
  const [phoneNumberTwo, setPhoneNumberTwo] = useState<string>('');
  const [tin, setTin] = useState<string>('');
  const [address, setAddress] = useState<string>('');
  const [zipCode, setZipCode] = useState<string>('');
  const [city, setCity] = useState<string>('');

  useEffect(() => {
    if (isSuccess) {
      Toast.show({
        type: 'goldposgreen',
        text1: responseMessage ? responseMessage : 'Operation successful',
        position: 'top',
        topOffset: 10,
      });

      dispatch(_listCustomers());

      setTimeout(() => {
        dispatch(clearCustomerState());
        setFirstName('');
        setLastName('');
        setEmailAddress('');
        setCompanyName('');
        setCity('');
        setPhoneNumberOne('');
        dispatch(hideCreateCustomerModal());
      }, 4000);
    }
    if (isError) {
      Toast.show({
        type: 'goldposred',
        text1: responseMessage ? responseMessage : 'Operation successful',
        position: 'top',
        topOffset: 10,
      });

      setTimeout(() => {
        dispatch(clearCustomerState());
      }, 4000);
    }
  }, [isError, isSuccess]);

  const handleAddingCustomer = () => {
    const data = {
      first_name: firstName,
      last_name: lastName,
      email: emailAddress,
      address: address,
      phone_number_primary: phoneNumberOne,
      phone_number_secondary: phoneNumberTwo,
      tin: tin,
      zipCode: zipCode,
      city: city,
    };

    dispatch(_addCustomer(data));
  };

  return (
    <View style={styles.main}>
      <View
        style={{
          width: '100%',
          height: 60,
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#2A333E',
        }}>
        <Text style={{fontSize: 16, color: colors.white, fontWeight: '500'}}>
          Add Customer
        </Text>

        <TouchableOpacity
          onPress={() => dispatch(hideCreateCustomerModal())}
          style={{
            position: 'absolute',
            top: 20,
            right: 20,
          }}>
          <Text
            style={{
              fontSize: 14,
              fontWeight: '500',
              color: colors.orange,
              textAlign: 'center',
            }}>
            Cancel
          </Text>
        </TouchableOpacity>
      </View>

      <View
        style={{
          width: '100%',
          height: 56,
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          paddingHorizontal: 80,
          marginTop: 20,
        }}>
        <GPTextInput
          placeholder="First Name*"
          value={firstName}
          onChange={(value: string) => setFirstName(value)}
          styles={{minWidth: 250}}
          placeholderTextColor="#7a7a7a"
        />

        <GPTextInput
          placeholder="Last Name*"
          value={lastName}
          onChange={(value: string) => setLastName(value)}
          styles={{width: 250}}
          placeholderTextColor="#7a7a7a"
        />

        <GPTextInput
          placeholder="Company Name"
          value={companyName}
          onChange={(value: string) => setCompanyName(value)}
          styles={{width: 250}}
          placeholderTextColor="#7a7a7a"
        />
      </View>

      <View
        style={{
          width: '100%',
          height: 56,
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          paddingHorizontal: 80,
          marginTop: 20,
        }}>
        <GPTextInput
          placeholder="Email Address"
          value={emailAddress}
          onChange={(value: string) => setEmailAddress(value)}
          styles={{width: 250}}
          placeholderTextColor="#7a7a7a"
        />

        <GPTextInput
          placeholder="Phone Number 1*"
          value={phoneNumberOne}
          onChange={(value: string) => setPhoneNumberOne(value)}
          styles={{width: 250}}
          placeholderTextColor="#7a7a7a"
        />

        <GPTextInput
          placeholder="Phone Number 2"
          value={phoneNumberTwo}
          onChange={(value: string) => setPhoneNumberTwo(value)}
          styles={{width: 250}}
          placeholderTextColor="#7a7a7a"
        />
      </View>

      <View
        style={{
          width: '100%',
          height: 56,
          flexDirection: 'row',
          justifyContent: 'flex-start',
          alignItems: 'center',
          paddingHorizontal: 80,
          marginTop: 20,
        }}>
        <GPTextInput
          placeholder="Tax Identification Number"
          value={tin}
          onChange={(value: string) => setTin(value)}
          styles={{marginRight: 60, width: 250}}
          placeholderTextColor="#7a7a7a"
        />

        <GPTextInput
          placeholder="Address line*"
          value={address}
          onChange={(value: string) => setAddress(value)}
          styles={{marginHorizontal: 4, width: 250}}
          placeholderTextColor="#7a7a7a"
        />
      </View>

      <View
        style={{
          width: '100%',
          height: 56,
          flexDirection: 'row',
          justifyContent: 'flex-start',
          alignItems: 'center',
          paddingHorizontal: 80,
          marginTop: 20,
        }}>
        <GPTextInput
          placeholder="Zipcode"
          value={zipCode}
          onChange={(value: string) => setZipCode(value)}
          styles={{marginRight: 60, width: 250}}
          placeholderTextColor="#7a7a7a"
        />

        <GPTextInput
          placeholder="City"
          value={city}
          onChange={(value: string) => setCity(value)}
          styles={{width: 250}}
          placeholderTextColor="#7a7a7a"
        />
      </View>

      {/* button */}
      <View
        style={{
          width: 'auto',
          height: 'auto',
          position: 'absolute',
          bottom: 235,
          right: 80,
        }}>
        <GPButton
          disabled={firstName.length === 0}
          title="Create New Customer"
          styles={{width: dimentions.col * 4, height: dimentions.row * 1.5}}
          onPress={() => handleAddingCustomer()}
          loading={loading}
        />
      </View>
    </View>
  );
};

export default AddCustomer;

const styles = StyleSheet.create({
  main: {
    width: dimentions.col * 16,
    height: dimentions.row * 16,
    alignItems: 'center',
    backgroundColor: colors.bg_light,
  },
});
