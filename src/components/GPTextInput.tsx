import {StyleSheet, Text, TextInput, View} from 'react-native';
import React from 'react';
import {colors} from '../constants/theme';

interface Input {
  styles?: any;
  placeholder?: string;
  onChange?: any;
  value: string;
  error?: string;
  placeholderTextColor?: string;
  autoFocus?: boolean;
  input_styles?: any;
}

const GPTextInput = ({
  value,
  placeholder,
  styles,
  error,
  onChange,
  autoFocus,
  placeholderTextColor,
  input_styles,
}: Input) => {
  return (
    <>
      <View style={[styles_inner.main, styles]}>
        <TextInput
          placeholder={placeholder}
          placeholderTextColor={placeholderTextColor}
          onChangeText={onChange}
          autoCapitalize="none"
          autoCorrect={false}
          autoFocus={autoFocus}
          value={value}
          style={[styles_inner.input, input_styles]}
        />
      </View>
    </>
  );
};

export default GPTextInput;

const styles_inner = StyleSheet.create({
  main: {
    minWidth: 200,
    marginVertical: 2,
  },
  input: {
    height: '100%',
    width: '100%',
    fontWeight: '400',
    fontSize: 14,
    color: colors.white,
    borderBottomWidth: 1,
    borderBottomColor: '#425062',
  },
  error_cont: {
    height: 15,
    width: '90%',
    paddingHorizontal: 8,
    marginBottom: 4,
  },
  error_txt: {
    color: 'red',
    fontSize: 12,
  },
});
