import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { colors } from '../constants/theme'

const CurrentTimeComponent = () => {
  return (
    <View style={styles.main}>
      <Text style={styles.text}>2022-07-30,  1:17 PM</Text>
    </View>
  )
}

export default CurrentTimeComponent

const styles = StyleSheet.create({
    main:{
     height:35,
     justifyContent:"center"
    },
    text:{
        fontSize:14,
        fontWeight:"400",
        color:"#7a7a7a"
    }
})