import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {colors} from '../constants/theme';

interface Item {
  first_name: string;
  last_name: string;
  role: any;
  id: string | number;
}

interface Props {
  item: Item;
  onPress?: () => void;
}

const ClockInButton = ({item, onPress}: Props) => {
  return (
    <TouchableOpacity style={styles.main} onPress={onPress}>
      <Text style={{flex: 1, fontSize: 20, fontWeight: '700', color: '#fff'}}>
        {`${item.first_name} ${item.last_name} `}
      </Text>

      {/* TODO:determine using permissions */}
      <View
        style={[
          styles.tag,
          {
            backgroundColor: item?.role.isDefault
              ? colors.gp_pink
              : colors.gp_pink,
          },
        ]}>
        <Text style={styles.tag_txt}>{item?.role?.role_title}</Text>
      </View>
    </TouchableOpacity>
  );
};

export default ClockInButton;

const styles = StyleSheet.create({
  main: {
    width: '100%',
    height: 50,
    backgroundColor: '#2A333E',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    marginBottom: 12,
  },
  tag: {
    width: 150,
    height: 26,
    backgroundColor: '#6698CA',
    borderRadius: 3,
    justifyContent: 'center',
    alignItems: 'center',
  },
  tag_txt: {
    fontWeight: '500',
    fontSize: 12,
    color: '#fff',
    textTransform: 'uppercase',
  },
});
