import { StyleSheet, Text, View } from 'react-native'
import React from 'react'


const NetworkComponent = () => {
  return (
    <View style={styles.main}>
      <Text style={styles.text}>
        Your offline ! Check on your connection
     </Text>
    </View>
  )
}

export default NetworkComponent

const styles = StyleSheet.create({
    main:{
        height:40,
        width:"100%",
        backgroundColor:"red",
        justifyContent:"center",
        alignItems:"center"
    },
    text:{
        fontSize:14,
        color:"#fff",
        textAlign:"center"
    }
})