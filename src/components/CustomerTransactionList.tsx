import {StyleSheet, Text, View, FlatList} from 'react-native';
import React, {useEffect, useState} from 'react';
import {colors, dimentions} from '../constants/theme';
import {CustomerType, ParkedOrder} from '../types/main';
import timeFormater from '../utils/timeFormater';
import GPButton from './GPButton';
import {useSelector} from 'react-redux';
import {RootState, useAppDispatch} from '../redux/store';
import {useNavigation} from '@react-navigation/native';
import {TAKING_PAYMENT_SCREEN} from '../constants/screenNames';
import {unparkOrder} from '../redux/features/directSale.slice';
import {_addSelectedOrder} from '../redux/features/order.slice';

const CustomerTransactionList = ({customer}: {customer: CustomerType}) => {
  const navigation = useNavigation<any>();
  const dispatch = useAppDispatch();
  const {addedMenuItems, fromParkedSale} = useSelector(
    (state: RootState) => state.directSaleSlice,
  );
  const [parkedOrders, setParkedOrders] = useState<ParkedOrder[] | null>(null);
  useEffect(() => {
    if (customer) {
      customer.parkedSales &&
        customer.parkedSales.length > 0 &&
        setParkedOrders(customer.parkedSales);
    }
  }, [customer]);

  useEffect(() => {
    if (addedMenuItems && fromParkedSale) {
      navigation.navigate(TAKING_PAYMENT_SCREEN);
    }
  }, [addedMenuItems]);

  const handleSelectingOrder = (order: ParkedOrder) => {
    /**
     * parse menu items
     */

    let menuItems = [];
    order.menuItems &&
      order.menuItems.forEach(e => {
        menuItems.push(JSON.parse(e));
      });

    // onPress={() => {
    //   selectedOrder && dispatch(unparkOrder(selectedOrder));
    // }}
  };

  return (
    <View style={{flex: 1, marginTop: 10}}>
      {/* header */}
      <View
        style={{
          width: dimentions.col * 19,
          height: 60,
          flexDirection: 'row',
        }}>
        <View
          style={{
            width: dimentions.col * 2.5,
            height: '100%',
            marginHorizontal: 2,
          }}>
          <Text style={{color: '#A5A5A5', fontSize: 20}}>DATE</Text>
        </View>
        <View
          style={{
            width: dimentions.col * 2.5,
            height: '100%',
            marginHorizontal: 2,
          }}>
          <Text style={{color: '#A5A5A5', fontSize: 20}}>TYPE</Text>
        </View>
        <View
          style={{
            width: dimentions.col * 2,
            height: '100%',
            marginHorizontal: 2,
          }}>
          <Text style={{color: '#A5A5A5', fontSize: 20}}>NO</Text>
        </View>
        <View
          style={{
            width: dimentions.col * 2,
            height: '100%',
            marginHorizontal: 2,
          }}>
          <Text style={{color: '#A5A5A5', fontSize: 20}}> DUE DATE</Text>
        </View>
        <View
          style={{
            width: dimentions.col * 2.5,
            height: '100%',
            marginHorizontal: 2,
          }}>
          <Text style={{color: '#A5A5A5', fontSize: 20}}>BALANCE</Text>
        </View>
        <View
          style={{
            width: dimentions.col * 2.5,
            height: '100%',
            marginHorizontal: 2,
          }}>
          <Text style={{color: '#A5A5A5', fontSize: 20}}>TOTAL</Text>
        </View>
        <View
          style={{
            width: dimentions.col * 2,
            height: '100%',
            marginHorizontal: 2,
          }}>
          <Text style={{color: '#A5A5A5', fontSize: 20}}>STATUS</Text>
        </View>
        <View
          style={{
            width: dimentions.col * 2.5,
            height: '100%',
            marginHorizontal: 2,
          }}>
          <Text style={{color: '#A5A5A5', fontSize: 20}}>ACTION</Text>
        </View>
      </View>

      {/*  */}

      <FlatList
        data={parkedOrders}
        //  keyExtractor={(item) => item.id}
        showsVerticalScrollIndicator={false}
        renderItem={({item, index}) => {
          return (
            <View
              key={index}
              style={{
                width: dimentions.col * 19,
                height: 60,
                flexDirection: 'row',
              }}>
              <View
                style={{
                  width: dimentions.col * 2.5,
                  height: '100%',
                  marginHorizontal: 2,
                }}>
                <Text
                  style={{color: '#ffffff', fontSize: 16, fontWeight: '200'}}>
                  {timeFormater(item.createdAt)?.full}
                </Text>
              </View>
              <View
                style={{
                  width: dimentions.col * 2.5,
                  height: '100%',
                  marginHorizontal: 2,
                }}>
                <Text
                  style={{color: '#ffffff', fontSize: 16, fontWeight: '200'}}>
                  Open order
                </Text>
              </View>
              <View
                style={{
                  width: dimentions.col * 2,
                  height: '100%',
                  marginHorizontal: 2,
                }}>
                <Text
                  style={{color: '#ffffff', fontSize: 16, fontWeight: '200'}}>
                  {item.orderNumber}
                </Text>
              </View>
              <View
                style={{
                  width: dimentions.col * 2,
                  height: '100%',
                  marginHorizontal: 2,
                }}>
                <Text
                  style={{color: '#ffffff', fontSize: 16, fontWeight: '200'}}>
                  ----
                </Text>
              </View>
              <View
                style={{
                  width: dimentions.col * 2.5,
                  height: '100%',
                  marginHorizontal: 2,
                }}>
                <Text
                  style={{color: '#ffffff', fontSize: 16, fontWeight: '200'}}>
                  {item.total}
                </Text>
              </View>
              <View
                style={{
                  width: dimentions.col * 2.5,
                  height: '100%',
                  marginHorizontal: 2,
                }}>
                <Text
                  style={{color: '#ffffff', fontSize: 16, fontWeight: '200'}}>
                  TOTAL
                </Text>
              </View>
              <View
                style={{
                  width: dimentions.col * 2,
                  height: '100%',
                  marginHorizontal: 2,
                }}>
                <View
                  style={{
                    width: 80,
                    height: 30,
                    borderRadius: 6,
                    backgroundColor: item.isOpen
                      ? colors.orange
                      : colors.gp_green,
                  }}>
                  <Text
                    style={{
                      color: '#ffffff',
                      textAlign: 'center',
                      fontSize: 16,
                      fontWeight: '200',
                    }}>
                    {item.isOpen ? 'Open' : 'Close'}
                  </Text>
                </View>
              </View>
              <View
                style={{
                  width: dimentions.col * 2.5,
                  height: '100%',
                  marginHorizontal: 2,
                }}>
                <GPButton
                  title="Receive Payment"
                  styles={{
                    height: 35,
                    backgroundColor: 'transparent',
                    borderWidth: 1,
                    borderColor: '#7a7a7a',
                    minWidth: 'auto',
                    width: '100%',
                  }}
                  text_styles={{fontSize: 16, fontWeight: '200'}}
                  onPress={() => {
                    dispatch(_addSelectedOrder(item));
                    dispatch(unparkOrder(item));
                  }}
                />
              </View>
            </View>
          );
        }}
      />
    </View>
  );
};

export default CustomerTransactionList;

const styles = StyleSheet.create({});
