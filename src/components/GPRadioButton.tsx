import {
  StyleSheet,
  Text,
  View,
  Pressable,
  ActivityIndicator,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import {colors} from '../constants/theme';

interface RadioBtn {
  loading: boolean;
  selected: boolean;
  onPress: (arg: CallBack) => void;
}

type CallBack = {
  isSelected: boolean;
};

const GPRadioButton = ({selected, onPress, loading}: RadioBtn) => {
  const [isSelected, setIsSelected] = useState(false);

  useEffect(() => {
    selected != undefined && setIsSelected(selected);
  }, [selected]);

  return (
    <Pressable
      style={styles.main}
      onPress={() => {
        onPress({isSelected: !isSelected});
      }}>
      {loading ? (
        <ActivityIndicator />
      ) : (
        isSelected && <View style={styles.inner}></View>
      )}
    </Pressable>
  );
};

export default GPRadioButton;

const styles = StyleSheet.create({
  main: {
    width: 30,
    height: 30,
    borderRadius: 15,
    borderWidth: 1,
    borderColor: colors.orange,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 4,
  },
  inner: {
    width: 20,
    height: 20,
    borderRadius: 10,
    backgroundColor: colors.orange,
  },
});
