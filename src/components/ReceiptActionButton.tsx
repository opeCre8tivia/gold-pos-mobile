import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {colors, dimentions} from '../constants/theme';

type IconStyles = {
  fill?: string;
  width?: string;
  height?: string;
};

interface Props {
  onPress: () => void;
  text: string;
  icon?: any;
  Icon?: any;
  iconStyles?: IconStyles;
}

const ReceiptActionButton = ({
  onPress,
  text,
  icon,
  Icon,
  iconStyles,
}: Props) => {
  return (
    <View
      style={{alignItems: 'center', margin: 8, width: dimentions.col * 2.5}}>
      <TouchableOpacity style={styles.main} onPress={onPress}>
        {/* <Image source={icon} style={{width: '100%', height: '100%'}} /> */}
        {Icon && <Icon fill={iconStyles && iconStyles?.fill} />}
      </TouchableOpacity>

      <Text
        style={{
          fontSize: 13,
          color: colors.white,
          textAlign: 'center',
          marginVertical: 6,
        }}>
        {text}
      </Text>
    </View>
  );
};

export default ReceiptActionButton;

const styles = StyleSheet.create({
  main: {
    width: 60,
    height: 60,
    borderRadius: 30,
    padding: 16,
    backgroundColor: colors.white,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
