import React, {useState} from 'react';
import type {PropsWithChildren} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Pressable,
} from 'react-native';
import BackArrowIcon from '../assets/svgIcons/backArrow.icon';
import {colors, dimentions} from '../constants/theme';
import ChevronUpIcon from '../assets/svgIcons/chevronUp.icon';
import GPCheckbox from './GPCheckbox';

type AccordionItemPros = PropsWithChildren<{
  title: string;
  text: string;
  isResolved: boolean;
}>;

const GPCollapsible = ({
  children,
  title,
  text,
  isResolved,
}: AccordionItemPros): JSX.Element => {
  const [expanded, setExpanded] = useState(false);

  function toggleItem() {
    setExpanded(!expanded);
  }

  const body = <View style={styles.accordBody}>{children}</View>;

  return (
    <View style={styles.accordContainer}>
      <Pressable style={styles.accordHeader} onPress={toggleItem}>
        <View style={{flexDirection: 'row'}}>
          {isResolved ? (
            <GPCheckbox isChecked={true} onPress={() => {}} size={30} />
          ) : (
            <View
              style={{
                width: 30,
                height: 30,
                borderColor: colors.white,
                borderWidth: 1,
                borderRadius: 15,
                justifyContent: 'center',
                alignItems: 'center',
                marginRight: 10,
              }}>
              <Text
                style={{color: colors.white, fontSize: 14, fontWeight: '500'}}>
                1
              </Text>
            </View>
          )}
          <View>
            <Text style={styles.accordTitle}>{title}</Text>
            <Text style={{fontWeight: '300', color: '#A5A5A5', fontSize: 12}}>
              {text}
            </Text>
          </View>
        </View>
        <View style={{width: 20, height: 20}}>
          {/* <BackArrowIcon fill={colors.btn_gray} width={10} height={10} /> */}
          <ChevronUpIcon />
        </View>
      </Pressable>
      {expanded && body}
    </View>
  );
};

export default GPCollapsible;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  accordContainer: {
    paddingBottom: 4,
    height: 'auto',
  },
  accordHeader: {
    height: dimentions.row * 2,
    padding: 12,
    backgroundColor: colors.btn_blue,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
  },
  accordTitle: {
    fontSize: 20,
    color: colors.white,
    fontWeight: '500',
  },
  accordBody: {
    paddingVertical: 20,
    paddingHorizontal: 12,
    backgroundColor: colors.btn_blue,
  },
  textSmall: {
    fontSize: 16,
  },
  seperator: {
    height: 12,
  },
});
