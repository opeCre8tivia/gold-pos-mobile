import {
  StyleSheet,
  Text,
  TextInput,
  View,
  Image,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import React from 'react';

interface Props {
  placeholder: string;
  onChangeText?: (e: any) => void;
  input_styles?: any;
  value?: any;
  onSearchIconClick?: () => void;
  loading?: boolean;
}

const GPSearchInput = ({
  placeholder,
  onChangeText,
  onSearchIconClick,
  input_styles,
  value,
  loading,
}: Props) => {
  return (
    <View style={[styles.main, input_styles]}>
      <TouchableOpacity style={styles.icon} onPress={onSearchIconClick}>
        {loading ? (
          <ActivityIndicator size={18} color="#ffffff" />
        ) : (
          <Image source={require('../assets/icons/bx_search.png')} />
        )}
      </TouchableOpacity>
      <TextInput
        placeholder={placeholder}
        onChangeText={onChangeText}
        value={value}
        style={{width: '100%', height: '100%', color: '#ffffff', fontSize: 14}}
        placeholderTextColor="#7a7a7a"
      />
    </View>
  );
};

export default GPSearchInput;

const styles = StyleSheet.create({
  main: {
    width: '100%',
    height: 50,
    backgroundColor: '#303137',
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 6,
  },
  icon: {
    width: '10%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
