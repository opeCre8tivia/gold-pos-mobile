import {FlatList, StyleSheet, Text, View} from 'react-native';
import React, {useEffect} from 'react';
import GPButton from './GPButton';
import {dimentions} from '../constants/theme';
import {testPrint} from '../utils/testPrint';
import {connectToBluetoothDevice} from '../utils/connectToBluetoothDevice';
import GPRadioButton from './GPRadioButton';
import {useDispatch, useSelector} from 'react-redux';
import {RootState} from '../redux/store';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {BLUETOOTH_DEVICE_KEY} from '../constants/asyncStorage';
import {BluetoothManager} from '@brooons/react-native-bluetooth-escpos-printer';
import {_addConnectedDeviceAddress} from '../redux/features/printer.slice';
import {
  connectToBluetoothPrinters,
  disconnectBluetoothPrinter,
} from '../redux/actions/printer.actions';
import PrintTestButton from './PrintTestButton';

interface ResultProps {
  pairedDevices: any[];
  unPairedDevices: any[];
}

interface RadioBtnValue {
  borderColor: string;
  id: string;
  selected: boolean;
  value: string;
}

const PrinterSearchResults = ({
  pairedDevices,
  unPairedDevices,
}: ResultProps) => {
  const dispatch = useDispatch<any>();
  const {connected_device_address, device_conn_loading, error_message} =
    useSelector((state: RootState) => state.printerSlice);

  // Get Already connected device
  useEffect(() => {
    getStoredDevice();
  }, []);

  const getStoredDevice = async () => {
    try {
      let address = await AsyncStorage.getItem(BLUETOOTH_DEVICE_KEY);

      // check if device with address is connected
      if (address) {
        let connectedAddress =
          await BluetoothManager.getConnectedDeviceAddress();
        if (connectedAddress === JSON.parse(address)) {
          //set it to redux
          dispatch(_addConnectedDeviceAddress(connectedAddress));
        }
      }
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <View style={styles.printer_search_cont}>
      <View style={styles.header}>
        <Text style={{color: '#fff', alignItems: 'center'}}>
          {' '}
          Bluetooth printer
        </Text>
      </View>

      {pairedDevices && pairedDevices.length > 0 && (
        <View
          style={{
            width: '100%',
            height: 40,
            borderBottomColor: '#7a7a7a',
            borderBottomWidth: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text style={{color: '#7a7a7a'}}>Paired Devices</Text>
        </View>
      )}

      <FlatList
        data={pairedDevices}
        renderItem={({item, index}) => (
          <View
            style={{
              width: '100%',
              flexDirection: 'row',
              height: 50,
              justifyContent: 'space-between',
              paddingHorizontal: 8,
            }}>
            <View>
              <Text style={{color: '#fff', fontSize: 13}}>{item.name}</Text>
              <Text style={{fontSize: 8, color: '#fff'}}>{item.address}</Text>
            </View>

            <View>
              <GPRadioButton
                loading={device_conn_loading}
                selected={
                  connected_device_address === item.address ? true : false
                }
                onPress={({isSelected}) => {
                  isSelected
                    ? //@ts-ignore
                      dispatch(connectToBluetoothPrinters(item))
                    : dispatch(disconnectBluetoothPrinter(item));
                }}
              />
            </View>
          </View>
        )}
      />

      {unPairedDevices && unPairedDevices.length > 0 && (
        <View
          style={{
            width: '100%',
            height: 40,
            borderBottomColor: '#7a7a7a',
            borderBottomWidth: 1,
            justifyContent: 'center',
            alignItems: 'center',
            marginBottom: 8,
          }}>
          <Text style={{color: '#7a7a7a'}}> Un Paired Devices</Text>
        </View>
      )}
      <FlatList
        data={unPairedDevices}
        renderItem={({item, index}) => (
          <View
            style={{
              width: '100%',
              flexDirection: 'row',
              height: 50,
              justifyContent: 'space-between',
              paddingHorizontal: 8,
            }}>
            <View>
              <Text style={{color: '#fff', fontSize: 13}}>{item.name}</Text>
              <Text style={{fontSize: 8, color: '#fff'}}>{item.address}</Text>
            </View>

            <View>
              <GPButton
                title="Pair"
                styles={{height: 40}}
                onPress={() =>
                  connectToBluetoothDevice(item.address, item?.name)
                }
              />
            </View>
          </View>
        )}
      />

      <View
        style={{
          height: 50,
          alignItems: 'center',
          justifyContent: 'center',
          marginVertical: 10,
        }}>
        <PrintTestButton onPress={() => testPrint()} />
      </View>
    </View>
  );
};

export default PrinterSearchResults;

const styles = StyleSheet.create({
  printer_search_cont: {
    width: dimentions.col * 10,
    minHeight: dimentions.row * 6,
    maxHeight: dimentions.row * 12,
    backgroundColor: '#1F2937',
    padding: 10,
  },
  header: {
    height: 60,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
