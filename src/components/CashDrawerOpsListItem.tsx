import {parseISO, getISODay} from 'date-fns';
import React, {useState} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {colors} from '../constants/theme';
import {_addSelectedSale} from '../redux/features/printer.slice';
import {RootState} from '../redux/store';
import {CashDrawerOperation} from '../types/main';
import PadlockIcon from '../assets/svgIcons/padlock.icon';
import {useNavigation} from '@react-navigation/native';
import {CASHDRAWER_OPS_DETAILS_SCREEN} from '../constants/screenNames';

const CashDrawerOpsListItem = ({
  drawerOps,
}: {
  drawerOps: CashDrawerOperation;
}) => {
  const navigation = useNavigation<any>();
  const {selectedSale} = useSelector((state: RootState) => state.printerSlice);

  return (
    <TouchableOpacity
      onPress={() => {
        if (drawerOps.isActive) {
          navigation.navigate(CASHDRAWER_OPS_DETAILS_SCREEN, {
            opsId: drawerOps.id,
          });
        }
      }}
      style={[
        styles.main,
        {
          backgroundColor:
            selectedSale?.id === drawerOps?.id ? '#5B6879' : '#2A333E',
        },
      ]}>
      <View style={{flex: 1, flexDirection: 'row', paddingVertical: 8}}>
        {/* lock icon */}
        <View
          style={{
            width: 20,
            height: 40,
            marginRight: 15,
            marginTop: -8,
            alignItems: 'flex-end',
          }}>
          <PadlockIcon
            fill={drawerOps.isActive ? colors.gp_green : colors.btn_gray}
          />
        </View>

        {/* content */}

        <View>
          <Text
            style={{
              fontSize: 18,
              color: '#fff',
              fontWeight: '500',
            }}>
            {drawerOps.cashDrawer?.name}
          </Text>
          <Text
            style={{
              fontSize: 14,
              color: drawerOps.isActive ? colors.gp_green : colors.btn_gray,
              fontWeight: '700',
              marginHorizontal: 6,
            }}>
            {drawerOps.employee?.first_name}
          </Text>
        </View>
      </View>

      <View style={{flex: 1, flexDirection: 'row', justifyContent: 'flex-end'}}>
        <Text
          style={{
            fontSize: 22,
            color: selectedSale?.id === drawerOps?.id ? '#ffffff' : '#A5A5A5',
            fontWeight: '700',
            textAlign: 'right',
            marginTop: 8,
          }}>
          {drawerOps.currentBalance}
        </Text>
        <View
          style={[
            styles.right_chevron,
            {
              backgroundColor:
                selectedSale?.id === drawerOps?.id ? colors.orange : '#7a7a7a',
            },
          ]}>
          <Image
            source={require('../assets/icons/forwardArrow.png')}
            style={{width: 20, height: 20}}
            resizeMode="contain"
          />
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default CashDrawerOpsListItem;

const styles = StyleSheet.create({
  main: {
    width: '100%',
    height: 70,
    backgroundColor: '#2A333E',
    borderRadius: 6,
    paddingLeft: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 6,
    overflow: 'hidden',
  },
  right_chevron: {
    width: 26,
    height: '100%',
    backgroundColor: '#7a7a7a',
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 8,
  },
});
