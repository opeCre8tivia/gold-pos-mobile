import React from 'react'
import { Image, StyleSheet, Text, View } from 'react-native'
import { colors, dimentions, size } from '../constants/theme'

interface Props {
    text:string,
    icon_path:any,
    focused:boolean,
    button_styles?:any,
}

const GPBottomTab = ({text,icon_path,focused,button_styles}:Props) => {
  return (
    <View style={[styles.main,button_styles]}>
        <Image source={icon_path} style={{width:25,height:25,marginHorizontal:8}} resizeMode="contain" />
        <Text style={[styles.tabText,{color:focused? colors.white:"#A5A5A5"}]}>
            {text}
        </Text>
    </View>
  )
}

export default GPBottomTab

const styles = StyleSheet.create({
    main:{
        width:dimentions.vw/6,
        height:"100%",
        flexDirection:"row",
        justifyContent:"center",
        alignItems:"center",
        paddingBottom:4
    },
    tabText:{
        fontSize:size.sm,
        fontWeight:"500",
        color:"#A5A5A5",
        textTransform:'uppercase'
    }
})