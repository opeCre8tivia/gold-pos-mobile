import {
  ActivityIndicator,
  Pressable,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import React from 'react';
import {colors} from '../constants/theme';

interface Props {
  value: boolean;
  onChange: (a: boolean) => void;
  loading?: boolean;
}

const BTconnSwitch = ({value, onChange, loading}: Props) => {
  return (
    <>
      {loading ? (
        <View style={styles.main}>
          <ActivityIndicator size={20} color={'#ffffff'} />
        </View>
      ) : (
        <Pressable onPress={() => onChange(value)} style={styles.main}>
          <View
            style={{
              ...styles.device_conn_light,
              backgroundColor: value === true ? 'lime' : 'gray',
            }}></View>
        </Pressable>
      )}
    </>
  );
};

export default BTconnSwitch;

const styles = StyleSheet.create({
  main: {
    width: 60,
    height: 60,
    borderRadius: 30,
    backgroundColor: colors.bg_dark,
    justifyContent: 'center',
    alignItems: 'center',
  },
  device_conn_light: {
    width: 20,
    height: 20,
    borderRadius: 10,
    backgroundColor: 'blue',
    elevation: 10,
  },
});
