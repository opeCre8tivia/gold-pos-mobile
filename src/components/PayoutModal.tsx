import {
  Modal,
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  FlatList,
  ToastAndroid,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {colors, dimentions} from '../constants/theme';
import GPButton from './GPButton';
import DialButton from './DialButton';
import {_cashIn} from '../redux/actions/cashin.actions';
import {RootState, useAppDispatch} from '../redux/store';
import {useSelector} from 'react-redux';
import GPSuccessOrErrorModal from './GPSuccessOrErrorModal';
import {
  clearPayoutState,
  togglePayoutModalVisibility,
} from '../redux/features/payout.slice';
import GPRadioButton from './GPRadioButton';
import {_payOut} from '../redux/actions/payout.actions';
import {Toast} from 'react-native-toast-message/lib/src/Toast';
import {toastConfig} from '../../App';
import GPCheckbox from './GPCheckbox';

type Props = {
  visibility: boolean;
  cashDrawerOpsId: number;
};

const PayoutModal = ({visibility, cashDrawerOpsId}: Props) => {
  const {loading, isError, responseMessage, isSuccess} = useSelector(
    (state: RootState) => state.payoutSlice,
  );

  const {clocked_in_employees} = useSelector(
    (state: RootState) => state.clockingSlice,
  );
  const dispatch = useAppDispatch();
  const [reason, setReason] = useState<string>('');
  const [amount, setAmount] = useState<number>(0);

  const [showEnterAmountPad, setShowEnterAmountPad] = useState<boolean>(false);
  const [selectedEmployee, setSelectedEmployee] = useState<number | null>(null);

  useEffect(() => {
    if (isError) {
      //show toast
      Toast.show({
        type: 'goldposred',
        text1: responseMessage ? responseMessage : 'Something has gone wrong!',
        topOffset: 5,
      });

      setTimeout(() => {
        setAmount(0);
        setReason('');
        setSelectedEmployee(null);
        dispatch(clearPayoutState());
      }, 4000);
    }

    return () => {
      setAmount(0);
      setReason('');
      setSelectedEmployee(null);
    };
  }, [isError]);

  useEffect(() => {
    if (isSuccess) {
      //show toast
      Toast.show({
        type: 'goldposgreen',
        text1: `Payout of USh ${amount} from the cash drawer complete`,
        topOffset: 5,
      });

      //
      setTimeout(() => {
        setAmount(0);
        setReason('');
        setSelectedEmployee(null);
        setShowEnterAmountPad(false);
        dispatch(clearPayoutState());
      }, 4000);
    }

    return () => {
      setAmount(0);
      setReason('');
      setSelectedEmployee(null);
      setShowEnterAmountPad(false);
    };
  }, [isSuccess]);

  const handleSetAmount = (value: string) => {
    //concatnate current state if > 0 with value

    if (amount > 0) {
      let _newValue = amount.toString() + value;
      setAmount(parseInt(_newValue));
    } else {
      setAmount(parseInt(value));
    }
  };

  const confirmPayout = () => {
    /**
     * a cash in is recorded in the db, but it also updates
     * the respective cashdrawers operation current balance
     */
    if (!selectedEmployee || reason.length === 0 || amount === 0) {
      //show toast
      Toast.show({
        type: 'goldposwarning',
        text1: `Some Information is missing, try again`,
        topOffset: 5,
      });

      return;
    }

    if (selectedEmployee) {
      let payoutData = {
        reason: reason,
        amount: amount,
        cashdrawerOpsId: cashDrawerOpsId,
        payoutToId: selectedEmployee,
      };
      dispatch(_payOut(payoutData));
    }
  };

  const dials = [
    {title: '1', value: '1', hasIcon: false, icon: ''},
    {title: '2', value: '2', hasIcon: false, icon: ''},
    {title: '3', value: '3', hasIcon: false, icon: ''},
    {title: '4', value: '4', hasIcon: false, icon: ''},
    {title: '5', value: '5', hasIcon: false, icon: ''},
    {title: '6', value: '6', hasIcon: false, icon: ''},
    {title: '7', value: '7', hasIcon: false, icon: ''},
    {title: '8', value: '8', hasIcon: false, icon: ''},
    {title: '9', value: '9', hasIcon: false, icon: ''},
    {title: '0', value: '0', hasIcon: false, icon: ''},
    {title: '00', value: '00', hasIcon: false, icon: ''},
    {
      title: 'C',
      value: 'delete',
      hasIcon: false,
      icon: '',
      style_text: {color: '#F05052'},
    },
  ];

  return (
    <Modal
      visible={visibility}
      animationType={'fade'}
      transparent={true}
      style={{flex: 1}}>
      <View style={styles.main}>
        {/* errors */}

        {isError && (
          <GPSuccessOrErrorModal
            message={{
              isError: isError,
              msg: responseMessage ? responseMessage : '',
              title: 'Cash In Error',
              isSuccess: false,
            }}
          />
        )}
        {showEnterAmountPad ? (
          <View style={styles.dial_pad_wrapper}>
            <View style={styles.top_container}>
              <TouchableOpacity
                style={{flex: 1}}
                onPress={() => dispatch(togglePayoutModalVisibility())}>
                <Text
                  style={{
                    color: '#ED4C5C',
                    fontSize: 16,
                    fontWeight: '600',
                    textAlign: 'center',
                  }}>
                  Cancel
                </Text>
              </TouchableOpacity>

              <View style={{flex: 2}}>
                <Text
                  style={{
                    color: '#ffffff',
                    fontSize: 16,
                    fontWeight: '600',
                    textAlign: 'left',
                  }}>
                  Enter received amount
                </Text>
              </View>
            </View>

            {/* amount */}
            <View style={styles.dial_amount_container}>
              <View
                style={{
                  width: 80,
                  height: 80,
                  backgroundColor: '#0069A5',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Image
                  source={require('../assets/icons/mathIcon.png')}
                  style={{width: 30, height: 30}}
                />
              </View>

              <View
                style={{
                  height: '100%',
                  width: 60,
                  alignItems: 'center',
                  justifyContent: 'center',
                  marginHorizontal: 4,
                }}>
                <Text
                  style={{
                    fontWeight: '500',
                    fontSize: 25,
                    color: '#ffffff',
                    textAlign: 'center',
                  }}>
                  USh
                </Text>
              </View>

              <View
                style={{
                  height: '100%',
                  flex: 1,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    fontWeight: '500',
                    fontSize: 25,
                    color: '#ffffff',
                    textAlign: 'left',
                    width: '100%',
                  }}>
                  {amount}
                </Text>
              </View>
            </View>
            <View style={styles.dial_pad_inner_wrapper}>
              {/* pad */}
              <View style={styles.dial_pad_container}>
                {dials &&
                  dials.map((item, index) => (
                    <DialButton
                      data={item}
                      key={index}
                      styles={{
                        width: 114,
                        height: 46,
                        margin: 2,
                        backgroundColor: '#303137',
                      }}
                      handlePress={(value: string) => {
                        value === 'delete'
                          ? setAmount(0)
                          : handleSetAmount(item.value);
                      }}
                    />
                  ))}
              </View>

              {/* button */}
              <View style={styles.button_container}>
                <GPButton
                  title={`Confirm - USh ${amount}`}
                  styles={{
                    width: '100%',
                    height: 70,
                    backgroundColor: '#0069A5',
                    borderRadius: 3,
                  }}
                  text_styles={{fontSize: 18, fontWeight: '500'}}
                  onPress={() => confirmPayout()}
                  loading={loading}
                />
              </View>
            </View>
          </View>
        ) : (
          <View style={styles.container}>
            {/* header */}
            <View style={styles.header}>
              <Text style={{color: '#7a7a7a', fontSize: 12}}>Close</Text>
              <Text
                style={{
                  textAlign: 'center',
                  width: '85%',
                  fontWeight: '700',
                  color: '#ffffff',
                  fontSize: 18,
                }}>
                PayOut
              </Text>
            </View>
            {/* description */}
            <View
              style={{
                height: dimentions.row * 1.2,
                borderBottomColor: '#7a7a7a',
                borderWidth: 1,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  color: colors.white,
                  fontSize: 12,
                  textAlign: 'center',
                }}>
                Select employee your paying out to
              </Text>
            </View>

            {/* list of employees */}
            <View
              style={{
                margin: 20,
                backgroundColor: '#2A333E',
              }}>
              {clocked_in_employees && (
                <FlatList
                  data={clocked_in_employees}
                  showsVerticalScrollIndicator={false}
                  renderItem={({item, index}) => {
                    let isSelected = item.id === selectedEmployee;
                    return (
                      <View
                        style={{
                          flexDirection: 'row',
                          alignItems: 'center',
                          borderBottomColor: '#7a7a7a',
                          borderBottomWidth: 1,
                          padding: 14,
                        }}>
                        <GPCheckbox
                          onPress={isChecked => {
                            if (!isChecked) {
                              setSelectedEmployee(item.id);
                            }
                            if (isChecked) {
                              setSelectedEmployee(null);
                            }
                          }}
                          isChecked={isSelected}
                        />
                        <Text
                          style={{
                            paddingHorizontal: 4,
                            fontWeight: '400',
                            color: '#fff',
                            fontSize: 18,
                          }}>
                          {`${item.first_name} ${item.last_name}`}
                        </Text>
                      </View>
                    );
                  }}
                />
              )}
            </View>
            {/* input */}
            <View
              style={{
                height: dimentions.row * 1.3,
                alignItems: 'center',
              }}>
              <TextInput
                onChangeText={(text: string) => setReason(text)}
                placeholder="Enter reason for paying out "
                placeholderTextColor="#7a7a7a"
                value={reason}
                style={{
                  backgroundColor: colors.btn_blue,
                  fontSize: 14,
                  paddingHorizontal: 8,
                  width: dimentions.col * 7.5,
                  marginTop: 6,
                  color: colors.white,
                }}
              />
            </View>
            {/* buttons */}
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
                width: '100%',
                marginTop: 6,
                paddingHorizontal: 8,
              }}>
              <GPButton
                title="Okay"
                onPress={() => setShowEnterAmountPad(!showEnterAmountPad)}
                styles={{
                  backgroundColor: colors.blue,
                  height: dimentions.row * 1.3,
                  width: dimentions.col * 3.7,
                }}
              />
              <GPButton
                title="Cancel"
                onPress={() => dispatch(togglePayoutModalVisibility())}
                styles={{
                  backgroundColor: colors.btn_blue,
                  height: dimentions.row * 1.3,
                  width: dimentions.col * 3.7,
                }}
              />
            </View>
          </View>
        )}
      </View>

      {/* 
        new instance of Toast so it can be rendered inside the modal
       */}

      <Toast config={toastConfig} />
    </Modal>
  );
};

export default PayoutModal;

const styles = StyleSheet.create({
  main: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.9)',
  },
  container: {
    width: dimentions.col * 8,
    height: dimentions.row * 8,
    backgroundColor: colors.black,
  },
  header: {
    height: dimentions.row * 1.5,
    width: '100%',
    backgroundColor: '#303137',
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 8,
  },
  pad_container: {
    backgroundColor: 'pink',
    width: dimentions.col * 8,
    height: dimentions.row * 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  dial_pad_wrapper: {
    width: dimentions.col * 6.5,
    height: dimentions.row * 10,
    alignItems: 'center',
    backgroundColor: 'pink',
  },
  top_container: {
    width: '100%',
    height: 50,
    backgroundColor: '#2A333E',
    flexDirection: 'row',
    alignItems: 'center',
  },

  dial_pad_inner_wrapper: {
    width: '100%',
    height: dimentions.row * 9,
    backgroundColor: colors.bg_dark,
    paddingVertical: 15,
    paddingHorizontal: 22,
  },
  dial_amount_container: {
    width: '100%',
    height: dimentions.row * 2,
    backgroundColor: colors.bg_light,
    flexDirection: 'row',
  },
  dial_pad_container: {
    width: '100%',
    height: dimentions.row * 5,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
    marginVertical: 15,
  },
  button_container: {
    width: '100%',
    height: dimentions.row * 2,
  },
});
