import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'
import { colors, size } from '../constants/theme'

interface Button {
  title:string,
  value:string,
  hasIcon:boolean,
  icon:any,
  style_text?:any
}
interface Props {
    styles?:any
    text_styles?:any,
    data:Button,
    handlePress:(a:any)=>void
}

const DialButton = ({data,handlePress,styles,text_styles}:Props) => {
  return (
    <TouchableOpacity
       onPress={()=> {
           data.hasIcon ? handlePress("delete") : handlePress(data.value)
       } } 
       style={[styles_inner.main,styles]}>

     {
        data.hasIcon ? <Image source={require('../assets/icons/backspace.png')}  /> :
        <Text style={[styles_inner.txt,text_styles,data?.style_text]}> {data.title} </Text>
     }

    </TouchableOpacity>
  )
}

export default DialButton

const styles_inner = StyleSheet.create({
    main:{
        width:90,
        height:72,
        borderRadius:3,
        backgroundColor:colors.btn_blue,
        margin:2,
        justifyContent:"center",
        alignItems:"center"
    },
    txt:{
        color:colors.white,
        fontSize:size.md,
        fontWeight:"500"
    }
})