import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'

interface Props {
    onPress?:()=>void,
    iconPath:any,
    text:string,
    iconStyles?:{},
    textStyles?:{}
}

const GPIcon = ({onPress,iconPath,text,iconStyles,textStyles}:Props) => {
  return (
    <TouchableOpacity style={[styles.main,iconStyles]} onPress={onPress}>
        <Image source={iconPath} style={{width:30,height:30}} resizeMode="contain" />
        <Text style={[styles.txt,textStyles]}>
            {text}
        </Text>
    </TouchableOpacity>
  )
}

export default GPIcon

const styles = StyleSheet.create({
    main:{
        width:60,
        height:"100%",
        justifyContent:"center",
        alignItems:"center",
    },
    txt:{
        fontSize:8,
        fontWeight:"500",
        color:"#fff",
        marginTop:2,

    }
})