import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {colors, dimentions, size} from '../constants/theme';
import {CustomerType} from '../types/main';

interface TabProps {
  sales_type: string;
  onPress: () => void;
  customer?: CustomerType | null;
}

const SalesPanelTab = ({sales_type, onPress, customer}: TabProps) => {
  return (
    <TouchableOpacity style={styles.btn} onPress={onPress}>
      {customer ? (
        <View
          style={{
            width: dimentions.col * 2.5,
            height: '100%',
            padding: 4,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          {/* <Text style={{color: colors.white, fontSize: 14}}>
            {customer.first_name}
          </Text> */}
          <Text style={{color: colors.white, fontSize: 12}}>
            {customer.email}
          </Text>
        </View>
      ) : (
        <Text style={styles.text}>{sales_type}</Text>
      )}
    </TouchableOpacity>
  );
};

export default SalesPanelTab;

const styles = StyleSheet.create({
  btn: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 'auto',
    height: dimentions.row * 1,
    backgroundColor: '#303137',
    borderRadius: 3,
    paddingHorizontal: 10,
    marginHorizontal: 3,
  },
  text: {
    fontSize: size.md,
    fontWeight: '300',
    color: colors.white,
    textAlign: 'center',
  },
});
