import { StyleSheet, View } from 'react-native'
import React from 'react'
import { colors, dimentions, size } from '../constants/theme'
import GPButton from './GPButton'
import DialButton from './DialButton'
import TotalPane from './TotalPane'

const DialPadPanel = () => {

  const dials = [
    { title: "C", value: "cancel", hasIcon: false, icon: "", style_text: { color: "#F05052" } },
    { title: ".", value: ".", hasIcon: false, icon: "" },
    { title: "DEL", value: "delete", hasIcon: true, icon: "" },
    { title: "7", value: "7", hasIcon: false, icon: "" },
    { title: "8", value: "8", hasIcon: false, icon: "" },
    { title: "9", value: "9", hasIcon: false, icon: "" },
    { title: "4", value: "4", hasIcon: false, icon: "" },
    { title: "5", value: "5", hasIcon: false, icon: "" },
    { title: "6", value: "6", hasIcon: false, icon: "" },
    { title: "1", value: "1", hasIcon: false, icon: "" },
    { title: "2", value: "2", hasIcon: false, icon: "" },
    { title: "3", value: "3", hasIcon: false, icon: "" },
    { title: "00", value: "00", hasIcon: false, icon: "" },
    { title: "0", value: "0", hasIcon: false, icon: "" },
    { title: "x", value: "*", hasIcon: false, icon: "", style_text: { color: "#536EB5" } },

  ]







  return (
    <View style={styles.main}>

      <TotalPane />
    
      <View style={styles.button_pane}>
        <View style={styles.dial_button_pane}>
         
            {
              dials && dials.map((item, index) => (
                <DialButton
                  data={item}
                  key={index}
                  styles={{ width:dimentions.col*5.5/3, height: dimentions.row*5.5/5, backgroundColor: "#303137" }}
                  handlePress={(value: string) => {
                    console.log(value, 'diaaal')
                    // value === "delete" ? deleteDial() : dialPin(value)
                  }}
                />
              ))
            }
        
        </View>

        <View style={styles.large_button_pane}>
          <GPButton title='Edit Order' styles={{ width:dimentions.col*1.8, height:dimentions.row*5.5/4}} />
          <GPButton title='On hold' styles={{ width:dimentions.col*1.8, height:dimentions.row*5.5/4}} />
          <GPButton title='Tables' styles={{ width:dimentions.col*1.8, height:dimentions.row*5.5/4 }} />
          <GPButton title='Cash' styles={{ backgroundColor: "#279870", width:dimentions.col*1.8, height:dimentions.row*6/4 }} />


        </View>
      </View>
    </View>
  )
}

export default DialPadPanel

const styles = StyleSheet.create({
  main: {
    width:dimentions.col*8,
    height:dimentions.row*8,
    borderTopColor: "#7a7a7a",
    backgroundColor: colors.bg_light,
    borderTopWidth: 1
  },
  button_pane: {
    width: "100%",
    height: "100%",
    flexDirection: "row"
  },
  dial_button_pane: {
    width:dimentions.col*6,
    height:dimentions.row*7,
    flexDirection:"row",
    flexWrap:"wrap",
    justifyContent: "flex-start",
    alignItems: "center",
    paddingLeft:size.sm
  },
  large_button_pane: {
    width:dimentions.col*2,
    height:dimentions.row*7,
    alignItems: "center",
  }
})