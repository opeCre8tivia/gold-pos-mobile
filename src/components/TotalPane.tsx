import {StyleSheet, Text, View} from 'react-native';
import React, {useState, useEffect} from 'react';
import {colors, dimentions, size} from '../constants/theme';
import {useSelector} from 'react-redux';
import {addCommasToPrice} from '../utils/addCommasToPrice';

const TotalPane = () => {
  const {addedMenuItemsTotal} = useSelector(
    (state: any) => state.directSaleSlice,
  );

  const [totalPrice, setTotalPrice] = useState('0.00');

  useEffect(() => {
    if (addedMenuItemsTotal) {
      let formatedPrice = addCommasToPrice(addedMenuItemsTotal);
      setTotalPrice(formatedPrice);
    }
  }, [addedMenuItemsTotal]);

  return (
    <View style={styles.total_pane}>
      <Text
        style={{
          flex: 3,
          fontSize: size.md,
          color: '#7a7a7a',
          fontWeight: '800',
          textAlign: 'right',
          padding: 3,
        }}>
        {' '}
        Order Total
      </Text>
      <View style={{flex: 1.4, flexDirection: 'row', padding: 3}}>
        <Text
          style={{
            fontSize: size.md,
            color: '#fff',
            fontWeight: '400',
            textAlign: 'right',
          }}>
          {' '}
          Ush:{' '}
        </Text>
        <Text
          style={{
            fontSize: size.md,
            color: '#fff',
            fontWeight: '400',
            textAlign: 'right',
          }}>
          {' '}
          {totalPrice}{' '}
        </Text>
      </View>
    </View>
  );
};

export default TotalPane;

const styles = StyleSheet.create({
  total_pane: {
    width: '100%',
    height: dimentions.row * 1.5,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: colors.bg_dark,
  },
});
