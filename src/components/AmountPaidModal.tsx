import {Modal, StyleSheet, Text, Pressable} from 'react-native';
import React, {useEffect} from 'react';
import {colors} from '../constants/theme';
import {Toast} from 'react-native-toast-message/lib/src/Toast';
import {toastConfig} from '../../App';
import {useSelector} from 'react-redux';
import {RootState, useAppDispatch} from '../redux/store';
import {clearOrderState} from '../redux/features/order.slice';

interface Props {
  amount: number;
  show: boolean;
  onPress: () => any;
}

const AmountPaidModal = ({amount, show, onPress}: Props) => {
  const dispatch = useAppDispatch();
  const {parkError, parkSuccess, parkResponseMessage} = useSelector(
    (state: RootState) => state.orderSlice,
  );

  useEffect(() => {
    if (parkError) {
      Toast.show({
        type: 'goldposred',
        text1: parkResponseMessage
          ? parkResponseMessage
          : 'Something has gone wrong',
        position: 'top',
        topOffset: 5,
      });

      setTimeout(() => {
        dispatch(clearOrderState());
      }, 4000);
    }

    if (parkSuccess) {
      Toast.show({
        type: 'goldposgreen',
        text1: parkResponseMessage
          ? parkResponseMessage
          : 'Order closed successfully',
        position: 'top',
        topOffset: 5,
      });

      setTimeout(() => {
        dispatch(clearOrderState());
      }, 4000);
    }

    return () => {};
  }, [parkError, parkSuccess]);

  return (
    <Modal visible={show} style={{flex: 1}}>
      <Pressable style={styles.main} onPress={onPress}>
        <Text
          style={{
            fontWeight: '700',
            fontSize: 60,
            color: colors.white,
            textAlign: 'center',
          }}>
          Amount paid
        </Text>
        <Text
          style={{
            fontWeight: '700',
            fontSize: 200,
            color: colors.white,
            textAlign: 'center',
          }}>
          {amount}
        </Text>
      </Pressable>

      <Toast config={toastConfig} />
    </Modal>
  );
};

export default AmountPaidModal;

const styles = StyleSheet.create({
  main: {
    flex: 1,
    width: '100%',
    height: '100%',
    backgroundColor: colors.bg_dark,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
