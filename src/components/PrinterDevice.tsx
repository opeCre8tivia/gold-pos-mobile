import React, {useState, useEffect} from 'react';
import {View, Text, Image, StyleSheet, ToastAndroid} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {colors, dimentions} from '../constants/theme';
import {RootState} from '../redux/store';
import {Switch, SwitchProps} from 'react-native-switch';
import {
  connectToBluetoothPrinters,
  disconnectBluetoothPrinter,
} from '../redux/actions/printer.actions';
import BTconnSwitch from './BTconnSwitch';

type Device = {
  address: string;
  name?: string;
};

interface Props {
  device: Device;
}

const PrinterDevice = ({device}: Props) => {
  const dispatch = useDispatch<any>();
  const {
    connected_device,
    connected_device_address,
    error_message,
    device_conn_loading,
  } = useSelector((state: RootState) => state.printerSlice);

  useEffect(() => {
    error_message && ToastAndroid.show(error_message, ToastAndroid.CENTER);
  }, [error_message]);

  return (
    <View style={styles.device}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
          height: '100%',
        }}>
        <View style={styles.device_img_cont}>
          <Image
            source={require('../assets/icons/button_printer.png')}
            style={{width: '100%', height: '100%'}}
          />
        </View>
        <View style={styles.device_details}>
          <Text style={{color: colors.white}}>{device.name}</Text>
        </View>
      </View>

      {/* 0/1 switch */}
      {/* TODO: remove react native switch module */}

      {/* conn or disconn light */}
      <BTconnSwitch
        value={connected_device?.address === connected_device_address}
        loading={device_conn_loading}
        onChange={val => {
          if (val) {
            connected_device &&
              dispatch(disconnectBluetoothPrinter(connected_device));
          } else if (!val) {
            connected_device &&
              dispatch(connectToBluetoothPrinters(connected_device));
          }
        }}
      />
    </View>
  );
};

export default PrinterDevice;

const styles = StyleSheet.create({
  main: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    position: 'relative',
  },
  no_device_cont: {
    width: dimentions.col * 6,
    minHeight: dimentions.row * 8,
    alignItems: 'center',
  },
  printer_icon_cont: {
    width: dimentions.col * 3,
    height: dimentions.row * 5,
    backgroundColor: '#1F2937',
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
  },
  txt_cont: {
    marginTop: 30,
  },
  txt: {
    textAlign: 'center',
    color: '#7a7a7a',
  },
  fab: {
    width: 60,
    height: 60,
    borderRadius: 30,
    backgroundColor: colors.orange,
    position: 'absolute',
    bottom: 150,
    right: 80,
    justifyContent: 'center',
    alignItems: 'center',
  },
  device: {
    height: 95,
    width: dimentions.col * 8,
    backgroundColor: colors.btn_blue,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 10,
  },
  device_details: {
    color: colors.white,
    fontSize: 14,
    marginLeft: 10,
  },
  device_img_cont: {
    width: 40,
    height: 40,
    borderRadius: 20,
    backgroundColor: colors.bg_dark,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 6,
  },
});
