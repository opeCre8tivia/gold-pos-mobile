import {StyleSheet, View} from 'react-native';
import React from 'react';
import BouncyCheckbox from 'react-native-bouncy-checkbox';
import {colors} from '../constants/theme';

type Props = {
  onPress: (isChecked: boolean) => any;
  isChecked: boolean;
  size?: number;
};

const GPCheckbox = ({onPress, isChecked, size = 30}: Props) => {
  return (
    <BouncyCheckbox
      onPress={() => {
        onPress(isChecked);
      }}
      fillColor={colors.orange}
      isChecked={isChecked}
      size={size}
      disableBuiltInState={true}
    />
  );
};

export default GPCheckbox;

const styles = StyleSheet.create({});
