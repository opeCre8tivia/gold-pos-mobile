import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { colors } from '../constants/theme'

const ComingSoonComponent = () => {
  return (
    <View style={styles.main}>
      <Text style={{textAlign:"center",fontSize:20,color:colors.bg_light}}>
         Coming Soon
      </Text>
    </View>
  )
}

export default ComingSoonComponent

const styles = StyleSheet.create({
    main:{
        width:"100%",
        flex:1,
        justifyContent:"center",
        alignItems:"center"
    }
})