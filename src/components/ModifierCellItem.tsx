import { StyleSheet, Text, View,Image } from 'react-native'
import React, { useEffect } from 'react'
import { colors, size } from '../constants/theme'

interface Modifier{
    name?:string,
    recipe_name?:string,
    productName?:string,
    id:number
}

interface Props {
    modifier:Modifier
}

const ModifierCellItem = ({modifier}:Props) => {  

    return (
        <View style={styles.main}>

            <View style={styles.icon_container}>
               <Image source={require("../assets/icons/modifierArrow.png")} />
            </View>
            <Text style={styles.text}>
                {modifier?.name} {modifier?.recipe_name} {modifier?.productName}
            </Text>
        </View>
    )
}

export default ModifierCellItem

const styles = StyleSheet.create({
    main:{
      width:"100%",
      height:"auto",
      paddingLeft:20,
      flexDirection:"row",
      alignItems:"center",
      marginBottom:1,
    },
    icon_container:{
      width:16,
      height:16,
      marginRight:4,
      justifyContent:"center",
      alignItems:"center"
    },
    text:{
       color:colors.white,
       fontSize:size.sm
    }
})