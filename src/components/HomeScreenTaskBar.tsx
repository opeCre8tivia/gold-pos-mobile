import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'
import GPElipsis from './GPElipsis'
import GPIcon from './GPIcon'
import Logo from './Logo'
import { useNavigation } from '@react-navigation/native'
import { REGISTER_SEARCH_SCREEN } from '../constants/screenNames'
import UserProfile from './UserProfile'
import { dimentions } from '../constants/theme'

const HomeScreenTaskBar = () => {

    const navigation = useNavigation<any>()

  return (
    <View style={styles.main}>
      {/* search and elipsis */}
      <View style={styles.elipsisSearchContainer}>
          {/* <GPElipsis/> */}
          <UserProfile />

        
                         
              <GPIcon 
                 iconPath={require('../assets/icons/search.png')} 
                 text="SEARCH"
                  onPress={()=> navigation.navigate(REGISTER_SEARCH_SCREEN)}
                    
              />
        
      </View>

     {/* logo */}
      <View style={styles.logoContainer}>
          <Logo styles={{width:180,height:100}} />
      </View>

     {/* sub menu */}
      <View style={styles.subMenuContainer}>
      <GPIcon iconPath={require('../assets/icons/bar.png')} text="SEND-BAR" iconStyles={{marginHorizontal:20}}/>
      <GPIcon iconPath={require('../assets/icons/kitchen.png')} text="SEND-KITCHEN" iconStyles={{marginHorizontal:20}}/>
      <GPIcon iconPath={require('../assets/icons/reciept.png')} text="RECIEPT" iconStyles={{marginHorizontal:20}}/>
      </View>

    </View>
  )
}

export default HomeScreenTaskBar

const styles = StyleSheet.create({
    main:{
        width:"100%",
        height:dimentions.row *2,
        backgroundColor:"#000",
        flexDirection:"row"
    },
    elipsisSearchContainer:{
        flex:1,
        flexDirection:"row",
        justifyContent:"space-between",
        alignItems:"center",
        paddingHorizontal:10
    },
    logoContainer:{
        flex:3,
        flexDirection:"row",
        alignItems:"center",
        justifyContent:"center"
        
    },
    subMenuContainer:{
        flex:1,
        flexDirection:"row",
        justifyContent:"flex-end",
        alignItems:"center",
        paddingHorizontal:10,
    }
})