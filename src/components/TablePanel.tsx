import {StyleSheet, Text, View, Pressable, Image} from 'react-native';
import React, {useState, useEffect} from 'react';
import GPTable from './GPTable';
import {colors, dimentions, size} from '../constants/theme';
import {useDispatch, useSelector} from 'react-redux';
import GPModal from './GPModal';
import {
  decreamentAddedItem,
  increamentAddedItem,
  removeAddedMenuItem,
} from '../redux/features/directSale.slice';
import TotalPane from './TotalPane';

const TablePanel = () => {
  const dispatch = useDispatch();
  const {addedMenuItems} = useSelector((state: any) => state.directSaleSlice);

  let _columns = [
    {title: 'QTY', field: 'qty'},
    {title: 'ITEM', field: 'name'},
    {title: 'TOTAL', field: 'total'},
  ];

  const [showRowEditModal, setShowRowEditModal] = useState(false);
  const [rowData, setRowData] = useState<any | null>(null);
  const [rowIndex, setRowIndex] = useState<number | null>(null);

  const handleEditingRow = (e: any) => {
    setShowRowEditModal(true);

    setRowIndex(e?.index);
    setRowData(e.data);
  };

  const handleIncreament = () => {
    /**
     * redux toolkit implements object.freeze, therefore to manipulate array we need to deep copy
     */
    let deepClonedArray = JSON.parse(JSON.stringify(addedMenuItems));
    //identify item from state
    let _stateItem = deepClonedArray.filter((i: any) => i.id === rowData.id)[0];
    //remove it
    deepClonedArray.splice(rowIndex, 1);

    //increament its qty
    _stateItem.qty = _stateItem.qty + 1;

    //update total
    _stateItem.total = _stateItem.price * _stateItem.qty;

    //update local state
    setRowData(_stateItem);

    //add it back to array
    deepClonedArray.splice(rowIndex, 0, _stateItem);

    //dispatch  increament action
    dispatch(increamentAddedItem(deepClonedArray));
  };

  const handleDecreament = () => {
    /**
     * redux toolkit implements object.freeze, therefore to manipulate array we need to deep copy
     */
    let deepClonedArray = JSON.parse(JSON.stringify(addedMenuItems));
    //identify item from state
    let _stateItem = deepClonedArray.filter((i: any) => i.id === rowData.id)[0];
    //remove it
    deepClonedArray.splice(rowIndex, 1);

    //decreament its qty

    if (_stateItem.qty === 1) {
      handleRemoveAddedItem(_stateItem);
      return;
    }
    _stateItem.qty = _stateItem.qty - 1;

    //update total
    _stateItem.total = _stateItem.price * _stateItem.qty;

    //update local state
    setRowData(_stateItem);

    //add it back to array
    deepClonedArray.splice(rowIndex, 0, _stateItem);

    //dispatch  increament action
    dispatch(decreamentAddedItem(deepClonedArray));
  };

  const handleRemoveAddedItem = (row: {
    price: number;
    qty: number;
    name: string;
    id: number;
  }) => {
    //remove item from state array | delete row from table
    /**
     * redux toolkit implements object.freeze, therefore to manipulate array we need to deep copy
     */
    let deepClonedArray = JSON.parse(JSON.stringify(addedMenuItems));
    //identify item from state
    let _newArray = deepClonedArray.filter((i: any) => i.id != row.id);

    dispatch(removeAddedMenuItem(_newArray));

    //close modal for UX purposes
    setShowRowEditModal(!showRowEditModal);
  };

  return (
    <View
      style={[
        styles.main,
        {
          height:
            addedMenuItems.length > 0
              ? dimentions.row * 12
              : dimentions.row * 6,
        },
      ]}>
      {
        <GPTable
          columns={_columns}
          data={addedMenuItems}
          header_styles={{backgroundColor: '#536EB5'}}
          header_cell_styles={{width: '32%'}}
          header_text_styles={{
            color: colors.white,
            fontSize: size.md,
            fontWeight: '500',
            textAlign: 'left',
            paddingLeft: size.sm,
          }}
          onRowPress={e => handleEditingRow(e)}
        />
      }

      {/* display total of added items */}
      <TotalPane />

      <GPModal
        title={`${rowData?.qty} x ${rowData?.name}`}
        visible={showRowEditModal}
        left_button_title="Done"
        right_button_title="Cancel"
        onPressLeftButton={() => setShowRowEditModal(!showRowEditModal)}
        onPressRightButton={() => setShowRowEditModal(!showRowEditModal)}>
        <Pressable
          onPress={() => handleIncreament()}
          style={{
            width: '100%',
            height: dimentions.row * 2,
            flexDirection: 'row',
            alignItems: 'center',
            marginVertical: 1,
          }}>
          <View
            style={{
              width: 30,
              height: 30,
              marginHorizontal: 10,
              marginLeft: (dimentions.col * 6) / 3,
            }}>
            <Image source={require('../assets/icons/addIcon.png')} />
          </View>
          <Text style={{fontSize: 14, color: '#fff', fontWeight: '500'}}>
            Add One
          </Text>
        </Pressable>

        <Pressable
          onPress={() => handleDecreament()}
          style={{
            width: '100%',
            height: dimentions.row * 2,
            flexDirection: 'row',
            alignItems: 'center',
            marginVertical: 1,
          }}>
          <View
            style={{
              width: 30,
              height: 30,
              marginHorizontal: 10,
              marginLeft: (dimentions.col * 6) / 3,
            }}>
            <Image source={require('../assets/icons/minusIcon.png')} />
          </View>
          <Text style={{fontSize: 14, color: '#fff', fontWeight: '500'}}>
            Remove One
          </Text>
        </Pressable>
      </GPModal>
    </View>
  );
};

export default TablePanel;

const styles = StyleSheet.create({
  main: {
    width: '100%',
    height: dimentions.row * 6,
  },
});
