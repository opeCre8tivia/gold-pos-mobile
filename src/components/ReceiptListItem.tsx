import {parseISO, getISODay} from 'date-fns';
import React, {useState} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {colors} from '../constants/theme';
import {_addSelectedSale} from '../redux/features/printer.slice';
import {RootState} from '../redux/store';

const ReceiptListItem = ({reciept}: any) => {
  const dispatch = useDispatch<any>();
  const {selectedSale} = useSelector((state: RootState) => state.printerSlice);

  const _servedAt = new Date(reciept.createdAt).toLocaleString();

  return (
    <TouchableOpacity
      onPress={() => dispatch(_addSelectedSale(reciept))}
      style={[
        styles.main,
        {
          backgroundColor:
            selectedSale?.id === reciept?.id ? '#5B6879' : '#2A333E',
        },
      ]}>
      <View style={{flex: 1, paddingVertical: 8}}>
        <Text
          style={{
            fontSize: 22,
            color: selectedSale?.id === reciept?.id ? '#fff' : '#A5A5A5',
            fontWeight: '700',
          }}>
          {reciept.orderNumber}
        </Text>

        <View style={{flexDirection: 'row', marginTop: 8}}>
          <View style={{flexDirection: 'row'}}>
            <Image source={require('../assets/icons/clockIcon.png')} />
            <Text
              style={{
                fontSize: 14,
                color: selectedSale?.id === reciept?.id ? '#ffffff' : '#A5A5A5',
                fontWeight: '700',
                marginHorizontal: 6,
              }}>
              {_servedAt}
            </Text>
          </View>

          <View style={{flexDirection: 'row'}}>
            <Image source={require('../assets/icons/userSmIcon.png')} />
            <Text
              style={{
                fontSize: 14,
                color: selectedSale?.id === reciept?.id ? '#ffffff' : '#A5A5A5',
                fontWeight: '700',
                marginHorizontal: 6,
              }}>
              {' '}
              {reciept?.employee?.first_name}{' '}
            </Text>
          </View>
        </View>
      </View>

      <View style={{flex: 1, flexDirection: 'row', justifyContent: 'flex-end'}}>
        <Text
          style={{
            fontSize: 22,
            color: selectedSale?.id === reciept?.id ? '#ffffff' : '#A5A5A5',
            fontWeight: '700',
            textAlign: 'right',
            marginTop: 8,
          }}>
          {reciept.total}
        </Text>
        <View
          style={[
            styles.right_chevron,
            {
              backgroundColor:
                selectedSale?.id === reciept?.id ? colors.orange : '#7a7a7a',
            },
          ]}>
          <Image
            source={require('../assets/icons/forwardArrow.png')}
            style={{width: 20, height: 20}}
            resizeMode="contain"
          />
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default ReceiptListItem;

const styles = StyleSheet.create({
  main: {
    width: '100%',
    height: 80,
    backgroundColor: '#2A333E',
    borderRadius: 6,
    paddingLeft: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 12,
    overflow: 'hidden',
  },
  right_chevron: {
    width: 26,
    height: '100%',
    backgroundColor: '#7a7a7a',
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 8,
  },
});
