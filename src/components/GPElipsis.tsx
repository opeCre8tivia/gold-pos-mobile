import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'


interface Elipsis {
    onPress?:()=> void
}

interface Dot {
    size:number,
    color:string

}
 interface Props {
     dot:Dot,
}

const GPElipsis = ({onPress}:Elipsis) => {

    const dots = [
        {
            size:8,
            color:"#35B858"    
        },
        {
            size:8,
            color:"#367CE4"  
        },
        {
            size:8,
            color:"#E46036"    
        },
    ]


 const Dot = ({dot}:Props)=>(
   <View style={{width:dot.size,height:dot.size,borderRadius:dot.size/2,backgroundColor:dot.color,margin:1}}>
   </View>
 )


  return (
    <TouchableOpacity
      onPress={onPress}
      style={styles.main}
    >
      {
        dots && dots.map((dot,index)=>(
           <Dot dot={dot} key={index}/>
        ))
      }
    </TouchableOpacity>
  )
}

export default GPElipsis

const styles = StyleSheet.create({
    main:{
        width:60,
        height:"100%",
        justifyContent:"center",
        alignItems:"center",
    }
})