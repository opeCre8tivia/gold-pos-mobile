import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  FlatList,
  Modal,
} from 'react-native';
import {colors} from '../constants/theme';
import TaskBar from './TaskBar';
import GPSearchInput from './GPSearchInput';
import GPButton from './GPButton';
import GPModal from './GPModal';
import AddCustomer from './AddCustomer';
import {useSelector} from 'react-redux';
import {RootState, useAppDispatch} from '../redux/store';
import {
  clearCustomerState,
  showCreateCustomerModal,
} from '../redux/features/customer.slice';
import {Toast} from 'react-native-toast-message/lib/src/Toast';
import {toastConfig} from '../../App';
import {_listCustomers} from '../redux/actions/customers.actions';
import {CustomerType} from '../types/main';
import {useNavigation} from '@react-navigation/native';
import {CUSTOMERS_DETAILS_SCREEN} from '../constants/screenNames';

const CustomerListAndSearch = () => {
  const dispatch = useAppDispatch();
  const navigation = useNavigation<any>();
  const {createCustomerModalIsVisible, customers, isError, isSuccess} =
    useSelector((state: RootState) => state.customerSlice);

  useEffect(() => {
    navigation.addListener('focus', () => {
      dispatch(_listCustomers());
    });
  }, []);

  useEffect(() => {
    if (isError || isSuccess) {
      dispatch(clearCustomerState());
    }
  }, [isError, isSuccess]);

  const handleViewCustomer = (customer: CustomerType) => {
    navigation.navigate(CUSTOMERS_DETAILS_SCREEN, {customer});
  };

  const ListElement = ({
    customer,
    list_styles,
  }: {
    customer: CustomerType;
    list_styles?: any;
  }) => {
    return (
      <View style={[styles.list_item, list_styles]}>
        <Text style={{flex: 1, color: '#fff', fontSize: 16, fontWeight: '300'}}>
          {customer.first_name}
        </Text>
        <Text style={{flex: 1, color: '#fff', fontSize: 16, fontWeight: '300'}}>
          {customer.last_name}
        </Text>
        <Text style={{flex: 2, color: '#fff', fontSize: 16, fontWeight: '300'}}>
          {customer.email}
        </Text>
        <Text style={{flex: 2, color: '#fff', fontSize: 16, fontWeight: '300'}}>
          {customer.address}
        </Text>
        <Text style={{flex: 1, color: '#fff', fontSize: 16, fontWeight: '300'}}>
          {customer.phone_number_primary}
        </Text>

        <View style={{width: 110, marginLeft: 10}}>
          <TouchableOpacity
            onPress={() => handleViewCustomer(customer)}
            style={{
              width: 100,
              height: 35,
              // backgroundColor: colors.btn_gray,
              justifyContent: 'center',
              alignItems: 'center',
              padding: 2,
              borderWidth: 1,
              borderColor: '#3E4347',
              borderRadius: 6,
            }}>
            <Text style={{color: '#fff', fontSize: 12, fontWeight: '500'}}>
              View
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  return (
    <View style={styles.main}>
      <TaskBar title="Customer" canMoveBack={false} />

      {/* search and button */}
      <View style={styles.top_container}>
        <GPSearchInput
          placeholder="Search for customers"
          input_styles={{height: 69, width: '88%', marginRight: 10}}
        />

        <GPButton
          title="Add Customer"
          styles={{
            height: 69,
            backgroundColor: colors.blue,
            paddingHorizintal: 10,
          }}
          text_styles={{fontSize: 13}}
          onPress={() => dispatch(showCreateCustomerModal())}
        />
      </View>
      {/* table head */}

      <View style={styles.header}>
        <View
          style={{
            flex: 1,
            height: 40,
            justifyContent: 'center',
          }}>
          <Text style={styles.header_text}>First Name</Text>
        </View>

        <View style={{flex: 1, height: 40, justifyContent: 'center'}}>
          <Text style={styles.header_text}>Last Name</Text>
        </View>

        <View style={{flex: 2, height: 40, justifyContent: 'center'}}>
          <Text style={styles.header_text}>Email Address</Text>
        </View>
        <View style={{flex: 2, height: 40, justifyContent: 'center'}}>
          <Text style={styles.header_text}>Address Line</Text>
        </View>

        <View
          style={{
            flex: 1,
            height: 40,
            justifyContent: 'center',
          }}>
          <Text style={styles.header_text}> Phone Number 1</Text>
        </View>

        <View
          style={{
            width: 110,
            height: 40,
            justifyContent: 'center',
            marginLeft: 25,
          }}>
          <Text style={styles.header_text}> Action</Text>
        </View>
      </View>

      <View style={{width: '90%'}}>
        <FlatList
          data={customers}
          renderItem={({item, index}) => {
            return (
              <ListElement
                customer={item}
                list_styles={{
                  backgroundColor:
                    index % 2 === 0 ? 'transparent' : 'transparent',
                }}
              />
            );
          }}
        />
      </View>

      {/* create customer */}

      <Modal
        visible={createCustomerModalIsVisible}
        transparent={true}
        animationType="fade">
        <View
          style={{
            flex: 1,
            backgroundColor: 'rgba(0,0,0,0.9)',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <AddCustomer />
        </View>
        <Toast config={toastConfig} />
      </Modal>
    </View>
  );
};

export default CustomerListAndSearch;

const styles = StyleSheet.create({
  main: {
    flex: 1,
    alignItems: 'center',
    padding: 10,
    backgroundColor: colors.bg_dark,
  },
  top_container: {
    width: '90%',
    marginVertical: 20,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  header: {
    width: '90%',
    height: 40,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderBottomColor: '#3E4347',
  },
  header_text: {
    flex: 1,
    textAlign: 'left',
    color: colors.white,
    fontSize: 16,
    fontWeight: '400',
  },
  list_item: {
    width: '100%',
    height: 52,
    marginVertical: 2,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 6,
    borderBottomWidth: 1,
    borderBottomColor: '#3E4347',
  },
});
