import {StyleSheet, Text, View, FlatList, TouchableOpacity} from 'react-native';
import React, {useEffect, useState} from 'react';
import {dimentions, size} from '../constants/theme';
import ModifierCellItem from './ModifierCellItem';
import {addCommasToPrice} from '../utils/addCommasToPrice';

interface Column {
  title: string;
  field: string;
}

interface Table {
  columns?: Column[];
  data?: any[];
  header_styles?: any;
  header_cell_styles?: any;
  header_text_styles?: any;
  onRowPress?: (e: any) => any;
}

interface Modifier {
  name: string;
  id: number;
}

const GPTable = ({
  columns,
  data,
  header_cell_styles,
  header_styles,
  header_text_styles,
  onRowPress,
}: Table) => {
  const [columnFiledValues, setColumnFiledValues] = useState<any | null>(null);

  //get column field keys
  useEffect(() => {
    let _columnFiledValues = columns && columns.map(c => c.field);
    setColumnFiledValues(_columnFiledValues);
  }, [columns]);

  const Row = ({item, itemIndex, onPress}: any) => {
    return (
      <TouchableOpacity
        style={[
          styles.row,
          {
            height:
              item?.modifiers?.length > 0
                ? dimentions.row * item?.modifiers?.length
                : dimentions.row * 1,
          },
        ]} //height adjusts according to number of modifiers
        onPress={() => {
          let _e = {
            data: item,
            index: itemIndex,
          };

          onPress(_e);
        }}>
        {columnFiledValues &&
          columnFiledValues.map((_fieldKeyValue: any, index: any) => {
            let _cell: any = {};
            if (item[_fieldKeyValue]) {
              //construct object from key |value
              let newObj = {
                [_fieldKeyValue]: item[_fieldKeyValue],
              };

              _cell = newObj;
            }

            return (
              <View style={styles.row_cell} key={index}>
                {index === 2 ? (
                  <Text style={styles.row_text}>
                    {addCommasToPrice(parseInt(_cell[_fieldKeyValue]))}
                  </Text>
                ) : (
                  <Text style={styles.row_text}>{_cell[_fieldKeyValue]}</Text>
                )}

                {index === 1 && (
                  <View>
                    {item?.modifiers &&
                      item?.modifiers.map(
                        (_modifier: Modifier, index: number) => (
                          <ModifierCellItem modifier={_modifier} key={index} />
                        ),
                      )}
                  </View>
                )}
              </View>
            );
          })}
      </TouchableOpacity>
    );
  };

  return (
    <View style={styles.main}>
      {/* header */}
      <View style={[styles.header, header_styles]}>
        {columns &&
          columns.map((item, index) => (
            <View style={header_cell_styles} key={index}>
              <Text style={header_text_styles}> {item?.title} </Text>
            </View>
          ))}
      </View>
      {/* body */}
      <View style={styles.body}>
        <FlatList
          data={data}
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{minHeight: '100%'}}
          renderItem={({item, index}) => (
            <Row
              item={item}
              itemIndex={index}
              key={index}
              onPress={onRowPress}
            />
          )}
        />
      </View>
    </View>
  );
};

export default GPTable;

const styles = StyleSheet.create({
  main: {
    width: '100%',
    height: '100%',
    padding: 5,
    alignItems: 'center',
  },
  header: {
    width: '100%',
    height: dimentions.row * 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },
  header_cell: {
    width: 'auto',
    height: '100%',
  },
  body: {
    width: '100%',
    height: '100%',
  },
  row: {
    flexDirection: 'row',
    width: '100%',
    height: dimentions.row * 1,
    borderBottomWidth: 0.5,
    borderBottomColor: '#3E4347',
  },
  row_cell: {
    width: '32%',
    height: '100%',
    justifyContent: 'center',
  },
  row_text: {
    color: '#fff',
    fontSize: size.md,
    fontWeight: '500',
    textAlign: 'left',
    paddingLeft: size.lg,
  },
});
