import {ScrollView, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import TabsPanel from './TabsPanel';
import TablePanel from './TablePanel';
import DialPadPanel from './DialPadPanel';
import {colors, dimentions} from '../constants/theme';
import {useSelector} from 'react-redux';

const SalesPanel = () => {
  const {addedMenuItems} = useSelector((state: any) => state.directSaleSlice);

  return (
    <View style={styles.main}>
      <TabsPanel />
      {/* duo panel wrapper */}
      <View style={styles.duo_panel_wrapper}>
        <TablePanel />
        {addedMenuItems && addedMenuItems.length === 0 ? (
          <DialPadPanel />
        ) : null}
      </View>
    </View>
  );
};

export default SalesPanel;

const styles = StyleSheet.create({
  main: {
    width: '100%',
    height: '100%',
    justifyContent: 'space-between',
  },
  duo_panel_wrapper: {
    width: '100%',
    height: dimentions.row * 14,
    justifyContent: 'space-between',
    backgroundColor: colors.bg_light,
  },
});
