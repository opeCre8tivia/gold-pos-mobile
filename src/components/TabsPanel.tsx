import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {colors, dimentions, size} from '../constants/theme';
import CurrentTimeComponent from './CurrentTimeComponent';
import SalesPanelTab from './SalesPanelTab';
import {RootState, useAppDispatch} from '../redux/store';
import {showAddCustomerToSaleModal} from '../redux/features/customer.slice';
import {useSelector} from 'react-redux';

const TabsPanel = () => {
  const {customerToAddToSale} = useSelector(
    (state: RootState) => state.customerSlice,
  );
  const dispatch = useAppDispatch();
  const [action, setAction] = useState<string | null>(null);
  //list of sales types
  const salesTypes = [
    {id: 1, sales_type: 'Takeaway', action: 'Direct Sale'},
    {id: 2, sales_type: 'Dine In', action: 'Inner Sale'},
    {
      id: 3,
      sales_type: 'Delivery',
      action: 'Direct Sale',
    },
    {id: 4, sales_type: 'Assign Customer', action: 'Quick Sale'},
  ];

  //set the default sales type and action
  useEffect(() => {
    !action && setAction(salesTypes[0].action);
  }, []);

  const setSalesType = (item: {sales_type: string; action: string}) => {
    //TODO: set sales type to global state and action locally
    setAction(item.action);
  };

  return (
    <View style={styles.main}>
      <View style={styles.header}>
        <Text style={styles.action_text}> Actions </Text>
        <Text style={styles.action_name}> {action} </Text>
        <CurrentTimeComponent />
      </View>

      <View style={styles.tabs_container}>
        {salesTypes &&
          salesTypes.map((item, index) => {
            return (
              <SalesPanelTab
                sales_type={item.sales_type}
                onPress={() => {
                  if (item.id === 4) {
                    dispatch(showAddCustomerToSaleModal());
                  } else {
                    setSalesType(item);
                  }
                }}
                key={index}
                customer={
                  item.id === 4 && customerToAddToSale
                    ? customerToAddToSale
                    : null
                }
              />
            );
          })}
      </View>
    </View>
  );
};

export default TabsPanel;

const styles = StyleSheet.create({
  main: {
    width: '100%',
    height: dimentions.row * 3,
    backgroundColor: colors.bg_light,
    padding: 5,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  header: {
    width: '100%',
    height: dimentions.row * 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  action_text: {
    fontSize: size.lg,
    fontWeight: '400',
    color: colors.orange,
  },
  action_name: {
    fontSize: size.lg,
    fontWeight: '400',
    color: colors.white,
  },
  tabs_container: {
    height: dimentions.row * 1,
    padding: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});
