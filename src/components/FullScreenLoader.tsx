import { StyleSheet, Text, View,ActivityIndicator } from 'react-native'
import React from 'react'
import { colors } from '../constants/theme'

const FullScreenLoader = () => {
  return (
    <View style={styles.main}>
         <ActivityIndicator color="#fff" size={30} />
         <Text> Loading...</Text>
    </View>
  )
}

export default FullScreenLoader

const styles = StyleSheet.create({
    main:{
        flex:1,
        justifyContent:"center",
        alignContent:"center",
        alignItems:"center",
        backgroundColor:colors.bg_dark
    }
})