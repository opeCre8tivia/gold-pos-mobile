import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'

interface Data {
    title:string,
    value:number,

}

interface Props {
    data:Data,
    button_styles?:any,
    onPress?:()=> any
}

const CustomPriceButton = ({data,button_styles,onPress}:Props) => {
  return (
    <TouchableOpacity
        style={[styles_inner.main,button_styles]}
       onPress={onPress}
     >
      <Text style={styles_inner.btn_text}>{data.title}</Text>
    </TouchableOpacity>
  )
}

export default CustomPriceButton

const styles_inner = StyleSheet.create({
    main:{
        backgroundColor:"#2A333E",
        width:90,
        height:60,
        justifyContent:"center",
        alignItems:"center",
        borderRadius:6,
        margin:2
    },
    btn_text:{
        fontWeight:"500",
        fontSize:20,
        color:"#A5A5A5"
    }
})