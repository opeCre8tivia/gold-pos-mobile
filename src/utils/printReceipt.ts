import {
  BluetoothEscposPrinter,
  BluetoothManager,
} from '@brooons/react-native-bluetooth-escpos-printer';
import {getAsyncDevices} from './getAsyncDevices';
import {BusinessSettings, Sale} from '../types/main';
import {gpFormatISO} from './gpFormatISO';

interface Employee {
  first_name: string;
  last_name: string;
}

interface Outlet {
  name: string;
}

export const printReceipt = async (
  sale: Sale,
  settings: BusinessSettings,
  outlet: Outlet,
  employee: Employee,
): Promise<boolean> => {
  try {
    /**
     * Check if printer is connected
     */

    let _conn_printer = await BluetoothManager.getConnectedDeviceAddress();
    if (typeof _conn_printer !== 'string') {
      return false;
    }

    /**
     * Try to connect to the printer
     */

    let _printer = await getAsyncDevices();
    _printer && (await BluetoothManager.connect(_printer.address));

    //Top space--------------------
    await BluetoothEscposPrinter.printText('\n\r', {});
    //-----------------------------

    /**
     * BUSINESS NAME | LOGO
     */
    await BluetoothEscposPrinter.printerAlign(
      BluetoothEscposPrinter.ALIGN.CENTER,
    );
    await BluetoothEscposPrinter.printText(
      `${settings?.tradeName?.toUpperCase()}\n\r`,
      {
        encoding: 'GBK',
        codepage: 0,
        widthtimes: 3,
        heigthtimes: 3,
        fonttype: 1,
      },
    );

    //space---------------------------------
    await BluetoothEscposPrinter.printText('\n\r', {});
    //-------------------------------------

    /**
     * BUSINESS ADDRESS
     */
    await BluetoothEscposPrinter.printerAlign(
      BluetoothEscposPrinter.ALIGN.CENTER,
    );
    await BluetoothEscposPrinter.printText(`${settings?.street}\n\r`, {});

    /**
     * BUSINESS CONTACT
     */
    await BluetoothEscposPrinter.printerAlign(
      BluetoothEscposPrinter.ALIGN.CENTER,
    );
    await BluetoothEscposPrinter.printText(`${settings?.contact}\n\r`, {});

    /**
     * BUSINESS TIN
     */

    if (settings.showVatOrTin === true) {
      await BluetoothEscposPrinter.printerAlign(
        BluetoothEscposPrinter.ALIGN.CENTER,
      );
      await BluetoothEscposPrinter.printText(
        `TIN:${settings.tinNumber}\n\r`,
        {},
      );
    }

    /**
     * BUSINESS VAT
     */

    if (settings.showVatOrTin) {
      settings.showVatOrTin === true &&
        (await BluetoothEscposPrinter.printerAlign(
          BluetoothEscposPrinter.ALIGN.CENTER,
        ));
      await BluetoothEscposPrinter.printText(
        `VAT:${settings?.vatNumber}\n\r`,
        {},
      );

      //space ---------------
      await BluetoothEscposPrinter.printText('\n\r', {});
      //---------------------
    }

    /**
     * RECEIPT TEXT
     */
    await BluetoothEscposPrinter.printerAlign(
      BluetoothEscposPrinter.ALIGN.CENTER,
    );
    await BluetoothEscposPrinter.printText(`Cash Receipt\n\r`, {
      encoding: 'GBK',
      codepage: 0,
      widthtimes: 2,
      heigthtimes: 2,
      fonttype: 1,
    });

    //space---------------------------------
    await BluetoothEscposPrinter.printText('\n\r', {});
    await BluetoothEscposPrinter.printText('\n\r', {});
    //-------------------------------------

    /**DATE */

    let _date = sale.createdAt && new Date(sale.createdAt);
    await BluetoothEscposPrinter.printerAlign(
      BluetoothEscposPrinter.ALIGN.LEFT,
    );
    await BluetoothEscposPrinter.printText(
      `${_date && _date.toLocaleString()}\n\r`,
      {},
    );

    /**OUTLET */
    await BluetoothEscposPrinter.printerAlign(
      BluetoothEscposPrinter.ALIGN.LEFT,
    );
    await BluetoothEscposPrinter.printText(`Outlet: ${outlet.name}\n\r`, {});

    /**
     * SERVED BY
     */
    await BluetoothEscposPrinter.printerAlign(
      BluetoothEscposPrinter.ALIGN.LEFT,
    );
    await BluetoothEscposPrinter.printText(
      `Cashier:${employee.first_name}\n\r`,
      {},
    );
    /**
     * ORDER NUMBER
     */
    await BluetoothEscposPrinter.printerAlign(
      BluetoothEscposPrinter.ALIGN.LEFT,
    );
    await BluetoothEscposPrinter.printText(
      `Order #:${sale?.orderNumber}\n\r`,
      {},
    );

    //space---------------------------------
    await BluetoothEscposPrinter.printText('\n\r', {});
    //-------------------------------------

    /**
     * Table Head
     */
    let columnWidths = settings.showTotalUnitPrice ? [4, 12, 8, 8] : [4, 20, 8];
    let row = settings.showTotalUnitPrice
      ? ['Qty', 'Item', 'Price', 'Amount']
      : ['Qty', 'Item', 'Amount'];
    let col_config = settings.showTotalUnitPrice
      ? [
          BluetoothEscposPrinter.ALIGN.LEFT,
          BluetoothEscposPrinter.ALIGN.LEFT,
          BluetoothEscposPrinter.ALIGN.LEFT,
          BluetoothEscposPrinter.ALIGN.LEFT,
        ]
      : [
          BluetoothEscposPrinter.ALIGN.LEFT,
          BluetoothEscposPrinter.ALIGN.LEFT,
          BluetoothEscposPrinter.ALIGN.LEFT,
        ];

    await BluetoothEscposPrinter.printColumn(
      columnWidths,
      [...col_config],
      [...row],
      {},
    );

    sale.menuItems.forEach(item => {
      let _item = JSON.parse(item);
      let _row = settings.showTotalUnitPrice
        ? [`${_item.qty}`, `${_item.name}`, `${_item.price}`, `${_item.total}`]
        : [`${_item.qty}`, `${_item.name}`, `${_item.total}`];
      BluetoothEscposPrinter.printColumn(
        columnWidths,
        [...col_config],
        [..._row],
        {},
      )
        .then(res => console.log(res))
        .catch(err => console.log(err));
    });

    if (settings.showDiscounts) {
      //space---------------------------------
      await BluetoothEscposPrinter.printText('\n\r', {});
      //-------------------------------------

      /**PREDISCOUNT */
      await BluetoothEscposPrinter.printColumn(
        [16, 16],
        [BluetoothEscposPrinter.ALIGN.LEFT, BluetoothEscposPrinter.ALIGN.RIGHT],
        [`Pre-discount`, ``],
        {},
      );

      /**DISCOUNT TOTAL */
      await BluetoothEscposPrinter.printColumn(
        [16, 16],
        [BluetoothEscposPrinter.ALIGN.LEFT, BluetoothEscposPrinter.ALIGN.RIGHT],
        [`Discount Total`, ``],
        {},
      );
    }

    //space---------------------------------
    await BluetoothEscposPrinter.printText('\n\r', {});
    //-------------------------------------

    /**SUB TOTAL */

    // await BluetoothEscposPrinter.printerAlign(
    //   BluetoothEscposPrinter.ALIGN.LEFT,
    // );
    // await BluetoothEscposPrinter.printText(`Sub Total\n\r`, {});

    /**TAX */
    // await BluetoothEscposPrinter.printerAlign(
    //   BluetoothEscposPrinter.ALIGN.LEFT,
    // );
    // await BluetoothEscposPrinter.printText(`Tax\n\r`, {});

    /**TOTAL */
    await BluetoothEscposPrinter.printColumn(
      [16, 16],
      [BluetoothEscposPrinter.ALIGN.LEFT, BluetoothEscposPrinter.ALIGN.RIGHT],
      [`Total`, `${sale.total}`],
      {},
    );

    /**RECEIVED */
    await BluetoothEscposPrinter.printColumn(
      [16, 16],
      [BluetoothEscposPrinter.ALIGN.LEFT, BluetoothEscposPrinter.ALIGN.RIGHT],
      [`Cash`, `${sale.amountRecieved}`],
      {},
    );
    /**CHANGE */
    await BluetoothEscposPrinter.printColumn(
      [16, 16],
      [BluetoothEscposPrinter.ALIGN.LEFT, BluetoothEscposPrinter.ALIGN.RIGHT],
      [`Change`, `${sale.change}`],
      {},
    );

    //space---------------------------------
    await BluetoothEscposPrinter.printText('\n\r', {});
    //-------------------------------------

    /**
     * MESSAGING
     */
    if (settings.showReceiptMessaging) {
      await BluetoothEscposPrinter.printerAlign(
        BluetoothEscposPrinter.ALIGN.CENTER,
      );
      await BluetoothEscposPrinter.printText(
        `${settings.recieptMessaging}\n\r`,
        {
          encoding: 'GBK',
          codepage: 0,
          widthtimes: 0.8,
          heigthtimes: 0.8,
          fonttype: 1,
        },
      );
    }

    await BluetoothEscposPrinter.printerAlign(
      BluetoothEscposPrinter.ALIGN.CENTER,
    );

    await BluetoothEscposPrinter.printText(
      `Powered by: www.goldpointofsale.com\n\r`,
      {
        encoding: 'GBK',
        codepage: 0,
        widthtimes: 0.5,
        heigthtimes: 0.5,
        fonttype: 1,
      },
    );

    //space---------------------------------
    await BluetoothEscposPrinter.printText('\n\r', {});
    await BluetoothEscposPrinter.printText('\n\r', {});
    //--------------------------------------

    return true;
  } catch (error) {
    console.log(error);
    return false;
  }
};
