import jwt from 'jsonwebtoken'

const HASH_KEY = "RockyDIDIT"

export const decodeWebToken = async (token: string | any) => {
    return await jwt.verify(token, HASH_KEY);
    
};
