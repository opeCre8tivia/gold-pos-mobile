import {BluetoothManager} from '@brooons/react-native-bluetooth-escpos-printer';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {BLUETOOTH_DEVICE_KEY} from '../constants/asyncStorage';

/**
 * The method connects a paired device; If device is not paired the paired dialog pops up
 * It then stores connected device info in async storage
 */
export const connectToBluetoothDevice = async (
  address: string,
  name: string,
) => {
  try {
  } catch (error) {
    console.log(error);
  }
};
