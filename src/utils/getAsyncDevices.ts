import AsyncStorage from '@react-native-async-storage/async-storage';
import {BLUETOOTH_DEVICE_KEY} from '../constants/asyncStorage';

interface Device {
  name?: string;
  address: string;
}

export const getAsyncDevices = async (): Promise<Device | null> => {
  try {
    let _asyncDevice = await AsyncStorage.getItem(BLUETOOTH_DEVICE_KEY);

    if (typeof _asyncDevice === 'string') {
      let _device: Device = JSON.parse(_asyncDevice);
      return _device;
    } else {
      return null;
    }
  } catch (error) {
    console.log(error);
    return null;
  }
};
