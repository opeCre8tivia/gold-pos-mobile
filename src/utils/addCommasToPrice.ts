export function addCommasToPrice(num: number) {
  let str = num.toString(); // Convert the number to a string
  let decimalIndex = str.indexOf('.'); // Find the index of the decimal point
  let integerPart = decimalIndex === -1 ? str : str.slice(0, decimalIndex); // Extract the integer part
  let fractionPart = decimalIndex === -1 ? '' : str.slice(decimalIndex); // Extract the fractional part, if any

  let result = '';
  let i = integerPart.length - 1; // Start from the right end of the integer part
  let count = 0;
  while (i >= 0) {
    result = integerPart[i] + result; // Append the current digit to the result
    count++;
    if (count === 3 && i !== 0) {
      // Add a comma after every 3 digits, except at the beginning
      result = ',' + result;
      count = 0;
    }
    i--;
  }

  return result + fractionPart; // Append the fractional part, if any
}
