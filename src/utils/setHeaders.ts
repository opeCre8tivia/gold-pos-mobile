import AsyncStorage from '@react-native-async-storage/async-storage';

export const setHeaders = async (): Promise<any> => {
  const token = await AsyncStorage.getItem('outlet_token');
  if (token) {
    return {
      'Content-Type': 'application/json',
      Authorization: JSON.parse(token),
    };
  } else {
    return {};
  }
};
