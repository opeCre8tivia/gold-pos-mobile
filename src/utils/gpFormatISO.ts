interface Format {
  datetime?: string;
  yymmdd: string;
  hrmmss: string;
  fullDateTime: string;
}

export const gpFormatISO = (isoString: string): Format => {
  /**
   * Remove the last 4 digits in an iso string
   */
  let format_one = isoString.split('.')[0];

  /**
   *  sepatate Time and dates
   */

  let format_two = format_one.split('T');
  let _yymmdd = format_two[0];
  let _hrmmss = format_two[1];

  return {
    yymmdd: _yymmdd,
    hrmmss: _hrmmss,
    fullDateTime: `${_yymmdd} ${_hrmmss}`,
  };
};
