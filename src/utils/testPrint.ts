import {
  BluetoothEscposPrinter,
  BluetoothTscPrinter,
} from '@brooons/react-native-bluetooth-escpos-printer';

export const testPrint = async () => {
  const business = {
    name: 'GOLD POS',
    logoUri: '',
    location: 'Mirembe Arcade L14 Nassar Lane Kampala Uganda',
    mobile: '0782604704',
    tin: '3450045',
    vat: '34005646',
    date: '3/4/2023 3:00 pm',
  };

  const settings = {
    showLogo: false,
    hideHeader: false,
  };

  const sale = {
    servedBy: 1,
    employee: {first_name: 'Dawin Jude'},
    outletId: 1,
    outlet: {name: 'Main OutLet'},
    amountRecieved: 1,
    denominations: [1, 3],
    change: 10,
    total: 10000,
    orderNumber: '11023',
    menuItems: [
      {item: 'Burger', qty: 2, price: 35000, amount: 70000},
      {item: 'Pan Fried Beef', qty: 1, price: 22000, amount: 22000},
      {
        item: 'Streetwise 3pc or Upsize Reg to Large Chips Add 350ml soda to SW',
        qty: 2,
        price: 35000,
        amount: 70000,
      },
    ],
    type: 'Cash',
    messaging: 'Thank you for choosing Gold Point of Sale !',
  };

  try {
    let options = {
      width: 40,
      height: 30,
      gap: 20,
      direction: BluetoothTscPrinter.DIRECTION.FORWARD,
      reference: [0, 0],
      tear: BluetoothTscPrinter.TEAR.ON,
      sound: 0,
      text: [
        {
          text: 'I am a testing txt',
          x: 20,
          y: 0,
          fonttype: BluetoothTscPrinter.FONTTYPE.SIMPLIFIED_CHINESE,
          rotation: BluetoothTscPrinter.ROTATION.ROTATION_0,
          xscal: BluetoothTscPrinter.FONTMUL.MUL_1,
          yscal: BluetoothTscPrinter.FONTMUL.MUL_1,
        },
        {
          text: '你在说什么呢?',
          x: 20,
          y: 50,
          fonttype: BluetoothTscPrinter.FONTTYPE.SIMPLIFIED_CHINESE,
          rotation: BluetoothTscPrinter.ROTATION.ROTATION_0,
          xscal: BluetoothTscPrinter.FONTMUL.MUL_1,
          yscal: BluetoothTscPrinter.FONTMUL.MUL_1,
        },
      ],
      qrcode: [
        {
          x: 20,
          y: 96,
          level: BluetoothTscPrinter.EEC.LEVEL_L,
          width: 3,
          rotation: BluetoothTscPrinter.ROTATION.ROTATION_0,
          code: 'show me the money',
        },
      ],
      barcode: [
        {
          x: 120,
          y: 96,
          type: BluetoothTscPrinter.BARCODETYPE.CODE128,
          height: 40,
          readable: 1,
          rotation: BluetoothTscPrinter.ROTATION.ROTATION_0,
          code: '1234567890',
        },
      ],
    };

    //Top space--------------------
    await BluetoothEscposPrinter.printText('\n\r', {});
    //-----------------------------

    /**
     * BUSINESS NAME | LOGO
     */
    await BluetoothEscposPrinter.printerAlign(
      BluetoothEscposPrinter.ALIGN.CENTER,
    );
    await BluetoothEscposPrinter.printText(
      `${business.name} TEST RECEIPT\n\r`,
      {
        encoding: 'GBK',
        codepage: 0,
        widthtimes: 3,
        heigthtimes: 3,
        fonttype: 1,
      },
    );

    //space---------------------------------
    await BluetoothEscposPrinter.printText('\n\r', {});
    //-------------------------------------

    /**
     * BUSINESS lOCATION
     */
    await BluetoothEscposPrinter.printerAlign(
      BluetoothEscposPrinter.ALIGN.CENTER,
    );
    await BluetoothEscposPrinter.printText(`${business.location}\n\r`, {});

    /**
     * BUSINESS TIN
     */
    await BluetoothEscposPrinter.printerAlign(
      BluetoothEscposPrinter.ALIGN.CENTER,
    );
    await BluetoothEscposPrinter.printText(`TIN:${business.tin}\n\r`, {});

    /**
     * BUSINESS VAT
     */
    await BluetoothEscposPrinter.printerAlign(
      BluetoothEscposPrinter.ALIGN.CENTER,
    );
    await BluetoothEscposPrinter.printText(`VAT:${business.vat}\n\r`, {});

    //space ---------------
    await BluetoothEscposPrinter.printText('\n\r', {});
    //---------------------

    /**
     * RECEIPT TEXT
     */
    await BluetoothEscposPrinter.printerAlign(
      BluetoothEscposPrinter.ALIGN.CENTER,
    );
    await BluetoothEscposPrinter.printText(`Cash Receipt\n\r`, {
      encoding: 'GBK',
      codepage: 0,
      widthtimes: 2,
      heigthtimes: 2,
      fonttype: 1,
    });

    //space---------------------------------
    await BluetoothEscposPrinter.printText('\n\r', {});
    await BluetoothEscposPrinter.printText('\n\r', {});
    //-------------------------------------

    /**DATE and OUTLET */
    await BluetoothEscposPrinter.printColumn(
      [16, 16],
      [BluetoothEscposPrinter.ALIGN.LEFT, BluetoothEscposPrinter.ALIGN.RIGHT],
      [`17/02/2023`, `${sale.outlet.name}`],
      {},
    );

    /**
     * SERVED BY
     */
    await BluetoothEscposPrinter.printerAlign(
      BluetoothEscposPrinter.ALIGN.LEFT,
    );
    await BluetoothEscposPrinter.printText(
      `${sale.employee.first_name}\n\r`,
      {},
    );

    //space---------------------------------
    await BluetoothEscposPrinter.printText('\n\r', {});
    //-------------------------------------

    /**
     * Table Head
     */
    let columnWidths = settings.hideHeader ? [4, 12, 8] : [4, 12, 8, 8];
    let row = settings.hideHeader
      ? ['Qty', 'Item', 'Amount']
      : ['Qty', 'Item', 'Price', 'Amount'];
    let col_config = settings.hideHeader
      ? [
          BluetoothEscposPrinter.ALIGN.LEFT,
          BluetoothEscposPrinter.ALIGN.CENTER,
          BluetoothEscposPrinter.ALIGN.RIGHT,
        ]
      : [
          BluetoothEscposPrinter.ALIGN.LEFT,
          BluetoothEscposPrinter.ALIGN.CENTER,
          BluetoothEscposPrinter.ALIGN.CENTER,
          BluetoothEscposPrinter.ALIGN.RIGHT,
        ];

    let val = settings.hideHeader
      ? ['2', 'Banana', '500']
      : ['2', 'Banana', '500', '1000'];

    await BluetoothEscposPrinter.printColumn(
      columnWidths,
      [...col_config],
      [...row],
      {},
    );

    sale.menuItems.forEach(item => {
      let _row = settings.hideHeader
        ? [`${item.qty}`, `${item.item}`, `${item.amount}`]
        : [`${item.qty}`, `${item.item}`, `${item.price}`, `${item.amount}`];
      BluetoothEscposPrinter.printColumn(
        columnWidths,
        [...col_config],
        [..._row],
        {},
      )
        .then(res => console.log(res))
        .catch(err => console.log(err));
    });

    //space---------------------------------
    await BluetoothEscposPrinter.printText('\n\r', {});
    //-------------------------------------

    /**PREDISCOUNT */
    await BluetoothEscposPrinter.printColumn(
      [16, 16],
      [BluetoothEscposPrinter.ALIGN.LEFT, BluetoothEscposPrinter.ALIGN.RIGHT],
      [`Pre-discount`, `200,000`],
      {},
    );

    /**DISCOUNT TOTAL */
    await BluetoothEscposPrinter.printColumn(
      [16, 16],
      [BluetoothEscposPrinter.ALIGN.LEFT, BluetoothEscposPrinter.ALIGN.RIGHT],
      [`Discount Total`, `12,000`],
      {},
    );

    //space---------------------------------
    await BluetoothEscposPrinter.printText('\n\r', {});
    //-------------------------------------

    /**SUB TOTAL */

    await BluetoothEscposPrinter.printerAlign(
      BluetoothEscposPrinter.ALIGN.LEFT,
    );
    await BluetoothEscposPrinter.printText(`Sub Total\n\r`, {});

    /**TAX */
    await BluetoothEscposPrinter.printerAlign(
      BluetoothEscposPrinter.ALIGN.LEFT,
    );
    await BluetoothEscposPrinter.printText(`Tax\n\r`, {});

    /**TOTAL */
    await BluetoothEscposPrinter.printColumn(
      [16, 16],
      [BluetoothEscposPrinter.ALIGN.LEFT, BluetoothEscposPrinter.ALIGN.RIGHT],
      [`Total`, `${sale.total}`],
      {},
    );

    /**PAYMENT TYPE */
    await BluetoothEscposPrinter.printColumn(
      [16, 16],
      [BluetoothEscposPrinter.ALIGN.LEFT, BluetoothEscposPrinter.ALIGN.RIGHT],
      [`Payment Type`, `${sale.type}`],
      {},
    );

    //space---------------------------------
    await BluetoothEscposPrinter.printText('\n\r', {});
    //-------------------------------------

    /**
     * MESSAGING
     */
    await BluetoothEscposPrinter.printerAlign(
      BluetoothEscposPrinter.ALIGN.CENTER,
    );
    await BluetoothEscposPrinter.printText(`${sale.messaging}\n\r`, {
      encoding: 'GBK',
      codepage: 0,
      widthtimes: 0.5,
      heigthtimes: 0.5,
      fonttype: 1,
    });

    //space---------------------------------
    await BluetoothEscposPrinter.printText('\n\r', {});
    await BluetoothEscposPrinter.printText('\n\r', {});
    //--------------------------------------
  } catch (error) {
    console.log(error);
  }
};
