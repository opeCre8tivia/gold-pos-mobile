const timeFormater = (time: string) => {
  if (!time) return;

  let _splitResult = time.split('T');
  let yearToDate = _splitResult[0];
  let hourToSeconds = _splitResult[1];

  //split it to get year,month,and date
  let _yearToDateSplitResult = yearToDate.split('-');

  let year = _yearToDateSplitResult[0];
  let month = _yearToDateSplitResult[1];
  let date = _yearToDateSplitResult[2];

  //split to get hour ,minutes and seconds

  let _hourToSecondsSplitResult = hourToSeconds.split(':');
  let hour = _hourToSecondsSplitResult[0];
  let minutes = _hourToSecondsSplitResult[1];
  let seconds = _hourToSecondsSplitResult[2].split('.')[0];
  let full = `${year}/${month}/${date} ${hour}:${minutes}`;

  return {
    year,
    month,
    date,
    hour,
    minutes,
    seconds,
    full,
  };
};

export default timeFormater;
