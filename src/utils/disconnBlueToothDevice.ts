import {BluetoothManager} from '@brooons/react-native-bluetooth-escpos-printer';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {BLUETOOTH_DEVICE_KEY} from '../constants/asyncStorage';

/**
 * The method connects a paired device; If device is not paired the paired dialog pops up
 * but also Disconnects if device is already conected
 */
export const disconnBluetoothDevice = async (device: {
  address: string;
  name: string;
}) => {
  try {
    let _connectedDevice: any =
      await BluetoothManager.getConnectedDeviceAddress();
    console.log(_connectedDevice, 'connected device');
    if (_connectedDevice === device.address) {
      let _isDisConn: any = await BluetoothManager.connect(device.address);
      console.log(_isDisConn);
      //remove from async storage
      let asyncDevices = await AsyncStorage.removeItem(BLUETOOTH_DEVICE_KEY);
      console.log(asyncDevices);
      //TODO: change to filter when dealing with multitple devices
      //  let _devices = asyncDevices && JSON.parse(asyncDevices)
      //filter out

      //store the remaining array in async
    }
  } catch (error) {
    console.log(error);
  }
};
