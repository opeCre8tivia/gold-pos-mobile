/*
 * Colors
 * Dimentions
 * and other settings
 */

import {Dimensions} from 'react-native';

//colors

export const colors = {
  task_bar_blue: '#2A333E',
  orange: '#E46036',
  blue: '#0069A5',
  blue_light: '#8CAEF2',
  bg_light: '#222428',
  bg_dark: '#17181C',
  deep_blue: '#050708',
  btn_blue: '#232A3B',
  btn_gray: '#303137',
  white: '#ffffff',
  black: '#000000',
  gp_green: '#279870',
  gp_pink: '#F6A192',
  warning: '#f48523',
};

//dimentions
export const dimentions = {
  vh: Dimensions.get('window').height,
  vw: Dimensions.get('window').width,

  /**
   * the total height is devided into 20 rows
   * each row is 1/20 of the total height
   */
  row: Dimensions.get('window').height / 20,
  /**
   * the total width is devided into 20 columns (cols)
   * each col is 1/20 of the total width
   */
  col: Dimensions.get('window').width / 20,
};

export const size = {
  sm: (Dimensions.get('window').height / 100) * 1.5,
  md: (Dimensions.get('window').height / 100) * 2,
  lg: (Dimensions.get('window').height / 100) * 3,
};
