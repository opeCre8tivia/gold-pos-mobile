/*
 * all screen names are exported from here
 *
 */

export const ENTER_PIN_SCREEN: string = 'ENTER_PIN_SCREEN';

//drawer host
export const REGISTER_DRAWER_HOST: string = 'REGISTER_DRAWER_HOST';

export const HOME_SCREEN: string = 'HOME_SCREEN';
export const REGISTER_SCREEN: string = 'REGISTER_SCREEN';
export const REGISTER_SEARCH_SCREEN: string = 'REGISTER_SEARCH_SCREEN';
export const HOME_REGISTER_SCREEN: string = 'HOME_REGISTER_SCREEN';

export const TABLES_STACK_SCREEN: string = 'TABLES_STACK_SCREEN';
export const ORDERS_STACK_SCREEN: string = 'ORDERS_STACK_SCREEN';
export const CUSTOMERS_STACK_SCREEN: string = 'CUSTOMERS_STACK_SCREEN';
export const CUSTOMERS_LIST_SCREEN: string = 'CUSTOMERS_LIST_SCREEN';
export const CUSTOMERS_DETAILS_SCREEN: string = 'CUSTOMERS_DETAILS_SCREEN';

// reciept stack screens
export const RECEIPTS_STACK_SCREEN: string = 'RECEIPTS_STACK_SCREEN';
export const RECEIPT_SCREEN: string = 'RECEIPT_SCREEN';

export const SETTINGS_STACK_SCREEN: string = 'SETTINGS_STACK_SCREEN';

export const CUSTOM_AMOUNT_SCREEN: string = 'CUSTOM_AMOUNT_SCREEN';
export const TAKING_PAYMENT_SCREEN: string = 'TAKING_PAYMENT_SCREEN';
export const CLOCKING_HOME_SCREEN: string = 'CLOCKING_HOME_SCREEN';
export const LANDING_SCREEN: string = 'LANDING_SCREEN';
export const CLOCK_IN_SCREEN: string = 'CLOCK_IN_SCREEN';
export const CLOCKED_IN_HOME_SCREEN: string = 'CLOCKED_IN_HOME_SCREEN';

//Settings
export const CONTROL_CENTER: string = 'CONTROL_CENTER';
export const PRINTERS: string = 'PRINTERS';
export const PAYMENTS: string = 'PAYMENTS';
export const HARDWARE: string = 'HARDWARE';
export const REPORTS: string = 'REPORTS';
export const DEVICE_CONFIGURATION: string = 'DEVICE_CONFIGURATION';
export const CREATE_PRINTER: string = 'CREATE_PRINTER';

//float counting

export const FLOAT_COUNTING_SCREEN: string = 'FLOAT_COUNTING_SCREEN';
export const CASHDRAWER_LIST_SCREEN: string = 'CASHDRAWER_LIST_SCREEN';

export const CASHDRAWER_OPS_LIST_SCREEN: string = 'CASHDRAWER_OPS_LIST_SCREEN';
export const CASHDRAWER_OPS_DETAILS_SCREEN: string =
  'CASHDRAWER_OPS_DETAILS_SCREEN';
export const SHIFTREVIEW_SCREEN: string = 'SHIFTREVIEW_SCREEN';
export const CLOSE_CASHDRAWER_SCREEN: string = 'CLOSE_CASHDRAWER_SCREEN';
