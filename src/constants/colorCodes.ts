export const category_colors = [
    {hex:"#B28E6A"},
    {hex:"#858B87"},
    {hex:"#003BD9"},
    {hex:"#2A333E"},
    {hex:"#279870"},
    {hex:"#AD0520"},
    {hex:"#B28E6A"},
    {hex:"#B28E6A"},
    {hex:"#CF943E"},
    {hex:"#CA825C"},
    {hex:"#6698CA"},
    {hex:"#B28E6A"},
    {hex:"#858B87"},
    {hex:"#003BD9"},
    {hex:"#2A333E"},
    {hex:"#279870"},
    {hex:"#AD0520"},
    {hex:"#B28E6A"},
    {hex:"#B28E6A"},
    {hex:"#CF943E"},
    {hex:"#CA825C"},
    {hex:"#6698CA"},
    {hex:"#B28E6A"},
    {hex:"#858B87"},
    {hex:"#003BD9"},
    {hex:"#2A333E"},
    {hex:"#279870"},
    {hex:"#AD0520"},
    {hex:"#B28E6A"},
    {hex:"#B28E6A"},
    {hex:"#CF943E"},
    {hex:"#CA825C"},
    {hex:"#6698CA"},
    {hex:"#B28E6A"},
    {hex:"#858B87"},
    {hex:"#003BD9"},
    {hex:"#2A333E"},
    {hex:"#279870"},
    {hex:"#AD0520"},
    {hex:"#B28E6A"},
    {hex:"#B28E6A"},
    {hex:"#CF943E"},
    {hex:"#CA825C"},
    {hex:"#6698CA"},
    {hex:"#B28E6A"},
    {hex:"#858B87"},
    {hex:"#003BD9"},
    {hex:"#2A333E"},
    {hex:"#279870"},
    {hex:"#AD0520"},
    {hex:"#B28E6A"},
    {hex:"#B28E6A"},
    {hex:"#CF943E"},
    {hex:"#CA825C"},
    {hex:"#6698CA"},
    {hex:"#B28E6A"},
    {hex:"#858B87"},
    {hex:"#003BD9"},
    {hex:"#2A333E"},
    {hex:"#279870"},
    {hex:"#AD0520"},
    {hex:"#B28E6A"},
    {hex:"#B28E6A"},
    {hex:"#CF943E"},
    {hex:"#CA825C"},
    {hex:"#6698CA"},
]


export const item_colors = [
    {hex:"#B28E6A"},
    {hex:"#858B87"},
    {hex:"#003BD9"},
    {hex:"#2A333E"},
    {hex:"#279870"},
    {hex:"#AD0520"},
    {hex:"#B28E6A"},
    {hex:"#B28E6A"},
    {hex:"#CF943E"},
    {hex:"#CA825C"},
    {hex:"#6698CA"},
    {hex:"#B28E6A"},
    {hex:"#858B87"},
    {hex:"#003BD9"},
    {hex:"#2A333E"},
    {hex:"#279870"},
    {hex:"#AD0520"},
    {hex:"#B28E6A"},
    {hex:"#B28E6A"},
    {hex:"#CF943E"},
    {hex:"#CA825C"},
    {hex:"#6698CA"},
    {hex:"#B28E6A"},
    {hex:"#858B87"},
    {hex:"#003BD9"},
    {hex:"#2A333E"},
    {hex:"#279870"},
    {hex:"#AD0520"},
    {hex:"#B28E6A"},
    {hex:"#B28E6A"},
    {hex:"#CF943E"},
    {hex:"#CA825C"},
    {hex:"#6698CA"},
    {hex:"#B28E6A"},
    {hex:"#858B87"},
    {hex:"#003BD9"},
    {hex:"#2A333E"},
    {hex:"#279870"},
    {hex:"#AD0520"},
    {hex:"#B28E6A"},
    {hex:"#B28E6A"},
    {hex:"#CF943E"},
    {hex:"#CA825C"},
    {hex:"#6698CA"},
    
]