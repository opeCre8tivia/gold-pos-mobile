import ControlCenterScreen from '../screens/ControlCenterScreen';
import PrintersScreen from '../screens/PrintersScreen';
import CreatePrinterScreen from '../screens/CreatePrinterScreen';
import BinIcon from '../assets/svgIcons/bin.icon';
import PrinterIcon from '../assets/svgIcons/printer.icon';

interface MenuItem {
  id: number;
  title: string;
  icon: string;
  component?: string;
}

export const settingsMenu: MenuItem[] = [
  {
    id: 1,
    title: 'Control Panel',
    icon: 'home',
    component: JSON.stringify(ControlCenterScreen),
  },
  {
    id: 2,
    title: 'Cash Drawers',
    icon: 'drawer',
    // component: JSON.stringify(PrintersScreen),
  },
  {
    id: 3,
    title: 'Printers',
    icon: 'printer',
    component: JSON.stringify(PrintersScreen),
  },
  {
    id: 4,
    title: 'Reports',
    icon: 'report',
    component: '#',
  },
  {
    id: 5,
    title: 'Payments',
    icon: 'payment',
    component: '#',
  },
  {
    id: 6,
    title: 'Hardware',
    icon: 'hardware',
    component: '#',
  },
  {
    id: 7,
    title: 'Device Configuration',
    icon: 'settings',
    component: '#',
  },
];
