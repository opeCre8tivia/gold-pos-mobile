/**
 * Outlet
 */

export interface Outlets {
  id: number;
  description?: string;
  createdBy?: int;
  employees?: Employee[];
}
/**
 * Employee
 */

export interface Employee {
  id: number;
  first_name: string;
  last_name: string;
  role_id: number;
  role: string;
}

/**
 * contains both Business settings and receipt settings
 */
export interface BusinessSettings {
  /**
   * receipt settings
   */
  showAddressLineOne?: boolean;
  showAddressLineTwo?: boolean;
  showDiscounts?: boolean;
  showLogo?: boolean;
  showModifierItems?: boolean;
  showReceiptMessaging?: boolean;
  showTableHeader?: boolean;
  showTotalUnitPrice?: boolean;
  showVatOrTin?: boolean;
  receiptWidth?: boolean;
  receiptHeight?: boolean;

  /**
   * business settings
   */

  tradeName: string;
  street: string;
  postalCode: string;
  city: string;
  country: string;
  additionalStreet: string;
  vatNumber: string;
  tinNumber: string;
  legalEntityName: string;
  email: string;
  contact: string;
  logoUri: string;
  recieptMessaging: string;
}

/**
 * Sales Item
 */

export interface SalesItem {
  qty: number;
  price: number;
  name: string;
  total: number;
}

/**
 * normal sale
 */

export interface Sale {
  id?: number;
  menuItems: string[]; //JSON Array
  total: number;
  amountRecieved: number;
  change: number;
  denominations: any[];
  servedBy: number;
  employee?: Employee;
  orderNumber: number;
  createdAt?: string;
}

export interface ParkedOrder {
  id?: number;
  outletId: number;
  servedBy: number;
  items: string[]; //JSON Array
  total: number;
  isVoid?: boolean;
  isOpen?: boolean;
  orderNumber?: number;
  createdAt?: any;
  employee?: Employee;
  menuItems?: any[];
  customerId?: number;
  customer?: CustomerType;
}

/**
 * svg icon props
 */

export interface SvgIconProps {
  fill?: string;
  width?: number;
  height?: number;
}

/**
 * cash drawers
 */

export interface CashDrawer {
  id?: number;
  name: string;
  outletId: number;
  outlet?: any;
  operations: any[];
  startingBalance: number;
  isAutomaticBalanceReset: boolean;
}

export interface CashDrawerOperation {
  id?: number;
  isActive: boolean;
  cashDrawerId: number;
  cashDrawer?: CashDrawer;
  employeeId: number;
  employee?: any;
  startingBalance: number;
  currentBalance: number;
  openedAt?: any;
  closedAt?: any;
  cashins?: Cashin[];
  cashdrops?: Cashdrop[];
  payouts?: Payout[];
  sales?: Sale[];
}

export interface Cashin {
  id: number;
  reason: string;
  amount: number;
  cashdrawerOpsId: number;
}
export interface Payout {
  id: number;
  reason: string;
  amount: number;
  cashdrawerOpsId: number;
  payoutToId: number;
  payoutTo?: any;
}
export interface Cashdrop {
  id: number;
  amount: number;
  cashdrawerOpsId: number;
}

/**
 * denomination
 */

export interface Denomination {
  id: number;
  name: string;
  type: string;
  value: number;
  quantity: number;
}

/**
 * customer
 */

export interface CustomerType {
  id?: number;
  first_name: string;
  last_name: string;
  phone_number_primary: string;
  phone_number_secondary?: string;
  email?: string;
  tin?: string;
  city?: string;
  company_name?: string;
  zip_code?: string;
  address?: string;
  outletId?: number;
  outlet?: any;
  parkedSales?: any[];
  sales?: any[];
  createdAt?: any;
  updatedAt?: any;
}
