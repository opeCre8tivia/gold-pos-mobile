import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {useSelector} from 'react-redux';
import {RootState, useAppDispatch} from '../redux/store';
import {_listParkedOrders} from '../redux/actions/parking.actions';
import {colors, dimentions} from '../constants/theme';
import TaskBar from '../components/TaskBar';
import {FlatList} from 'react-native-gesture-handler';
import OrderListItem from '../components/OrderListItem';
import GPSearchInput from '../components/GPSearchInput';
import FilterIcon from '../assets/svgIcons/filter.icon';
import {_addSelectedOrder} from '../redux/features/order.slice';
import ReceiptActionButton from '../components/ReceiptActionButton';

import PenIcon from '../assets/svgIcons/pen.icon';
import BinIcon from '../assets/svgIcons/bin.icon';
import MoneyIcon from '../assets/svgIcons/money.icon';
import {useNavigation} from '@react-navigation/native';
import {TAKING_PAYMENT_SCREEN} from '../constants/screenNames';
import {unparkOrder} from '../redux/features/directSale.slice';
import {ParkedOrder} from '../types/main';

const Orders = () => {
  const {parkedOrders, parkError, selectedOrder, loading} = useSelector(
    (state: RootState) => state.orderSlice,
  );
  const {addedMenuItems, fromParkedSale} = useSelector(
    (state: RootState) => state.directSaleSlice,
  );

  const {allSettings} = useSelector((state: RootState) => state.settingsSlice);
  const {outlet} = useSelector((state: RootState) => state.outletSlice);

  const dispatch = useAppDispatch();
  const navigation = useNavigation<any>();
  const [showActive, setShowActive] = useState<boolean>(true);
  const [showActionModal, setShowActionModal] = useState<boolean>(false);
  const [localTimeOfSale, setLocalTimeOfSale] = useState<any>(null);
  const [orderList, setOrderList] = useState<ParkedOrder[]>([]);

  useEffect(() => {
    if (parkedOrders && showActive) {
      let openOrders = parkedOrders.filter(order => order.isOpen);
      setOrderList(openOrders);
    }

    if (parkedOrders && !showActive) {
      let closedOrders = parkedOrders.filter(order => !order.isOpen);
      setOrderList(closedOrders);
    }
  }, [parkedOrders, showActive]);

  /**
   * fetch all daily sales when component comes to view
   */

  useEffect(() => {
    const unSubscribe = navigation.addListener('focus', () => {
      dispatch(_listParkedOrders());
      setShowActionModal(false);
      setShowActive(true);
    });

    return unSubscribe;
  }, []);

  /**
   * Select the first order in the array by default
   */
  useEffect(() => {
    if (parkedOrders && !selectedOrder) {
      dispatch(_addSelectedOrder(parkedOrders[0]));
    }
  }, [parkedOrders]);

  /**
   * convert time of sale to Locale string
   */
  useEffect(() => {
    let _date = selectedOrder?.createdAt && new Date(selectedOrder?.createdAt);
    _date && setLocalTimeOfSale(_date.toLocaleString());
  }, [selectedOrder]);

  /**
   * TODO: test for hacks
   */

  useEffect(() => {
    if (addedMenuItems && fromParkedSale) {
      navigation.navigate(TAKING_PAYMENT_SCREEN);
    }
  }, [addedMenuItems]);

  const handlePrintInvoice = () => {};

  const Col = ({
    a,
    b,
    c,
    d,
    colSizes = {a: 1, b: 2, c: 1, d: 1},
  }: {
    a: string | number;
    b?: string | number;
    c?: string | number;
    d?: string | number;
    colSizes?: {a: number; b: number; c: number; d: number};
  }) => {
    return (
      <View style={styles.col}>
        {/* 0.5 | 2.5 | 1| 1 */}
        <Text
          style={{
            ...styles.normal_text,
            width: dimentions.col * colSizes?.a,
            textAlign: 'left',
          }}>
          {a}
        </Text>
        <Text
          style={{
            ...styles.normal_text,
            width: dimentions.col * colSizes.b,
            textAlign: 'left',
          }}>
          {b}
        </Text>
        {allSettings?.showTotalUnitPrice && (
          <Text
            style={{
              ...styles.normal_text,
              width: dimentions.col * colSizes.c,
              textAlign: 'left',
            }}>
            {c}
          </Text>
        )}
        <Text
          style={{
            ...styles.normal_text,
            width: dimentions.col * colSizes.d,
            textAlign: 'left',
          }}>
          {d}
        </Text>
      </View>
    );
  };

  const Space = ({size = 2}: {size?: number}) => (
    <View style={{width: '100%', height: dimentions.row * size}}></View>
  );

  return (
    <View style={{flex: 1, backgroundColor: colors.bg_dark}}>
      <TaskBar title="Orders" canMoveBack={false} />
      <View style={{flex: 1, flexDirection: 'row'}}>
        {/* left */}
        <View style={styles.left_container}>
          {/* buttons */}
          <View style={{padding: 4}}>
            {/* switch buttons */}
            <View
              style={{
                width: '100%',
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
                height: 50,
              }}>
              <TouchableOpacity
                onPress={() => setShowActive(true)}
                style={{
                  flex: 1,
                  height: 44,
                  backgroundColor: showActive ? colors.blue : colors.btn_blue,
                  borderRadius: 3,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text
                  style={{color: '#ffffff', fontSize: 14, fontWeight: '500'}}>
                  Open
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => setShowActive(false)}
                style={{
                  flex: 1,
                  height: 44,
                  backgroundColor: showActive ? colors.btn_blue : colors.blue,
                  borderRadius: 3,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text
                  style={{color: '#ffffff', fontSize: 14, fontWeight: '500'}}>
                  Closed
                </Text>
              </TouchableOpacity>
            </View>
          </View>
          {/* filter and search */}
          <View
            style={{
              padding: 4,
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              width: '100%',
              height: 60,
            }}>
            <GPSearchInput
              placeholder="Search by order number"
              // onChangeText={value => setOrderId(value)}
              onSearchIconClick={() => {
                // orderId && getByOrderId(parseInt(orderId));
              }}
              loading={true}
              input_styles={{width: dimentions.col * 7.7}}
            />
            <TouchableOpacity
              style={{
                width: dimentions.col * 2,
                height: dimentions.row * 1.3,
                backgroundColor: colors.btn_blue,
                borderRadius: 6,
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
                paddingHorizontal: 10,
              }}>
              <View style={{width: 25, height: 25, marginRight: 10}}>
                <FilterIcon fill="#CA825C" width={10} height={10} />
              </View>
              <Text style={{color: '#CA825C', fontWeight: '400', fontSize: 20}}>
                Filter
              </Text>
            </TouchableOpacity>
          </View>

          {/* list items */}
          <View style={{padding: 4}}>
            {orderList.length > 0 ? (
              <FlatList
                data={orderList}
                showsVerticalScrollIndicator={false}
                renderItem={({item, index}) => {
                  return <OrderListItem order={item} key={index} />;
                }}
              />
            ) : (
              <View
                style={{
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    fontSize: 25,
                    textAlign: 'center',
                    fontWeight: 'bold',
                    color: '#7a7a7a',
                  }}>
                  NO ORDERS FOUND
                </Text>
              </View>
            )}
          </View>
        </View>
        {/* right */}
        <View style={styles.right_container}>
          {/* RECEIPT */}
          <ScrollView style={{flex: 1}} showsVerticalScrollIndicator={false}>
            {parkedOrders.length > 0 && (
              <View
                style={{
                  backgroundColor: '#fff',
                  width: dimentions.col * 9,
                  height: '100%',
                  flex: 1,
                  padding: 10,
                }}>
                {/* header */}
                <View style={styles.receipt_biz_name}>
                  {allSettings?.tradeName && (
                    <Text
                      style={{
                        textAlign: 'center',
                        fontSize: 22,
                        color: colors.black,
                        fontWeight: 'bold',
                      }}>
                      {allSettings?.tradeName?.toUpperCase()}
                    </Text>
                  )}
                </View>

                <Text style={styles.normal_text}>{allSettings?.street}</Text>
                <Text style={styles.normal_text}>{allSettings?.contact}</Text>

                {allSettings?.tinNumber && (
                  <Text
                    style={
                      styles.normal_text
                    }>{`TIN:${allSettings?.tinNumber}`}</Text>
                )}
                {allSettings?.vatNumber && (
                  <Text
                    style={
                      styles.normal_text
                    }>{`VAT:${allSettings?.vatNumber}`}</Text>
                )}

                {/* ----space */}
                <Space size={0.4} />

                <View style={{margin: 10}}>
                  <Text
                    style={{
                      fontSize: 20,
                      color: colors.black,
                      textAlign: 'center',
                      fontWeight: 'bold',
                    }}>
                    INVOICE
                  </Text>
                </View>

                <Space size={0.4} />

                {/* time and outlet */}
                <Col
                  a={`${localTimeOfSale && localTimeOfSale}`}
                  b=""
                  c=""
                  d={`${outlet?.name}`}
                  colSizes={{a: 3, b: 0.5, c: 0.5, d: 1}}
                />
                {/* order number */}

                <Col
                  a={`Order #${selectedOrder?.orderNumber}`}
                  b=""
                  c=""
                  d=""
                  colSizes={{a: 3, b: 0.5, c: 0.5, d: 1}}
                />
                {/* Served by */}
                <Col
                  a={`Served By:${selectedOrder?.employee?.first_name}`}
                  b=""
                  c=""
                  d=""
                  colSizes={{a: 3, b: 0.5, c: 0.5, d: 1}}
                />

                {/* Table header */}
                {allSettings?.showTableHeader && (
                  <Col a="Qty" b="Item" c="Price" d="Amount" />
                )}

                {/* list of sales items */}
                {selectedOrder && (
                  <>
                    {selectedOrder &&
                      selectedOrder.menuItems &&
                      selectedOrder.menuItems.map((item, index: number) => {
                        let _item: any = JSON.parse(item);

                        return (
                          <Col
                            a={_item.qty}
                            b={_item.name}
                            c={_item.price}
                            d={_item.total}
                            key={index}
                          />
                        );
                      })}
                  </>
                )}

                <Space size={0.5} />

                {/* discounts */}

                {/* sub total */}
                <Col a="Sub total" b="" c="" d="" />

                {/* tax */}
                <Col a="Tax" b="" c="" d="" />

                {/* total */}
                <Col a="Total" b="" c="" d={selectedOrder?.total} />

                {/* Type of payment */}
                <Col
                  a="Cash"
                  b=""
                  c=""
                  d={40}
                  colSizes={{a: 3, b: 0.5, c: 0.5, d: 1}}
                />
                {/* Change */}
                <Col
                  a="Change"
                  b=""
                  c=""
                  d={40}
                  colSizes={{a: 3, b: 0.5, c: 0.5, d: 1}}
                />

                <Space size={0.5} />

                {/* Receipt messaging */}
                {allSettings?.showReceiptMessaging && (
                  <Text
                    style={{
                      textAlign: 'center',
                      fontSize: 10,
                      color: colors.black,
                    }}>
                    {' '}
                    {allSettings?.recieptMessaging}
                  </Text>
                )}

                <Space size={0.3} />

                {/* gold pos default */}
                <Text
                  style={{
                    textAlign: 'center',
                    fontSize: 8,
                    color: colors.black,
                  }}>
                  {' '}
                  Powered by: www.goldpointofsale.com
                </Text>

                {/* ----space--- */}
              </View>
            )}
          </ScrollView>
          {/* green menu icon */}
          <TouchableOpacity
            style={{
              width: 80,
              height: 80,
              backgroundColor: '#279870',
              position: 'absolute',
              bottom: 80,
              right: 15,
              justifyContent: 'center',
              alignItems: 'center',
            }}
            onPress={() =>
              selectedOrder && setShowActionModal(!showActionModal)
            }>
            <Image source={require('../assets/icons/menuIcon.png')} />
          </TouchableOpacity>

          {/* modal */}
          {showActionModal && (
            <View style={styles.modal}>
              {/* title */}
              <Text
                style={{
                  color: '#fff',
                  fontSize: 20,
                  textAlign: 'center',
                  fontWeight: 'bold',
                }}>
                {' '}
                Actions{' '}
              </Text>

              <View style={{}}>
                {/*  */}

                <View style={{flexDirection: 'row'}}>
                  <ReceiptActionButton
                    onPress={() => console.log('#')}
                    text="UPDATE"
                    Icon={PenIcon}
                  />
                  <ReceiptActionButton
                    onPress={() => {
                      selectedOrder && dispatch(unparkOrder(selectedOrder));
                    }}
                    text="PAY"
                    Icon={MoneyIcon}
                  />
                  <ReceiptActionButton
                    onPress={() => console.log('#')}
                    text="VOID RECEIPT"
                    Icon={BinIcon}
                  />
                </View>

                {/*  */}
                <View style={{flexDirection: 'row'}}>
                  <ReceiptActionButton
                    onPress={() => console.log('#')}
                    text="TRANSFER"
                    Icon={MoneyIcon}
                  />
                </View>
              </View>

              {/* close  */}

              <TouchableOpacity
                onPress={() => setShowActionModal(!showActionModal)}
                style={{
                  width: dimentions.col * 2,
                  height: dimentions.row * 2,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    color: '#fff',
                    fontSize: 20,
                    textAlign: 'center',
                    fontWeight: 'bold',
                  }}>
                  Close
                </Text>
              </TouchableOpacity>
            </View>
          )}
        </View>
      </View>
    </View>
  );
};

export default Orders;

const styles = StyleSheet.create({
  left_container: {
    width: dimentions.col * 10,
    height: dimentions.row * 18,
  },
  right_container: {
    width: dimentions.col * 10,
    height: dimentions.row * 18,
    padding: 4,
    position: 'relative',
    alignItems: 'center',
  },
  modal: {
    width: dimentions.col * 10,
    height: dimentions.row * 16.5,
    backgroundColor: 'rgba(0,0,0,0.8)',
    position: 'absolute',
    right: 0,
    top: 0,
    paddingVertical: 30,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  normal_text: {
    textAlign: 'center',
    fontSize: 13,
    color: colors.black,
  },
  col: {
    width: '100%',
    height: 30,
    paddingHorizontal: 8,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  receipt_biz_name: {
    width: '100%',
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
