import {SafeAreaView, StyleSheet} from 'react-native';
import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {
  CASHDRAWER_LIST_SCREEN,
  CUSTOMERS_STACK_SCREEN,
  FLOAT_COUNTING_SCREEN,
  ORDERS_STACK_SCREEN,
  RECEIPTS_STACK_SCREEN,
  REGISTER_DRAWER_HOST,
  REGISTER_SCREEN,
  SETTINGS_STACK_SCREEN,
  TABLES_STACK_SCREEN,
} from '../constants/screenNames';
import Tables from './Tables';
import GPBottomTab from '../components/GPBottomTab';
import Orders from './Orders';
import Customers from './Customers';
import Receipts from './Receipts';
import Settings from './Settings';
import Register from './Register';
import {colors, dimentions} from '../constants/theme';
import RegisterTabDrawerHost from '../navigation/RegisterTabDrawerHost';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import CashDrawerListScreen from './CashDrawerListScreen';
import FloatCountingScreen from './FloatCountingScreen';
import {useSelector} from 'react-redux';
import {RootState} from '../redux/store';

const HomeScreen = () => {
  const {employee_should_count} = useSelector(
    (state: RootState) => state.cashdrawersSlice,
  );

  const HomeTab = createBottomTabNavigator();

  const CashDrawerStack = createNativeStackNavigator();
  return (
    <SafeAreaView style={{flex: 1}}>
      {employee_should_count ? (
        <CashDrawerStack.Navigator
          initialRouteName={CASHDRAWER_LIST_SCREEN}
          screenOptions={{
            headerShown: false,
          }}>
          <CashDrawerStack.Screen
            name={CASHDRAWER_LIST_SCREEN}
            component={CashDrawerListScreen}
          />
          <CashDrawerStack.Screen
            name={FLOAT_COUNTING_SCREEN}
            component={FloatCountingScreen}
          />
        </CashDrawerStack.Navigator>
      ) : (
        <HomeTab.Navigator
          defaultScreenOptions={{
            headerShown: false,
          }}
          screenOptions={{
            headerShown: false,
            tabBarShowLabel: false,
            tabBarStyle: {
              backgroundColor: '#323B45',
              height: dimentions.row,
              borderWidth: 0,
              padding: 0,
            },
          }}>
          <HomeTab.Screen
            name={REGISTER_DRAWER_HOST}
            component={RegisterTabDrawerHost}
            options={{
              headerShown: false,
              tabBarActiveTintColor: 'white',
              tabBarActiveBackgroundColor: colors.orange,
              tabBarIcon: ({focused}) => (
                <GPBottomTab
                  focused={focused}
                  text="Register"
                  icon_path={require('../assets/icons/homeIcon.png')}
                />
              ),
            }}
          />

          <HomeTab.Screen
            name={TABLES_STACK_SCREEN}
            component={Tables}
            options={{
              headerShown: false,
              tabBarActiveTintColor: 'white',
              tabBarActiveBackgroundColor: colors.orange,
              tabBarIcon: ({focused, color, size}) => (
                <GPBottomTab
                  text="Tables"
                  icon_path={require('../assets/icons/tableIcon.png')}
                  focused={focused}
                />
              ),
            }}
          />

          <HomeTab.Screen
            name={ORDERS_STACK_SCREEN}
            component={Orders}
            options={{
              headerShown: false,
              tabBarActiveTintColor: 'white',
              tabBarActiveBackgroundColor: colors.orange,
              tabBarIcon: ({focused}) => (
                <GPBottomTab
                  focused={focused}
                  text="Orders"
                  icon_path={require('../assets/icons/bagIcon.png')}
                />
              ),
            }}
          />

          <HomeTab.Screen
            name={CUSTOMERS_STACK_SCREEN}
            component={Customers}
            options={{
              headerShown: false,
              tabBarActiveTintColor: 'white',
              tabBarActiveBackgroundColor: colors.orange,
              tabBarIcon: ({focused}) => (
                <GPBottomTab
                  focused={focused}
                  text="Customers"
                  icon_path={require('../assets/icons/userGroupIcon.png')}
                />
              ),
            }}
          />

          <HomeTab.Screen
            name={RECEIPTS_STACK_SCREEN}
            component={Receipts}
            options={{
              headerShown: false,
              tabBarActiveTintColor: 'white',
              tabBarActiveBackgroundColor: colors.orange,
              tabBarIcon: ({focused}) => (
                <GPBottomTab
                  focused={focused}
                  text="Receipts"
                  icon_path={require('../assets/icons/recieptgrayIcon.png')}
                />
              ),
            }}
          />

          <HomeTab.Screen
            name={SETTINGS_STACK_SCREEN}
            component={Settings}
            options={{
              headerShown: false,
              tabBarActiveTintColor: 'white',
              tabBarActiveBackgroundColor: colors.orange,
              tabBarIcon: ({focused}) => (
                <GPBottomTab
                  focused={focused}
                  text="Settings"
                  icon_path={require('../assets/icons/settingsIcon.png')}
                />
              ),
            }}
          />
        </HomeTab.Navigator>
      )}
    </SafeAreaView>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({});
