import {
  FlatList,
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
} from 'react-native';
import React, {useState} from 'react';
import TaskBar from '../components/TaskBar';
import {colors, dimentions, size} from '../constants/theme';
import GPSearchInput from '../components/GPSearchInput';
import ClockInButton from '../components/ClockInButton';
import {ENTER_PIN_SCREEN} from '../constants/screenNames';
import {useNavigation} from '@react-navigation/native';
import {useSelector} from 'react-redux';

const ClockInScreen = () => {
  const navigation = useNavigation<any>();
  const {outlet, business, employees} = useSelector(
    (state: any) => state.outletSlice,
  );

  return (
    <SafeAreaView style={styles.outer_wrapper}>
      {/* <ScrollView style={{flex:1}}> */}
      <TaskBar title="Clock in" canMoveBack={true} navigation={navigation} />

      <View style={styles.main}>
        <View style={styles.left_wrapper}>
          <Text
            style={{
              fontWeight: '700',
              fontSize: size.md + 3,
              color: '#ffffff',
              textAlign: 'center',
              marginTop: size.md,
              fontFamily: 'Lato-Thin',
            }}>
            Clock In
          </Text>

          <View style={{width: '80%', marginVertical: 20}}>
            <GPSearchInput placeholder="Enter Name" />
          </View>

          <Text
            style={{
              fontWeight: '500',
              fontSize: 16,
              color: '#ffffff',
              textAlign: 'center',
              marginVertical: 15,
            }}>
            Tap on your name to clock in
          </Text>

          <View style={styles.managers}>
            {employees && (
              <FlatList
                data={employees}
                showsVerticalScrollIndicator={false}
                renderItem={({item, index}) => {
                  return (
                    <>
                      {item.role_id && (
                        <ClockInButton
                          item={item}
                          key={index}
                          onPress={() =>
                            navigation.navigate(ENTER_PIN_SCREEN, {
                              employeeData: item,
                            })
                          }
                        />
                      )}
                    </>
                  );
                }}
              />
            )}
          </View>
        </View>

        <View style={styles.right_wrapper}>
          <Image
            source={require('../assets/img/clockman.png')}
            style={{
              width: dimentions.col * 8,
              height: dimentions.row * 8,
            }}
            resizeMode="contain"
          />
        </View>
      </View>
      {/* </ScrollView> */}
    </SafeAreaView>
  );
};

export default ClockInScreen;

const styles = StyleSheet.create({
  outer_wrapper: {
    width: dimentions.vw,
    height: dimentions.vh,
    maxWidth: dimentions.vw,
    maxHeight: dimentions.vh,
  },
  main: {
    flex: 1,
    backgroundColor: colors.bg_dark,
    flexDirection: 'row',
    justifyContent: 'center',
    height: dimentions.vh,
  },
  left_wrapper: {
    width: dimentions.col * 10,
    height: dimentions.row * 18,
    alignItems: 'center',
  },
  managers: {
    width: '80%',
    height: dimentions.vh / 4,
  },
  workers: {},
  right_wrapper: {
    width: dimentions.col * 10,
    height: dimentions.row * 18,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
