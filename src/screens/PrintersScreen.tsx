import {
  StyleSheet,
  Text,
  View,
  Pressable,
  DeviceEventEmitter,
  PermissionsAndroid,
  FlatList,
  Image,
} from 'react-native';
import React, {useState, useEffect} from 'react';
// import useBLE from '../hooks/useBLE';
import {
  BluetoothManager,
  BluetoothEscposPrinter,
  BluetoothTscPrinter,
} from '@brooons/react-native-bluetooth-escpos-printer';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {colors, dimentions} from '../constants/theme';
import PrinterStack from '../navigation/PrinterStack';
import {useNavigation} from '@react-navigation/native';
import {CREATE_PRINTER} from '../constants/screenNames';
import CreatePrinterScreen from './CreatePrinterScreen';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {BLUETOOTH_DEVICE_KEY} from '../constants/asyncStorage';
import {useDispatch, useSelector} from 'react-redux';
import {_addConnectedDeviceAddress} from '../redux/features/printer.slice';
import {RootState} from '../redux/store';
import PrinterDevice from '../components/PrinterDevice';

const PrintersScreen = () => {
  //   const {requestPermissions, scanForDevices, allDevices} = useBLE();
  const navigation = useNavigation<any>();
  const dispatch = useDispatch<any>();

  const {connected_device, connected_device_address} = useSelector(
    (state: RootState) => state.printerSlice,
  );

  const [showPrinterStack, setShowPrinterStack] = useState(false);

  const NoDevices = () => (
    <View style={styles.no_device_cont}>
      <View style={styles.printer_icon_cont}>
        <Image source={require('../assets/icons/printer.png')} />
      </View>
      <View style={styles.txt_cont}>
        <Text style={styles.txt}>You have no printers yet</Text>
        <Text style={styles.txt}>To add the printer, press the (+) button</Text>
      </View>
    </View>
  );

  return (
    <View style={styles.main}>
      {!showPrinterStack ? (
        <>
          {connected_device ? (
            <PrinterDevice device={connected_device} />
          ) : (
            <NoDevices />
          )}
          {/* fab */}
          <Pressable
            style={styles.fab}
            onPress={() => {
              setShowPrinterStack(!showPrinterStack);
            }}>
            <Image source={require('../assets/icons/plus.png')} />
          </Pressable>
        </>
      ) : (
        <CreatePrinterScreen />
      )}
    </View>
  );
};

export default PrintersScreen;

const styles = StyleSheet.create({
  main: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    position: 'relative',
  },
  no_device_cont: {
    width: dimentions.col * 6,
    minHeight: dimentions.row * 8,
    alignItems: 'center',
  },
  printer_icon_cont: {
    width: dimentions.col * 3,
    height: dimentions.row * 5,
    backgroundColor: '#1F2937',
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
  },
  txt_cont: {
    marginTop: 30,
  },
  txt: {
    textAlign: 'center',
    color: '#7a7a7a',
  },
  fab: {
    width: 60,
    height: 60,
    borderRadius: 30,
    backgroundColor: colors.orange,
    position: 'absolute',
    bottom: 150,
    right: 80,
    justifyContent: 'center',
    alignItems: 'center',
  },
  device: {
    height: 95,
    width: dimentions.col * 8,
    backgroundColor: colors.btn_blue,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 10,
  },
  device_details: {
    color: colors.white,
    fontSize: 14,
    marginLeft: 10,
  },
  device_img_cont: {
    width: 40,
    height: 40,
    borderRadius: 20,
    backgroundColor: colors.bg_dark,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 6,
  },

  device_conn_light: {
    width: 10,
    height: 10,
    borderRadius: 5,
    backgroundColor: 'blue',
    elevation: 10,
  },
});
