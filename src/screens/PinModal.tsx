import {
  StyleSheet,
  Text,
  View,
  Modal,
  Vibration,
  ActivityIndicator,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import {useNavigation, useRoute} from '@react-navigation/native';
import {RootState, useAppDispatch} from '../redux/store';
import {useSelector} from 'react-redux';
import {colors} from '../constants/theme';
import DialButton from '../components/DialButton';
import {_confirmPIN} from '../redux/actions';
import {
  clearConfirmpinState,
  hidePinModal,
} from '../redux/features/confirmpin.slice';
import {Toast} from 'react-native-toast-message/lib/src/Toast';
import {toastConfig} from '../../App';
type Props = {
  visible: boolean;
};

const PinModal = ({visible}: Props) => {
  const navigation = useNavigation<any>();
  const dispatch = useAppDispatch();
  const {
    confirmError,
    confirmResponse,
    confirmSuccess,
    employeeToConfirm,
    loading,
  } = useSelector((state: RootState) => state.confirmpinSlice);
  const [dialValue, setDialValue] = useState<string>('');

  const [dashOneStyles, setDashOneStyles] = useState({});
  const [dashTwoStyles, setDashTwoStyles] = useState({});
  const [dashThreeStyles, setDashThreeStyles] = useState({});
  const [dashFourStyles, setDashFourStyles] = useState({});
  const [dashFiveStyles, setDashFiveStyles] = useState({});

  useEffect(() => {
    dialValue && dialValue.length === 5 && confirmpincode();
  }, [dialValue]);

  useEffect(() => {
    if (confirmError) {
      //vibrate device
      Vibration.vibrate();
      Toast.show({
        type: 'goldposred',
        text1: confirmResponse ? confirmResponse : '',
        position: 'top',
        topOffset: 10,
      });

      setTimeout(() => {
        dispatch(clearConfirmpinState());
      }, 4000);
    }

    if (confirmSuccess) {
      /**
       * hide modal when pin is finally modified
       */

      setTimeout(() => {
        dispatch(clearConfirmpinState());
        dispatch(hidePinModal());
      }, 2000);
    }

    //clear state on out
    return () => {
      dispatch(clearConfirmpinState());
    };
  }, [confirmError, confirmResponse]);

  useEffect(() => {
    if (dialValue) {
      dialValue.length >= 1
        ? setDashOneStyles({backgroundColor: colors.orange})
        : setDashOneStyles({});
      dialValue.length >= 2
        ? setDashTwoStyles({backgroundColor: colors.orange})
        : setDashTwoStyles({});
      dialValue.length >= 3
        ? setDashThreeStyles({backgroundColor: colors.orange})
        : setDashThreeStyles({});
      dialValue.length >= 4
        ? setDashFourStyles({backgroundColor: colors.orange})
        : setDashFourStyles({});
      dialValue.length >= 5
        ? setDashFiveStyles({backgroundColor: colors.orange})
        : setDashFiveStyles({});
    }

    !dialValue && setDashOneStyles({});
  }, [dialValue]);

  const confirmpincode = async () => {
    if (employeeToConfirm) {
      let data = {
        passCode: dialValue,
        employeeId: employeeToConfirm?.id,
      };

      dispatch(_confirmPIN(data));
    }
  };

  const dialPin = (value: string) => {
    if (dialValue?.length > 5) return;

    setDialValue(prev => (prev ? prev + value : value));
  };

  const deleteDial = () => {
    let newString = dialValue?.slice(0, dialValue.length - 1);
    setDialValue(newString);
  };
  const clearDial = () => {
    setDialValue('');
    setDashOneStyles({});
    setDashTwoStyles({});
    setDashThreeStyles({});
    setDashFourStyles({});
    setDashFiveStyles({});
  };

  const dials = [
    {title: '1', value: '1', hasIcon: false, icon: ''},
    {title: '2', value: '2', hasIcon: false, icon: ''},
    {title: '3', value: '3', hasIcon: false, icon: ''},
    {title: '4', value: '4', hasIcon: false, icon: ''},
    {title: '5', value: '5', hasIcon: false, icon: ''},
    {title: '6', value: '6', hasIcon: false, icon: ''},
    {title: '7', value: '7', hasIcon: false, icon: ''},
    {title: '8', value: '8', hasIcon: false, icon: ''},
    {title: '9', value: '9', hasIcon: false, icon: ''},
    {title: '0', value: '0', hasIcon: false, icon: ''},
    {title: 'C', value: 'C', hasIcon: false, icon: ''},
    {title: 'DEL', value: 'delete', hasIcon: true, icon: ''},
  ];

  const InputDash = ({ref, dashStyles}: {ref?: any; dashStyles?: any}) => (
    <View style={[styles.dash, dashStyles]} ref={ref}></View>
  );
  return (
    <Modal
      animationType="slide"
      visible={visible}
      transparent={true}
      style={{flex: 1}}>
      <View
        style={{
          flex: 1,
          backgroundColor: 'rgba(0,0,0,0.9)',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <View style={styles.main}>
          <View
            style={{
              width: '100%',
              height: 80,
              marginVertical: 8,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontSize: 20,
                fontWeight: '500',
                color: colors.white,
                textAlign: 'center',
              }}>
              Confirm it is you
            </Text>
          </View>

          <View style={styles.dashContainer}>
            {loading ? (
              <ActivityIndicator size={30} color={colors.white} />
            ) : (
              <>
                <InputDash dashStyles={dashOneStyles} />
                <InputDash dashStyles={dashTwoStyles} />
                <InputDash dashStyles={dashThreeStyles} />
                <InputDash dashStyles={dashFourStyles} />
                <InputDash dashStyles={dashFiveStyles} />
              </>
            )}
          </View>

          <View style={styles.pad}>
            <View style={styles.pad_inner_wrapper}>
              {dials &&
                dials.map((item, index) => (
                  <DialButton
                    data={item}
                    key={index}
                    handlePress={(value: string) => {
                      if (value === 'delete') {
                        deleteDial();
                      } else if (value === 'C') {
                        clearDial();
                      } else {
                        dialPin(value);
                      }
                    }}
                    styles={{backgroundColor: colors.blue}}
                  />
                ))}
            </View>
          </View>
        </View>

        {/* toast to show errors */}
        <Toast config={toastConfig} />
      </View>
    </Modal>
  );
};

export default PinModal;

const styles = StyleSheet.create({
  main: {
    flex: 1,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  display: {
    minWidth: 380,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
  },
  pad: {
    width: 380,
    height: 385,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 1,
  },
  pad_inner_wrapper: {
    width: '95%',
    height: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    flexWrap: 'wrap',

    paddingHorizontal: 2,
    paddingVertical: 8,
  },
  dial_text: {
    fontSize: 30,
    fontWeight: 'bold',
    color: '#7a7a7a',
    textAlign: 'center',
  },
  dashContainer: {
    width: 380,
    height: 40,
    flexDirection: 'row',
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'flex-end',
    padding: 1,
  },
  dash: {
    width: 30,
    height: 4,
    backgroundColor: '#fff',
    marginHorizontal: 6,
    borderRadius: 2,
  },
});
