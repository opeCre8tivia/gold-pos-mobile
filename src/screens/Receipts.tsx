import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import { RECEIPT_SCREEN } from '../constants/screenNames'
import RecieptScreen from './RecieptScreen'

const Receipts = () => {

  //set up reciept stack here
  const ReceiptStack = createNativeStackNavigator()
  return (
    <ReceiptStack.Navigator 
         initialRouteName={RECEIPT_SCREEN}
         screenOptions={{
           headerShown:false
         }}
         >
       <ReceiptStack.Screen name={RECEIPT_SCREEN} component={RecieptScreen} />
    </ReceiptStack.Navigator>
  )
}

export default Receipts

const styles = StyleSheet.create({})