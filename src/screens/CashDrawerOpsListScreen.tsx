import {StyleSheet, Text, View, TouchableOpacity, FlatList} from 'react-native';
import React, {useState, useEffect} from 'react';
import {colors, dimentions} from '../constants/theme';
import {RootState, useAppDispatch} from '../redux/store';
import {_getCashDrawerOperations} from '../redux/actions/cashdrawer.actions';
import {useSelector} from 'react-redux';
import CashDrawerOpsListItem from '../components/CashDrawerOpsListItem';
import {CashDrawerOperation} from '../types/main';
import {setCurrentCashDrawerOps} from '../redux/features/cashdrawers.slice';
import {useNavigation} from '@react-navigation/native';

const CashDrawerOpsListScreen = () => {
  const dispatch = useAppDispatch();
  const navigation = useNavigation();
  const {cashDrawerOperations, currentUsersCashDrawerOperation} = useSelector(
    (state: RootState) => state.cashdrawersSlice,
  );

  const {currentOperator} = useSelector(
    (state: RootState) => state.clockingSlice,
  );

  const [showActive, setShowActive] = useState<boolean>(true);
  const [operations, setOperations] = useState<CashDrawerOperation[]>([]);

  useEffect(() => {
    navigation.addListener('focus', () => {
      dispatch(_getCashDrawerOperations(currentOperator.id));
    });
  }, []);

  /**
   * toggle cashDrawer Operations
   */

  useEffect(() => {
    // console.log(cashDrawerOperations, '----current ops--->');
    if (cashDrawerOperations && showActive) {
      /**
       * The current employee should only see cashdrawer operations
       * related to them
       */
      let _arrayOfActiveOperatons = cashDrawerOperations.filter(
        item => item.isActive,
      );

      let _operationsRelatedToCurrentUser = _arrayOfActiveOperatons.filter(
        ops => ops.employeeId === currentOperator.id,
      );
      //set it to be available globally
      dispatch(setCurrentCashDrawerOps(_operationsRelatedToCurrentUser[0]));
      setOperations(_operationsRelatedToCurrentUser);
    }

    if (cashDrawerOperations && !showActive) {
      let _array = cashDrawerOperations.filter(item => !item.isActive);
      setOperations(_array);
    }
  }, [showActive, cashDrawerOperations]);

  return (
    <View style={styles.main}>
      {/* switch buttons */}
      <View
        style={{
          width: '100%',
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
          height: 50,
        }}>
        <TouchableOpacity
          onPress={() => setShowActive(true)}
          style={{
            flex: 1,
            height: 44,
            backgroundColor: showActive ? colors.blue : colors.btn_blue,
            borderRadius: 3,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text style={{color: '#ffffff', fontSize: 14, fontWeight: '500'}}>
            Active
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => setShowActive(false)}
          style={{
            flex: 1,
            height: 44,
            backgroundColor: showActive ? colors.btn_blue : colors.blue,
            borderRadius: 3,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text style={{color: '#ffffff', fontSize: 14, fontWeight: '500'}}>
            Closed
          </Text>
        </TouchableOpacity>
      </View>

      {/* drawer ops list */}
      <View style={styles.listWrapper}>
        {operations.length > 0 ? (
          <FlatList
            data={operations}
            showsVerticalScrollIndicator={false}
            initialNumToRender={2}
            renderItem={({item, index}) => (
              <CashDrawerOpsListItem drawerOps={item} key={index} />
            )}
          />
        ) : (
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={{color: colors.white}}>
              No cash drawers at the moment
            </Text>
          </View>
        )}
      </View>
    </View>
  );
};

export default CashDrawerOpsListScreen;

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: '#000000',
    padding: 4,
  },

  listWrapper: {
    paddingHorizontal: 30,
    height: dimentions.row * 14,
  },
});
