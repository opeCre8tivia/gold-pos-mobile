import React, {useState, useEffect, useRef} from 'react';
import {ScrollView, StyleSheet, Text, View, Vibration} from 'react-native';
import {colors, dimentions} from '../constants/theme';
import TaskBar from '../components/TaskBar';
import DialButton from '../components/DialButton';
import {useNavigation, useRoute} from '@react-navigation/native';
import {useSelector, useDispatch} from 'react-redux';
import GPSuccessOrErrorModal from '../components/GPSuccessOrErrorModal';
import {_verifyPassCode} from '../redux/actions';
import {clearverifyPasscodeSliceState} from '../redux/features/verifyPasscode.slice';
import {CLOCKED_IN_HOME_SCREEN} from '../constants/screenNames';
import {useAppDispatch} from '../redux/store';

const EnterPinScreen = () => {
  const dashRef = useRef(null);

  const navigation = useNavigation<any>();
  const dispatch = useAppDispatch();
  const {isError, isSuccess, error_message} = useSelector(
    (state: any) => state.verifyPasscodeSlice,
  );
  const [dialValue, setDialValue] = useState<string>('');
  // const [passCode,setPassCode] = useState<string | null>( null)

  const {employeeData} = useRoute<any>().params;

  const [message, setMessage] = useState({
    isError: false,
    isSuccess: false,
    title: '',
    msg: '',
  });

  const [dashOneStyles, setDashOneStyles] = useState({});
  const [dashTwoStyles, setDashTwoStyles] = useState({});
  const [dashThreeStyles, setDashThreeStyles] = useState({});
  const [dashFourStyles, setDashFourStyles] = useState({});
  const [dashFiveStyles, setDashFiveStyles] = useState({});

  useEffect(() => {
    dialValue && dialValue.length === 5 && verifyPasscodeAndClockUserIn();
  }, [dialValue]);

  useEffect(() => {
    if (isError) {
      //vibrate device
      Vibration.vibrate();

      setMessage({
        ...message,
        isError: true,
        title: 'Verification Error',
        msg: error_message,
      });

      // reset errors after 4 seconds
      setTimeout(() => {
        dispatch(clearverifyPasscodeSliceState());
        setMessage({
          ...message,
          isError: false,
          title: '',
          msg: '',
        });
      }, 4000);
    }

    if (isSuccess) {
      /**
       * navigate to clokedUser Home Screen
       */

      navigation.navigate(CLOCKED_IN_HOME_SCREEN, {employeeData});
    }

    //clear state on out
    return () => {
      // dispatch(clearverifyPasscodeSliceState());
    };
  }, [isError, isSuccess]);
  //handle dash ref

  useEffect(() => {
    if (dialValue) {
      dialValue.length >= 1
        ? setDashOneStyles({backgroundColor: colors.orange})
        : setDashOneStyles({});
      dialValue.length >= 2
        ? setDashTwoStyles({backgroundColor: colors.orange})
        : setDashTwoStyles({});
      dialValue.length >= 3
        ? setDashThreeStyles({backgroundColor: colors.orange})
        : setDashThreeStyles({});
      dialValue.length >= 4
        ? setDashFourStyles({backgroundColor: colors.orange})
        : setDashFourStyles({});
      dialValue.length >= 5
        ? setDashFiveStyles({backgroundColor: colors.orange})
        : setDashFiveStyles({});
    }

    !dialValue && setDashOneStyles({});
  }, [dialValue]);

  const verifyPasscodeAndClockUserIn = async () => {
    let data = {
      passCode: dialValue,
      employeeId: employeeData.id,
    };

    dispatch(_verifyPassCode(data));
  };

  const dialPin = (value: string) => {
    if (dialValue?.length > 5) return;

    setDialValue(prev => (prev ? prev + value : value));
  };

  const deleteDial = () => {
    let newString = dialValue?.slice(0, dialValue.length - 1);
    setDialValue(newString);
  };
  const clearDial = () => {
    setDialValue('');
    setDashOneStyles({});
    setDashTwoStyles({});
    setDashThreeStyles({});
    setDashFourStyles({});
    setDashFiveStyles({});
  };

  const dials = [
    {title: '1', value: '1', hasIcon: false, icon: ''},
    {title: '2', value: '2', hasIcon: false, icon: ''},
    {title: '3', value: '3', hasIcon: false, icon: ''},
    {title: '4', value: '4', hasIcon: false, icon: ''},
    {title: '5', value: '5', hasIcon: false, icon: ''},
    {title: '6', value: '6', hasIcon: false, icon: ''},
    {title: '7', value: '7', hasIcon: false, icon: ''},
    {title: '8', value: '8', hasIcon: false, icon: ''},
    {title: '9', value: '9', hasIcon: false, icon: ''},
    {title: '0', value: '0', hasIcon: false, icon: ''},
    {title: 'C', value: 'C', hasIcon: false, icon: ''},
    {title: 'DEL', value: 'delete', hasIcon: true, icon: ''},
  ];

  const InputDash = ({ref, dashStyles}: {ref?: any; dashStyles?: any}) => (
    <View style={[styles.dash, dashStyles]} ref={ref}></View>
  );

  return (
    <ScrollView>
      <View style={styles.main}>
        <TaskBar title="Passcode Login" navigation={navigation} />

        {isError && <GPSuccessOrErrorModal message={message} />}

        <View
          style={{
            width: '100%',
            height: 80,
            marginVertical: 8,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text style={{fontSize: 35, fontWeight: '500', color: colors.white}}>
            Enter your Personal PIN..
          </Text>
        </View>

        <View style={styles.dashContainer}>
          {/* <View style={styles.dash} ref={dashRef} ></View> */}
          <InputDash dashStyles={dashOneStyles} />
          <InputDash dashStyles={dashTwoStyles} />
          <InputDash dashStyles={dashThreeStyles} />
          <InputDash dashStyles={dashFourStyles} />
          <InputDash dashStyles={dashFiveStyles} />
        </View>

        {/* <View style={styles.display}>
                <Text style={styles.dial_text}> {dialValue} </Text>
            </View> */}

        <View style={styles.pad}>
          <View style={styles.pad_inner_wrapper}>
            {dials &&
              dials.map((item, index) => (
                <DialButton
                  data={item}
                  key={index}
                  handlePress={(value: string) => {
                    if (value === 'delete') {
                      deleteDial();
                    } else if (value === 'C') {
                      clearDial();
                    } else {
                      dialPin(value);
                    }
                  }}
                  styles={{backgroundColor: colors.blue}}
                />
              ))}
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

export default EnterPinScreen;

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: colors.bg_light,
    alignItems: 'center',
    height: dimentions.vh,
  },
  display: {
    minWidth: 380,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomColor: colors.white,
    borderBottomWidth: 1,
    marginBottom: 8,
  },
  pad: {
    width: 380,
    height: 385,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 1,
  },
  pad_inner_wrapper: {
    width: '95%',
    height: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    flexWrap: 'wrap',
    backgroundColor: 'transparent',

    paddingHorizontal: 2,
    paddingVertical: 8,
  },
  dial_text: {
    fontSize: 30,
    fontWeight: 'bold',
    color: '#7a7a7a',
    textAlign: 'center',
  },
  dashContainer: {
    width: 290,
    height: 40,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 1,
  },
  dash: {
    width: 30,
    height: 4,
    backgroundColor: '#fff',
    marginHorizontal: 6,
    borderRadius: 2,
  },
});
