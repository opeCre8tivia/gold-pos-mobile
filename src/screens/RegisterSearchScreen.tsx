import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import TaskBar from '../components/TaskBar';
import {colors} from '../constants/theme';
import GPSearchInput from '../components/GPSearchInput';
import {useNavigation} from '@react-navigation/native';
import {useDispatch, useSelector} from 'react-redux';
import {addMenuItem} from '../redux/features/directSale.slice';

interface MenuItem {
  id: number;
  name: string;
  qty: number;
  price: number;
  total: number;
}

interface SearchResults {
  item: MenuItem;
}

const RegisterSearchScreen = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const {addedMenuItemsTotal, addedMenuItems} = useSelector(
    (state: any) => state.directSaleSlice,
  );
  const {menuItems} = useSelector((state: any) => state.menuDataSlice);

  const [searchResults, setSearchResults] = useState<MenuItem[] | null>(null);
  const [searchKeyWord, setSearchKeyWord] = useState<string>('');

  useEffect(() => {
    searchKeyWord.length > 0 && search(searchKeyWord, menuItems);
    searchKeyWord.length === 0 && setSearchResults(null); //clears the search results on back
  }, [searchKeyWord]);

  const search = (_keyword: string, arrayToSearch: MenuItem[]) => {
    let _results: any[] = [];

    arrayToSearch.forEach((item, index) => {
      let _name = item?.name.toLowerCase();
      if (_name.includes(_keyword.toLowerCase())) {
        _results.push(item);
      }
    });

    setSearchResults(_results);
  };

  const handleAddingMenuItem = (item: MenuItem) => {
    let _item = {
      id: item.id,
      name: item.name,
      qty: 1,
      price: item.price,
      total: item.price * 1,
    };

    dispatch(addMenuItem(_item));
  };

  const SearchResultElement = ({item}: SearchResults) => {
    //determine if item is already added
    let result = addedMenuItems.filter((e: MenuItem) => e.id === item.id);

    return (
      <View style={styles.search_item_cont}>
        <Text style={{flex: 1, color: '#fff', fontSize: 16, fontWeight: '500'}}>
          {item.name}
        </Text>
        <Text style={{flex: 1, color: '#fff', fontSize: 16, fontWeight: '500'}}>
          {item.price}
        </Text>

        <View style={{flex: 1}}>
          {result?.length > 0 ? (
            <TouchableOpacity
              style={{
                width: 100,
                height: 30,
                backgroundColor: colors.gp_green,
                justifyContent: 'center',
                alignItems: 'center',
                padding: 2,
                borderRadius: 6,
              }}>
              <Text style={{color: '#fff', fontSize: 12, fontWeight: '500'}}>
                Added
              </Text>
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              onPress={() => handleAddingMenuItem(item)}
              style={{
                width: 100,
                height: 30,
                backgroundColor: colors.orange,
                justifyContent: 'center',
                alignItems: 'center',
                padding: 2,
                borderRadius: 6,
              }}>
              <Text style={{color: '#fff', fontSize: 12, fontWeight: '500'}}>
                Add to Order
              </Text>
            </TouchableOpacity>
          )}
        </View>
      </View>
    );
  };

  return (
    <View style={styles.main}>
      <TaskBar title="Search" navigation={navigation} canMoveBack={true} />

      <View style={styles.main_wrapper}>
        <GPSearchInput
          placeholder="Search for a Menu Item"
          input_styles={{width: '80%'}}
          value={searchKeyWord}
          onChangeText={val => setSearchKeyWord(val)}
        />

        {/* spacer */}

        <View style={{height: 10, width: '100%'}}></View>
        <ScrollView style={{width: '80%'}}>
          {searchResults &&
            searchResults.map((item, index) => (
              <SearchResultElement item={item} key={index} />
            ))}
        </ScrollView>
      </View>
    </View>
  );
};

export default RegisterSearchScreen;

const styles = StyleSheet.create({
  main: {
    flex: 1,
  },
  main_wrapper: {
    flex: 1,
    height: '100%',
    backgroundColor: colors.bg_dark,
    alignItems: 'center',
    paddingVertical: 30,
  },
  search_item_cont: {
    width: '100%',
    backgroundColor: '#303137',
    height: 50,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 12,
  },
});
