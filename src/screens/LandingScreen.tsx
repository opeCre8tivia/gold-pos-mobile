import React, {useEffect, useState} from 'react';
import {Image, StyleSheet, Text, View, ScrollView} from 'react-native';
import {colors, dimentions, size} from '../constants/theme';
import Logo from '../components/Logo';
import GPButton from '../components/GPButton';
import {useNavigation} from '@react-navigation/native';
import {CLOCK_IN_SCREEN} from '../constants/screenNames';

import {useSelector, useDispatch} from 'react-redux';
import {_getOutletInfo} from '../redux/actions';
import FullScreenLoader from '../components/FullScreenLoader';
import GPSuccessOrErrorModal from '../components/GPSuccessOrErrorModal';
import DeviceInfo from 'react-native-device-info';

const LandingScreen = () => {
  const navigation = useNavigation<any>();
  const dispatch = useDispatch<any>();
  const {outlet, business, loading, isError, isSuccess, employees} =
    useSelector((state: any) => state.outletSlice);
  const [ip, setIp] = useState<any>('0.0.0.0');
  const [message, setMessage] = useState({
    isError: false,
    isSuccess: false,
    title: '',
    msg: '',
  });

  useEffect(() => {
    !outlet && dispatch(_getOutletInfo());
  }, [outlet]);

  useEffect(() => {
    setDeviceIp();
  }, []);

  useEffect(() => {
    if (isError) {
      setMessage({
        ...message,
        isError: true,
        title: 'Outlet Profile Error',
        msg: 'Failed to Load Outlet Profile!',
      });
    }

    if (isSuccess) {
      //silently
    }
  }, [isError, isSuccess]);

  const setDeviceIp = async () => {
    let _ip = await DeviceInfo.getIpAddress();
    if (_ip) {
      setIp(_ip);
    }
  };

  return (
    <>
      {loading ? (
        <FullScreenLoader />
      ) : (
        <ScrollView style={{flex: 1}}>
          <View style={styles.main}>
            {isError && <GPSuccessOrErrorModal message={message} />}

            <View style={styles.left_wrapper}>
              <View style={styles.logo}>
                <Logo
                  styles={{
                    width: dimentions.col * 3.5,
                    height: dimentions.row * 2.5,
                  }}
                />
              </View>

              <View style={styles.txt_container}>
                <Text
                  style={{
                    fontSize: size.sm + 1,
                    fontWeight: '700',
                    color: '#7a7a7a',
                  }}>
                  Business Name
                </Text>

                <Text
                  style={{
                    fontSize: size.md,
                    fontWeight: '700',
                    color: '#ffffff',
                  }}>
                  {business?.business_name}
                </Text>
              </View>

              <View style={styles.txt_container}>
                <Text
                  style={{
                    fontSize: size.sm + 1,
                    fontWeight: '700',
                    color: '#7a7a7a',
                  }}>
                  Outlet Name
                </Text>

                <Text
                  style={{
                    fontSize: size.md,
                    fontWeight: '700',
                    color: '#ffffff',
                  }}>
                  {outlet?.name}
                </Text>
              </View>

              <View style={styles.txt_container}>
                <Text
                  style={{
                    fontSize: size.sm + 1,
                    fontWeight: '700',
                    color: '#7a7a7a',
                  }}>
                  Wi-Fi and IP
                </Text>

                <Text
                  style={{
                    fontSize: size.md,
                    fontWeight: '700',
                    color: '#ffffff',
                  }}>
                  {ip}
                </Text>
              </View>

              <View style={styles.txt_container}>
                <Text
                  style={{
                    fontSize: size.sm + 1,
                    fontWeight: '700',
                    color: '#7a7a7a',
                  }}>
                  Software Version
                </Text>

                <Text
                  style={{
                    fontSize: size.md,
                    fontWeight: '700',
                    color: '#ffffff',
                  }}>
                  v:1.0.1
                </Text>
              </View>
            </View>

            <View style={styles.right_wrapper}>
              <View style={styles.image_container}>
                <Image
                  source={require('../assets/icons/whiteCrossForkKnife.png')}
                  style={{
                    width: dimentions.col * 4,
                    height: dimentions.row * 4,
                  }}
                  resizeMode="contain"
                />
              </View>

              <Text
                style={{
                  fontSize: size.md + 3,
                  fontWeight: '400',
                  color: '#ffffff',
                  fontFamily: 'Lato-Black',
                }}>
                Clockin below to make sales
              </Text>

              <View style={styles.button}>
                <GPButton
                  title="Clock in/out"
                  styles={{
                    height: dimentions.row * 1.5,
                    width: dimentions.row * 6,
                    backgroundColor: colors.blue,
                  }}
                  onPress={() => navigation.navigate(CLOCK_IN_SCREEN)}
                />
              </View>
            </View>
          </View>
        </ScrollView>
      )}
    </>
  );
};

export default LandingScreen;

const styles = StyleSheet.create({
  main: {
    flex: 1,
    flexDirection: 'row',
    height: dimentions.vh,
  },
  left_wrapper: {
    width: dimentions.col * 4,
    height: dimentions.vh,
    backgroundColor: colors.bg_light,
    alignItems: 'center',
    paddingHorizontal: 10,
    paddingVertical: 5,
  },
  right_wrapper: {
    width: dimentions.col * 16,
    height: dimentions.vh,
    backgroundColor: colors.bg_dark,
    alignItems: 'center',
    paddingHorizontal: 10,
    paddingVertical: 5,
  },
  logo: {
    width: '100%',
    height: 120,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 100,
  },
  txt_container: {
    width: '100%',
    minHeight: 80,
    alignItems: 'center',
    marginBottom: 5,
  },
  image_container: {
    width: '100%',
    height: 200,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 100,
  },
  button: {
    width: '100%',
    height: 80,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
