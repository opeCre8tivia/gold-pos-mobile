import {FlatList, StyleSheet, Text, View, Image} from 'react-native';
import React, {useEffect, useState} from 'react';
import TaskBar from '../components/TaskBar';
import {colors, dimentions} from '../constants/theme';
import GPSearchInput from '../components/GPSearchInput';
import ClockInButton from '../components/ClockInButton';
import {useDispatch, useSelector} from 'react-redux';
import {RootState} from '../redux/store';
import {_clockoutEmployee, _getEmployeesInShift} from '../redux/actions';
import {setCurrentOperator, setShiftOn} from '../redux/features/clocking.slice';
import {setWhoIsServing} from '../redux/features/directSale.slice';
import {useRoute} from '@react-navigation/native';
import {confirmPIN} from '../redux/features/confirmpin.slice';
import {Employee} from '../types/main';
import {_getCashDrawerOperations} from '../redux/actions/cashdrawer.actions';
import {bypassCountingScreens} from '../redux/features/cashdrawers.slice';

const ClockInClockOutScreen = () => {
  const {clocked_in_employees, currentOperator} = useSelector(
    (state: RootState) => state.clockingSlice,
  );
  const {cashDrawerOperations} = useSelector(
    (state: RootState) => state.cashdrawersSlice,
  );
  const {confirmSuccess, employeeToConfirm} = useSelector(
    (state: RootState) => state.confirmpinSlice,
  );
  const dispatch = useDispatch<any>();
  const {employeeData} = useRoute<any>().params;

  const [selectedEmployee, setSelectedEmployee] = useState<Employee | null>(
    null,
  );

  useEffect(() => {
    dispatch(_getEmployeesInShift());
  }, []);

  useEffect(() => {
    if (selectedEmployee) {
      dispatch(confirmPIN(selectedEmployee));
      dispatch(_getCashDrawerOperations(selectedEmployee.id));
    }

    return () => {
      setSelectedEmployee(null);
    };
  }, [selectedEmployee]);

  /**
   * when pin is confirmed one now navigates to counting screen
   */

  useEffect(() => {
    if (confirmSuccess) {
      handleNavigatingToSales();
    }
  }, [confirmSuccess]);

  const handleNavigatingToSales = () => {
    /**
     * If an employee has any open cashdrawer
     * they should bypass counting screens
     */

    if (employeeToConfirm && cashDrawerOperations) {
      let has_open_cashdrawer = cashDrawerOperations.some(
        drawer => drawer.employeeId === employeeToConfirm.id && drawer.isActive,
      );

      has_open_cashdrawer && dispatch(bypassCountingScreens());
    }
    dispatch(setWhoIsServing(employeeToConfirm));
    dispatch(setCurrentOperator(employeeToConfirm));
    dispatch(setShiftOn());

    setSelectedEmployee(null);
  };

  return (
    <View style={styles.main}>
      <TaskBar title="Your now clocked in" canMoveBack={false} />

      <View style={styles.main_wrapper}>
        <View style={styles.left_wrapper}>
          <Text
            style={{
              fontWeight: '500',
              fontSize: 18,
              color: '#ffffff',
              marginTop: 30,
              width: '80%',
            }}>
            Tap on your name to proceed
          </Text>

          <View style={{width: '80%', marginVertical: 20}}>
            <GPSearchInput placeholder="Search for your name" />
          </View>

          <Text
            style={{
              fontWeight: '500',
              fontSize: 16,
              color: '#fff',
              width: '80%',
            }}>
            {/* Select name to proceed to sales */}
          </Text>

          <View style={styles.managers}>
            {clocked_in_employees && (
              <FlatList
                data={clocked_in_employees}
                showsVerticalScrollIndicator={false}
                renderItem={({item, index}) => {
                  return (
                    <>
                      <ClockInButton
                        key={index}
                        item={item}
                        onPress={() => {
                          setSelectedEmployee(item);
                        }}
                      />
                    </>
                  );
                }}
              />
            )}
          </View>
        </View>

        <View style={styles.right_wrapper}>
          <Image
            source={require('../assets/img/chefboy.png')}
            style={{
              width: dimentions.col * 8,
              height: dimentions.row * 8,
            }}
            resizeMode="contain"
          />
        </View>
      </View>
    </View>
  );
};

export default ClockInClockOutScreen;

const styles = StyleSheet.create({
  main: {
    flex: 1,
  },
  main_wrapper: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.bg_dark,
  },
  left_wrapper: {
    flex: 1,
    height: '100%',
    alignItems: 'center',
  },
  right_wrapper: {
    flex: 1,
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  managers: {
    width: '80%',
    height: dimentions.vh / 4,
  },
  shift_members: {
    width: '80%',
    height: dimentions.vh / 2,
  },
  sales_period_notice: {
    width: '80%',
    height: 50,
    backgroundColor: '#303137',
    borderRadius: 6,
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 16,
  },
});
