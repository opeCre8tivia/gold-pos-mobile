import {FlatList, SafeAreaView, StyleSheet, Text, View} from 'react-native';
import React, {useEffect, useState} from 'react';
import TaskBar from '../components/TaskBar';
import {colors, dimentions} from '../constants/theme';
import GPButton from '../components/GPButton';
import Logo from '../components/Logo';
import ClockInButton from '../components/ClockInButton';
import {useDispatch, useSelector} from 'react-redux';
import {_getEmployeesInShift} from '../redux/actions';
import {useNavigation} from '@react-navigation/native';
import {HOME_SCREEN} from '../constants/screenNames';
import {setShiftOn} from '../redux/features/clocking.slice';

const ClockingHomeScreen = () => {
  const dispatch = useDispatch<any>();
  const navigation = useNavigation<any>();
  const {
    loading,
    isSuccess,
    isError,
    clocked_in_list_error_message,
    clocked_in_employees,
  } = useSelector((state: any) => state.clockingSlice);

  //fetch clocked in users
  useEffect(() => {
    dispatch(_getEmployeesInShift());
  }, []);

  const handleClockin = () => {
    dispatch(setShiftOn());
  };

  return (
    <SafeAreaView style={{flex: 1}}>
      <TaskBar title="Gold Point Of Sale" canMoveBack={false} />

      <View style={styles.main}>
        <View style={styles.left_wrapper}>
          <View style={{width: '80%', alignItems: 'flex-start', flex: 6}}>
            <View
              style={{
                height: 120,
                width: 250,
                justifyContent: 'center',
                alignItems: 'center',
                marginTop: 30,
              }}>
              <Logo styles={{width: 200, height: 100}} />
            </View>

            <Text
              style={{
                fontWeight: '500',
                fontSize: 16,
                color: '#fff',
                marginBottom: 20,
              }}>
              Welcome! Tap an active user, View the floor plan or clock in/out
            </Text>

            <View style={{height: dimentions.vh / 2.5}}>
              {clocked_in_employees && (
                <FlatList
                  data={clocked_in_employees}
                  showsVerticalScrollIndicator={false}
                  renderItem={({item, index}) => (
                    <ClockInButton item={item} key={index} />
                  )}
                />
              )}
            </View>
          </View>

          <View
            style={{
              width: '100%',
              alignItems: 'center',
              justifyContent: 'flex-end',
              flex: 1,
            }}>
            <GPButton
              title="start Selling"
              styles={{width: '80%', height: 50}}
              onPress={() => handleClockin()}
            />
          </View>
        </View>

        <View style={styles.right_wrapper}>
          <GPButton
            title="View Floor Plan"
            styles={{width: '80%', height: 50}}
          />
        </View>
      </View>
    </SafeAreaView>
  );
};

export default ClockingHomeScreen;

const styles = StyleSheet.create({
  main: {
    flex: 1,
    maxHeight: dimentions.vh - 50,
    backgroundColor: colors.bg_dark,
    flexDirection: 'row',
    alignItems: 'center',
  },
  left_wrapper: {
    flex: 1,
    height: '100%',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingBottom: 40,
  },
  right_wrapper: {
    flex: 1,
    height: '100%',
    justifyContent: 'flex-end',
    alignItems: 'center',
    paddingBottom: 40,
  },
});
