import { useNavigation } from '@react-navigation/native'
import React,{useState} from 'react'
import { Image, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import DialButton from '../components/DialButton'
import GPButton from '../components/GPButton'
import TaskBar from '../components/TaskBar'
import { colors } from '../constants/theme'
import { setCustomRecievedAmount } from '../redux/features/directSale.slice'

const CustomAmountScreen = () => {
    
    const navigation = useNavigation()
    const dispatch = useDispatch<any>()
    const {customRecievedAmount} = useSelector((state:any)=> state.directSaleSlice)
    const [dialValue, setDialValue] = useState<string>()


    const dialPin=(value:string)=>{
          setDialValue((prev)=> prev ? prev + value : value)
    }

    const deleteDial =()=>{
        let newString = dialValue?.slice(0,dialValue.length - 1)
        setDialValue(newString)
    }

    const handleSetCustomAmount=()=>{
         dialValue && dispatch(setCustomRecievedAmount(parseInt(dialValue)))
         navigation.goBack()
    }

    const dials = [
        {title:"7",value:"7",hasIcon:false,icon:""},
        {title:"8",value:"8",hasIcon:false,icon:""},
        {title:"9",value:"9",hasIcon:false,icon:""},
        {title:"4",value:"4",hasIcon:false,icon:""},
        {title:"5",value:"5",hasIcon:false,icon:""},
        {title:"6",value:"6",hasIcon:false,icon:""},
        {title:"1",value:"1",hasIcon:false,icon:""},
        {title:"2",value:"2",hasIcon:false,icon:""},
        {title:"3",value:"3",hasIcon:false,icon:""},
        {title:"0",value:"0",hasIcon:false,icon:""},
        {title:"00",value:"00",hasIcon:false,icon:""},
        {title:"C",value:"delete",hasIcon:false,icon:"",style_text:{color:"#F05052"}},
      
     ]

  return (
    <ScrollView>
    <View style={styles.main}>
      <TaskBar title="Direct Sale" canMoveBack={true} navigation={navigation}/>

      {/* dial pad */}
      <View style={styles.dial_pad_wrapper}>

        <View style={styles.top_container}>
            <TouchableOpacity style={{flex:1}}>
                <Text style={{color:"#ED4C5C",fontSize:16,fontWeight:"600",textAlign:"center"}}>Cancel</Text>
            </TouchableOpacity>

            <View style={{flex:2}}>
                <Text style={{color:"#ffffff",fontSize:16,fontWeight:"600",textAlign:"left"}}>
                    Enter received amount
                </Text>
            </View>
        </View>

        <View style={styles.dial_pad_inner_wrapper}>
            {/* amount */}
            <View style={styles.dial_amount_container}>
                <View style={{width:80,height:80,backgroundColor:"#0069A5",justifyContent:"center",alignItems:"center"}}>
                    <Image source={require('../assets/icons/mathIcon.png')} style={{width:30,height:30}}/>
                </View>

                <View style={{height:"100%",width:60,alignItems:"center",justifyContent:"center",marginHorizontal:4}}>
                <Text style={{fontWeight:"500",fontSize:25,color:"#ffffff",textAlign:"center",}}>
                      USh
                </Text>
                </View>

                
                <View style={{height:"100%",flex:1,alignItems:"center",justifyContent:"center"}}>
                   
                   <Text style={{fontWeight:"500",fontSize:25,color:"#ffffff",textAlign:"left",width:"100%"}}> 
                     {dialValue} 
                  </Text> 
                </View>
            </View>

            {/* pad */}
            <View style={styles.dial_pad_container}>
            {
             dials && dials.map((item,index)=>(
                <DialButton 
                   data={item} 
                   key={index}
                   styles={{width:114,height:46,margin:2,backgroundColor:"#303137"}}
                   handlePress={(value:string)=> {
                        value === "delete" ? deleteDial() : dialPin(value)
                   } }
                    />
             ))
           }
            </View>

            {/* button */}
            <View style={styles.button_container}>
               <GPButton 
                   title={`Confirm - USh ${dialValue}`}
                   styles={{width:"100%",height:70,backgroundColor:"#0069A5",borderRadius:3}}
                   text_styles={{fontSize:18,fontWeight:"500"}}
                   onPress={()=> handleSetCustomAmount()}
               />
            </View>

        </View>

      </View>


    </View>
    </ScrollView>
  )
}

export default CustomAmountScreen

const styles = StyleSheet.create({
    main:{
        flex:1,
        backgroundColor:colors.bg_light,
        alignItems:"center"
    },
    dial_pad_wrapper:{
        width:400,
        height:480,
        alignItems:"center",
        marginTop:80, //TODO: center appropriately
    },
    top_container:{
        width:"100%",
        height:50,
        backgroundColor:"#2A333E",
        flexDirection:"row",
        alignItems:"center"
    },

  
    dial_pad_inner_wrapper:{
        width:"100%",
        height:430,
        backgroundColor:colors.bg_dark,
        paddingVertical:15,
        paddingHorizontal:22
    },
    dial_amount_container:{
        width:"100%",
        height:80,
        backgroundColor:colors.bg_light,
        flexDirection:'row'
     },
    dial_pad_container:{
        width:"100%",
        height:200,
        flexDirection:"row",
        flexWrap:"wrap",
        justifyContent:"center",
        marginVertical:15,
    },
    button_container:{
        width:"100%",
        height:90
    }
})