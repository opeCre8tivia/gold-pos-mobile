import React, {useState, useEffect} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import TaskBar from '../components/TaskBar';
import {colors, dimentions} from '../constants/theme';
import GPButton from '../components/GPButton';
import DialButton from '../components/DialButton';
import {useDispatch, useSelector} from 'react-redux';
import {RootState, useAppDispatch} from '../redux/store';
import {
  addDenominationToCount,
  clearCashDrawerState,
  count,
  modifyCount,
  resetCount,
} from '../redux/features/cashdrawers.slice';
import {useNavigation, useRoute} from '@react-navigation/native';

import {CashDrawerOperation, Denomination} from '../types/main';
import {_createCashDrawerOperation} from '../redux/actions/cashdrawer.actions';
import GPSuccessOrErrorModal from '../components/GPSuccessOrErrorModal';

type Props = {};

const FloatCountingScreen = (props: Props) => {
  const {
    denominationToCount,
    counted,
    cashDrawers,
    isError,
    isSuccess,
    loading,
    responseMessage,
  } = useSelector((state: RootState) => state.cashdrawersSlice);

  const {currentOperator} = useSelector(
    (state: RootState) => state.clockingSlice,
  );

  const dispatch = useAppDispatch();
  const navigation = useNavigation();
  const {cashDrawerId} = useRoute<any>().params;

  /**
   * the dial pad is from 0 to 9
   * to enable one punch in numbers like 10, 15 , 35 etc we need extra work arounds
   * we shall levarage previous state, if there is avalue we concatinate with new one
   */
  const [quantity, setQuantity] = useState<number>();

  const [denomination, setDenomination] = useState<Denomination>();

  useEffect(() => {
    if (denomination && quantity) {
      handleCounting(denomination, quantity);
    }
  }, [denomination, quantity]);

  useEffect(() => {
    if (isError) {
      setTimeout(() => {
        dispatch(clearCashDrawerState());
      }, 4000);
    }
  }, [isError]);

  const cancelCount = () => {
    dispatch(resetCount());
  };

  const confirmAndAddCashDrawerOperation = () => {
    let _cashDrawer = cashDrawers?.find(drawer => drawer.id === cashDrawerId);
    let confirmedAmount = counted.reduce((acc, val, arr) => {
      return acc + val.value * val.quantity;
    }, 0);

    if (confirmedAmount === 0) return;

    if (_cashDrawer) {
      let data: CashDrawerOperation = {
        cashDrawerId: cashDrawerId,
        startingBalance: _cashDrawer?.startingBalance,
        currentBalance: confirmedAmount,
        employeeId: currentOperator.id,
        isActive: true,
      };

      dispatch(_createCashDrawerOperation(data));
    }
  };

  const handleSettingQuantity = (value: string) => {
    setQuantity(prev => {
      return prev ? parseInt(prev.toString() + value) : parseInt(value);
    });
  };

  const handleCounting = (note: Denomination, quantity: number) => {
    /**
     * check if it doesnt already exist in the counted array
     */
    const isAlreadyAdded = counted.some(item => item.id === note.id);

    /**
     *
     */
    note.quantity = quantity;

    if (!isAlreadyAdded) {
      dispatch(count(note));
    }

    if (isAlreadyAdded) {
      dispatch(modifyCount(note));
    }
  };

  const dials = [
    {title: '1', value: '1', hasIcon: false, icon: ''},
    {title: '2', value: '2', hasIcon: false, icon: ''},
    {title: '3', value: '3', hasIcon: false, icon: ''},
    {title: '4', value: '4', hasIcon: false, icon: ''},
    {title: '5', value: '5', hasIcon: false, icon: ''},
    {title: '6', value: '6', hasIcon: false, icon: ''},
    {title: '7', value: '7', hasIcon: false, icon: ''},
    {title: '8', value: '8', hasIcon: false, icon: ''},
    {title: '9', value: '9', hasIcon: false, icon: ''},
    {title: '0', value: '0', hasIcon: false, icon: ''},
    {title: '00', value: '00', hasIcon: false, icon: ''},
    {
      title: 'C',
      value: 'delete',
      hasIcon: false,
      icon: '',
      style_text: {color: '#F05052'},
    },
  ];

  const notes = [
    {
      id: 1,
      name: '1,000',
      value: 1000,
      type: 'paper',
      quantity: 1,
    },
    {
      id: 2,
      name: '2,000',
      value: 2000,
      type: 'paper',
      quantity: 1,
    },
    {
      id: 3,
      name: '5,000',
      value: 5000,
      type: 'paper',
      quantity: 1,
    },
    {
      id: 4,
      name: '10,000',
      value: 10000,
      type: 'paper',
      quantity: 1,
    },
  ];

  const CountRow = ({note}: {note: Denomination}) => {
    let isActiveRow = denominationToCount && note.id === denominationToCount.id;
    const noteFromCountedState =
      counted.length > 0 && counted.find(item => item.id === note.id);
    return (
      <TouchableOpacity
        style={{
          width: '100%',
          height: 50,
          flexDirection: 'row',
          alignItems: 'center',
          padding: 2,
          backgroundColor: isActiveRow ? '#393C49' : '#303137',
        }}
        onPress={() => {
          dispatch(addDenominationToCount(note));
          setDenomination(note);
          setQuantity(0);
        }}>
        <View style={{minWidth: '30%'}}>
          <Text
            style={{
              textAlign: 'center',
              color: colors.white,
              fontSize: 14,
            }}>
            {note.name}
          </Text>
        </View>
        <View style={{minWidth: '30%'}}>
          <Text
            style={{
              textAlign: 'center',
              color: colors.white,
              fontSize: 14,
            }}>
            {`x${noteFromCountedState ? noteFromCountedState.quantity : 0}`}
          </Text>
        </View>
        <View style={{minWidth: '40%'}}>
          <Text
            style={{
              textAlign: 'center',
              color: colors.white,
              fontSize: 14,
            }}>
            {`${
              noteFromCountedState
                ? noteFromCountedState.value * noteFromCountedState.quantity
                : '--'
            }`}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <View style={{flex: 1, backgroundColor: colors.bg_dark}}>
      <TaskBar title="Float Count" canMoveBack={false} />
      <View style={{flex: 1, flexDirection: 'row'}}>
        {/* error modal */}
        {isError && responseMessage && (
          <GPSuccessOrErrorModal
            message={{
              isError: true,
              msg: responseMessage,
              isSuccess: false,
              title: 'Counting Error',
            }}
          />
        )}
        {/* left */}
        <View style={{width: dimentions.col * 10}}>
          <View
            style={{
              width: '80%',
              minHeight: 100,
              backgroundColor: '#303137',
              borderRadius: 6,
              margin: 10,
            }}>
            {/* table head */}
            <View
              style={{
                width: '100%',
                height: 50,
                flexDirection: 'row',
                alignItems: 'center',
                borderBottomColor: '#3E4347',
                borderBottomWidth: 1,
                padding: 2,
              }}>
              <View style={{minWidth: '30%'}}>
                <Text
                  style={{
                    textAlign: 'center',
                    color: colors.white,
                    fontSize: 14,
                  }}>
                  {' '}
                  Notes
                </Text>
              </View>
              <View style={{minWidth: '30%'}}>
                <Text
                  style={{
                    textAlign: 'center',
                    color: colors.white,
                    fontSize: 14,
                  }}>
                  Quantity
                </Text>
              </View>
              <View style={{minWidth: '40%'}}>
                <Text
                  style={{
                    textAlign: 'center',
                    color: colors.white,
                    fontSize: 14,
                  }}>
                  Total
                </Text>
              </View>
            </View>

            {/* table body */}

            {notes &&
              notes.map((note, index) => <CountRow note={note} key={index} />)}
          </View>
        </View>
        {/* right */}
        <View style={{width: dimentions.col * 10}}>
          {/* dial pad */}

          <View style={styles.dial_pad_wrapper}>
            <View style={styles.dial_pad_inner_wrapper}>
              {/* count amount */}

              <Text style={{fontSize: 16, color: '#fff'}}>
                {' '}
                Input the total amount of notes of{' '}
                {`${denominationToCount ? denominationToCount.value : '0.00'}`}
              </Text>
              {/* pad */}
              <View style={styles.dial_pad_container}>
                {dials &&
                  dials.map((item, index) => (
                    <DialButton
                      data={item}
                      key={index}
                      styles={{
                        width: 114,
                        height: 46,
                        margin: 2,
                        backgroundColor: '#303137',
                      }}
                      handlePress={(value: string) => {
                        value === 'delete'
                          ? cancelCount()
                          : denominationToCount &&
                            setDenomination(denominationToCount);
                        handleSettingQuantity(item.value);
                      }}
                    />
                  ))}
              </View>

              {/* button */}
              <View style={styles.button_container}>
                <GPButton
                  title={`Confirm - USh ${counted.reduce((acc, val, arr) => {
                    return acc + val.value * val.quantity;
                  }, 0)}`}
                  styles={{
                    width: '100%',
                    height: 70,
                    backgroundColor: '#0069A5',
                    borderRadius: 3,
                  }}
                  text_styles={{fontSize: 18, fontWeight: '500'}}
                  onPress={() => confirmAndAddCashDrawerOperation()}
                  loading={loading}
                />
              </View>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
};

export default FloatCountingScreen;

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: colors.bg_light,
    alignItems: 'center',
  },
  dial_pad_wrapper: {
    width: 400,
    height: 480,
    alignItems: 'center',
    marginTop: 80, //TODO: center appropriately
  },
  top_container: {
    width: '100%',
    height: 50,
    backgroundColor: '#2A333E',
    flexDirection: 'row',
    alignItems: 'center',
  },

  dial_pad_inner_wrapper: {
    width: '100%',
    height: 430,
    backgroundColor: colors.bg_dark,
    paddingVertical: 15,
    paddingHorizontal: 22,
  },
  dial_amount_container: {
    width: '100%',
    height: 80,
    backgroundColor: colors.bg_light,
    flexDirection: 'row',
  },
  dial_pad_container: {
    width: '100%',
    height: 200,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
    marginVertical: 15,
  },
  button_container: {
    width: '100%',
    height: 90,
  },
});
