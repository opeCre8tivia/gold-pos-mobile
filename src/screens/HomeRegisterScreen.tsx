import {
  SafeAreaView,
  StyleSheet,
  View,
  Modal,
  FlatList,
  Text,
} from 'react-native';
import React, {useEffect} from 'react';
import HomeScreenTaskBar from '../components/HomeScreenTaskBar';
import SalesPanel from '../components/SalesPanel';
import {colors, dimentions} from '../constants/theme';
import MenuPanel from '../components/MenuPanel';
import {getAsyncDevices} from '../utils/getAsyncDevices';
import {useDispatch, useSelector} from 'react-redux';
import {
  _addConnectedDevice,
  _addConnectedDeviceAddress,
} from '../redux/features/printer.slice';
import {RootState} from '../redux/store';
import {BluetoothManager} from '@brooons/react-native-bluetooth-escpos-printer';
import {_getAllSettings} from '../redux/actions';
import {Toast} from 'react-native-toast-message/lib/src/Toast';
import {toastConfig} from '../../App';
import {_listCustomers} from '../redux/actions/customers.actions';
import GPCheckbox from '../components/GPCheckbox';
import {
  hideAddCustomerToSaleModal,
  setCustomerToAddToSale,
} from '../redux/features/customer.slice';

const HomeRegisterScreen = () => {
  const dispatch = useDispatch<any>();
  const {connected_device} = useSelector(
    (state: RootState) => state.printerSlice,
  );
  const {customers, addCustomerToSaleModalIsVisible, customerToAddToSale} =
    useSelector((state: RootState) => state.customerSlice);

  /**
   *  get all business settings
   */

  useEffect(() => {
    dispatch(_getAllSettings());
    dispatch(_listCustomers());
  }, []);

  /**
   *  enable bluetooth if it's off
   *
   */
  useEffect(() => {
    (async function () {
      try {
        let _isBluetoothOn = await BluetoothManager.checkBluetoothEnabled();

        if (_isBluetoothOn === false) {
          await BluetoothManager.enableBluetooth();
        }
      } catch (error) {
        console.log(error);
      }
    })();
  }, []);

  /**
   * get previous connected device from asyncstorage and set to redux
   *
   */
  useEffect(() => {
    (async function () {
      try {
        let _device = await getAsyncDevices();
        _device && dispatch(_addConnectedDevice(_device));
      } catch (error) {
        console.log(error);
      }
    })();
  }, []);

  /**
   * try to re-connect with device if within reach
   * fail silently
   */

  useEffect(() => {
    (async function () {
      try {
        if (connected_device) {
          let _conn = await BluetoothManager.connect(connected_device.address);
          if (typeof _conn === 'string') {
            dispatch(_addConnectedDeviceAddress(connected_device.address));
          }
        }
      } catch (error) {
        console.log(error);
      }
    })();
  }, [connected_device]);

  /**
   * monitoring adding cutomer to sale
   */

  useEffect(() => {
    dispatch(hideAddCustomerToSaleModal());
  }, [customerToAddToSale]);

  return (
    <SafeAreaView style={{flex: 1}}>
      <HomeScreenTaskBar />

      <View style={styles.content}>
        <View style={styles.sales}>
          <SalesPanel />
        </View>

        <View style={styles.menu}>
          <MenuPanel />
        </View>
      </View>

      {/* add customer Modal */}

      <Modal
        visible={addCustomerToSaleModalIsVisible}
        transparent={true}
        animationType="fade">
        <View
          style={{
            flex: 1,
            backgroundColor: 'rgba(0,0,0,0.9)',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View
            style={{
              backgroundColor: colors.bg_dark,
              width: dimentions.col * 16,
              height: dimentions.row * 18,
            }}>
            <View
              style={{
                width: '100%',
                height: 80,
                backgroundColor: colors.btn_gray,
                justifyContent: 'center',
                alignItems: 'center',
                marginBottom: 20,
              }}>
              <Text style={{color: '#ffffff', fontWeight: '600', fontSize: 16}}>
                Select Customer
              </Text>
            </View>

            <FlatList
              data={customers}
              showsVerticalScrollIndicator={false}
              renderItem={({item, index}) => (
                <View
                  style={{
                    flexDirection: 'row',
                    width: '100%',
                    paddingHorizontal: 30,
                    height: 60,
                    justifyContent: 'space-between',
                  }}>
                  <Text style={{color: '#ffffff'}}>{item.first_name}</Text>
                  <Text style={{color: '#ffffff'}}>
                    {item.phone_number_primary}
                  </Text>
                  <Text style={{color: '#ffffff'}}>{item.email}</Text>

                  <GPCheckbox
                    isChecked={
                      customerToAddToSale && item.id === customerToAddToSale.id
                        ? true
                        : false
                    }
                    onPress={isChecked => {
                      dispatch(setCustomerToAddToSale(item));
                    }}
                  />
                </View>
              )}
            />
          </View>
        </View>
        <Toast config={toastConfig} />
      </Modal>
    </SafeAreaView>
  );
};

export default HomeRegisterScreen;

const styles = StyleSheet.create({
  content: {
    width: '100%',
    height: '100%',
    flexDirection: 'row',
  },
  sales: {
    width: dimentions.col * 8,
    height: dimentions.row * 17,
  },
  menu: {
    width: dimentions.col * 12,
    backgroundColor: colors.bg_dark,
    height: dimentions.row * 17,
    overflow: 'hidden',
  },
});
