import {StyleSheet, Text, View, Pressable} from 'react-native';
import React, {useEffect, useState} from 'react';
import {colors} from '../constants/theme';
import BackArrowIcon from '../assets/svgIcons/backArrow.icon';
import {useRoute, useNavigation} from '@react-navigation/native';
import {CustomerType} from '../types/main';
import CustomerTransactionList from '../components/CustomerTransactionList';
import CustomerInfo from '../components/CustomerInfo';
import {addCommasToPrice} from '../utils/addCommasToPrice';
import {useAppDispatch} from '../redux/store';
import {_listCustomers} from '../redux/actions/customers.actions';

enum TabKeys {
  keyOne = 1,
  keyTwo = 2,
}

const tabs = [
  {
    key: TabKeys.keyOne,
    content: 'Transactions List',
  },
  {
    key: TabKeys.keyTwo,
    content: 'Customer Details',
  },
];

const CustomerDetails = () => {
  const {customer}: any = useRoute().params;
  const navigation = useNavigation();
  const dispatch = useAppDispatch();

  const [customerDetails, setCustomerDetails] = useState<CustomerType | null>(
    null,
  );

  const [openSalesTotal, setOpenSalesTotal] = useState<number>(0);
  const [activeKey, setActiveKey] = useState<TabKeys>(TabKeys.keyOne);

  useEffect(() => {
    if (customer) {
      setCustomerDetails(customer);
      let _customer: CustomerType = customer;
      if (_customer.parkedSales) {
        let _openSales = _customer.parkedSales.reduce((acc, sale, arr) => {
          return acc + sale.total;
        }, 0);

        setOpenSalesTotal(_openSales);
      }
    }
  }, [customer]);

  return (
    <View style={{flex: 1, backgroundColor: colors.black, padding: 30}}>
      <View
        style={{
          width: '100%',
          height: 100,
          backgroundColor: colors.btn_blue,
          borderTopLeftRadius: 20,
          borderTopRightRadius: 20,
        }}>
        <View
          style={{
            width: '100%',
            height: 52,
            borderBottomColor: '#3E4347',
            borderBottomWidth: 1,
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            paddingHorizontal: 30,
          }}>
          {/* name */}
          <Pressable
            onPress={() => navigation.goBack()}
            style={{
              flexDirection: 'row',
              width: 400,
              alignItems: 'center',
            }}>
            <View style={{width: 12, height: 12, marginHorizontal: 10}}>
              <BackArrowIcon fill={colors.orange} width={14} height={14} />
            </View>
            <Text
              style={{fontWeight: '500', fontSize: 14, color: colors.white}}>
              {customerDetails && customerDetails.first_name}
            </Text>
          </Pressable>

          {/* accounts */}
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'flex-end',
              flex: 1,
            }}>
            <Text
              style={{
                fontWeight: '500',
                fontSize: 14,
                color: colors.white,
                marginHorizontal: 10,
              }}>
              Open
            </Text>
            <Text
              style={{fontWeight: '500', fontSize: 14, color: colors.white}}>
              Ush {addCommasToPrice(openSalesTotal)}
            </Text>
          </View>
        </View>
        <View
          style={{
            width: '100%',
            height: 52,
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            paddingHorizontal: 30,
          }}>
          {/* accounts */}
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'flex-end',
              flex: 1,
            }}>
            <Text
              style={{
                fontWeight: '500',
                fontSize: 14,
                color: colors.white,
                marginHorizontal: 10,
              }}>
              Overdue
            </Text>
            <Text
              style={{fontWeight: '500', fontSize: 14, color: colors.orange}}>
              Ush {addCommasToPrice(openSalesTotal)}
            </Text>
          </View>
        </View>
      </View>

      <View
        style={{
          width: '100%',
          height: 60,
          borderBottomColor: '#3E4347',
          borderBottomWidth: 1,
          flexDirection: 'row',
          alignItems: 'flex-end',
          justifyContent: 'flex-start',
        }}>
        {tabs.map((tab, index) => {
          const isActive = tab.key === activeKey;
          return (
            <View style={{flex: 1}} key={index}>
              {activeKey && (
                <Pressable
                  // key={index}
                  onPress={() => setActiveKey(tab.key)}
                  style={{
                    borderBottomColor: isActive ? colors.orange : 'transparent',
                    borderBottomWidth: 4,
                    width: 200,
                    height: 40,
                    justifyContent: 'center',
                    marginRight: 20,
                  }}>
                  <Text style={{color: colors.white, fontSize: 16}}>
                    {tab.content}
                  </Text>
                </Pressable>
              )}
            </View>
          );
        })}
      </View>

      {/* details */}
      <View style={{flex: 1}}>
        {activeKey === TabKeys.keyOne
          ? customerDetails && (
              <CustomerTransactionList customer={customerDetails} />
            )
          : customerDetails && <CustomerInfo customer={customerDetails} />}
      </View>
    </View>
  );
};

export default CustomerDetails;

const styles = StyleSheet.create({});
