import {
  FlatList,
  Image,
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  Pressable,
  TouchableOpacity,
  ScrollView,
  Modal,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import TaskBar from '../components/TaskBar';
import {colors, dimentions} from '../constants/theme';
import GPSearchInput from '../components/GPSearchInput';
import ReceiptListItem from '../components/ReceiptListItem';
import {useDispatch, useSelector} from 'react-redux';
import {RootState} from '../redux/store';
import {
  _getDailySale,
  _getDailySales,
  _getReceiptVoidingReasons,
  _voidAsale,
} from '../redux/actions';
import {Sale, SalesItem} from '../types/main';
import {_addSelectedSale} from '../redux/features/printer.slice';
import ReceiptActionButton from '../components/ReceiptActionButton';
import {printReceipt} from '../utils/printReceipt';
import PenIcon from '../assets/svgIcons/pen.icon';
import LeftCurvedArrowIcon from '../assets/svgIcons/leftCurvedArrow.icon';
import BinIcon from '../assets/svgIcons/bin.icon';
import MoneyIcon from '../assets/svgIcons/money.icon';
import PrinterIcon from '../assets/svgIcons/printer.icon';
import EnvelopeIcon from '../assets/svgIcons/envelope.icon';
import {useNavigation} from '@react-navigation/native';
import GPCheckbox from '../components/GPCheckbox';
import GPButton from '../components/GPButton';
import {Toast} from 'react-native-toast-message/lib/src/Toast';
import {toastConfig} from '../../App';
import {clearOrderState} from '../redux/features/order.slice';
import {_sendEmail} from '../redux/actions/emailing.actions';

const RecieptScreen = () => {
  const dispatch = useDispatch<any>();
  const navigation = useNavigation();

  const {dailySales, selectedSale, businessSettings, get_sale_loading} =
    useSelector((state: RootState) => state.printerSlice);

  const {servedBy} = useSelector((state: any) => state.directSaleSlice);
  const {outlet} = useSelector((state: RootState) => state.outletSlice);
  const {allSettings} = useSelector((state: RootState) => state.settingsSlice);
  const {voidingReasons, parkError, parkSuccess, parkResponseMessage} =
    useSelector((state: RootState) => state.orderSlice);

  const [showActionModal, setShowActionModal] = useState<boolean>(false);
  const [showVoidingModal, setShowVoidingModal] = useState<boolean>(false);
  const [orderId, setOrderId] = useState<string>();
  const [localeTimeOfSale, setLocaleTimeOfSale] = useState<
    Date | string | null
  >(null);

  const [selectedReason, setSelectedReason] = useState<number | null>(null);

  //TODO move reaceipt to re-usable component

  /**
   * fetch all daily sales on screen load
   */
  // useEffect(() => {
  //   dispatch(_getDailySales());
  // }, []);

  /**
   * fetch all daily sales when component comes to view
   */

  useEffect(() => {
    const unSubscribe = navigation.addListener('focus', () => {
      dispatch(_getDailySales());
      dispatch(_getReceiptVoidingReasons());
    });

    return unSubscribe;
  }, []);

  /**
   * Select the first sale in the array by default
   */
  useEffect(() => {
    if (dailySales && !selectedSale) {
      dispatch(_addSelectedSale(dailySales[0]));
    }
  }, [dailySales]);

  /**
   * convert time of sale to Locale string
   */
  useEffect(() => {
    let _date = selectedSale?.createdAt && new Date(selectedSale?.createdAt);
    _date && setLocaleTimeOfSale(_date.toLocaleString());
  }, [selectedSale]);

  /**
   *
   */

  useEffect(() => {
    if (parkSuccess) {
      Toast.show({
        type: 'goldposgreen',
        text1: parkResponseMessage
          ? parkResponseMessage
          : 'Operation Successfull',
        position: 'top',
        topOffset: 10,
      });

      setTimeout(() => {
        dispatch(_getDailySales());
        dispatch(clearOrderState());
      }, 4000);
    }
    if (parkError) {
      Toast.show({
        type: 'goldposred',
        text1: parkResponseMessage
          ? parkResponseMessage
          : 'Operation Successfull',
        position: 'top',
        topOffset: 10,
      });

      setTimeout(() => {
        dispatch(clearOrderState());
      }, 4000);
    }
  }, [parkError, parkSuccess]);

  /**
   * get sale by order id
   */

  const getByOrderId = (id: number) => {
    if (typeof id !== 'number') return;

    dispatch(_getDailySale(id));
  };

  /* voiding an order */

  const handleVoidingSale = () => {
    if (selectedReason && selectedSale && selectedSale.id) {
      dispatch(_voidAsale({saleId: selectedSale.id, reasonId: selectedReason}));
    }
  };

  const handlePrintReceipt = async () => {
    try {
      //TODO : notify user when business settings are null
      let _sale = selectedSale;
      let _settings = allSettings;

      let res =
        _sale &&
        _settings &&
        (await printReceipt(_sale, _settings, outlet, servedBy));
    } catch (error) {
      console.log(error);
    }
  };

  const Col = ({
    a,
    b,
    c,
    d,
    colSizes = {a: 1, b: 2, c: 1, d: 1},
  }: {
    a: string | number;
    b?: string | number;
    c?: string | number;
    d?: string | number;
    colSizes?: {a: number; b: number; c: number; d: number};
  }) => {
    return (
      <View style={styles.col}>
        {/* 0.5 | 2.5 | 1| 1 */}
        <Text
          style={{
            ...styles.normal_text,
            width: dimentions.col * colSizes?.a,
            textAlign: 'left',
          }}>
          {a}
        </Text>
        <Text
          style={{
            ...styles.normal_text,
            width: dimentions.col * colSizes.b,
            textAlign: 'left',
          }}>
          {b}
        </Text>
        {allSettings?.showTotalUnitPrice && (
          <Text
            style={{
              ...styles.normal_text,
              width: dimentions.col * colSizes.c,
              textAlign: 'left',
            }}>
            {c}
          </Text>
        )}
        <Text
          style={{
            ...styles.normal_text,
            width: dimentions.col * colSizes.d,
            textAlign: 'left',
          }}>
          {d}
        </Text>
      </View>
    );
  };

  const Space = ({size = 2}: {size?: number}) => (
    <View style={{width: '100%', height: dimentions.row * size}}></View>
  );

  return (
    <SafeAreaView style={{flex: 1}}>
      <TaskBar title="Receipts" canMoveBack={false} />

      <View style={styles.main_wrapper}>
        <View style={styles.left_wrapper}>
          <View style={{height: dimentions.row * 1.5, paddingHorizontal: 15}}>
            <GPSearchInput
              placeholder="Search by order number"
              onChangeText={value => setOrderId(value)}
              onSearchIconClick={() => {
                orderId && getByOrderId(parseInt(orderId));
              }}
              loading={get_sale_loading}
            />
          </View>

          <View style={{height: dimentions.row * 13}}>
            {/* display sales that happened same day as current day */}
            {dailySales.length > 0 ? (
              <FlatList
                data={dailySales}
                showsVerticalScrollIndicator={false}
                renderItem={({item, index}) => (
                  <ReceiptListItem reciept={item} />
                )}
              />
            ) : (
              <View
                style={{
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    fontSize: 25,
                    textAlign: 'center',
                    fontWeight: 'bold',
                    color: '#7a7a7a',
                  }}>
                  NO RECEIPTS FOUND
                </Text>
              </View>
            )}
          </View>
        </View>

        <View style={styles.right_wrapper}>
          <ScrollView style={{flex: 1}} showsVerticalScrollIndicator={false}>
            {dailySales.length > 0 && (
              <View
                style={{
                  backgroundColor: '#fff',
                  width: dimentions.col * 5,
                  height: '100%',
                  flex: 1,
                  padding: 10,
                }}>
                {/* header */}
                <View style={styles.receipt_biz_name}>
                  {allSettings?.tradeName && (
                    <Text
                      style={{
                        textAlign: 'center',
                        fontSize: 22,
                        color: colors.black,
                        fontWeight: 'bold',
                      }}>
                      {allSettings?.tradeName?.toUpperCase()}
                    </Text>
                  )}
                </View>

                <Text style={styles.normal_text}>{allSettings?.street}</Text>
                <Text style={styles.normal_text}>{allSettings?.contact}</Text>

                {allSettings?.tinNumber && (
                  <Text
                    style={
                      styles.normal_text
                    }>{`TIN:${allSettings?.tinNumber}`}</Text>
                )}
                {allSettings?.vatNumber && (
                  <Text
                    style={
                      styles.normal_text
                    }>{`VAT:${allSettings?.vatNumber}`}</Text>
                )}

                {/* ----space */}
                <Space size={0.4} />

                <View style={{margin: 10}}>
                  <Text
                    style={{
                      fontSize: 20,
                      color: colors.black,
                      textAlign: 'center',
                      fontWeight: 'bold',
                    }}>
                    Cash Receipt
                  </Text>
                </View>

                <Space size={0.4} />

                {/* time and outlet */}
                <Col
                  a={`${localeTimeOfSale && localeTimeOfSale}`}
                  b=""
                  c=""
                  d={`${outlet?.name}`}
                  colSizes={{a: 3, b: 0.5, c: 0.5, d: 1}}
                />
                {/* order number */}

                <Col
                  a={`Order #${selectedSale?.orderNumber}`}
                  b=""
                  c=""
                  d=""
                  colSizes={{a: 3, b: 0.5, c: 0.5, d: 1}}
                />
                {/* Served by */}
                <Col
                  a={`Served By:${selectedSale?.employee?.first_name}`}
                  b=""
                  c=""
                  d=""
                  colSizes={{a: 3, b: 0.5, c: 0.5, d: 1}}
                />

                {/* Table header */}
                {allSettings?.showTableHeader && (
                  <Col a="Qty" b="Item" c="Price" d="Amount" />
                )}

                {/* list of sales items */}
                {selectedSale && (
                  <>
                    {selectedSale &&
                      selectedSale.menuItems.map((item, index: number) => {
                        let _item: SalesItem = JSON.parse(item);

                        return (
                          <Col
                            a={_item.qty}
                            b={_item.name}
                            c={_item.price}
                            d={_item.total}
                            key={index}
                          />
                        );
                      })}
                  </>
                )}

                <Space size={0.5} />

                {/* discounts */}

                {/* sub total */}
                <Col a="Sub total" b="" c="" d="" />

                {/* tax */}
                <Col a="Tax" b="" c="" d="" />

                {/* total */}
                <Col a="Total" b="" c="" d={selectedSale?.total} />

                {/* Type of payment */}
                <Col
                  a="Cash"
                  b=""
                  c=""
                  d={selectedSale?.amountRecieved}
                  colSizes={{a: 3, b: 0.5, c: 0.5, d: 1}}
                />
                {/* Change */}
                <Col
                  a="Change"
                  b=""
                  c=""
                  d={selectedSale?.change}
                  colSizes={{a: 3, b: 0.5, c: 0.5, d: 1}}
                />

                <Space size={0.5} />

                {/* Receipt messaging */}
                {allSettings?.showReceiptMessaging && (
                  <Text
                    style={{
                      textAlign: 'center',
                      fontSize: 10,
                      color: colors.black,
                    }}>
                    {' '}
                    {allSettings?.recieptMessaging}
                  </Text>
                )}

                <Space size={0.3} />

                {/* gold pos default */}
                <Text
                  style={{
                    textAlign: 'center',
                    fontSize: 8,
                    color: colors.black,
                  }}>
                  {' '}
                  Powered by: www.goldpointofsale.com
                </Text>

                {/* ----space--- */}
              </View>
            )}
          </ScrollView>

          {/* green menu icon */}
          <TouchableOpacity
            style={{
              width: 80,
              height: 80,
              backgroundColor: '#279870',
              position: 'absolute',
              bottom: 26,
              right: 15,
              justifyContent: 'center',
              alignItems: 'center',
            }}
            onPress={() =>
              selectedSale && setShowActionModal(!showActionModal)
            }>
            <Image source={require('../assets/icons/menuIcon.png')} />
          </TouchableOpacity>

          {/* modal */}
          {showActionModal && (
            <View style={styles.modal}>
              {/* title */}
              <Text
                style={{
                  color: '#fff',
                  fontSize: 20,
                  textAlign: 'center',
                  fontWeight: 'bold',
                }}>
                {' '}
                Actions{' '}
              </Text>

              <View style={{}}>
                {/*  */}

                <View style={{flexDirection: 'row'}}>
                  <ReceiptActionButton
                    onPress={() => console.log('#')}
                    text="VOID PAYMENTS"
                    Icon={PenIcon}
                  />
                  <ReceiptActionButton
                    onPress={() => console.log('#')}
                    text="REFUND RECEIPT"
                    Icon={LeftCurvedArrowIcon}
                  />
                  <ReceiptActionButton
                    onPress={() => setShowVoidingModal(true)}
                    text="VOID RECEIPT"
                    Icon={BinIcon}
                  />
                </View>

                {/*  */}
                <View style={{flexDirection: 'row'}}>
                  <ReceiptActionButton
                    onPress={() => console.log('#')}
                    text="CHANGE PAYMENT"
                    Icon={MoneyIcon}
                  />
                  <ReceiptActionButton
                    onPress={() => selectedSale && handlePrintReceipt()}
                    text="PRINT"
                    Icon={PrinterIcon}
                    iconStyles={{fill: colors.btn_blue}}
                  />
                  <ReceiptActionButton
                    onPress={() => {
                      // dispatch(_sendEmail())
                    }}
                    text="EMAIL"
                    Icon={EnvelopeIcon}
                  />
                </View>
              </View>

              {/* close  */}

              <TouchableOpacity
                onPress={() => setShowActionModal(!showActionModal)}
                style={{
                  width: dimentions.col * 2,
                  height: dimentions.row * 2,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    color: '#fff',
                    fontSize: 20,
                    textAlign: 'center',
                    fontWeight: 'bold',
                  }}>
                  Close
                </Text>
              </TouchableOpacity>
            </View>
          )}
        </View>
      </View>

      {/* voiding modal */}

      <Modal
        visible={showVoidingModal}
        animationType="fade"
        transparent={true}
        style={{flex: 1}}>
        <View
          style={{
            flex: 1,
            backgroundColor: 'rgba(0,0,0,0.9)',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View
            style={{
              backgroundColor: '#17181C',
              width: dimentions.col * 8,
              height: dimentions.row * 13,
            }}>
            <View
              style={{
                width: '100%',
                height: dimentions.row * 2,
                backgroundColor: '#2A333E',
                flexDirection: 'row',
                alignItems: 'center',
                paddingHorizontal: 12,
              }}>
              <Pressable
                style={{flex: 1}}
                onPress={() => setShowVoidingModal(false)}>
                <Text style={{color: '#7A7A7A'}}>Close</Text>
              </Pressable>

              <Text
                style={{
                  flex: 1,
                  textAlign: 'center',
                  color: '#fff',
                  fontWeight: '600',
                  fontSize: 16,
                }}>
                {' '}
                Void Order
              </Text>

              <View style={{flex: 1}}></View>
            </View>

            {/*  */}
            <View
              style={{
                height: 46,
                width: '100%',
                justifyContent: 'center',
                alignItems: 'center',
                borderBottomColor: '#7a7a7a',
                borderBottomWidth: 1,
              }}>
              <Text style={{color: '#fff', fontSize: 12, textAlign: 'center'}}>
                Select a reason to void
              </Text>
            </View>

            <View style={{paddingHorizontal: 20, marginTop: 20}}>
              <FlatList
                data={voidingReasons}
                showsVerticalScrollIndicator={false}
                renderItem={({item, index}) => {
                  const isSelected = item.id === selectedReason;

                  return (
                    <View
                      key={index}
                      style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        backgroundColor: '#2A333E',
                        borderBottomColor: '#3E4347',
                        borderBottomWidth: 1,
                        padding: 14,
                      }}>
                      <GPCheckbox
                        onPress={isChecked => {
                          if (!isChecked) {
                            setSelectedReason(item.id);
                          }
                          if (isChecked) {
                            setSelectedReason(null);
                          }
                        }}
                        isChecked={isSelected}
                      />
                      <Text
                        style={{
                          paddingHorizontal: 4,
                          fontWeight: '400',
                          color: '#fff',
                          fontSize: 18,
                        }}>
                        {item.reason}
                      </Text>
                    </View>
                  );
                }}
              />

              {/*  */}
              <GPButton
                title="Done"
                onPress={() => handleVoidingSale()}
                styles={{
                  height: dimentions.row * 1.5,
                  width: '100%',
                  backgroundColor: colors.blue,
                  marginTop: 14,
                }}
              />
            </View>
          </View>
        </View>

        {/* toast instance */}

        <Toast config={toastConfig} />
      </Modal>
    </SafeAreaView>
  );
};

export default RecieptScreen;

const styles = StyleSheet.create({
  main_wrapper: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    backgroundColor: colors.bg_dark,
  },
  left_wrapper: {
    flex: 1,
    height: '100%',
    alignItems: 'center',
    padding: 25,
  },
  right_wrapper: {
    flex: 1,
    height: '100%',
    alignItems: 'center',
    padding: 25,
    position: 'relative',
  },
  modal: {
    width: dimentions.col * 10,
    height: dimentions.row * 16,
    backgroundColor: 'rgba(0,0,0,0.8)',
    position: 'absolute',
    right: 0,
    top: 0,
    paddingVertical: 30,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  receipt_biz_name: {
    width: '100%',
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
  normal_text: {
    textAlign: 'center',
    fontSize: 13,
    color: colors.black,
  },
  col: {
    width: '100%',
    height: 30,
    paddingHorizontal: 8,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});
