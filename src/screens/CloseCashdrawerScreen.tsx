import {StyleSheet, Text, View, TextInput} from 'react-native';
import React, {useEffect, useState} from 'react';
import {colors, dimentions} from '../constants/theme';
import {useSelector} from 'react-redux';
import {RootState, useAppDispatch} from '../redux/store';
import GPButton from '../components/GPButton';
import {useNavigation} from '@react-navigation/native';
import {Toast} from 'react-native-toast-message/lib/src/Toast';
import {clearCashDrawerState} from '../redux/features/cashdrawers.slice';
import {
  _closeCashDrawerOperations,
  _getCashDrawerOperations,
} from '../redux/actions/cashdrawer.actions';

type Props = {};

const CloseCashdrawerScreen = (props: Props) => {
  const dispatch = useAppDispatch();
  const navigation = useNavigation();
  const {drawerOperationBeingReviewed} = useSelector(
    (state: RootState) => state.shiftreviewSlice,
  );

  const {isError, isSuccess, responseMessage, loading} = useSelector(
    (state: RootState) => state.cashdrawersSlice,
  );

  const {currentOperator} = useSelector(
    (state: RootState) => state.clockingSlice,
  );
  const [actualInput, setActualInput] = useState<string>('');
  const [comment, setComment] = useState<string>('');
  const [cashDiffrence, setCashDifference] = useState<number>(0);

  useEffect(() => {
    if (actualInput.length > 0) {
      let _numberActual = parseInt(actualInput);
      getCashDifference(_numberActual);
    }
    if (actualInput.length === 0) {
      let _numberActual = 0;
      getCashDifference(_numberActual);
    }
  }, [actualInput]);

  const getCashDifference = (inputValue: number) => {
    if (drawerOperationBeingReviewed) {
      let _diff = inputValue - drawerOperationBeingReviewed.currentBalance;
      setCashDifference(_diff);
    }
  };

  useEffect(() => {
    if (isSuccess) {
      Toast.show({
        type: 'goldposgreen',
        position: 'top',
        topOffset: 10,
        text1: responseMessage ? responseMessage : 'Operation Successfull',
      });

      /**
       * re load the cashdrawer state
       */

      dispatch(_getCashDrawerOperations(currentOperator.id));

      setTimeout(() => {
        dispatch(clearCashDrawerState());
        setActualInput('');
        setComment('');
        navigation.goBack();
      }, 4000);
    }

    if (isError) {
      Toast.show({
        type: 'goldposred',
        position: 'top',
        topOffset: 10,
        text1: responseMessage ? responseMessage : 'Something has gone wrong ',
      });

      setTimeout(() => {
        dispatch(clearCashDrawerState());
      }, 4000);
    }
  }, [isError, isSuccess]);

  const handleClosingCashDrawer = (id: number) => {
    dispatch(_closeCashDrawerOperations({id}));
  };

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: colors.black,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <View
        style={{
          width: dimentions.col * 8,
          height: dimentions.row * 2,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: colors.btn_blue,
        }}>
        <Text style={{color: colors.white, fontWeight: '700', fontSize: 20}}>
          Close Out Balance
        </Text>
      </View>
      <View
        style={{
          width: dimentions.col * 8,
          height: dimentions.row * 8,
          justifyContent: 'flex-start',
          alignItems: 'center',
          backgroundColor: colors.bg_light,
          padding: 20,
        }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            width: '100%',
          }}>
          <View style={{width: '50%'}}>
            <Text style={{color: '#7A7A7A', fontSize: 14, fontWeight: '400'}}>
              Cash Expected
            </Text>
            <Text
              style={{color: colors.white, fontSize: 14, fontWeight: '400'}}>
              {drawerOperationBeingReviewed?.currentBalance}
            </Text>
          </View>
          <View
            style={{
              alignItems: 'flex-start',
              width: '50%',
            }}>
            <Text
              style={{color: colors.gp_green, fontSize: 14, fontWeight: '400'}}>
              Actual Cash
            </Text>
            <TextInput
              onChangeText={(val: any) => setActualInput(val)}
              value={actualInput}
              placeholder="Ush 0"
              placeholderTextColor="#ffffff"
              keyboardType="number-pad"
              style={{
                borderBottomColor: '#7a7a7a',
                borderBottomWidth: 1,
                width: '100%',
                color: colors.white,
              }}
            />
          </View>
        </View>
        {/* row 2 */}
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            width: '100%',
          }}>
          <View style={{width: '50%'}}>
            <Text style={{color: '#7A7A7A', fontSize: 14, fontWeight: '400'}}>
              {cashDiffrence < 0
                ? 'Cash short'
                : cashDiffrence > 0
                ? 'Cash Over'
                : 'No Diffrence'}
            </Text>
            <Text
              style={{color: colors.white, fontSize: 14, fontWeight: '400'}}>
              {cashDiffrence}
            </Text>
          </View>
          <View
            style={{
              alignItems: 'flex-start',
              width: '50%',
            }}>
            <Text
              style={{color: colors.gp_green, fontSize: 14, fontWeight: '400'}}>
              Comment
            </Text>
            <TextInput
              onChangeText={(val: any) => setComment(val)}
              value={comment}
              placeholder="---"
              placeholderTextColor="#ffffff"
              style={{
                borderBottomColor: '#7a7a7a',
                borderBottomWidth: 1,
                width: '100%',
                color: colors.white,
              }}
            />
          </View>
        </View>

        <View
          style={{
            width: '100%',
            height: dimentions.row * 2,
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'flex-end',
          }}>
          <View style={{justifyContent: 'center', height: 50}}>
            <Text
              style={{
                width: '100%',
                fontSize: 14,
                fontWeight: '500',
                color: colors.orange,
              }}>
              Count Float
            </Text>
          </View>
          <View style={{flexDirection: 'row'}}>
            <GPButton
              onPress={() => navigation.goBack()}
              title="Cancel"
              styles={{
                width: dimentions.col * 2.5,
                height: dimentions.row * 1.2,
                backgroundColor: '#303137',
              }}
            />
            <GPButton
              loading={loading}
              onPress={() => {
                if (
                  drawerOperationBeingReviewed &&
                  drawerOperationBeingReviewed.id
                ) {
                  handleClosingCashDrawer(drawerOperationBeingReviewed.id);
                }
              }}
              title="Close Drawer"
              styles={{
                width: dimentions.col * 2.5,
                height: dimentions.row * 1.2,
                backgroundColor: colors.blue,
              }}
            />
          </View>
        </View>
      </View>
    </View>
  );
};

export default CloseCashdrawerScreen;

const styles = StyleSheet.create({});
