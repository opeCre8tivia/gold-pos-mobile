import React, {useEffect} from 'react';
import {Text, View, StyleSheet, FlatList} from 'react-native';
import TaskBar from '../components/TaskBar';
import {colors} from '../constants/theme';
import {useDispatch, useSelector} from 'react-redux';
import {RootState} from '../redux/store';
import {CashDrawer} from '../types/main';
import {_getCashDrawers} from '../redux/actions/cashdrawer.actions';
import CashDrawerListItem from '../components/CashDrawerListItem';

type Props = {};

const CashDrawerListScreen = (props: Props) => {
  const {isError, isSuccess, responseMessage, cashDrawers} = useSelector(
    (state: RootState) => state.cashdrawersSlice,
  );

  const dispatch = useDispatch<any>();

  useEffect(() => {
    dispatch(_getCashDrawers());
  }, []);

  return (
    <View style={{flex: 1, backgroundColor: colors.bg_dark}}>
      <TaskBar canMoveBack={false} title="CASH DRAWERS" />
      <View style={styles.innerWrapper}>
        <Text style={styles.heading}>
          Select Cash Drawer To start float count{' '}
        </Text>
        <Text style={styles.text}>
          Each cash drawer at your establishment will require an individual
          float count{' '}
        </Text>

        <View
          style={{
            width: '100%',
            marginTop: 20,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          {cashDrawers && (
            <FlatList
              data={cashDrawers}
              showsVerticalScrollIndicator={false}
              renderItem={({item, index}) => (
                <CashDrawerListItem drawer={item} key={index} />
              )}
            />
          )}
        </View>
      </View>
    </View>
  );
};

export default CashDrawerListScreen;

const styles = StyleSheet.create({
  innerWrapper: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingVertical: 20,
    paddingHorizontal: 10,
  },
  heading: {
    fontSize: 25,
    fontWeight: '600',
    color: '#ffffff',
    textAlign: 'center',
  },
  text: {
    fontSize: 14,
    fontWeight: '400',
    color: '#ffffff',
    textAlign: 'center',
    width: '30%',
  },
});
