import {StyleSheet, Text, View, SafeAreaView} from 'react-native';
import React from 'react';
import ComingSoonComponent from '../components/ComingSoonComponent';
import CustomerListAndSearch from '../components/CustomerListAndSearch';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {
  CUSTOMERS_DETAILS_SCREEN,
  CUSTOMERS_LIST_SCREEN,
} from '../constants/screenNames';
import CustomerList from './CustomerList';
import CustomerDetails from './CustomerDetails';

const Customers = () => {
  const CustomerStack = createNativeStackNavigator();
  return (
    <SafeAreaView style={{flex: 1}}>
      <CustomerStack.Navigator
        initialRouteName={CUSTOMERS_LIST_SCREEN}
        screenOptions={{headerShown: false}}>
        <CustomerStack.Screen
          name={CUSTOMERS_LIST_SCREEN}
          component={CustomerList}
        />
        <CustomerStack.Screen
          name={CUSTOMERS_DETAILS_SCREEN}
          component={CustomerDetails}
        />
      </CustomerStack.Navigator>
    </SafeAreaView>
  );
};

export default Customers;

const styles = StyleSheet.create({});
