import {
  ActivityIndicator,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {colors} from '../constants/theme';
import GPButton from '../components/GPButton';
import DeviceInfo from 'react-native-device-info';
import {useSelector, useDispatch} from 'react-redux';
import {useNavigation} from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {_linkDevice} from '../redux/actions';

import {Camera, useCameraDevices} from 'react-native-vision-camera';
import {useScanBarcodes, BarcodeFormat} from 'vision-camera-code-scanner';
import {clearLinkingMessages} from '../redux/features/linking.slice';

const LinkDeviceScreen = () => {
  const {isError, isSuccess, error_msg, loading, token} = useSelector(
    (state: any) => state.linkingSlice,
  );
  const dispatch = useDispatch<any>();

  const [showScanner, setShowScanner] = useState(false);
  const [linkInfo, setLinkInfo] = useState<any | null>(null);

  //vision cam
  const [hasPermission, setHasPermission] = React.useState(false);
  const devices = useCameraDevices();

  const device = devices.back;

  const [frameProcessor, barcodes] = useScanBarcodes([BarcodeFormat.QR_CODE], {
    checkInverted: true,
  });

  React.useEffect(() => {
    (async () => {
      const status = await Camera.requestCameraPermission();
      setHasPermission(status === 'authorized');
    })();
  }, []);

  useEffect(() => {
    barcodes.map((barcode, idx) => {
      !linkInfo && setLinkInfo(barcode.displayValue);
    });
  }, [barcodes]);

  useEffect(() => {
    token && manageLinkingLocally();
  }, [token]);

  useEffect(() => {
    if (linkInfo) {
      setShowScanner(false);
      handleLinking(linkInfo);
    }
  }, [linkInfo]);

  //Reset errors
  useEffect(() => {
    if (isError) {
      setTimeout(() => {
        dispatch(clearLinkingMessages());
      }, 4000);
    }
  }, [isError]);

  //clear link info in case of an error or success
  useEffect(() => {
    if (isError || isSuccess) {
      setLinkInfo(null);
    }
  }, [isError, isSuccess]);

  const handleLinking = async (code: string) => {
    try {
      //close scanner and show loader

      let _ip = await DeviceInfo.getIpAddress();
      let _name = await DeviceInfo.getDeviceName();
      let _codeName = await DeviceInfo.getCodename();
      let _model = DeviceInfo.getModel();
      let _fingerPrint = await DeviceInfo.getFingerprint();
      let _uniqueId = await DeviceInfo.getUniqueId();

      let data = {
        code: code,
        deviceName: _name,
        deviceModel: _model,
        deviceCodeName: _codeName,
        ip: _ip,
        fingerPrint: _fingerPrint,
        uniqueId: _uniqueId,
      };

      // setLinkInfo(null)
      //@ts-ignore
      dispatch(_linkDevice(data));
    } catch (error) {
      console.log(error);
    }
  };

  const manageLinkingLocally = async () => {
    try {
      let _outletToken = await AsyncStorage.getItem('outlet_token');

      if (_outletToken) {
        //TODO: what do we do if device is already linked
      } else {
        let _isSet = await AsyncStorage.setItem(
          'outlet_token',
          JSON.stringify(token),
        );
      }
    } catch (error) {
      console.log(error, 'async error');
    }
  };

  return (
    <View style={styles.main}>
      <>
        <View
          style={{
            width: '100%',
            height: 300,
            justifyContent: 'center',
            alignItems: 'center',
            alignContent: 'center',
          }}>
          {error_msg && (
            <View
              style={{
                width: '60%',
                height: 40,
                borderWidth: 2,
                borderColor: 'red',
                justifyContent: 'center',
                alignItems: 'center',
                marginBottom: 8,
              }}>
              <Text style={{color: '#fff'}}>{error_msg}</Text>
            </View>
          )}
          {showScanner ? (
            <>
              <Text
                style={{
                  color: '#7a7a7a',
                  fontSize: 12,
                  textAlign: 'center',
                  width: '40%',
                  padding: 4,
                  marginBottom: 10,
                }}>
                Please move your camera over the QR Code
              </Text>
              <View
                style={{
                  width: '40%',
                  height: 250,
                  alignSelf: 'center',
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderWidth: 2,
                  borderColor: '#fff',
                  marginBottom: 20,
                }}>
                {device != null && hasPermission && (
                  <>
                    <Camera
                      style={StyleSheet.absoluteFill}
                      device={device}
                      isActive={true}
                      frameProcessor={frameProcessor}
                      frameProcessorFps={5}
                    />
                  </>
                )}
              </View>
            </>
          ) : !showScanner && loading ? (
            <View
              style={{
                width: '100%',
                height: 300,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <ActivityIndicator size={30} color="#fff" />
            </View>
          ) : isSuccess ? (
            <Text
              style={{
                color: '#fff',
                fontSize: 14,
                width: '100%',
                textAlign: 'center',
              }}>
              wait as we prepair your workspace... 0%
            </Text>
          ) : (
            <View>
              <Text style={{color: '#fff'}}>
                CLICK THE BUTTON BELOW TO LINK YOUR DEVICE
              </Text>
            </View>
          )}
        </View>

        <GPButton
          title={showScanner ? 'CLOSE SCANNER' : 'LINK DEVICE'}
          styles={{height: 40, width: '40%'}}
          onPress={() => {
            console.log('BTN PRESS');
            setShowScanner(!showScanner);
          }}
        />
      </>
    </View>
  );
};

export default LinkDeviceScreen;

const styles = StyleSheet.create({
  main: {
    flex: 1,
    height: '100%',
    backgroundColor: colors.bg_dark,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
