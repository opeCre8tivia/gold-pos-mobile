import {
  Image,
  StyleSheet,
  Text,
  View,
  FlatList,
  Modal,
  ToastAndroid,
  ScrollView,
  SafeAreaView,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import {colors, dimentions} from '../constants/theme';
import TaskBar from '../components/TaskBar';
import PaymentOptionButton from '../components/PaymentOptionButton';
import CustomPriceButton from '../components/CustomPriceButton';
import GPButton from '../components/GPButton';
import {useDispatch, useSelector} from 'react-redux';
import {_recordSale} from '../redux/actions';
import {clearPaymentState} from '../redux/features/payment.slice';
import GPSuccessOrErrorModal from '../components/GPSuccessOrErrorModal';
import AmountPaidModal from '../components/AmountPaidModal';
import {
  CUSTOM_AMOUNT_SCREEN,
  HOME_REGISTER_SCREEN,
} from '../constants/screenNames';
import {useNavigation} from '@react-navigation/native';
import {clearDirectSaleState} from '../redux/features/directSale.slice';
import {printReceipt} from '../utils/printReceipt';
import {RootState, useAppDispatch} from '../redux/store';
import {_closeParkedOrder} from '../redux/actions/parking.actions';

interface Item {
  id: number;
  name: string;
  qty: number;
  price: number;
}

interface Props {
  item: Item;
}

const TakingPaymentsScreen = () => {
  const dispatch = useAppDispatch();
  const navigation = useNavigation<any>();
  const {
    addedMenuItemsTotal,
    addedMenuItems,
    servedBy,
    customRecievedAmount,
    fromParkedSale,
  } = useSelector((state: RootState) => state.directSaleSlice);
  const {selectedOrder} = useSelector((state: RootState) => state.orderSlice);

  const {outlet} = useSelector((state: RootState) => state.outletSlice);
  const {loading, isError, isSuccess, payment_error, current_sale} =
    useSelector((state: RootState) => state.paymentSlice);
  const {allSettings} = useSelector((state: RootState) => state.settingsSlice);
  const {currentUsersCashDrawerOperation} = useSelector(
    (state: RootState) => state.cashdrawersSlice,
  );

  const [message, setMessage] = useState({
    isError: false,
    isSuccess: false,
    title: '',
    msg: '',
  });

  const [showPaySuccessModal, setShowPaySuccessModal] = useState(false);
  const [recievedAmount, setRecievedAmount] = useState(0);
  const [change, setChange] = useState(0);
  const [denominations, setDenominations] = useState<any[]>([]);

  /**
   * if the sale is from a parked order
   * we need to update the order status to isOpen=false onSuccess
   */

  useEffect(() => {
    if (isSuccess && fromParkedSale) {
      selectedOrder &&
        selectedOrder.id &&
        dispatch(_closeParkedOrder(selectedOrder.id));
    }
  }, [isSuccess]);

  useEffect(() => {
    if (isError) {
      setMessage({
        ...message,
        isError: true,
        title: 'Payment Error',
        msg: payment_error ? payment_error : '',
      });

      // reset errors after 4 seconds
      setTimeout(() => {
        dispatch(clearPaymentState());
        setMessage({
          ...message,
          isError: false,
          title: '',
          msg: '',
        });
      }, 4000);
    }

    if (isSuccess) {
      /**Print receipt */
      (async function () {
        let _settings = allSettings;

        /**
         * If business settings {} is empty or null, show toast
         *
         */
        let bizKeyz = _settings && Object.keys(_settings);
        if (bizKeyz?.length === 0) {
          ToastAndroid.showWithGravityAndOffset(
            'No business settings found',
            ToastAndroid.CENTER,
            ToastAndroid.LONG,
            0,
            600,
          );

          return;
        }

        let res =
          current_sale &&
          _settings &&
          (await printReceipt(current_sale, _settings, outlet, servedBy));

        if (res === false) {
          ToastAndroid.showWithGravityAndOffset(
            'Printer seems to be Offline',
            ToastAndroid.CENTER,
            ToastAndroid.LONG,
            0,
            600,
          );
        }
      })();

      /**show success modal */
      setShowPaySuccessModal(!showPaySuccessModal);
    }
  }, [isError, isSuccess]);

  useEffect(() => {
    customRecievedAmount && setRecievedAmount(customRecievedAmount);
  }, [customRecievedAmount]);

  useEffect(() => {
    //calculate and set the balance / change
    let _change = recievedAmount - addedMenuItemsTotal;

    if (_change >= 0) {
      setChange(_change);
    }
  }, [recievedAmount]);

  let customList = [
    {
      title: 'custom',
      value: 0,
    },
    {
      title: '1,000',
      value: 1000,
    },
    {
      title: '2,000',
      value: 2000,
    },
    {
      title: '5,000',
      value: 5000,
    },
    {
      title: '10,000',
      value: 10000,
    },
    {
      title: '20,000',
      value: 20000,
    },
    {
      title: '50,000',
      value: 50000,
    },
  ];

  /**
   * - organise and save payment record in backend
   * - if error | display error
   * -if success:
   *   * Flash the payment done Component
   *   * Prepare reciept
   *   *
   */
  const handleRecordingPayment = () => {
    if (recievedAmount === 0) {
      //display toast

      ToastAndroid.showWithGravityAndOffset(
        'Please Add Amount have you recieved',
        ToastAndroid.SHORT,
        ToastAndroid.BOTTOM,
        0,
        100,
      );
      return;
    }

    //can't pay if amount received is less than total amount

    if (recievedAmount < addedMenuItemsTotal) {
      //display toast

      ToastAndroid.showWithGravityAndOffset(
        'Amount received is less',
        ToastAndroid.SHORT,
        ToastAndroid.BOTTOM,
        0,
        100,
      );
      return;
    }

    const _sale = {
      outletId: outlet?.id,
      servedBy: servedBy?.id,
      items: addedMenuItems,
      total: addedMenuItemsTotal,
      amountRecieved: recievedAmount,
      change: recievedAmount - addedMenuItemsTotal,
      denominations: denominations,
      cashDrawerOperationId: currentUsersCashDrawerOperation
        ? currentUsersCashDrawerOperation.id
        : undefined,
    };

    dispatch(_recordSale(_sale));
  };

  //handle billing

  const changeHandling = (_denomination: number) => {
    //check if its a number
    if (typeof _denomination === 'string') {
      console.log(_denomination, 'wrong type');
      return;
    }

    setRecievedAmount(recievedAmount + _denomination);
  };

  const OrderSummaryItem = ({item}: Props) => (
    <View
      style={{
        height: 60,
        width: '100%',
        borderTopWidth: 1,
        borderTopColor: '#3E4347',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingHorizontal: 15,
      }}>
      <View
        style={{
          height: '100%',
          width: 100,
          justifyContent: 'center',
          alignItems: 'flex-start',
        }}>
        <Text
          style={{
            fontWeight: '300',
            fontSize: 16,
            color: '#ffffff',
            marginHorizontal: 10,
          }}>
          {item.qty}
        </Text>
      </View>

      <Text style={{fontWeight: '300', fontSize: 16, color: '#ffffff'}}>
        {item.name}
      </Text>
    </View>
  );
  return (
    <SafeAreaView style={{flex: 1}}>
      <View style={styles.main}>
        <TaskBar
          title="Direct sale"
          canMoveBack={true}
          navigation={navigation}
        />

        {isError && <GPSuccessOrErrorModal message={message} />}

        {/* main wrapper */}
        <View style={styles.main_wrapper}>
          <View style={styles.order_summary_cont}>
            <View
              style={{
                height: 80,
                width: '100%',
                borderBottomWidth: 1,
                borderBottomColor: '#3E4347',
                justifyContent: 'center',
                paddingHorizontal: 15,
              }}>
              <Text style={{fontWeight: '500', fontSize: 20, color: '#ffffff'}}>
                Order Summary
              </Text>
            </View>
            {/* items */}
            <View style={{flex: 1}}>
              <FlatList
                data={addedMenuItems}
                showsVerticalScrollIndicator={false}
                renderItem={({item, index}) => (
                  <OrderSummaryItem item={item} key={index} />
                )}
              />
            </View>

            <View
              style={{
                height: 60,
                width: '100%',
                borderTopWidth: 1,
                borderTopColor: '#3E4347',
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                paddingHorizontal: 15,
              }}>
              <Text style={{fontWeight: '400', fontSize: 18, color: '#ffffff'}}>
                Order Total
              </Text>

              <Text style={{fontWeight: '400', fontSize: 18, color: '#ffffff'}}>
                Ush {addedMenuItemsTotal}
              </Text>
            </View>
          </View>

          <View style={styles.payment_summary_cont}>
            <View
              style={{
                height: 80,
                width: '100%',
                borderBottomWidth: 1,
                borderBottomColor: '#3E4347',
                justifyContent: 'center',
                paddingHorizontal: 15,
              }}>
              <Text style={{fontWeight: '500', fontSize: 20, color: '#ffffff'}}>
                Payment Summary
              </Text>
            </View>

            {/* cash button */}
            <PaymentOptionButton title="Cash" />

            <Text
              style={{
                fontSize: 20,
                fontWeight: '500',
                color: '#ffffff',
                textAlign: 'center',
              }}>
              Mobile Money
            </Text>

            <PaymentOptionButton
              title="Airtel Money"
              styles={{backgroundColor: '#2A333E'}}
            />

            <PaymentOptionButton
              title="MTN MoMo"
              styles={{backgroundColor: '#2A333E'}}
            />

            <Text
              style={{
                fontSize: 20,
                fontWeight: '500',
                color: '#ffffff',
                textAlign: 'center',
              }}>
              Others
            </Text>
          </View>

          {/* Payment amount */}

          <View style={styles.payment_amount_cont}>
            <View style={styles.payment_amount_top_view}>
              <View style={{flex: 1, flexDirection: 'row'}}>
                <Image
                  source={require('../assets/icons/reciept.png')}
                  style={{width: 30, height: 40}}
                />
                <View style={{marginHorizontal: 8}}>
                  <Text
                    style={{fontWeight: '500', fontSize: 20, color: '#fff'}}>
                    Payment amount
                  </Text>
                  <Text
                    style={{fontWeight: '500', fontSize: 14, color: '#A5A5A5'}}>
                    Edit for partial payments
                  </Text>
                </View>
              </View>

              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  justifyContent: 'flex-end',
                  height: 60,
                  alignItems: 'center',
                }}>
                <Text style={{fontWeight: '500', fontSize: 20, color: '#fff'}}>
                  {addedMenuItemsTotal}
                </Text>

                <View
                  style={{
                    width: 30,
                    height: 20,
                    marginHorizontal: 10,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Image source={require('../assets/icons/forwardArrow.png')} />
                </View>
              </View>
            </View>

            {/* ------- */}
            <View style={styles.customize_container}>
              <View style={styles.payment_amount_top_view}>
                <View style={{flex: 1, flexDirection: 'row'}}>
                  <Image
                    source={require('../assets/icons/dbIcon.png')}
                    style={{width: 40, height: 40}}
                  />
                  <View style={{marginHorizontal: 8, justifyContent: 'center'}}>
                    <Text
                      style={{fontWeight: '500', fontSize: 20, color: '#fff'}}>
                      Received amount
                    </Text>
                  </View>
                </View>

                <View
                  style={{
                    flex: 1,
                    flexDirection: 'row',
                    justifyContent: 'flex-end',
                    height: 60,
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{fontWeight: '500', fontSize: 20, color: '#fff'}}>
                    {recievedAmount}
                  </Text>

                  <View
                    style={{
                      width: 30,
                      height: 20,
                      marginHorizontal: 10,
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <Image
                      source={require('../assets/icons/forwardArrow.png')}
                    />
                  </View>
                </View>
              </View>

              {/* custom price buttons */}

              <View style={styles.custom_btn_cont}>
                {customList &&
                  customList.map((data, index) => (
                    <CustomPriceButton
                      data={data}
                      key={index}
                      onPress={() => {
                        //if data.value === 0 that means custom has been clicked | redirect to custom price screen
                        if (data.value === 0) {
                          navigation.navigate(CUSTOM_AMOUNT_SCREEN);
                        }

                        changeHandling(data.value);
                        setDenominations(prev => [...prev, data.value]);
                      }}
                    />
                  ))}
              </View>
            </View>

            {/* bottom */}

            <View style={styles.change_cont}>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  alignItems: 'center',
                  paddingHorizontal: 14,
                }}>
                <Image source={require('../assets/icons/cashchange.png')} />
                <Text
                  style={{
                    fontWeight: '500',
                    fontSize: 20,
                    color: '#ffffff',
                    marginHorizontal: 8,
                  }}>
                  Change
                </Text>
              </View>

              <View
                style={{
                  height: '100%',
                  flex: 1,
                  justifyContent: 'center',
                  paddingHorizontal: 14,
                }}>
                <Text
                  style={{
                    fontWeight: '500',
                    fontSize: 20,
                    color: '#ffffff',
                    marginHorizontal: 8,
                    textAlign: 'right',
                  }}>
                  {change}
                </Text>
              </View>
            </View>

            <GPButton
              title={`Pay - USh ${addedMenuItemsTotal}`}
              styles={{
                width: '100%',
                borderRadius: 6,
                backgroundColor: '#279870',
                height: 60,
                marginVertical: 10,
              }}
              onPress={() =>
                addedMenuItemsTotal > 0 && handleRecordingPayment()
              }
              loading={loading}
            />
          </View>
        </View>

        {/* display  final modal */}
        <AmountPaidModal
          amount={addedMenuItemsTotal}
          show={showPaySuccessModal}
          onPress={() => {
            dispatch(clearPaymentState());
            dispatch(clearDirectSaleState());
            setShowPaySuccessModal(!showPaySuccessModal);
            //navigate to register sales screen
            navigation.navigate(HOME_REGISTER_SCREEN);
          }}
        />
      </View>
    </SafeAreaView>
  );
};

export default TakingPaymentsScreen;

const styles = StyleSheet.create({
  main: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: colors.bg_dark,
  },
  main_wrapper: {
    flex: 1,
    flexDirection: 'row',
  },
  order_summary_cont: {
    width: dimentions.col * 7,
    height: dimentions.row * 16,
    margin: 6,
    backgroundColor: colors.bg_light,
  },
  payment_summary_cont: {
    width: dimentions.col * 3.4,
    height: dimentions.row * 16,
    margin: 6,
    backgroundColor: colors.bg_light,
    alignItems: 'center',
  },
  payment_amount_cont: {
    flex: 1,
    margin: 6,
    width: dimentions.col * 9.5,
    height: dimentions.row * 16,
  },
  payment_amount_top_view: {
    width: '100%',
    height: dimentions.row * 2,
    backgroundColor: colors.bg_light,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 8,
    borderRadius: 6,
  },
  customize_container: {
    width: '100%',
    height: dimentions.row * 8,
    backgroundColor: colors.bg_light,
    marginVertical: 20,
    borderRadius: 6,
  },
  custom_btn_cont: {
    width: '80%',
    flexDirection: 'row',
    flex: 1,
    padding: 8,
    flexWrap: 'wrap',
  },
  change_cont: {
    height: dimentions.row * 2,
    width: '100%',
    backgroundColor: '#222428',
    borderRadius: 6,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});
