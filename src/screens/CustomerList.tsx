import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import CustomerListAndSearch from '../components/CustomerListAndSearch';

type Props = {};

const CustomerList = (props: Props) => {
  return (
    <View style={{flex: 1}}>
      <CustomerListAndSearch />
    </View>
  );
};

export default CustomerList;

const styles = StyleSheet.create({});
