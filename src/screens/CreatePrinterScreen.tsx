import {
  ActivityIndicator,
  Alert,
  FlatList,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import Reactf, {useEffect} from 'react';
import PrinterSearch from '../components/PrinterSearch';
import PrinterSearchResults from '../components/PrinterSearchResults';
import SelectPrinter from '../components/SelectPrinter';
import {useSelector} from 'react-redux';
import {RootState} from '../redux/store';

const CreatePrinterScreen = () => {
  const {loading, pairedDevices, unPairedDevices, isError, scan_error_message} =
    useSelector((state: RootState) => state.printerSlice);

  return (
    <View style={styles.main}>
      {loading ? (
        <PrinterSearch />
      ) : (pairedDevices && pairedDevices.length > 0) ||
        (unPairedDevices && unPairedDevices.length > 0) ? (
        <PrinterSearchResults
          pairedDevices={pairedDevices}
          unPairedDevices={unPairedDevices}
        />
      ) : (
        <SelectPrinter />
      )}
    </View>
  );
};

export default CreatePrinterScreen;

const styles = StyleSheet.create({
  main: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  header: {
    height: 60,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
