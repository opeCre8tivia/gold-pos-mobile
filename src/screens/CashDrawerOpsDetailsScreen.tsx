import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import React, {useEffect, useState} from 'react';
import {useRoute} from '@react-navigation/native';
import {CashDrawerOperation} from '../types/main';
import {useDispatch, useSelector} from 'react-redux';
import {RootState, useAppDispatch} from '../redux/store';
import {colors, dimentions} from '../constants/theme';
import {useNavigation} from '@react-navigation/native';
import GPButton from '../components/GPButton';
import CashInModal from '../components/CashInModal';
import {toggleCashInModalVisibility} from '../redux/features/cashin.slice';
import PayoutModal from '../components/PayoutModal';
import {togglePayoutModalVisibility} from '../redux/features/payout.slice';
import {_getCashDrawerOperations} from '../redux/actions/cashdrawer.actions';
import CashdropModal from '../components/CashdropModal';
import {toggleCashdropModalVisibility} from '../redux/features/cashdrop.slice';
import {Toast} from 'react-native-toast-message/lib/src/Toast';
import {SHIFTREVIEW_SCREEN} from '../constants/screenNames';
import {_setCashDrawerOpsForReview} from '../redux/features/shiftreview.slice';

const CashDrawerOpsDetailsScreen = () => {
  const {cashDrawerOperations, currentUsersCashDrawerOperation} = useSelector(
    (state: RootState) => state.cashdrawersSlice,
  );
  const {showCashInModal} = useSelector(
    (state: RootState) => state.cashinSlice,
  );
  const {currentOperator} = useSelector(
    (state: RootState) => state.clockingSlice,
  );
  const payoutState = useSelector((state: RootState) => state.payoutSlice);
  const cashdropState = useSelector((state: RootState) => state.cashdropSlice);
  const {opsId} = useRoute<any>().params;
  const navigation = useNavigation<any>();
  const dispatch = useAppDispatch();

  const [operation, setOperation] = useState<CashDrawerOperation | null>(null);

  useEffect(() => {
    if (opsId && cashDrawerOperations) {
      let currentOperation = cashDrawerOperations.find(op => op.id === opsId);

      currentOperation && setOperation(currentOperation);
    }
  }, [opsId, cashDrawerOperations]);

  useEffect(() => {
    if (
      !showCashInModal ||
      !payoutState.showPayoutModal ||
      !cashdropState.showCashdropModal
    ) {
      dispatch(_getCashDrawerOperations(currentOperator.id));
    }
  }, [
    showCashInModal,
    payoutState.showPayoutModal,
    cashdropState.showCashdropModal,
  ]);

  const handleMovingToShiftReview = () => {
    dispatch(_setCashDrawerOpsForReview(operation));
    navigation.navigate(SHIFTREVIEW_SCREEN);
  };

  return (
    <View style={{flex: 1, backgroundColor: colors.bg_dark}}>
      {/* top */}
      <View style={styles.topTabMain}>
        <View style={styles.topTopInnerWrapper}>
          <TouchableOpacity
            onPress={() => navigation.goBack()}
            style={{
              flexDirection: 'row',
              justifyContent: 'flex-start',
              alignItems: 'center',
              height: '100%',
              flex: 1,
              paddingHorizontal: 4,
            }}>
            <Image
              source={require('../assets/icons/backArrow.png')}
              style={{marginHorizontal: 4}}
            />
            <Text
              style={{fontSize: 18, fontWeight: '500', color: colors.white}}>
              {operation?.cashDrawer?.name}
            </Text>
          </TouchableOpacity>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'flex-end',
              paddingHorizontal: 50,
            }}>
            <Text
              style={{fontSize: 12, color: colors.white, fontWeight: '400'}}>
              {' '}
              StartingBalance : {operation?.startingBalance}
            </Text>
          </View>
        </View>
        <View style={styles.topBottomInnerWrapper}>
          <Text style={{fontSize: 18, color: colors.white, fontWeight: '300'}}>
            Balance :
          </Text>
          <Text
            style={{color: colors.blue_light, fontSize: 20, fontWeight: '500'}}>
            {' '}
            {operation?.currentBalance}
          </Text>
        </View>
      </View>
      {/* middle table */}

      <View style={styles.middleMain}>
        <View
          style={{
            backgroundColor: '#2F303A',
            width: '100%',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            padding: 4,
          }}>
          <Text
            style={{
              flex: 1,
              textAlign: 'center',
              fontWeight: '400',
              fontSize: 18,
              color: '#A5A5A5',
              padding: 0,
              margin: 0,
            }}>
            ACTION
          </Text>
          <Text
            style={{
              flex: 1,
              textAlign: 'center',
              fontWeight: '400',
              fontSize: 18,
              color: '#A5A5A5',
            }}>
            AMOUNT
          </Text>
          <Text
            style={{
              flex: 1,
              textAlign: 'center',
              fontWeight: '400',
              fontSize: 18,
              color: '#A5A5A5',
            }}>
            USER
          </Text>
        </View>
        {/* payments */}
        <View
          style={{
            height: 50,
            borderBottomColor: '#7a7a7a',
            borderBottomWidth: 1,
            width: '100%',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            padding: 4,
          }}>
          <Text
            style={{
              flex: 1,
              textAlign: 'center',
              fontWeight: '400',
              fontSize: 14,
              color: '#ffffff',
              padding: 0,
              margin: 0,
            }}>
            Payments
          </Text>
          <Text
            style={{
              flex: 1,
              textAlign: 'center',
              fontWeight: '400',
              fontSize: 14,
              color: '#ffffff',
            }}>
            {operation &&
              operation.sales &&
              operation.sales.reduce((acc, item, arr) => {
                return acc + item.total;
              }, 0)}
          </Text>
          <Text
            style={{
              flex: 1,
              textAlign: 'center',
              fontWeight: '400',
              fontSize: 14,
              color: '#ffffff',
            }}>
            {operation?.employee.first_name}
          </Text>
        </View>

        {/* cashins */}
        <View
          style={{
            height: 50,
            borderBottomColor: '#7a7a7a',
            borderBottomWidth: 1,
            width: '100%',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            padding: 4,
          }}>
          <Text
            style={{
              flex: 1,
              textAlign: 'center',
              fontWeight: '400',
              fontSize: 14,
              color: '#ffffff',
              padding: 0,
              margin: 0,
            }}>
            Cashin
          </Text>
          <Text
            style={{
              flex: 1,
              textAlign: 'center',
              fontWeight: '400',
              fontSize: 14,
              color: '#ffffff',
            }}>
            {operation &&
              operation.cashins &&
              operation?.cashins.reduce((acc, item, arr) => {
                return acc + item.amount;
              }, 0)}
          </Text>
          <Text
            style={{
              flex: 1,
              textAlign: 'center',
              fontWeight: '400',
              fontSize: 14,
              color: '#ffffff',
            }}>
            {operation?.employee.first_name}
          </Text>
        </View>

        {/* payouts */}

        <View
          style={{
            height: 50,
            borderBottomColor: '#7a7a7a',
            borderBottomWidth: 1,
            width: '100%',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            padding: 4,
          }}>
          <Text
            style={{
              flex: 1,
              textAlign: 'center',
              fontWeight: '400',
              fontSize: 14,
              color: '#ffffff',
              padding: 0,
              margin: 0,
            }}>
            Payout
          </Text>
          <Text
            style={{
              flex: 1,
              textAlign: 'center',
              fontWeight: '400',
              fontSize: 14,
              color: colors.orange,
            }}>
            -
            {operation &&
              operation.payouts &&
              operation?.payouts.reduce((acc, item, arr) => {
                return acc + item.amount;
              }, 0)}
          </Text>
          <Text
            style={{
              flex: 1,
              textAlign: 'center',
              fontWeight: '400',
              fontSize: 14,
              color: '#ffffff',
            }}>
            {operation?.employee.first_name}
          </Text>
        </View>

        {/* cashdrops */}
        <View
          style={{
            height: 50,
            borderBottomColor: '#7a7a7a',
            borderBottomWidth: 1,
            width: '100%',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            padding: 4,
          }}>
          <Text
            style={{
              flex: 1,
              textAlign: 'center',
              fontWeight: '400',
              fontSize: 14,
              color: '#ffffff',
              padding: 0,
              margin: 0,
            }}>
            Cashdrop
          </Text>
          <Text
            style={{
              flex: 1,
              textAlign: 'center',
              fontWeight: '400',
              fontSize: 14,
              color: colors.orange,
            }}>
            -
            {operation &&
              operation.cashdrops &&
              operation?.cashdrops.reduce((acc, item, arr) => {
                return acc + item.amount;
              }, 0)}
          </Text>
          <Text
            style={{
              flex: 1,
              textAlign: 'center',
              fontWeight: '400',
              fontSize: 14,
              color: '#ffffff',
            }}>
            {operation?.employee.first_name}
          </Text>
        </View>
      </View>

      {/* bottom button COntainer */}
      <View style={styles.bottomMain}>
        <View
          style={{
            width: '100%',
            flexDirection: 'row',
            alignItems: 'center',
            paddingHorizontal: 10,
            marginBottom: 15,
          }}>
          <View
            style={{
              flex: 1,
              height: 1,
              backgroundColor: colors.white,
            }}></View>
          <Text
            style={{
              fontSize: 18,
              color: colors.white,
              fontWeight: '400',
              marginHorizontal: 10,
            }}>
            During Shift
          </Text>
          <View
            style={{
              flex: 1,
              height: 1,
              backgroundColor: colors.white,
            }}></View>
        </View>

        <View style={{flexDirection: 'row', justifyContent: 'center'}}>
          <GPButton
            title="Cash In "
            styles={{
              backgroundColor: colors.blue_light,
              height: dimentions.row * 1.3,
              width: dimentions.col * 3,
            }}
            text_styles={{color: colors.black, fontSize: 14, fontWeight: '400'}}
            onPress={() => dispatch(toggleCashInModalVisibility())}
            disabled={cashDrawerOperations?.every(ops => !ops.isActive)}
          />
          <GPButton
            title="Cash Out "
            styles={{
              backgroundColor: colors.blue_light,
              color: '#000',
              height: dimentions.row * 1.3,
              width: dimentions.col * 3,
            }}
            text_styles={{color: colors.black, fontSize: 14, fontWeight: '400'}}
            disabled={cashDrawerOperations?.every(ops => !ops.isActive)}
          />
          <GPButton
            title="Pay Out"
            onPress={() => dispatch(togglePayoutModalVisibility())}
            styles={{
              backgroundColor: colors.blue_light,
              color: '#000',
              height: dimentions.row * 1.3,
              width: dimentions.col * 3,
            }}
            text_styles={{color: colors.black, fontSize: 14, fontWeight: '400'}}
            disabled={cashDrawerOperations?.every(ops => !ops.isActive)}
          />
        </View>
        <View style={{flexDirection: 'row', justifyContent: 'center'}}>
          <GPButton
            title="Cash Drop "
            onPress={() => dispatch(toggleCashdropModalVisibility())}
            styles={{
              backgroundColor: colors.blue_light,
              height: dimentions.row * 1.3,
              width: dimentions.col * 3,
            }}
            text_styles={{color: colors.black, fontSize: 14, fontWeight: '400'}}
            disabled={cashDrawerOperations?.every(ops => !ops.isActive)}
          />
          <GPButton
            title="Shift Review"
            styles={{
              backgroundColor: colors.blue_light,
              color: '#000',
              height: dimentions.row * 1.3,
              width: dimentions.col * 3,
            }}
            text_styles={{color: colors.black, fontSize: 14, fontWeight: '400'}}
            onPress={() => handleMovingToShiftReview()}
            disabled={cashDrawerOperations?.every(ops => !ops.isActive)}
          />
          <GPButton
            title="Adjust Starting Balance"
            styles={{
              backgroundColor: colors.blue_light,
              color: '#000',
              height: dimentions.row * 1.3,
              width: dimentions.col * 3,
            }}
            text_styles={{color: colors.black, fontSize: 14, fontWeight: '400'}}
            disabled={cashDrawerOperations?.every(ops => !ops.isActive)}
          />
        </View>

        {/* cashin modal ---> */}
        <CashInModal visibility={showCashInModal} cashDrawerOpsId={opsId} />
        {/* payout modal */}
        <PayoutModal
          visibility={payoutState.showPayoutModal}
          cashDrawerOpsId={opsId}
        />

        {/* cashdrop modal */}
        <CashdropModal
          visibility={cashdropState.showCashdropModal}
          cashDrawerOpsId={opsId}
        />
      </View>
    </View>
  );
};

export default CashDrawerOpsDetailsScreen;

const styles = StyleSheet.create({
  topTabMain: {
    width: '100%',
    height: dimentions.row * 3,
    borderTopRightRadius: 10,
    backgroundColor: colors.task_bar_blue,
    marginBottom: 4,
  },
  topTopInnerWrapper: {
    width: '100%',
    height: 55,
    borderBottomColor: '#3E4347',
    borderBottomWidth: 1,
    padding: 5,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  topBottomInnerWrapper: {
    width: '100%',
    height: 55,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    paddingHorizontal: 50,
  },
  middleMain: {
    width: '100%',
    height: dimentions.row * 8,
    backgroundColor: colors.task_bar_blue,
    marginBottom: 4,
  },
  bottomMain: {
    width: '100%',
    height: dimentions.row * 6,
    backgroundColor: colors.task_bar_blue,
  },
});
