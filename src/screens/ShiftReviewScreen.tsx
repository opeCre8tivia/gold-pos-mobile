import {
  StyleSheet,
  Text,
  View,
  Image,
  Modal,
  Pressable,
  ActivityIndicator,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import {colors, dimentions} from '../constants/theme';
import BackArrowIcon from '../assets/svgIcons/backArrow.icon';
import {useSelector} from 'react-redux';
import {RootState, useAppDispatch} from '../redux/store';
import ClockIcon from '../assets/svgIcons/clock.icon';
import GPCollapsible from '../components/GPCollapsible';
import {CashDrawerOperation, ParkedOrder} from '../types/main';
import GPButton from '../components/GPButton';
import {useNavigation} from '@react-navigation/native';
import {_listParkedOrders} from '../redux/actions/parking.actions';
import {CLOSE_CASHDRAWER_SCREEN} from '../constants/screenNames';
import {Toast} from 'react-native-toast-message/lib/src/Toast';
import {clearCashDrawerState} from '../redux/features/cashdrawers.slice';
import {
  _closeCashDrawerOperations,
  _getCashDrawerOperations,
} from '../redux/actions/cashdrawer.actions';

type Props = {};

const ShiftReviewScreen = (props: Props) => {
  const navigation = useNavigation<any>();
  const dispatch = useAppDispatch();
  const {currentOperator} = useSelector(
    (state: RootState) => state.clockingSlice,
  );
  const {drawerOperationBeingReviewed} = useSelector(
    (state: RootState) => state.shiftreviewSlice,
  );
  const {parkedOrders} = useSelector((state: RootState) => state.orderSlice);
  const {cashDrawerOperations, isError, isSuccess, responseMessage, loading} =
    useSelector((state: RootState) => state.cashdrawersSlice);

  const [openOrders, setOpenOrders] = useState<ParkedOrder[]>([]);
  const [closedOrders, setClosedOrders] = useState<ParkedOrder[]>([]);
  const [openCashdrawerOps, setOpenCashdrawerOps] = useState<
    CashDrawerOperation[]
  >([]);
  const [closedCashdrawerOps, setClosedCashdrawerOps] = useState<
    CashDrawerOperation[]
  >([]);

  //fetch orders on load
  useEffect(() => {
    if (parkedOrders.length === 0) {
      dispatch(_listParkedOrders());
    }
  }, []);
  //fetch cashdrawer operations on load
  useEffect(() => {
    navigation.addListener('focus', () => {
      dispatch(_getCashDrawerOperations(currentOperator.id));
    });

    return () => {
      dispatch(clearCashDrawerState());
    };
  }, []);

  useEffect(() => {
    console.log(isSuccess);
    console.log(responseMessage);

    if (isSuccess || isError) {
      setTimeout(() => {
        dispatch(clearCashDrawerState());
      }, 3000);
    }
  }, [isSuccess, isError]);

  useEffect(() => {
    if (parkedOrders) {
      let _openOrders = parkedOrders.filter(order => order.isOpen);
      _openOrders && setOpenOrders(_openOrders);

      let _closedOrders = parkedOrders.filter(order => !order.isOpen);
      _closedOrders && setClosedOrders(_closedOrders);
    }
  }, [parkedOrders]);

  useEffect(() => {
    if (cashDrawerOperations) {
      let _openDrawers = cashDrawerOperations.filter(ops => ops.isActive);
      _openDrawers && setOpenCashdrawerOps(_openDrawers);

      let _closedDrawers = cashDrawerOperations.filter(ops => !ops.isActive);
      _closedDrawers && setClosedCashdrawerOps(_closedDrawers);
    }
  }, [cashDrawerOperations]);

  return (
    <View style={styles.main}>
      {/* close drawer modal */}

      {/* left */}
      <View style={styles.left_wrapper}>
        <View style={styles.top_left_container}>
          <View
            style={{
              height: dimentions.row * 1.5,
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              borderBottomColor: '#7a7a7a',
              borderBottomWidth: 1,
              paddingHorizontal: 20,
            }}>
            <Pressable
              style={{flexDirection: 'row', alignItems: 'center'}}
              onPress={() => navigation.goBack()}>
              <View style={{width: 15, height: 15, marginHorizontal: 5}}>
                <BackArrowIcon fill={colors.orange} width={10} height={10} />
              </View>
              <Text
                style={{fontSize: 18, fontWeight: '500', color: colors.white}}>
                Shift Review
              </Text>
            </Pressable>
            {/*  */}
            <View style={{flexDirection: 'row'}}>
              <Image source={require('../assets/icons/userSmIcon.png')} />
              <Text
                style={{
                  fontSize: 14,
                  color: '#A5A5A5',
                  fontWeight: '300',
                  marginHorizontal: 6,
                }}>
                {currentOperator.first_name}
              </Text>
            </View>

            {/*  */}
          </View>

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              height: dimentions.row * 1.5,
              padding: 10,
            }}>
            <Text
              style={{
                fontSize: 14,
                color: '#A5A5A5',
                fontWeight: '300',
                marginHorizontal: 6,
              }}>
              Fri, 16/04/2023
            </Text>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <View style={{width: 20, height: 20}}>
                <ClockIcon />
              </View>
              <Text
                style={{
                  fontSize: 14,
                  color: '#A5A5A5',
                  fontWeight: '300',
                  marginHorizontal: 6,
                }}>
                10:00 AM - 4:28 PM
              </Text>
            </View>
          </View>
        </View>
        <View style={styles.bottom_left_container}>
          <View
            style={{
              height: dimentions.row * 2.5,
              borderBottomColor: '#7a7a7a',
              borderBottomWidth: 1,
            }}>
            <View
              style={{
                backgroundColor: '#2F303A',
                width: '100%',
                height: dimentions.row * 0.8,
                paddingHorizontal: 10,
              }}>
              <Text style={{color: '#A5A5A5', fontSize: 15, fontWeight: '400'}}>
                EMPLOYEE AMOUNT
              </Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}>
              <Text
                style={{
                  fontSize: 12,
                  color: '#A5A5A5',
                  fontWeight: '300',
                  marginHorizontal: 6,
                }}>
                Cash in Drawer (collected cash sales)
              </Text>
              <Text
                style={{
                  fontSize: 12,
                  color: '#A5A5A5',
                  fontWeight: '300',
                  marginHorizontal: 6,
                }}>
                {drawerOperationBeingReviewed?.currentBalance}
              </Text>
            </View>
          </View>
          <View
            style={{
              height: dimentions.row * 2.5,
              borderBottomColor: '#7a7a7a',
              borderBottomWidth: 1,
            }}>
            <View
              style={{
                backgroundColor: '#2F303A',
                width: '100%',
                height: dimentions.row * 0.8,
                paddingHorizontal: 10,
              }}>
              <Text style={{color: '#A5A5A5', fontSize: 15, fontWeight: '400'}}>
                SALES AND TAXES SUMMARY
              </Text>
            </View>
          </View>
        </View>
      </View>
      {/* right */}
      <View style={styles.right_wrapper}>
        <GPCollapsible
          title="Close Orders"
          text="Close all open checks before completing shift review"
          isResolved={openOrders.length === 0}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'flex-start',
              alignItems: 'center',
            }}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'flex-start',
                alignItems: 'center',
                marginRight: 25,
              }}>
              {/* box number */}
              <View
                style={{
                  minWidth: 35,
                  height: 25,
                  backgroundColor: '#45474E',
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderRadius: 3,
                  paddingHorizontal: 5,
                  marginRight: 10,
                }}>
                <Text
                  style={{
                    color: colors.white,
                    fontSize: 12,
                  }}>
                  {openOrders.length}
                </Text>
              </View>

              <Text style={{color: colors.white, fontWeight: '300'}}>
                {`Open Order${openOrders.length === 1 ? '' : 's'}`}
              </Text>
            </View>

            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'flex-start',
                alignItems: 'center',
              }}>
              {/* box number */}
              <View
                style={{
                  minWidth: 35,
                  height: 25,
                  backgroundColor: '#45474E',
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderRadius: 3,
                  paddingHorizontal: 5,
                  marginRight: 10,
                }}>
                <Text
                  style={{
                    color: colors.white,
                    fontSize: 12,
                  }}>
                  {closedOrders.length}
                </Text>
              </View>

              <Text style={{color: colors.white, fontWeight: '300'}}>
                {`Closed Order${closedOrders.length === 1 ? '' : 's'}`}
              </Text>
            </View>
          </View>
        </GPCollapsible>
        <GPCollapsible
          title="Close Cash Drawer"
          text="Close  cash drawer before completing shift review"
          isResolved={openCashdrawerOps.length === 0}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'flex-start',
              alignItems: 'center',
            }}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'flex-start',
                alignItems: 'center',
                marginRight: 25,
              }}>
              {/* box number */}
              <View
                style={{
                  minWidth: 35,
                  height: 25,
                  backgroundColor: '#45474E',
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderRadius: 3,
                  paddingHorizontal: 5,
                  marginRight: 10,
                }}>
                {loading ? (
                  <ActivityIndicator size={12} color="#ffffff" />
                ) : (
                  <Text
                    style={{
                      color: colors.white,
                      fontSize: 12,
                    }}>
                    {openCashdrawerOps.length}
                  </Text>
                )}
              </View>

              <Text style={{color: colors.white, fontWeight: '300'}}>
                {`Open Cash drawer${openCashdrawerOps.length === 1 ? '' : 's'}`}
              </Text>
            </View>

            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'flex-start',
                alignItems: 'center',
              }}>
              {/* box number */}
              <View
                style={{
                  minWidth: 35,
                  height: 25,
                  backgroundColor: '#45474E',
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderRadius: 3,
                  paddingHorizontal: 5,
                  marginRight: 10,
                }}>
                {loading ? (
                  <ActivityIndicator size={12} color="#ffffff" />
                ) : (
                  <Text
                    style={{
                      color: colors.white,
                      fontSize: 12,
                    }}>
                    {closedCashdrawerOps.length}
                  </Text>
                )}
              </View>

              <Text style={{color: colors.white, fontWeight: '300'}}>
                {`Closed Cash drawer${
                  closedCashdrawerOps.length === 1 ? '' : 's'
                }`}
              </Text>
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'flex-end',
              marginVertical: 10,
            }}>
            <GPButton
              disabled={!drawerOperationBeingReviewed ? true : false}
              title="Close Cash Drawer"
              styles={{
                height: 30,
                width: dimentions.col * 3,
                backgroundColor: colors.blue_light,
              }}
              text_styles={{color: '#00000'}}
              onPress={() => navigation.navigate(CLOSE_CASHDRAWER_SCREEN)}
            />
          </View>
        </GPCollapsible>
        <GPCollapsible
          title="Clock out"
          text="Finish shift review by clocking out or start a new shift"
          isResolved={openOrders.length === 0}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'flex-start',
              alignItems: 'center',
            }}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'flex-start',
                alignItems: 'center',
                marginRight: 25,
              }}>
              {/* box number */}
              <View
                style={{
                  minWidth: 35,
                  height: 25,
                  backgroundColor: '#45474E',
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderRadius: 3,
                  paddingHorizontal: 5,
                  marginRight: 10,
                }}>
                <Text
                  style={{
                    color: colors.white,
                    fontSize: 12,
                  }}>
                  {openCashdrawerOps.length}
                </Text>
              </View>

              <Text style={{color: colors.white, fontWeight: '300'}}>
                {`Open Cash drawer${openCashdrawerOps.length === 1 ? '' : 's'}`}
              </Text>
            </View>

            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'flex-start',
                alignItems: 'center',
              }}>
              {/* box number */}
              <View
                style={{
                  minWidth: 35,
                  height: 25,
                  backgroundColor: '#45474E',
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderRadius: 3,
                  paddingHorizontal: 5,
                  marginRight: 10,
                }}>
                <Text
                  style={{
                    color: colors.white,
                    fontSize: 12,
                  }}>
                  {closedCashdrawerOps.length}
                </Text>
              </View>

              <Text style={{color: colors.white, fontWeight: '300'}}>
                {`Closed Cash drawer${
                  closedCashdrawerOps.length === 1 ? '' : 's'
                }`}
              </Text>
            </View>
          </View>
        </GPCollapsible>
      </View>

      {/* ---- ---- */}

      <View>
        {/* <Modal
          visible={showCloseDrawerModal}
          transparent={true}
          animationType="fade"
          style={{flex: 1}}>
          <View style={{backgroundColor: 'rgba(0,0,0,0.9)'}}></View>
        </Modal> */}
      </View>
    </View>
  );
};

export default ShiftReviewScreen;

const styles = StyleSheet.create({
  main: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: colors.black,
  },
  left_wrapper: {
    width: dimentions.col * 7,
    height: dimentions.row * 18,
  },
  right_wrapper: {
    width: dimentions.col * 8.5,
    height: dimentions.row * 18,
    paddingHorizontal: 4,
  },
  top_left_container: {
    width: dimentions.col * 6,
    height: dimentions.row * 3,
    backgroundColor: colors.btn_blue,
    borderTopEndRadius: 20,
    borderTopStartRadius: 20,
    marginBottom: 2,
  },
  bottom_left_container: {
    width: dimentions.col * 6,
    height: dimentions.row * 8,
    backgroundColor: colors.btn_blue,
  },
});
