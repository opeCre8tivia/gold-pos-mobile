import React, {useCallback, useEffect, useMemo, useState} from 'react';
import {
  View,
  SafeAreaView,
  StyleSheet,
  FlatList,
  PermissionsAndroid,
} from 'react-native';

import {colors, dimentions} from '../constants/theme';
import TaskBar from '../components/TaskBar';
import {useDispatch, useSelector} from 'react-redux';
import {RootState} from '../redux/store';
import {
  selectMenuItem,
  setInitialRoute,
  setMenu,
} from '../redux/features/settings.slice';
import {settingsMenu} from '../constants/settingsMenu';
import ControlCenterScreen from './ControlCenterScreen';
import PrintersScreen from './PrintersScreen';
import CreatePrinterScreen from './CreatePrinterScreen';
import SideNavItem from '../components/SideNavItem';
import {useNavigation} from '@react-navigation/native';
import PrinterIcon from '../assets/svgIcons/printer.icon';
import HomeIcon from '../assets/svgIcons/home.icon';
import ReportIcon from '../assets/svgIcons/report.icon';
import PaymentIcon from '../assets/svgIcons/payment.icon';
import BluetoothIcon from '../assets/svgIcons/bluetooth.icon';
import SettingsIcon from '../assets/svgIcons/settings.icon';
import CashDrawerIcon from '../assets/svgIcons/cashDrawer.icon';
import CashDrawerStack from '../navigation/CashDrawerStack';

export default function Settings() {
  const dispatch = useDispatch();
  const navigation = useNavigation();

  const {selectedItem, menu} = useSelector(
    (state: RootState) => state.settingsSlice,
  );

  const icons: any = {
    home: HomeIcon,
    printer: PrinterIcon,
    report: ReportIcon,
    payment: PaymentIcon,
    hardware: BluetoothIcon,
    settings: SettingsIcon,
    drawer: CashDrawerIcon,
  };

  /**
   * set then settings menu  onload
   */
  useEffect(() => {
    dispatch(setMenu(settingsMenu));
  }, []);

  /**
   * set menu every time icon comes to view
   */
  useEffect(() => {
    const unSubscribe = navigation.addListener('focus', () => {
      dispatch(setMenu(settingsMenu));
    });

    return unSubscribe;
  }, []);

  /**
   * once menu is set, Set Initial route
   */

  useEffect(() => {
    menu.length > 0 && dispatch(setInitialRoute({id: 2}));
  }, [menu]);

  /**
   * Get all necessary bluetooth permissions
   */

  useEffect(() => {
    requestForBluetoothDevices();
  }, []);

  /**
   * parse the stringified component and set to local state
   */

  const requestForBluetoothDevices = async () => {
    try {
      let blConn = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.BLUETOOTH_CONNECT,
        {
          title: 'Bluetooth connect Permissions',
          message: 'To use blutooth the app needs connection permissions',
          buttonPositive: 'Ok',
          buttonNegative: 'Cancel',
          buttonNeutral: 'May be later',
        },
      );

      let blScan = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.BLUETOOTH_SCAN,
        {
          title: 'Bluetooth scan Permissions',
          message: 'To use blutooth the app needs scan permissions',
          buttonPositive: 'Ok',
          buttonNegative: 'Cancel',
          buttonNeutral: 'May be later',
        },
      );

      let blfinel = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
        {
          title: 'Bluetooth Location Permissions',
          message: 'To use blutooth the app needs to access your location',
          buttonPositive: 'Ok',
          buttonNegative: 'Cancel',
          buttonNeutral: 'May be later',
        },
      );

      let blcorse = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
        {
          title: 'Bluetooth Location Permissions',
          message: 'To use blutooth the app needs to access your location',
          buttonPositive: 'Ok',
          buttonNegative: 'Cancel',
          buttonNeutral: 'May be later',
        },
      );
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <SafeAreaView>
      <View style={styles.main}>
        {/* menu panel */}
        <View style={styles.menu_container}>
          <View style={styles.menu_top}></View>
          <FlatList
            data={menu}
            renderItem={({item, index}) => {
              let isSelected = item.id === selectedItem?.id ? true : false;

              return (
                <SideNavItem
                  onPress={() => dispatch(selectMenuItem(item))}
                  key={index}
                  title={item.title}
                  Icon={icons[item.icon]}
                  fill={isSelected ? colors.orange : colors.white}
                  textStyles={{
                    color: isSelected ? colors.orange : colors.white,
                  }}
                />
              );
            }}
          />
        </View>

        {/* component container | host */}
        <View style={styles.host}>
          <TaskBar
            title={selectedItem ? selectedItem.title : 'Settings'}
            canMoveBack={false}
            taskBarStyles={{
              backgroundColor: '#262832',
              borderRadius: 6,
              margin: 4,
            }}
          />

          <View style={{flex: 1, marginHorizontal: 4}}>
            {selectedItem ? (
              selectedItem.id === 1 ? (
                <ControlCenterScreen />
              ) : selectedItem.id === 2 ? (
                <CashDrawerStack />
              ) : selectedItem.id === 3 ? (
                <PrintersScreen />
              ) : selectedItem.id === 3 ? (
                <CreatePrinterScreen />
              ) : null
            ) : null}
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  main: {
    width: dimentions.vw,
    height: dimentions.vh,
    backgroundColor: '#000000',
    flexDirection: 'row',
  },
  menu_container: {
    width: dimentions.col * 4,
    height: '60%',
    backgroundColor: '#262832',
    padding: 8,
    marginVertical: 4,
    marginLeft: 20,
    borderTopLeftRadius: 20,
    borderBottomLeftRadius: 20,
  },
  menu_top: {
    height: 70,
    width: '98%',
    marginBottom: 5,
  },
  host: {
    width: dimentions.col * 16,
    height: dimentions.vh - 70,
    padding: 4,
  },
});
