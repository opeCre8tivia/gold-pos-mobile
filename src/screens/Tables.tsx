import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import ComingSoonComponent from '../components/ComingSoonComponent'

const Tables = () => {
  return (
    <View style={{ flex: 1 }}>
      <ComingSoonComponent />
    </View>
  )
}

export default Tables

const styles = StyleSheet.create({})