import React from 'react';
import {StyleSheet} from 'react-native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {
  CASHDRAWER_LIST_SCREEN,
  CUSTOM_AMOUNT_SCREEN,
  FLOAT_COUNTING_SCREEN,
  HOME_REGISTER_SCREEN,
  REGISTER_DRAWER_HOST,
  REGISTER_SEARCH_SCREEN,
  TAKING_PAYMENT_SCREEN,
} from '../constants/screenNames';
import HomeRegisterScreen from './HomeRegisterScreen';
import RegisterSearchScreen from './RegisterSearchScreen';
import TakingPaymentsScreen from './TakingPaymentsScreen';
import CustomAmountScreen from './CustomAmountScreen';
import RegisterTabDrawerHost from '../navigation/RegisterTabDrawerHost';
import FloatCountingScreen from './FloatCountingScreen';
import CashDrawerListScreen from './CashDrawerListScreen';

const Register = () => {
  //set up reciept stack here
  const ReceiptStack = createNativeStackNavigator();
  return (
    <ReceiptStack.Navigator
      initialRouteName={HOME_REGISTER_SCREEN}
      screenOptions={{
        headerShown: false,
      }}>
      <ReceiptStack.Screen
        name={HOME_REGISTER_SCREEN}
        component={HomeRegisterScreen}
      />
      <ReceiptStack.Screen
        name={REGISTER_SEARCH_SCREEN}
        component={RegisterSearchScreen}
      />
      <ReceiptStack.Screen
        name={TAKING_PAYMENT_SCREEN}
        component={TakingPaymentsScreen}
      />
      <ReceiptStack.Screen
        name={CUSTOM_AMOUNT_SCREEN}
        component={CustomAmountScreen}
      />
    </ReceiptStack.Navigator>
  );
};

export default Register;

const styles = StyleSheet.create({});
