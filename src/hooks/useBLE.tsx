import {useEffect, useState} from 'react';
import {PermissionsAndroid, Platform} from 'react-native';
import {BleManager} from 'react-native-ble-plx';
import {NativeAppEventEmitter} from 'react-native';

type PermissionCallBack = (result: boolean) => void;

interface BluetoothLowEnergyApi {
  requestPermissions(callback: PermissionCallBack): Promise<void>;
  scanForDevices(): void;
  allDevices: any[];
}

const bleManager = new BleManager();

export default function useBLE(): BluetoothLowEnergyApi {
  const [allDevices, setAllDevices] = useState<unknown[]>([]);

  const requestPermissions = async (callBack: PermissionCallBack) => {
    if (Platform.OS === 'android') {
      let grantedStatus = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: 'Location Permissions',
          message: 'To use blutooth the app needs location permissions',
          buttonPositive: 'Ok',
          buttonNegative: 'Cancel',
          buttonNeutral: 'May be later',
        },
      );

      callBack(grantedStatus === PermissionsAndroid.RESULTS.GRANTED);
    } else {
      callBack(true);
    }
  };

  const scanForDevices = () => {
    bleManager.startDeviceScan(null, null, (error, device) => {
      if (error) {
        console.log(JSON.stringify(error));
      }

      if (device) {
        //set device to state
        setAllDevices(prevState => [...prevState, device]);
      }
    });
  };

  return {
    requestPermissions,
    scanForDevices,
    allDevices,
  };
}
