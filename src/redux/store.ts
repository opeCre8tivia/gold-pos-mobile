import {configureStore} from '@reduxjs/toolkit';
import {useDispatch} from 'react-redux';

import linkingSlice from './features/linking.slice';
import outletSlice from './features/outlet.slice';
import verifyPasscodeSlice from './features/verifyPasscode.slice';
import clockingSlice from './features/clocking.slice';
import directSaleSlice from './features/directSale.slice';
import menuDataSlice from './features/menuData.slice';
import paymentSlice from './features/payment.slice';
import settingsSlice from './features/settings.slice';
import printerSlice from './features/printer.slice';
import cashdrawersSlice from './features/cashdrawers.slice';
import cashinSlice from './features/cashin.slice';
import payoutSlice from './features/payout.slice';
import cashdropSlice from './features/cashdrop.slice';
import confirmpinSlice from './features/confirmpin.slice';
import orderSlice from './features/order.slice';
import shiftreviewSlice from './features/shiftreview.slice';
import customerSlice from './features/customer.slice';

// ...
const store = configureStore({
  reducer: {
    linkingSlice,
    outletSlice,
    verifyPasscodeSlice,
    clockingSlice,
    directSaleSlice,
    menuDataSlice,
    paymentSlice,
    settingsSlice,
    printerSlice,
    cashdrawersSlice,
    cashinSlice,
    payoutSlice,
    cashdropSlice,
    confirmpinSlice,
    orderSlice,
    shiftreviewSlice,
    customerSlice,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch; //ts
export const useAppDispatch: () => AppDispatch = useDispatch;
export default store;
