import {createAsyncThunk} from '@reduxjs/toolkit';
import axios from 'axios';
import {API_URL} from '../../config';
import {AppDispatch} from '../store';
import {setHeaders} from '../../utils/setHeaders';

type PayoutResponse = any;
type PayoutInput = {
  reason: string;
  amount: number;
  cashdrawerOpsId: number;
  payoutToId: number;
};

export const _payOut = createAsyncThunk<
  PayoutResponse,
  PayoutInput,
  {dispatch: AppDispatch}
>('payout/create', async function (payoutData) {
  try {
    let AuthHeaders = await setHeaders();
    const {data} = await axios.post(
      `${API_URL}/cashdrawer/payout`,
      payoutData,
      {
        headers: AuthHeaders,
      },
    );

    return data;
  } catch (error) {
    console.log(error, '---pay out err');
    return {
      isError: true,
      msg: 'Error! paying out ',
    };
  }
});
