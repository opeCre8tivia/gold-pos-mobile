import {createAsyncThunk} from '@reduxjs/toolkit';
import axios from 'axios';
import {API_URL} from '../../config';
import {AppDispatch} from '../store';
import {setHeaders} from '../../utils/setHeaders';

type CashdropResponse = any;
type CashdropInput = {
  amount: number;
  cashdrawerOpsId: number;
};

export const _cashDrop = createAsyncThunk<
  CashdropResponse,
  CashdropInput,
  {dispatch: AppDispatch}
>('cashdrop/create', async function (cashdropData) {
  try {
    let AuthHeaders = await setHeaders();
    const {data} = await axios.post(
      `${API_URL}/cashdrawer/cashdrop`,
      cashdropData,
      {
        headers: AuthHeaders,
      },
    );

    return data;
  } catch (error) {
    console.log(error, '---cashdrop out err');
    return {
      isError: true,
      msg: 'Error! doing cashdrop',
    };
  }
});
