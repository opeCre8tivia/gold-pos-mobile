import {createAsyncThunk} from '@reduxjs/toolkit';
import {API_URL} from '../../config';
import axios from 'axios';
import {ParkedOrder} from '../../types/main';
import {AppDispatch} from '../store';
import {setHeaders} from '../../utils/setHeaders';

type ParkResponse = any;
type ParkInput = ParkedOrder;

export const _parkOrder = createAsyncThunk<
  ParkResponse,
  ParkInput,
  {dispatch: AppDispatch}
>('parking/create', async function (orderData) {
  try {
    let AuthHeaders = await setHeaders();
    const {data} = await axios.post(`${API_URL}/sales/park`, orderData, {
      headers: AuthHeaders,
    });

    return data;
  } catch (error) {
    console.log(error);
    return {
      isError: true,
      msg: 'Error parking order',
    };
  }
});

export const _listParkedOrders = createAsyncThunk(
  'park/list',
  async function () {
    try {
      let AuthHeaders = await setHeaders();
      const {data} = await axios.get(`${API_URL}/sales/park/list`, {
        headers: AuthHeaders,
      });

      return data;
    } catch (error) {
      console.log(error);
      return {
        isError: true,
        msg: 'Something has gone wrong',
      };
    }
  },
);

type CloseParkedOrderResponse = any;
type CloseParkedOrderInput = number;

export const _closeParkedOrder = createAsyncThunk<
  CloseParkedOrderResponse,
  CloseParkedOrderInput,
  {dispatch: AppDispatch}
>('park/close', async function (id) {
  try {
    let AuthHeaders = await setHeaders();
    const {data} = await axios.put(
      `${API_URL}/sales/park/close/${id}`,
      {isOpen: true},
      {
        headers: AuthHeaders,
      },
    );

    console.log(data, '------clode resss');
    return data;
  } catch (error) {
    console.log(error);
    return {
      isError: true,
      msg: 'Something has gone wrong',
    };
  }
});
