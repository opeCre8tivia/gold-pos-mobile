import {createAsyncThunk} from '@reduxjs/toolkit';
import axios from 'axios';
import {API_URL} from '../../config';
import {CustomerType} from '../../types/main';
import {AppDispatch} from '../store';
import {setHeaders} from '../../utils/setHeaders';

type CustomerResponse = any;
type CustomerCreateInput = CustomerType;
export const _addCustomer = createAsyncThunk<
  CustomerResponse,
  CustomerCreateInput,
  {dispatch: AppDispatch}
>('customer/create', async function (customerData) {
  try {
    let AuthHeaders = await setHeaders();
    const {data} = await axios.post(
      `${API_URL}/customer/create`,
      customerData,
      {
        headers: AuthHeaders,
      },
    );
    return data;
  } catch (error) {
    console.log(error);
    return {
      isError: true,
      msg: 'Error creating customer',
    };
  }
});

export const _listCustomers = createAsyncThunk(
  'customer/list',
  async function () {
    try {
      let AuthHeaders = await setHeaders();
      const {data} = await axios.get(`${API_URL}/customer/list`, {
        headers: AuthHeaders,
      });
      return data;
    } catch (error) {
      console.log(error);

      return {
        isError: true,
        msg: 'Error retrieving customer',
      };
    }
  },
);
