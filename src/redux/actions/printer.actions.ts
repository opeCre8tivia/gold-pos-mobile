import {BluetoothManager} from '@brooons/react-native-bluetooth-escpos-printer';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {createAsyncThunk} from '@reduxjs/toolkit';
import {BLUETOOTH_DEVICE_KEY} from '../../constants/asyncStorage';

/**
 * An action that scans for both paired and
 * un paired devices
 */
export const scanForBluetoothPrinters = createAsyncThunk(
  'printer/search',
  async function () {
    try {
      const {
        found,
        paired,
      }: {
        found: Array<{address: string; name?: string}>;
        paired: any[];
      } = JSON.parse(await BluetoothManager.scanDevices());

      return {
        isError: false,
        msg: 'scan Successfull',
        data: {
          pairedDevices: paired,
          unPairedDevices: found,
        },
      };
    } catch (error) {
      console.log(error);
      return {
        isError: true,
        msg: 'Error! Scan Failed',
      };
    }
  },
);

/**
 * An action that connects to bluetooth devices
 */
export const connectToBluetoothPrinters = createAsyncThunk(
  'printer/connect',
  async function (device: {address: string; name?: string}) {
    try {
      await BluetoothManager.connect(device.address);

      let _connectedAddress: any =
        await BluetoothManager.getConnectedDeviceAddress();

      if (_connectedAddress === device.address) {
        await AsyncStorage.setItem(
          BLUETOOTH_DEVICE_KEY,
          JSON.stringify(device),
        );

        return {
          isError: false,
          msg: 'Connection Successfull',
          data: device,
        };
      }
    } catch (error) {
      console.log(error);
      return {
        isError: true,
        msg: 'Error! Connection Failed',
      };
    }
  },
);

export const disconnectBluetoothPrinter = createAsyncThunk(
  'printer/dis_connect',
  async function (device: {address: string; name?: string}) {
    try {
      let _connectedDevice: any =
        await BluetoothManager.getConnectedDeviceAddress();

      if (_connectedDevice === device.address) {
        await BluetoothManager.disconnect(device.address);

        //TODO: change to filter when dealing with multitple devices
        //  let _devices = asyncDevices && JSON.parse(asyncDevices)
        //filter out

        //store the remaining array in async

        return {
          isError: false,
          msg: 'Device disconnected successfully',
        };
      }
    } catch (error) {
      console.log(error);
      return {
        isError: true,
        msg: 'Device disconnection Failed',
      };
    }
  },
);
