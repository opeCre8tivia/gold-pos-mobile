import {createAsyncThunk} from '@reduxjs/toolkit';
import axios from 'axios';
import {API_URL} from '../../config';
import {AppDispatch} from '../store';
import {setHeaders} from '../../utils/setHeaders';

type CashinResponse = any;
type CashInInput = {
  reason: string;
  amount: number;
  cashdrawerOpsId: number;
};

export const _cashIn = createAsyncThunk<
  CashinResponse,
  CashInInput,
  {dispatch: AppDispatch}
>('cashIn/create', async function (cashIndata) {
  try {
    let AuthHeaders = await setHeaders();
    const {data} = await axios.post(
      `${API_URL}/cashdrawer/cashin`,
      cashIndata,
      {
        headers: AuthHeaders,
      },
    );

    return data;
  } catch (error) {
    console.log(error, '---cash in err');
    return {
      isError: true,
      msg: 'Error! cashing in ',
    };
  }
});
