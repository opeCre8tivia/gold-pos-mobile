import {createAsyncThunk} from '@reduxjs/toolkit';
import axios from 'axios';
import {API_URL} from '../../config';
import {setHeaders} from '../../utils/setHeaders';

export const _sendEmail = createAsyncThunk('send/email', async function () {
  try {
    let AuthHeaders = await setHeaders();
    const {data} = await axios.get(`${API_URL}/emailing/send`, {
      headers: AuthHeaders,
    });

    console.log(data, '----email sending test---->');

    return data;
  } catch (error) {
    console.log(error, '---cash in err');
    return {
      isError: true,
      msg: 'Error! cashing in ',
    };
  }
});
