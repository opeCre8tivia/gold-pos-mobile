import {createAsyncThunk} from '@reduxjs/toolkit';
import axios from 'axios';
import {API_URL} from '../../config';
import {setHeaders} from '../../utils/setHeaders';
import {CashDrawerOperation} from '../../types/main';
import {AppDispatch} from '../store';
import {Dispatch} from 'react';

export const _getCashDrawers = createAsyncThunk(
  'cashdrawer/list',
  async function () {
    try {
      let AuthHeaders = await setHeaders();
      const {data} = await axios.get(`${API_URL}/cashdrawer/outlet/list`, {
        headers: AuthHeaders,
      });

      return data;
    } catch (error) {
      console.log(error);
      return {
        isError: true,
        msg: 'Something has gone wrong!',
      };
    }
  },
);

type CreateOpsResponse = any;
type CreateOpsInput = CashDrawerOperation;

export const _createCashDrawerOperation = createAsyncThunk<
  CreateOpsResponse,
  CreateOpsInput,
  {dispatch: AppDispatch}
>('cashdrawer/create_operation', async function (opsInfo) {
  try {
    let AuthHeaders = await setHeaders();
    const {data} = await axios.post(
      `${API_URL}/cashdrawer/outlet/operation/create`,
      opsInfo,
      {
        headers: AuthHeaders,
      },
    );

    return data;
  } catch (error) {
    console.log(error);
    return {
      isError: true,
      msg: 'Something has gone wrong!',
    };
  }
});

/**
 * the method gets cashdrawer operations by current employees id
 */

export const _getCashDrawerOperations = createAsyncThunk<
  any,
  number,
  {dispatch: AppDispatch}
>('cashdrawer/list/operations', async function (employeeId) {
  try {
    let AuthHeaders = await setHeaders();
    console.log(employeeId, '-------------> employee id');
    const {data} = await axios.get(
      `${API_URL}/cashdrawer/outlet/operation/list/${employeeId}`,
      {
        headers: AuthHeaders,
      },
    );
    return data;
  } catch (error) {
    console.log(error);
    return {
      isError: true,
      msg: 'Something has gone wrong!',
    };
  }
});

type CloseOpsResponse = any;
type CLoseOpsInput = {
  id: number;
};
export const _closeCashDrawerOperations = createAsyncThunk<
  CloseOpsResponse,
  CLoseOpsInput,
  {dispatch: AppDispatch}
>('cashdrawer/close/operations', async function ({id}) {
  try {
    let AuthHeaders = await setHeaders();
    const {data} = await axios.put(`${API_URL}/cashdrawer/close/${id}`, {
      headers: AuthHeaders,
    });
    return data;
  } catch (error) {
    console.log(error);
    return {
      isError: true,
      msg: 'Something has gone wrong!',
    };
  }
});
