import {createAsyncThunk} from '@reduxjs/toolkit';
import axios from 'axios';
import {API_URL} from '../../config';
import {setHeaders} from '../../utils/setHeaders';
import {AppDispatch} from '../store';

export const _linkDevice = createAsyncThunk<any>(
  'device/linking',
  async function (data: any, thunk): Promise<any> {
    try {
      let response = await axios.post(`${API_URL}/device/link`, data);

      if (response?.data?.isError) {
        return response.data;
      }

      return {
        isError: false,
        msg: response.data.msg,
        data: response.data.payload,
      };
    } catch (error) {
      console.log(error, 'link error');

      return {
        isError: true,
        msg: 'Device connection Error',
      };
    }
  },
);

export const _getOutletInfo = createAsyncThunk(
  'outlet/outletInfo',
  async function (): Promise<any> {
    try {
      let AuthHeaders = await setHeaders();

      let response = await axios.get(`${API_URL}/device/outlet`, {
        headers: AuthHeaders,
      });

      if (response?.data?.isError) {
        return response.data;
      }

      return {
        isError: false,
        msg: response.data.msg,
        outlet: response.data.payload.outlet,
        business: response.data.payload.business,
        employees: response.data.payload.employees,
        business_hours: '0-0',
      };
    } catch (error) {
      return {
        isError: true,
        msg: 'Problem getting outlet profile',
      };
    }
  },
);

//verify passcode and clockin user
type VerifyResponse = any;
type VerifyInput = {
  passCode: string;
  employeeId: number;
};
export const _verifyPassCode = createAsyncThunk<
  VerifyResponse,
  VerifyInput,
  {dispatch: AppDispatch}
>('outlet/verifyPin', async function (data, thunk): Promise<any> {
  try {
    let AuthHeaders = await setHeaders();

    let response = await axios.post(`${API_URL}/clocking/verify`, data, {
      headers: AuthHeaders,
    });

    if (response?.data?.isError) {
      return {
        isError: true,
        msg: response.data.msg,
      };
    }

    return {
      isError: false,
      msg: response.data.msg,
    };
  } catch (error) {
    console.log(error, 'pin verify error');
    return {
      isError: true,
      msg: 'Error Try again',
    };
  }
});

//Confirm PIN code; this happens whenever pin code model pops up
type ConfirmPINResponse = any;
type ConfirmPINInput = {
  passCode: string;
  employeeId: number;
};
export const _confirmPIN = createAsyncThunk<
  ConfirmPINResponse,
  ConfirmPINInput,
  {dispatch: AppDispatch}
>('outlet/confirmPin', async function (data, thunk): Promise<any> {
  try {
    let AuthHeaders = await setHeaders();

    let response = await axios.post(`${API_URL}/clocking/confirm`, data, {
      headers: AuthHeaders,
    });

    if (response?.data?.isError) {
      return {
        isError: true,
        msg: response.data.msg,
      };
    }

    return {
      isError: false,
      msg: response.data.msg,
    };
  } catch (error) {
    console.log(error, 'pin confirmation error');
    return {
      isError: true,
      msg: 'Error Try again',
    };
  }
});

//clockout user
export const _clockoutEmployee = createAsyncThunk<any>(
  'outlet/clockout',
  async function (data, thunk): Promise<any> {
    try {
      let AuthHeaders = await setHeaders();

      let response = await axios.post(`${API_URL}/clocking/clockout`, data, {
        headers: AuthHeaders,
      });

      if (response?.data?.isError) {
        return {
          isError: true,
          msg: response.data.msg,
        };
      }

      return {
        isError: false,
        msg: response.data.msg,
      };
    } catch (error) {
      console.log(error, 'Clocking out user');
      return {
        isError: true,
        msg: 'Error Clocking out Try again',
      };
    }
  },
);

export const _getEmployeesInShift = createAsyncThunk<any>(
  'outlet/shiftList',
  async function (): Promise<any> {
    try {
      let AuthHeaders = await setHeaders();

      let response = await axios.get(`${API_URL}/clocking/list/clockedin`, {
        headers: AuthHeaders,
      });

      console.log(response.data, 'list response');

      if (response?.data?.isError) {
        return {
          isError: true,
          msg: response.data.msg,
        };
      }

      return {
        isError: false,
        msg: response.data.msg,
        data: response.data.payload,
      };
    } catch (error) {
      console.log(error, 'Shift Employee List Error');
      return {
        isError: true,
        msg: 'Error organising Shift Try again',
      };
    }
  },
);

/**   menus ations */

export const _getMenus = createAsyncThunk<any>(
  'outlet/menus',
  async function (): Promise<any> {
    try {
      let AuthHeaders = await setHeaders();

      console.log(AuthHeaders);
      let response = await axios.get(`${API_URL}/menu/get/outlet/all`, {
        headers: AuthHeaders,
      });

      if (response?.data?.isError) {
        return {
          isError: true,
          msg: response.data.msg,
        };
      }

      let _allMenuItems: any[] = [];
      let _allCategories: any[] = [];

      response.data &&
        response.data.payload.forEach((menu: any) => {
          menu.menuItemCategory?.forEach((category: any) => {
            _allCategories.push(category);

            category.menuItems?.forEach((menuItem: any) => {
              _allMenuItems.push(menuItem);
            });
          });
        });

      return {
        isError: false,
        msg: 'success',
        data: response.data.payload,
        menuItems: _allMenuItems,
        categories: _allCategories,
      };
    } catch (error) {
      console.log(error, 'menu get error');
      return {
        isError: true,
        msg: 'Error getting menu try a gain',
      };
    }
  },
);

type RecordSaleResponse = any;
type RecoredSaleInput = {
  outletId: number;
  servedBy: number;
  items: any[];
  total: number;
  amountRecieved: number;
  change: number;
  denominations: any[];
};

export const _recordSale = createAsyncThunk<
  RecordSaleResponse,
  RecoredSaleInput,
  {dispatch: AppDispatch}
>('outlet/record_sale', async function (data): Promise<any> {
  try {
    let AuthHeaders = await setHeaders();

    let response = await axios.post(`${API_URL}/sales/make`, data, {
      headers: AuthHeaders,
    });

    if (response?.data?.isError) {
      return {
        isError: true,
        msg: response.data.msg,
      };
    }

    return {
      isError: false,
      msg: 'success',
      data: response.data.payload,
    };
  } catch (error) {
    console.log(error, 'Payment Error');
    return {
      isError: true,
      msg: 'Error handling Payment',
    };
  }
});

/**
 * get daily sales
 */

export const _getDailySales = createAsyncThunk(
  'outlet/daily_sales',
  async function (): Promise<any> {
    try {
      let AuthHeaders = await setHeaders();

      let response = await axios.get(`${API_URL}/sales/daily`, {
        headers: AuthHeaders,
      });

      if (response?.data?.isError) {
        return {
          isError: true,
          msg: response.data.msg,
        };
      }

      return {
        isError: false,
        msg: 'success',
        data: response.data.payload,
      };
    } catch (error) {
      console.log(error, 'Error getting sales');
      return {
        isError: true,
        msg: 'Error getting sales',
      };
    }
  },
);

export const _getDailySale = createAsyncThunk(
  'outlet/daily_sale',
  async function (orderId: number): Promise<any> {
    try {
      let AuthHeaders = await setHeaders();

      let response = await axios.post(
        `${API_URL}/sales/daily/orderid`,
        {
          orderId: orderId,
        },
        {
          headers: AuthHeaders,
        },
      );

      if (response?.data?.isError) {
        return {
          isError: true,
          msg: response.data.msg,
        };
      }

      return {
        isError: false,
        msg: 'success',
        data: [response.data.payload],
      };
    } catch (error) {
      console.log(error, 'Error getting sale');
      return {
        isError: true,
        msg: 'Error getting sale',
      };
    }
  },
);

export const _getAllSettings = createAsyncThunk(
  'get/all_settings',
  async function (): Promise<any> {
    try {
      let AuthHeaders = await setHeaders();

      let response = await axios.get(
        `${API_URL}/general/business/settings/all`,
        {
          headers: AuthHeaders,
        },
      );

      if (response?.data?.isError) {
        return {
          isError: true,
          msg: response.data.msg,
        };
      }

      return {
        isError: false,
        msg: 'success',
        data: response.data.payload,
      };
    } catch (error) {
      console.log(error, 'Error getting sale');
      return {
        isError: true,
        msg: 'Error getting sale',
      };
    }
  },
);

export const _getReceiptVoidingReasons = createAsyncThunk(
  'get/all_receipt_voiding_reasons',
  async function (): Promise<any> {
    try {
      let AuthHeaders = await setHeaders();

      let response = await axios.get(
        `${API_URL}/voiding/receipt/reason/list/mobile`,
        {
          headers: AuthHeaders,
        },
      );

      return response.data;
    } catch (error) {
      console.log(error, 'Error getting Reasons');
      return {
        isError: true,
        msg: 'Error getting reasons',
      };
    }
  },
);

type VoidingResponse = any;
type VoidingInput = {
  saleId: number;
  reasonId: number;
};

export const _voidAsale = createAsyncThunk<
  VoidingResponse,
  VoidingInput,
  {dispatch: AppDispatch}
>('post/void_asale', async function (voidingData): Promise<any> {
  try {
    let AuthHeaders = await setHeaders();

    let response = await axios.post(`${API_URL}/voiding/sale`, voidingData, {
      headers: AuthHeaders,
    });

    return response.data;
  } catch (error) {
    console.log(error, 'Error Voiding sale');
    return {
      isError: true,
      msg: 'Error voiding sale',
    };
  }
});
