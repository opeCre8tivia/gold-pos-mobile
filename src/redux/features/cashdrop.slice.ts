import {createSlice} from '@reduxjs/toolkit';
import {_payOut} from '../actions/payout.actions';
import {_cashDrop} from '../actions/cashdrop.actions';

interface InitialState {
  loading: boolean;
  isError: boolean;
  isSuccess: boolean;
  responseMessage: string | null;
  showCashdropModal: boolean;
}

const initialState: InitialState = {
  isError: false,
  isSuccess: false,
  loading: false,
  responseMessage: null,
  showCashdropModal: false,
};

const cashdropSlice = createSlice({
  name: 'cashdrop',
  initialState: initialState,
  reducers: {
    clearCashdropState: state => {
      state.loading = false;
      state.isError = false;
      state.isSuccess = false;
      state.responseMessage = null;
    },
    toggleCashdropModalVisibility: state => {
      state.showCashdropModal = !state.showCashdropModal;
    },
  },
  extraReducers: builder => {
    builder.addCase(_cashDrop.pending, state => {
      state.loading = true;
    });
    builder.addCase(_cashDrop.fulfilled, (state, action) => {
      state.loading = false;
      if (action.payload.isError) {
        state.isError = true;
        state.isSuccess = false;
        state.responseMessage = action.payload.msg;
      }
      if (!action.payload.isError) {
        state.isError = false;
        state.isSuccess = true;
        state.responseMessage = action.payload.msg;
        state.showCashdropModal = false;
      }
    });
    builder.addCase(_cashDrop.rejected, (state, action: any) => {
      state.loading = true;
      state.isError = true;
      state.isSuccess = false;
      state.responseMessage = action.payload.msg;
    });
  },
});

export const {clearCashdropState, toggleCashdropModalVisibility} =
  cashdropSlice.actions;
export default cashdropSlice.reducer;
