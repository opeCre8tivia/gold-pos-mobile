import { createSlice } from "@reduxjs/toolkit";
import { _getMenus } from "../actions";

interface InitialState {
    rowMenuData: any[]
    selectedMenu:number | null //id
    categories: any[]
    menuItems: any[]
    modifiers: any[]
    isError: boolean


}

const initialState:InitialState = {
    rowMenuData: [],
    selectedMenu:null,
    categories: [],
    menuItems: [],
    modifiers: [],
    isError: false
}


export const menuDataSlice = createSlice({
    name: "getMenuData",
    initialState: initialState,
    reducers: {
         selectMenu:(state,action)=>{
             state.selectedMenu = action.payload
         }
    },
    extraReducers: (builder) => {
        builder.addCase(_getMenus.fulfilled, (state, action) => {

            if (action.payload.isError === false) {
                state.rowMenuData = action.payload.data
                state.menuItems = action.payload.menuItems
                state.categories = action.payload.categories
            }

        })

        builder.addCase(_getMenus.rejected, (state) => {

            state.isError = true

        })

    }
})

export const {
   selectMenu
} = menuDataSlice.actions

export default menuDataSlice.reducer
