import {createSlice} from '@reduxjs/toolkit';
import {CashDrawerOperation} from '../../types/main';

interface InitialState {
  drawerOperationBeingReviewed: CashDrawerOperation | null;
}

const initialState: InitialState = {
  drawerOperationBeingReviewed: null,
};
const shiftReveiwSlice = createSlice({
  name: 'shiftreview',
  initialState: initialState,
  reducers: {
    _setCashDrawerOpsForReview: (state, action) => {
      state.drawerOperationBeingReviewed = action.payload;
    },
  },
});

export const {_setCashDrawerOpsForReview} = shiftReveiwSlice.actions;

export default shiftReveiwSlice.reducer;
