import { createSlice } from "@reduxjs/toolkit";
import { _verifyPassCode } from "../actions";

interface OutletInfo {
    loading:boolean,
    isError:boolean,
    error_message:string | null,
    isSuccess:boolean
}

const initialState:OutletInfo = {
    loading:true,
    error_message:null,
    isError:false,
    isSuccess:false
}



const verifyPasscodeSlice = createSlice({
    name:"verifyPassCode",
    initialState:initialState,
    reducers:{
        clearverifyPasscodeSliceState : (state)=>{
            state.loading = false
            state.isSuccess = false
            state.isError = false
            state.error_message = null
        }
    },
    extraReducers:(builder)=>{
        builder.addCase(_verifyPassCode.pending,(state)=>{
            state.loading = true
        })

        builder.addCase(_verifyPassCode.fulfilled,(state,action)=>{
         
            if(action.payload.isError){
                state.loading = false
                state.isError = true
                state.isSuccess = false
                state.error_message = action.payload.msg
            }
            if(action.payload.isError === false) {
              state.loading = false
              state.isSuccess = true
              state.isError = false
              

            }
        })

        builder.addCase(_verifyPassCode.rejected,(state,action)=>{

             state.loading = false
             state.isError = true
             state.isSuccess = false
             state.error_message = "Error Try again !"


        })

    }
})



export const {
    clearverifyPasscodeSliceState
} = verifyPasscodeSlice.actions

export default verifyPasscodeSlice.reducer