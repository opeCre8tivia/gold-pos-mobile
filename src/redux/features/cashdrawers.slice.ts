import {createSlice} from '@reduxjs/toolkit';
import {CashDrawer, CashDrawerOperation, Denomination} from '../../types/main';
import {
  _closeCashDrawerOperations,
  _createCashDrawerOperation,
  _getCashDrawerOperations,
  _getCashDrawers,
} from '../actions/cashdrawer.actions';

interface Deno extends Denomination {
  quantity: number;
}

interface InitialState {
  loading: boolean;
  isError: boolean;
  isSuccess: boolean;
  responseMessage: string | null;
  cashDrawers: CashDrawer[] | null;
  cashDrawerOperations: CashDrawerOperation[] | null;
  currentUsersCashDrawerOperation: CashDrawerOperation | null;
  denominationToCount: Deno | null;
  counted: Deno[];
  employee_should_count: boolean;
}

const initialState: InitialState = {
  loading: false,
  isError: false,
  isSuccess: false,
  cashDrawerOperations: null,
  cashDrawers: null,
  responseMessage: null,
  denominationToCount: null,
  counted: [],
  employee_should_count: true,
  currentUsersCashDrawerOperation: null,
};

const cashDrawerSlice = createSlice({
  name: 'cashDrawer/slice',
  initialState: initialState,
  reducers: {
    clearCashDrawerState: state => {
      state.loading = false;
      state.isSuccess = false;
      state.isError = false;
      state.responseMessage = null;
    },
    resetCount: state => {
      state.isSuccess = false;
      state.isError = false;
      state.responseMessage = null;
      state.counted = [];
      state.denominationToCount = null;
    },
    bypassCountingScreens: state => {
      state.employee_should_count = false;
    },

    addDenominationToCount: (state, action) => {
      state.denominationToCount = action.payload;
    },
    count: (state, action) => {
      state.counted = [...state.counted, action.payload];
    },
    modifyCount: (state, action) => {
      let newCounted = state.counted.filter(
        item => item.id != action.payload.id,
      );
      newCounted = [...newCounted, action.payload];
      state.counted = newCounted;
    },
    setCurrentCashDrawerOps: (state, action) => {
      state.currentUsersCashDrawerOperation = action.payload;
    },
  },
  extraReducers: builder => {
    builder.addCase(_getCashDrawers.pending, state => {
      state.loading = true;
    });
    builder.addCase(_getCashDrawers.fulfilled, (state, action) => {
      state.loading = false;

      if (action.payload.isError) {
        // state.isError = true;
        // state.isSuccess = false;
        state.responseMessage = action.payload.msg;
      }
      if (!action.payload.isError) {
        // state.isError = false;
        // state.isSuccess = true;
        state.responseMessage = action.payload.msg;
        state.cashDrawers = action.payload.payload;
      }
    });
    builder.addCase(_getCashDrawers.rejected, (state, action: any) => {
      state.loading = false;
      state.isError = true;
      state.isSuccess = false;
      state.responseMessage = action.payload.msg;
    });

    // creating cash drawer

    builder.addCase(_createCashDrawerOperation.pending, state => {
      state.loading = true;
    });
    builder.addCase(_createCashDrawerOperation.fulfilled, (state, action) => {
      state.loading = false;
      if (action.payload.isError) {
        state.isError = true;
        state.isSuccess = false;
        state.responseMessage = action.payload.msg;
      }
      if (!action.payload.isError) {
        state.isError = false;
        state.isSuccess = true;
        state.responseMessage = action.payload.msg;
        state.employee_should_count = false; //this anbles app to swith to a diffrent nav stack
      }
    });
    builder.addCase(
      _createCashDrawerOperation.rejected,
      (state, action: any) => {
        state.loading = false;
        state.isError = true;
        state.isSuccess = false;
        state.responseMessage = action.payload.msg;
      },
    );

    builder.addCase(_getCashDrawerOperations.pending, state => {
      state.loading = true;
    });

    builder.addCase(_getCashDrawerOperations.fulfilled, (state, action) => {
      state.loading = false;
      if (action.payload.isError) {
        state.isError = true;
        state.isSuccess = false;
        state.responseMessage = action.payload.msg;
      }
      if (!action.payload.isError) {
        state.isError = false;
        state.isSuccess = true;
        state.responseMessage = action.payload.msg;
        state.cashDrawerOperations = action.payload.payload;
      }
    });
    builder.addCase(_getCashDrawerOperations.rejected, (state, action: any) => {
      state.loading = false;
      state.isError = true;
      state.isSuccess = false;
      state.responseMessage = action.payload.msg;
    });

    // closing drawer
    builder.addCase(_closeCashDrawerOperations.pending, state => {
      state.loading = true;
    });
    builder.addCase(_closeCashDrawerOperations.fulfilled, (state, action) => {
      state.loading = false;
      if (action.payload.isError) {
        state.isError = true;
        state.isSuccess = false;
        state.responseMessage = action.payload.msg;
      }
      if (!action.payload.isError) {
        state.isError = false;
        state.isSuccess = true;
        state.responseMessage = action.payload.msg;
      }
    });
    builder.addCase(
      _closeCashDrawerOperations.rejected,
      (state, action: any) => {
        state.loading = false;
        state.isError = false;
        state.isSuccess = true;
        state.responseMessage = action.payload.msg;
      },
    );
  },
});

export const {
  clearCashDrawerState,
  addDenominationToCount,
  count,
  modifyCount,
  resetCount,
  bypassCountingScreens,
  setCurrentCashDrawerOps,
} = cashDrawerSlice.actions;

export default cashDrawerSlice.reducer;
