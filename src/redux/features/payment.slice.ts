import {createSlice} from '@reduxjs/toolkit';
import {Sale} from '../../types/main';
import {_getMenus, _recordSale} from '../actions';

interface InitialState {
  loading: boolean;
  isError: boolean;
  isSuccess: boolean;
  payment_error: string | null;
  current_sale: Sale | null;
}

const initialState: InitialState = {
  loading: false,
  isError: false,
  isSuccess: false,
  payment_error: null,
  current_sale: null,
};

export const paymentSlice = createSlice({
  name: 'payment',
  initialState: initialState,
  reducers: {
    clearPaymentState: state => {
      state.loading = false;
      state.isError = false;
      state.isSuccess = false;
      state.payment_error = null;
    },
  },
  extraReducers: builder => {
    builder.addCase(_recordSale.pending, state => {
      state.loading = true;
    });

    builder.addCase(_recordSale.fulfilled, (state, action) => {
      state.loading = false;

      if (action.payload.isError) {
        state.isError = true;
        state.isSuccess = false;
        state.payment_error = action.payload.msg;
      }

      if (!action.payload.isError) {
        state.isSuccess = true;
        state.isError = false;
        state.payment_error = null;
        state.current_sale = action.payload.data;
      }
    });

    builder.addCase(_recordSale.rejected, (state, action) => {
      state.loading = false;
      state.isError = true;
      state.payment_error = 'Error ! try again';
    });
  },
});

export const {clearPaymentState} = paymentSlice.actions;

export default paymentSlice.reducer;
