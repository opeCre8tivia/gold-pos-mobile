import { createSlice } from "@reduxjs/toolkit";
import {_linkDevice} from '../actions'

interface Action {
    isError:boolean,
    isSuccess:boolean,
    loading:boolean,
    error_msg:string
}

const linkingSlice= createSlice({
    name:"linking",
    initialState:{
        loading:false,
        token :null,
        isError:false,
        error_msg:null,
        isSuccess:false,
        
    },
    reducers:{
        clearLinkingMessages:(state)=>{
            state.error_msg = null
            state.isError = false
            state.isError = false
        }
    },
    extraReducers:(builder)=>{
         builder.addCase(_linkDevice.pending,(state)=>{
                  state.loading = true
         }) 
         builder.addCase(_linkDevice.fulfilled,(state,action)=>{
                 if(action.payload.isError){
                    state.loading = false
                    state.isError = true
                    state.error_msg = action.payload.msg
                 }
                 else{
                    state.loading = false
                    state.token = action.payload.data
                    state.isSuccess = true
                 }
         })
         builder.addCase(_linkDevice.rejected,(state,action)=>{
                state.loading = false
                state.isError = true
         })

    }

})

export const {
    clearLinkingMessages
} = linkingSlice.actions
export default linkingSlice.reducer