import {createSlice} from '@reduxjs/toolkit';
import {ParkedOrder} from '../../types/main';
import {
  _closeParkedOrder,
  _listParkedOrders,
  _parkOrder,
} from '../actions/parking.actions';
import {_getReceiptVoidingReasons, _voidAsale} from '../actions';

interface InitialState {
  loading: boolean;
  parkError: boolean;
  parkSuccess: boolean;
  parkResponseMessage: string | null;
  parkedOrders: ParkedOrder[];
  selectedOrder: ParkedOrder | null;
  voidingReasons: {id: number; isArchived: boolean; reason: string}[];
}

const initialState: InitialState = {
  loading: false,
  parkError: false,
  parkSuccess: false,
  parkResponseMessage: null,
  parkedOrders: [],
  selectedOrder: null,
  voidingReasons: [],
};

const orderSlice = createSlice({
  name: 'orders',
  initialState: initialState,
  reducers: {
    clearOrderState: state => {
      state.parkError = false;
      state.parkSuccess = false;
      state.parkResponseMessage = null;
      state.loading = false;
    },
    _addSelectedOrder: (state, action) => {
      state.selectedOrder = action.payload;
    },
  },
  extraReducers: builder => {
    builder.addCase(_parkOrder.pending, state => {
      state.loading = true;
    }),
      builder.addCase(_parkOrder.fulfilled, (state, action) => {
        state.loading = false;
        if (action.payload.isError) {
          state.parkError = true;
          state.parkSuccess = false;
          state.parkResponseMessage = action.payload.msg;
        }
        if (!action.payload.isError) {
          state.parkError = false;
          state.parkSuccess = true;
          state.parkResponseMessage = action.payload.msg;
        }
      });
    builder.addCase(_parkOrder.rejected, (state, action: any) => {
      state.loading = false;
      state.parkError = true;
      state.parkSuccess = false;
      state.parkResponseMessage = action.payload.msg;
    });

    // listing parked orders

    builder.addCase(_listParkedOrders.pending, state => {
      state.loading = true;
    });
    builder.addCase(_listParkedOrders.fulfilled, (state, action) => {
      state.loading = false;
      if (action.payload.isError) {
        state.parkError = true;
        state.parkSuccess = false;
        state.parkResponseMessage = action.payload.msg;
      }
      if (!action.payload.isError) {
        // state.parkError = false;
        // state.parkSuccess = true;
        // state.parkResponseMessage = action.payload.msg;
        state.parkedOrders = action.payload.payload;
      }
    });
    builder.addCase(_listParkedOrders.rejected, (state, action: any) => {
      state.loading = false;
      state.parkError = true;
      state.parkSuccess = false;
      state.parkResponseMessage = action.payload.msg;
    });

    //closing an order

    builder.addCase(_closeParkedOrder.pending, (state, action: any) => {
      state.loading = true;
    });
    builder.addCase(_closeParkedOrder.fulfilled, (state, action: any) => {
      state.loading = true;
      state.loading = false;
      if (action.payload.isError) {
        state.parkError = true;
        state.parkSuccess = false;
        state.parkResponseMessage = action.payload.msg;
      }
      if (!action.payload.isError) {
        state.parkError = false;
        state.parkSuccess = true;
        state.parkResponseMessage = action.payload.msg;
      }
    });

    builder.addCase(_closeParkedOrder.rejected, (state, action: any) => {
      state.loading = false;
      state.parkError = true;
      state.parkSuccess = false;
      state.parkResponseMessage = action.payload.msg;
    });

    // voiding reasons

    builder.addCase(
      _getReceiptVoidingReasons.fulfilled,
      (state, action: any) => {
        state.loading = false;
        if (action.payload.isError) {
          //  fail silently
        }
        if (!action.payload.isError) {
          state.voidingReasons = action.payload.payload;
        }
      },
    );

    // voiding sale

    builder.addCase(_voidAsale.pending, state => {
      state.loading = true;
    });
    builder.addCase(_voidAsale.fulfilled, (state, action) => {
      state.loading = true;
      if (action.payload.isError) {
        state.parkError = true;
        state.parkSuccess = false;
        state.parkResponseMessage = action.payload.msg;
      }
      if (!action.payload.isError) {
        state.parkError = false;
        state.parkSuccess = true;
        state.parkResponseMessage = action.payload.msg;
      }
    });
    builder.addCase(_voidAsale.rejected, (state, action: any) => {
      state.loading = true;

      state.parkError = true;
      state.parkSuccess = false;
      state.parkResponseMessage = action.payload.msg;
    });
  },
});

export const {clearOrderState, _addSelectedOrder} = orderSlice.actions;
export default orderSlice.reducer;
