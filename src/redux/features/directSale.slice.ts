import {createSlice} from '@reduxjs/toolkit';
import {ParkedOrder} from '../../types/main';

interface InitialState {
  addedMenuItems: any[];
  addedMenuItemsTotal: number;
  servedBy: any;
  customRecievedAmount: number | null;
  fromParkedSale: boolean;
}

const initialState: InitialState = {
  addedMenuItems: [],
  addedMenuItemsTotal: 0,
  servedBy: null,
  customRecievedAmount: null,
  fromParkedSale: false,
};

export const directSaleSlice = createSlice({
  name: 'directSale',
  initialState: initialState,
  reducers: {
    setWhoIsServing: (state, action) => {
      state.servedBy = action.payload;
    },
    addMenuItem: (state, action) => {
      state.addedMenuItems = [...state.addedMenuItems, action.payload];
      //TODO: use reduce to calculate total price
      state.addedMenuItemsTotal = state.addedMenuItems.reduce(
        (acc, val, arr) => acc + parseInt(val.price),
        0,
      );
    },
    increamentAddedItem: (state, action: {payload: any[]}) => {
      state.addedMenuItems = action.payload;
      state.addedMenuItemsTotal = state.addedMenuItems.reduce(
        (acc, val, arr) => acc + val.total,
        0,
      );
    },
    decreamentAddedItem: (state, action: {payload: any[]}) => {
      state.addedMenuItems = action.payload;
      state.addedMenuItemsTotal = state.addedMenuItems.reduce(
        (acc, val, arr) => acc + val.total,
        0,
      );
    },
    removeAddedMenuItem: (state, action: {payload: any[]}) => {
      state.addedMenuItems = action.payload;
      state.addedMenuItemsTotal =
        action.payload.length === 0
          ? 0
          : state.addedMenuItems.reduce((acc, val, arr) => acc + val.total, 0);
    },
    setCustomRecievedAmount: (state, action) => {
      state.customRecievedAmount = action.payload;
    },
    clearDirectSaleState: state => {
      state.addedMenuItems = [];
      state.addedMenuItemsTotal = 0;
      state.customRecievedAmount = 0;
      state.fromParkedSale = false;
    },
    updateAddedMenuItem: (state, action) => {
      //remove object from list
      let _newArr = state.addedMenuItems.filter(
        (item: any) => item.id !== action.payload.id,
      );
      //replace it using splice | unshift
      _newArr.unshift(action.payload);

      state.addedMenuItems = _newArr;
    },

    unparkOrder: (state, action: {type: string; payload: ParkedOrder}) => {
      let _addedMenuItems: any[] = [];

      action.payload.menuItems?.filter(item => {
        let _parsed = JSON.parse(item);
        _addedMenuItems.push(_parsed);
      });
      let _total = action.payload.total;

      if (_addedMenuItems && _total) {
        state.addedMenuItems = _addedMenuItems;
        state.addedMenuItemsTotal = _total;
        state.servedBy = action.payload.employee;
        state.fromParkedSale = true;
      }
    },
  },
});

export const {
  addMenuItem,
  increamentAddedItem,
  decreamentAddedItem,
  removeAddedMenuItem,
  clearDirectSaleState,
  setWhoIsServing,
  setCustomRecievedAmount,
  updateAddedMenuItem,
  unparkOrder,
} = directSaleSlice.actions;

export default directSaleSlice.reducer;
