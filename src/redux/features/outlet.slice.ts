import { createSlice } from "@reduxjs/toolkit";
import { _getOutletInfo } from "../actions";

interface OutletInfo {
    loading:boolean
    outlet:any
    business:any
    employees:any
    business_hours:any
    isSalesPeriodOn:boolean
    isError:boolean
    isSuccess:boolean
}

const initialState:OutletInfo = {
    loading:true,
    outlet:null,
    business:null,
    employees:null,
    business_hours:null,
    isSalesPeriodOn:false,
    isError:false,
    isSuccess:false
}



const outletSlice = createSlice({
    name:"outlet",
    initialState:initialState,
    reducers:{
        clearOutletState : (state)=>{
            state.loading = false
            state.isSuccess = false
            state.isError = false
        }
    },
    extraReducers:(builder)=>{
        builder.addCase(_getOutletInfo.pending,(state)=>{
            state.loading = true
        })

        builder.addCase(_getOutletInfo.fulfilled,(state,action)=>{
           
              
            if(action.payload.isError){
                state.loading = false
                state.isError = true
                state.isSuccess = false
            }
            else {
              state.loading = false
              state.isSuccess = true
              state.isError = false

              state.outlet = action.payload.outlet
              state.business = action.payload.business
              state.employees = action.payload.employees

              state.business_hours = action.payload.business_hours
            }
        })

        builder.addCase(_getOutletInfo.rejected,(state,action)=>{

             state.loading = false
             state.isError = true
             state.isSuccess = false

        })

    }
})



export const {
    clearOutletState
} = outletSlice.actions

export default outletSlice.reducer