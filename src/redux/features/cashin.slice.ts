import {createSlice} from '@reduxjs/toolkit';
import {_cashIn} from '../actions/cashin.actions';

interface InitialState {
  loading: boolean;
  isError: boolean;
  isSuccess: boolean;
  responseMessage: string | null;
  showCashInModal: boolean;
}

const initialState: InitialState = {
  isError: false,
  isSuccess: false,
  loading: false,
  responseMessage: null,
  showCashInModal: false,
};

const cashinSlice = createSlice({
  name: 'cashin',
  initialState: initialState,
  reducers: {
    clearCashInState: state => {
      state.loading = false;
      state.isError = false;
      state.isSuccess = false;
      state.responseMessage = null;
      state.showCashInModal = false;
    },
    toggleCashInModalVisibility: state => {
      state.showCashInModal = !state.showCashInModal;
    },
  },
  extraReducers: builder => {
    builder.addCase(_cashIn.pending, state => {
      state.loading = true;
    });
    builder.addCase(_cashIn.fulfilled, (state, action) => {
      state.loading = false;
      if (action.payload.isError) {
        state.isError = true;
        state.isSuccess = false;
        state.responseMessage = action.payload.msg;
      }
      if (!action.payload.isError) {
        state.isError = false;
        state.isSuccess = true;
        state.responseMessage = action.payload.msg;
        state.showCashInModal = false;
      }
    });
    builder.addCase(_cashIn.rejected, (state, action: any) => {
      state.loading = true;
      state.isError = true;
      state.isSuccess = false;
      state.responseMessage = action.payload.msg;
    });
  },
});

export const {clearCashInState, toggleCashInModalVisibility} =
  cashinSlice.actions;
export default cashinSlice.reducer;
