import {createSlice} from '@reduxjs/toolkit';
import {Employee} from '../../types/main';
import {_confirmPIN} from '../actions';

interface InitialState {
  confirmError: boolean;
  confirmSuccess: boolean;
  confirmResponse: string | null;
  enteredPIN: string;
  showConfirmPinModal: boolean;
  employeeToConfirm: Employee | null;
  loading: boolean;
}

interface ConfirmAction {
  type: string;

  /**
   * the payload must be the employee object
   */
  payload: Employee;
}

const initialState: InitialState = {
  confirmError: false,
  confirmSuccess: false,
  enteredPIN: '',
  confirmResponse: null,
  showConfirmPinModal: false,
  employeeToConfirm: null,
  loading: false,
};

const confirmpinSlice = createSlice({
  name: 'confirmpin',
  initialState: initialState,
  reducers: {
    clearConfirmpinState: state => {
      state.confirmError = false;
      state.confirmSuccess = false;
      state.enteredPIN = '';
      state.confirmResponse = null;
      state.loading = false;
    },
    confirmPIN: (state, action: ConfirmAction) => {
      state.showConfirmPinModal = true;
      state.employeeToConfirm = action.payload;
    },
    hidePinModal: state => {
      state.showConfirmPinModal = false;
    },
  },
  extraReducers: builder => {
    builder.addCase(_confirmPIN.pending, state => {
      state.loading = true;
    }),
      builder.addCase(_confirmPIN.fulfilled, (state, action) => {
        state.loading = false;
        if (action.payload.isError) {
          state.confirmError = true;
          state.confirmSuccess = false;
          state.confirmResponse = action.payload.msg;
        }
        if (!action.payload.isError) {
          state.confirmError = false;
          state.confirmSuccess = true;
          state.confirmResponse = action.payload.msg;
        }
      });
    builder.addCase(_confirmPIN.rejected, (state, action: any) => {
      state.loading = false;
      state.confirmError = true;
      state.confirmSuccess = false;
      state.confirmResponse = action.payload.msg;
    });
  },
});

export const {clearConfirmpinState, confirmPIN, hidePinModal} =
  confirmpinSlice.actions;
export default confirmpinSlice.reducer;
