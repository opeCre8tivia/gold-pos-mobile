import {createSlice} from '@reduxjs/toolkit';
import {BusinessSettings} from '../../types/main';
import {_getAllSettings} from '../actions';

interface MenuItem {
  id: number;
  title: string;
  icon: string;
  component: string;
}

interface InitialState {
  menu: MenuItem[] | [];
  selectedItem: MenuItem | null;
  allSettings: BusinessSettings | null;
}

const initialState: InitialState = {
  menu: [],
  selectedItem: null,
  allSettings: null,
};

const settingSlice = createSlice({
  name: 'settings/screen',
  initialState: initialState,
  reducers: {
    setMenu: (state, action) => {
      state.menu = action.payload;
    },
    selectMenuItem: (state, action) => {
      state.selectedItem = action.payload;
    },
    setInitialRoute: (state, action) => {
      state.selectedItem = state.menu.filter(
        (item: MenuItem) => item.id === action.payload.id,
      )[0];
    },
  },
  extraReducers: builder => {
    builder.addCase(_getAllSettings.fulfilled, (state, action) => {
      if (action.payload.isError === false) {
        state.allSettings = action.payload.data;
      }
    });
  },
});

export const {selectMenuItem, setInitialRoute, setMenu} = settingSlice.actions;
export default settingSlice.reducer;
