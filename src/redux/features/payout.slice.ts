import {createSlice} from '@reduxjs/toolkit';
import {_payOut} from '../actions/payout.actions';

interface InitialState {
  loading: boolean;
  isError: boolean;
  isSuccess: boolean;
  responseMessage: string | null;
  showPayoutModal: boolean;
}

const initialState: InitialState = {
  isError: false,
  isSuccess: false,
  loading: false,
  responseMessage: null,
  showPayoutModal: false,
};

const payoutSlice = createSlice({
  name: 'payout',
  initialState: initialState,
  reducers: {
    clearPayoutState: state => {
      state.loading = false;
      state.isError = false;
      state.isSuccess = false;
      state.responseMessage = null;
    },
    togglePayoutModalVisibility: state => {
      state.showPayoutModal = !state.showPayoutModal;
    },
  },
  extraReducers: builder => {
    builder.addCase(_payOut.pending, state => {
      state.loading = true;
    });
    builder.addCase(_payOut.fulfilled, (state, action) => {
      state.loading = false;
      if (action.payload.isError) {
        state.isError = true;
        state.isSuccess = false;
        state.responseMessage = action.payload.msg;
      }
      if (!action.payload.isError) {
        state.isError = false;
        state.isSuccess = true;
        state.responseMessage = action.payload.msg;
        state.showPayoutModal = false;
      }
    });
    builder.addCase(_payOut.rejected, (state, action: any) => {
      state.loading = true;
      state.isError = true;
      state.isSuccess = false;
      state.responseMessage = action.payload.msg;
    });
  },
});

export const {clearPayoutState, togglePayoutModalVisibility} =
  payoutSlice.actions;
export default payoutSlice.reducer;
