import {createSlice} from '@reduxjs/toolkit';
import {BusinessSettings, Sale} from '../../types/main';
import {_getDailySale, _getDailySales} from '../actions';
import {
  connectToBluetoothPrinters,
  disconnectBluetoothPrinter,
  scanForBluetoothPrinters,
} from '../actions/printer.actions';

interface Device {
  name?: string;
  address: string;
}

interface Employee {
  id: number;
  first_name: string;
  last_name: string;
}

interface InitialState {
  pairedDevices: Array<Device>;
  unPairedDevices: Array<Device>;
  loading: boolean;
  device_conn_loading: boolean;
  isError: boolean;
  error_message: string | null;
  connected_device_address: string | null;
  connected_device: {address: string; name?: string} | null;
  dailySales: Sale[];
  selectedSale: Sale | null;
  businessSettings: BusinessSettings | null;
  get_sale_loading: boolean;
}

const initialState: InitialState = {
  loading: false,
  device_conn_loading: false,
  pairedDevices: [],
  unPairedDevices: [],
  isError: false,
  error_message: null,
  connected_device_address: null,
  connected_device: null,
  dailySales: [],
  selectedSale: null,
  businessSettings: null,
  get_sale_loading: false,
};

const printerSlice = createSlice({
  name: 'settings/screen',
  initialState: initialState,
  reducers: {
    _resetBluetoothScanState: state => {
      state.isError = false;
      state.error_message = null;
      state.loading = false;
    },
    _addConnectedDeviceAddress: (state, action) => {
      state.connected_device_address = action.payload;
    },
    _addConnectedDevice: (state, action) => {
      state.connected_device = action.payload;
    },
    _clearConnectedDeviceAddress: state => {
      state.connected_device_address = null;
    },
    _addSelectedSale: (state, action) => {
      state.selectedSale = action.payload;
    },
  },
  extraReducers: builder => {
    builder.addCase(scanForBluetoothPrinters.pending, state => {
      state.loading = true;
    });
    builder.addCase(scanForBluetoothPrinters.fulfilled, (state, action) => {
      state.loading = false;
      if (action.payload.data) {
        state.pairedDevices = action.payload.data.pairedDevices;
        state.unPairedDevices = action.payload.data.unPairedDevices;
      }
    });
    builder.addCase(scanForBluetoothPrinters.rejected, (state, action: any) => {
      state.loading = false;
      state.isError = true;
      state.error_message = action.payload.msg;
    });

    //connecting to BT device

    builder.addCase(connectToBluetoothPrinters.pending, state => {
      state.device_conn_loading = true;
    });
    builder.addCase(connectToBluetoothPrinters.fulfilled, (state, action) => {
      state.device_conn_loading = false;
      if (action.payload?.data) {
        state.connected_device_address = action.payload?.data?.address;
        state.connected_device = action.payload?.data;
      }
    });
    builder.addCase(
      connectToBluetoothPrinters.rejected,
      (state, action: any) => {
        state.device_conn_loading = false;
        state.isError = true;
        state.error_message = action.payload.msg;
      },
    );

    // dis connecting from BT device

    builder.addCase(disconnectBluetoothPrinter.pending, state => {
      state.device_conn_loading = true;
    });

    builder.addCase(disconnectBluetoothPrinter.fulfilled, state => {
      state.device_conn_loading = false;
      state.connected_device_address = null;
    });

    builder.addCase(
      disconnectBluetoothPrinter.rejected,
      (state, action: any) => {
        state.device_conn_loading = false;
        state.error_message = action.payload.msg;
      },
    );

    // get daily & current sales
    //fail silently
    builder.addCase(_getDailySales.fulfilled, (state, action) => {
      if (action.payload.isError === false) {
        state.dailySales = action.payload.data;
      }
    });

    //get daily sale by order number
    builder.addCase(_getDailySale.pending, state => {
      state.get_sale_loading = true;
    });
    builder.addCase(_getDailySale.fulfilled, (state, action) => {
      if (action.payload.isError === false) {
        state.get_sale_loading = false;
        state.dailySales = action.payload.data;
      }
      if (action.payload.isError === true) {
        state.get_sale_loading = false;
        state.dailySales = [];
        state.error_message = action.payload.msg;
      }
    });
    builder.addCase(_getDailySale.rejected, (state, action: any) => {
      state.get_sale_loading = false;
      state.error_message = action.payload.msg;
    });
  },
});

export const {
  _resetBluetoothScanState,
  _addConnectedDeviceAddress,
  _clearConnectedDeviceAddress,
  _addConnectedDevice,
  _addSelectedSale,
} = printerSlice.actions;
export default printerSlice.reducer;
