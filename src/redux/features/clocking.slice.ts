import {createSlice} from '@reduxjs/toolkit';
import {_clockoutEmployee, _getEmployeesInShift, _linkDevice} from '../actions';
import {Employee} from '../../types/main';

interface InitialState {
  loading: boolean;
  clocked_in_employees: Employee[] | null;
  currentOperator: any;
  isError: boolean;
  isSuccess: boolean;
  isShiftOn: boolean;
  clocked_in_list_error_message: string | null;
}

const initialState: InitialState = {
  currentOperator: null,
  loading: false,
  isError: false,
  isSuccess: false,
  isShiftOn: false,
  clocked_in_employees: null,
  clocked_in_list_error_message: null,
};

const clockingSlice = createSlice({
  name: 'clocking',
  initialState: initialState,
  reducers: {
    clearClockingMessages: state => {
      state.clocked_in_list_error_message = null;
      state.isError = false;
      state.isSuccess = false;
    },
    setShiftOn: state => {
      state.isShiftOn = true;
    },
    setShiftOf: state => {
      state.isShiftOn = false;
      state.currentOperator = null;
      state.isError = false;
      state.isSuccess = false;
    },
    setCurrentOperator: (state, action) => {
      state.currentOperator = action.payload;
    },
  },
  extraReducers: builder => {
    builder.addCase(_getEmployeesInShift.pending, state => {
      state.loading = true;
    });
    builder.addCase(_getEmployeesInShift.fulfilled, (state, action) => {
      if (action.payload.isError) {
        state.loading = false;
        state.isError = true;
        state.clocked_in_list_error_message = action.payload.msg;
      }
      if (action.payload.isError === false) {
        state.loading = false;
        state.clocked_in_employees = action.payload.data;
        state.isSuccess = true;
      }
    });
    builder.addCase(_getEmployeesInShift.rejected, (state, action) => {
      state.loading = false;
      state.isError = true;
    });
    //clocking out
    builder.addCase(_clockoutEmployee.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(_clockoutEmployee.fulfilled, (state, action) => {
      state.loading = false;
      state.currentOperator = null;
      state.isShiftOn = false;
    });
    builder.addCase(_clockoutEmployee.rejected, (state, action) => {
      state.loading = false;
    });
  },
});

export const {
  clearClockingMessages,
  setShiftOf,
  setShiftOn,
  setCurrentOperator,
} = clockingSlice.actions;
export default clockingSlice.reducer;
