import {createSlice} from '@reduxjs/toolkit';
import {_payOut} from '../actions/payout.actions';
import {_cashDrop} from '../actions/cashdrop.actions';
import {CustomerType} from '../../types/main';
import {_addCustomer, _listCustomers} from '../actions/customers.actions';

interface InitialState {
  loading: boolean;
  isError: boolean;
  isSuccess: boolean;
  responseMessage: string | null;
  customers: CustomerType[] | null;
  createCustomerModalIsVisible: boolean;
  addCustomerToSaleModalIsVisible: boolean;
  customerToAddToSale: CustomerType | null;
}

const initialState: InitialState = {
  isError: false,
  isSuccess: false,
  loading: false,
  responseMessage: null,
  customers: null,
  createCustomerModalIsVisible: false,
  addCustomerToSaleModalIsVisible: false,
  customerToAddToSale: null,
};

const customerSlice = createSlice({
  name: 'customers',
  initialState: initialState,
  reducers: {
    clearCustomerState: state => {
      state.loading = false;
      state.isError = false;
      state.isSuccess = false;
      state.responseMessage = null;
    },

    showCreateCustomerModal: state => {
      state.createCustomerModalIsVisible = true;
    },
    hideCreateCustomerModal: state => {
      state.createCustomerModalIsVisible = false;
    },

    showAddCustomerToSaleModal: state => {
      state.addCustomerToSaleModalIsVisible = true;
    },
    hideAddCustomerToSaleModal: state => {
      state.addCustomerToSaleModalIsVisible = false;
    },
    setCustomerToAddToSale: (state, action) => {
      state.customerToAddToSale = action.payload;
    },
    removeCustomerToAddToSale: state => {
      state.customerToAddToSale = null;
    },
  },
  extraReducers: builder => {
    builder.addCase(_addCustomer.pending, state => {
      state.loading = true;
    });
    builder.addCase(_addCustomer.fulfilled, (state, action) => {
      state.loading = false;
      if (action.payload.isError) {
        state.isError = true;
        state.isSuccess = false;
        state.responseMessage = action.payload.msg;
      }
      if (!action.payload.isError) {
        state.isError = false;
        state.isSuccess = true;
        state.responseMessage = action.payload.msg;
      }
    });
    builder.addCase(_addCustomer.rejected, (state, action: any) => {
      state.loading = false;
      state.isError = true;
      state.isSuccess = false;
      state.responseMessage = action.payload.msg;
    });

    builder.addCase(_listCustomers.pending, state => {
      state.loading = true;
    });
    builder.addCase(_listCustomers.fulfilled, (state, action) => {
      state.loading = false;
      if (action.payload.isError) {
        state.isError = true;
        state.isSuccess = false;
        state.responseMessage = action.payload.msg;
      }
      if (!action.payload.isError) {
        state.isError = false;
        state.isSuccess = true;
        state.responseMessage = action.payload.msg;
        state.customers = action.payload.payload;
      }
    });

    builder.addCase(_listCustomers.rejected, (state, action: any) => {
      state.loading = false;
      state.isError = true;
      state.isSuccess = false;
      state.responseMessage = action.payload.msg;
    });
  },
});

export const {
  clearCustomerState,
  showCreateCustomerModal,
  hideCreateCustomerModal,
  showAddCustomerToSaleModal,
  hideAddCustomerToSaleModal,
  setCustomerToAddToSale,
  removeCustomerToAddToSale,
} = customerSlice.actions;
export default customerSlice.reducer;
