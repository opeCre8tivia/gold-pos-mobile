import React from 'react';

import {SvgXml} from 'react-native-svg';

interface Icon {
  width?: number;
  height?: number;
  fill?: string;
}

const BackArrowIcon = ({width = 19, height = 21, fill = '#ffffff'}: Icon) => {
  const xml = `<svg width=${width} height=${height} viewBox="0 0 11 14" fill="none" xmlns="http://www.w3.org/2000/svg">
  <path d="M11 0L6.11959e-07 7L11 14L11 0Z" fill=${fill}/>
  </svg>
`;
  return <SvgXml xml={xml} width="100%" height="100%" />;
};

export default BackArrowIcon;
