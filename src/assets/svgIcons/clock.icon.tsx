import React from 'react';

import {SvgXml} from 'react-native-svg';

interface Icon {
  width?: number;
  height?: number;
  fill?: string;
}

const ClockIcon = ({width = 19, height = 21, fill = '#ffffff'}: Icon) => {
  const xml = `<svg width=${width} height=${height} viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
  <path d="M12.5 30C5.59625 30 0 24.4037 0 17.5C0 10.5963 5.59625 5 12.5 5C19.4037 5 25 10.5963 25 17.5C24.9925 24.4 19.4 29.9925 12.5 30ZM12.5 7.5C10.5222 7.5 8.58879 8.08649 6.9443 9.1853C5.29981 10.2841 4.01808 11.8459 3.2612 13.6732C2.50433 15.5004 2.3063 17.5111 2.69215 19.4509C3.078 21.3907 4.03041 23.1725 5.42893 24.5711C6.82746 25.9696 8.60929 26.922 10.5491 27.3079C12.4889 27.6937 14.4996 27.4957 16.3268 26.7388C18.1541 25.9819 19.7159 24.7002 20.8147 23.0557C21.9135 21.4112 22.5 19.4778 22.5 17.5C22.497 14.8487 21.4425 12.3069 19.5678 10.4322C17.6931 8.5575 15.1513 7.50298 12.5 7.5ZM18.75 18.75H11.25V11.25H13.75V16.25H18.75V18.75Z" fill=${fill}/>
  </svg>`;

  return <SvgXml xml={xml} width="100%" height="100%" />;
};

export default ClockIcon;
