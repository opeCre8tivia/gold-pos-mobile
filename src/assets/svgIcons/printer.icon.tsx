import React from 'react';

import {SvgXml} from 'react-native-svg';

interface Icon {
  width?: number;
  height?: number;
  fill?: string;
}

const PrinterIcon = ({width = 19, height = 21, fill = '#ffffff'}: Icon) => {
  const xml = `<svg width=${width} height=${height} viewBox="0 0 46 42" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M38.75 12H7.25C3.515 12 0.5 15.015 0.5 18.75V32.25H9.5V41.25H36.5V32.25H45.5V18.75C45.5 15.015 42.485 12 38.75 12ZM32 36.75H14V25.5H32V36.75ZM38.75 21C37.5125 21 36.5 19.9875 36.5 18.75C36.5 17.5125 37.5125 16.5 38.75 16.5C39.9875 16.5 41 17.5125 41 18.75C41 19.9875 39.9875 21 38.75 21ZM36.5 0.75H9.5V9.75H36.5V0.75Z" fill=${fill}/>
</svg>
`;
  return <SvgXml xml={xml} width="100%" height="100%" />;
};

export default PrinterIcon;
