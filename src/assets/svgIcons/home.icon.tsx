import React from 'react';

import {SvgXml} from 'react-native-svg';

interface Icon {
  width?: number;
  height?: number;
  fill?: string;
}

const HomeIcon = ({width = 19, height = 21, fill = '#ffffff'}: Icon) => {
  const xml = `<svg width=${width} height=${height} viewBox="0 0 19 21" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M9.5 0.842773L0.5 9.87277V19.0003C0.5 19.3981 0.658035 19.7796 0.93934 20.0609C1.22064 20.3422 1.60218 20.5003 2 20.5003H7.25V13.0003H11.75V20.5003H17C17.3978 20.5003 17.7794 20.3422 18.0607 20.0609C18.342 19.7796 18.5 19.3981 18.5 19.0003V9.82027L9.5 0.842773Z" fill=${fill}/>
</svg>`;

  return <SvgXml xml={xml} width="100%" height="100%" />;
};

export default HomeIcon;
