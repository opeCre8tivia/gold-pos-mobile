import React from 'react';

import {SvgXml} from 'react-native-svg';

const xml = `<svg width="62" height="54" viewBox="0 0 52 44" fill="none" xmlns="http://www.w3.org/2000/svg">
<path fill-rule="evenodd" clip-rule="evenodd" d="M49.3327 41.642C43.6242 34.6735 38.555 30.7196 34.124 29.7793C29.6942 28.8401 25.4767 28.6978 21.4704 29.3535V41.8333L2.66602 21.4691L21.4704 2.16663V14.0281C28.8775 14.0865 35.174 16.7441 40.361 22C45.5469 27.2558 48.5382 33.8031 49.3327 41.642Z" fill="#2A333E" stroke="white" stroke-width="4" stroke-linejoin="round"/>
</svg>`;

const LeftCurvedArrowIcon = () => {
  return <SvgXml xml={xml} width="100%" height="100%" />;
};

export default LeftCurvedArrowIcon;
