import sum from '../../src/utils/sum';

describe('Sums two numbers', () => {
  it('sums two numbers passed as arguments', () => {
    const result = sum(4, 4);
    const expected = 8;

    expect(result).toBe(expected);
  });
});
