import React from 'react';
import {SafeAreaView, StyleSheet, View, Text} from 'react-native';

import {NavigationContainer} from '@react-navigation/native';
import {Provider} from 'react-redux';
import store from './src/redux/store';
import NavHost from './src/navigation/NavHost';
import Toast from 'react-native-toast-message';
import {colors, dimentions} from './src/constants/theme';

/*
  1. Create the config
*/
export const toastConfig = {
  /*
    Or create a completely new type - `tomatoToast`,
    building the layout from scratch.

    I can consume any custom `props` I want.
    They will be passed when calling the `show` method (see below)
  */
  goldposgreen: ({text1, props}: {text1?: any; props: any}) => (
    <View
      style={{
        height: 50,
        width: dimentions.vw,
        backgroundColor: colors.gp_green,
        alignItems: 'center',
        justifyContent: 'center',
      }}>
      <Text
        style={{
          fontSize: 18,
          fontWeight: '700',
          color: '#ffffff',
          textAlign: 'center',
        }}>
        {text1}
      </Text>
    </View>
  ),
  goldposred: ({text1, props}: {text1?: any; props: any}) => (
    <View
      style={{
        height: 50,
        width: dimentions.vw,
        backgroundColor: 'red',
        alignItems: 'center',
        justifyContent: 'center',
      }}>
      <Text
        style={{
          fontSize: 18,
          fontWeight: '700',
          color: '#ffffff',
          textAlign: 'center',
        }}>
        {text1}
      </Text>
    </View>
  ),
  goldposwarning: ({text1, props}: {text1?: any; props: any}) => (
    <View
      style={{
        height: 50,
        width: dimentions.vw,
        backgroundColor: colors.warning,
        alignItems: 'center',
        justifyContent: 'center',
        zIndex: 60,
      }}>
      <Text
        style={{
          fontSize: 18,
          fontWeight: '700',
          color: '#000000',
          textAlign: 'center',
        }}>
        {text1}
      </Text>
    </View>
  ),
};

const App = () => {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <SafeAreaView style={styles.main}>
          <NavHost />
          <Toast config={toastConfig} />
        </SafeAreaView>
      </NavigationContainer>
    </Provider>
  );
};

const styles = StyleSheet.create({
  main: {
    flex: 1,
  },
});

export default App;
